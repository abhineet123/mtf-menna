#ifndef TRACKER_BASE_H
#define TRACKER_BASE_H

#include "opencv2/core/core.hpp"
#include <string>
// base class for all trackers
namespace mtf {
	class TrackerBase{
	protected:
		cv::Mat cv_corners_mat;
	public:
		std::string name;

		TrackerBase(){}
		virtual ~TrackerBase(){}

		virtual void initialize(const cv::Mat &img, const cv::Mat &corners) = 0;
		virtual void update(const cv::Mat &img) = 0;

		// overloaded variants that avoid passing the image repeatedly by sharing
		// memory with the buffer where the images are read in by the input pipeline
		virtual void initialize(const cv::Mat &corners) = 0;
		virtual void update() = 0;

		// return the trackers's current location as a 2x4 matrix in the same format
		// as provided to the initialize function
		virtual const cv::Mat& getRegion() { return cv_corners_mat; }

		// modify the tracker's internal state so that the tracked object is 
		// placed at the given location
		virtual void setRegion(const cv::Mat& corners) { corners.copyTo(cv_corners_mat); }

		// return true if the tracker requires the raw RGB image
		virtual bool rgbInput() const { return false; }
	};
}
#endif
