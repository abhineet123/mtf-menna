#ifndef MTF_H
#define MTF_H

//! External interface of the MTF library; this is the only header that needs to be included to use the framework
//! Provides functions to create trackers corresponding to different combinations of 
//! search methods, appearance models and state space models as well as some third party 

//#define MTF_VERSION_MAJOR @MTF_VERSION_MAJOR@
//#define MTF_VERSION_MINOR @MTF_VERSION_MINOR@

// parameters for the different modules
#include "mtf/Config/parameters.h"

// search methods
#include "mtf/SM/ESM.h"
#include "mtf/SM/AESM.h"
#include "mtf/SM/GNN.h"
#include "mtf/SM/ICLK.h"
#include "mtf/SM/FCLK.h"
#include "mtf/SM/FALK.h"
#include "mtf/SM/IALK.h"
#include "mtf/SM/FCGD.h"
#include "mtf/SM/PF.h"
#ifndef DISABLE_NN
#include "mtf/SM/NN.h"
#endif
//#include "mtf/SM/HACLK.h"
//#include "mtf/SM/ESMH.h"
//#include "mtf/SM/HESM.h"
//#include "mtf/SM/FESM.h"
//#include "mtf/SM/IALK2.h"

// composite
#include "mtf/SM/CascadeTracker.h"
#include "mtf/SM/CascadeSM.h"
#include "mtf/SM/GridTrackerCV.h"
#include "mtf/SM/GridTracker.h"
#include "mtf/SM/RKLT.h"
#include "mtf/SM/ParallelTracker.h"
#include "mtf/SM/ParallelSM.h"

// appearance models
#include "mtf/AM/SSD.h"
#include "mtf/AM/NSSD.h"
#include "mtf/AM/ZNCC.h"
#include "mtf/AM/SCV.h"
#include "mtf/AM/LSCV.h"
#include "mtf/AM/RSCV.h"
#include "mtf/AM/LRSCV.h"
#include "mtf/AM/KLD.h"
#include "mtf/AM/LKLD.h"	
#include "mtf/AM/MI.h"
#include "mtf/AM/SPSS.h"
#include "mtf/AM/SSIM.h"
#include "mtf/AM/NCC.h"
#include "mtf/AM/CCRE.h"
#include "mtf/AM/FMaps.h"

// state space models
#include "mtf/SSM/LieHomography.h"
#include "mtf/SSM/CornerHomography.h"
#include "mtf/SSM/HomographySL3.h"
#include "mtf/SSM/Homography.h"
#include "mtf/SSM/Affine.h"
#include "mtf/SSM/Similitude.h"
#include "mtf/SSM/Isometry.h"
#include "mtf/SSM/Transcaling.h"
#include "mtf/SSM/Translation.h"

// Third Party Trackers
#ifndef DISABLE_LEARNING_TRACKERS
// learning based trackers
#include "mtf/ThirdParty/DSST/DSST.h"
#include "mtf/ThirdParty/KCF/KCFTracker.h"
#include "mtf/ThirdParty/RCT/CompressiveTracker.h"
#include "mtf/ThirdParty/CMT/CMT.h"
#include "mtf/ThirdParty/TLD/TLD.h"
#include "mtf/ThirdParty/Struck/Struck.h"
#ifndef DISABLE_PFSL3
#include "mtf/ThirdParty/PFSL3/PFSL3.h"
#endif
#ifndef DISABLE_VISP
#include "mtf/ThirdParty/ViSP//ViSP.h"
#endif
#endif
#ifndef DISABLE_XVISION
// Xvision trackers
#include "mtf/ThirdParty/Xvision/xvSSDTrans.h"
#include "mtf/ThirdParty/Xvision/xvSSDAffine.h"
#include "mtf/ThirdParty/Xvision/xvSSDSE2.h"
#include "mtf/ThirdParty/Xvision/xvSSDRT.h"
#include "mtf/ThirdParty/Xvision/xvSSDRotate.h"
#include "mtf/ThirdParty/Xvision/xvSSDScaling.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidTrans.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidAffine.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidSE2.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidRT.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidRotate.h"
#include "mtf/ThirdParty/Xvision/xvColor.h"
#include "mtf/ThirdParty/Xvision/xvEdgeTracker.h"
#include "mtf/ThirdParty/Xvision/xvSSDGrid.h"
#include "mtf/ThirdParty/Xvision/xvSSDGridLine.h"
bool using_xv_tracker = false;
#endif

_MTF_BEGIN_NAMESPACE

using namespace params;

enum class SearchMethods{
	ESM, AESM, FCLK, ICLK, FALK, IALK, NN, HACLK, FCGD, 
	NNIC, NN3IC, NNFC, NNES, GRID, GRIC, RKLT, CASCADE
};

inline void initializeFrame(cv::Mat &frame_rgb, cv::Mat &frame_gs,
	const cv::Mat &frame_raw){
	frame_rgb.create(frame_raw.rows, frame_raw.cols, CV_32FC3);
	frame_gs.create(frame_raw.rows, frame_raw.cols, CV_32FC1);
}
// converts the raw 8 bit RGB image captured, for instance using cv::imread
// into the appropriate grayscale format and type needed by MTF;
// additionally applies the provided preprocessing
template<class PreProcType>
inline void updateFrame(cv::Mat &frame_rgb, cv::Mat &frame_gs,
	const cv::Mat &frame_raw, const PreProcType &pre_proc){
	assert(frame_rgb.type() == CV_32FC3);
	assert(frame_gs.type() == CV_32FC1);

	frame_raw.convertTo(frame_rgb, frame_rgb.type());
	cv::cvtColor(frame_rgb, frame_gs, CV_BGR2GRAY);
	pre_proc.apply(frame_gs);
}

template< class ModuleType >
ModuleType module(const char* module_name){
	throw std::domain_error("invalid module type");
}
template<>
inline SearchMethods module<SearchMethods>(const char *sm_type){
	if(!strcmp(sm_type, "esm")){
		return SearchMethods::ESM;
	} else if(!strcmp(sm_type, "aesm")){
		return SearchMethods::AESM;
	} else if(!strcmp(sm_type, "iclk")){
		return SearchMethods::ICLK;
	} else if(!strcmp(sm_type, "fclk")){
		return SearchMethods::FCLK;
	} else if(!strcmp(sm_type, "falk")){
		return SearchMethods::FALK;
	} else if(!strcmp(sm_type, "ialk")){
		return SearchMethods::IALK;
	} else if(!strcmp(sm_type, "fcgd")){
		return SearchMethods::FCGD;
	} else if(!strcmp(sm_type, "nn")){
		return SearchMethods::NN;
	} else if(!strcmp(sm_type, "nnic")){// NNIC with 1 layer of NN
		return SearchMethods::NNIC;
	} else if(!strcmp(sm_type, "nn3ic")){// NNIC with 3 layers of NN
		return SearchMethods::NN3IC;
	} else if(!strcmp(sm_type, "nnfc")){
		return SearchMethods::NNFC;
	} else if(!strcmp(sm_type, "nnes")){
		return SearchMethods::NNES;
	} else if(!strcmp(sm_type, "haclk")){
		return SearchMethods::HACLK;
	} else if(!strcmp(sm_type, "casc")){// general purpose cascaded tracker 
		return SearchMethods::CASCADE;
	} else if(!strcmp(sm_type, "grid")){
		return SearchMethods::GRID;
	} else if(!strcmp(sm_type, "gric")){// Grid + ICLK
		return SearchMethods::GRIC;
	} else if(!strcmp(sm_type, "rkl")){// Grid + Template tracker with SPI
		return SearchMethods::RKLT;
	}else{
		throw  std::invalid_argument("invalid search method provided");
	}
}

inline ImageBase *getPixMapperObj(const char *pix_mapper_type, ImgParams *img_params){
	if(!pix_mapper_type)
		return nullptr;
	if(!strcmp(pix_mapper_type, "ssd")){
		return new SSD(img_params);
	} else if(!strcmp(pix_mapper_type, "nssd")){
		NSSDParams *nssd_params = new NSSDParams(img_params, norm_pix_max, norm_pix_min, debug_mode);
		return new NSSD(nssd_params);
	} else if(!strcmp(pix_mapper_type, "zncc")){
		return new ZNCC(img_params);
	} else if(!strcmp(pix_mapper_type, "scv")){
		SCVParams *scv_params = new SCVParams(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, debug_mode);
		return new SCV(scv_params);
	} else if(!strcmp(pix_mapper_type, "lscv")){
		LSCVParams *lscv_params = new LSCVParams(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping,  lscv_show_subregions, debug_mode);
		return new LSCV(lscv_params);
	} else if(!strcmp(pix_mapper_type, "lrscv") || !strcmp(pix_mapper_type, "lrsc")){
		LRSCVParams *lrscv_params = new LRSCVParams(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping,  lscv_show_subregions, debug_mode);
		return new LRSCV(lrscv_params);
	} else if(!strcmp(pix_mapper_type, "rscv")){
		RSCVParams *rscv_params = new RSCVParams(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, debug_mode);
		return new RSCV(rscv_params);
	} else{
		throw std::invalid_argument("getPixMapperObj::Invalid pixel mapper type provided");
	}
}

TrackerBase *getTrackerObj(const char *sm_type, const char *am_type, const char *ssm_type,
	ImgParams *img_params);
// 3rd party trackers
TrackerBase *getTrackerObj(const char *sm_type, ImgParams *img_params);

//template< class AMType, class SSMType >
//TrackerBase *getFESMObj(
//	typename AMType::ParamType *am_params = nullptr,
//	typename SSMType::ParamType *ssm_params = nullptr);


template< class AMType, class SSMType >
TrackerBase *getTrackerObj(const char *sm_type,
	typename AMType::ParamType *am_params = nullptr,
	typename SSMType::ParamType *ssm_params = nullptr){
	typedef SearchMethod<AMType, SSMType> SMType;
    if(!strcmp(sm_type, "esm")){
		ESMParams *esm_params = new ESMParams(max_iters, epsilon,
			static_cast<ESMParams::JacType>(jac_type),
			static_cast<ESMParams::HessType>(hess_type), 
			sec_ord_hess, spi_enable, spi_thresh, debug_mode);
		return new ESM<AMType, SSMType>(esm_params, am_params, ssm_params);
	}  else if(!strcmp(sm_type, "aesm")){
		ESMParams *esm_params = new ESMParams(max_iters, epsilon,
			static_cast<ESMParams::JacType>(jac_type), 
			static_cast<ESMParams::HessType>(hess_type),
			sec_ord_hess, spi_enable, spi_thresh, debug_mode);
		return new AESM<AMType, SSMType>(esm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "iclk") || !strcmp(sm_type, "ic")){
		ICLKParams *iclk_params = new ICLKParams(max_iters, epsilon, 
			static_cast<ICLKParams::HessType>(hess_type), sec_ord_hess, 
			ic_update_ssm, ic_chained_warp, debug_mode);
		return new ICLK<AMType, SSMType>(iclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "fclk") || !strcmp(sm_type, "fc")){
		FCLKParams *fclk_params = new FCLKParams(max_iters, epsilon, 
			static_cast<FCLKParams::HessType>(hess_type), sec_ord_hess, 
			update_templ, fc_chained_warp, debug_mode);
		return new FCLK<AMType, SSMType>(fclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "falk") || !strcmp(sm_type, "fa")){
		FALKParams *falk_params = new FALKParams(max_iters, epsilon, 
			static_cast<FALKParams::HessType>(hess_type), sec_ord_hess,
			update_templ, debug_mode);
		return new FALK<AMType, SSMType>(falk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "ialk") || !strcmp(sm_type, "ia")){
		IALKParams *ialk_params = new IALKParams(max_iters, epsilon, 
			static_cast<IALKParams::HessType>(hess_type), sec_ord_hess,
			debug_mode);
		return new IALK<AMType, SSMType>(ialk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "fcgd")){
		FCGDParams *fclk_params = new FCGDParams(max_iters, epsilon, gd_learning_rate,
			debug_mode, hess_type);
		return new FCGD<AMType, SSMType>(fclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "pf")){
		PFParams *pf_params = new PFParams(pf_max_iters, pf_n_particles, epsilon,
			static_cast<PFParams::DynamicModel>(pf_dyn_model), 
			static_cast<PFParams::UpdateType>(pf_upd_type),
			static_cast<PFParams::LikelihoodFunc>(pf_likelihood_func),
			static_cast<PFParams::ResamplingType>(pf_resampling_type),
			pf_reset_to_mean, pf_mean_of_corners,
			pf_ssm_sigma, pf_pix_sigma, pf_measurement_sigma, debug_mode);
		return new PF<AMType, SSMType>(pf_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "pfic")){// PF + ICLK
		vector<SMType*> trackers;
		trackers.resize(2);
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params));
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params));
		CascadeSMParams * casc_params = new CascadeSMParams(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, casc_params);
	}
#ifndef DISABLE_NN
	// NN tracker
	else if(!strcmp(sm_type, "nn")){
		NNParams *nn_params = new NNParams(nn_max_iters, nn_n_samples, epsilon, 
			nn_ssm_sigma, nn_corner_sigma_d, nn_corner_sigma_t, nn_pix_sigma,
			static_cast<NNIndex::IdxType>(nn_index_type), nn_index_config_file,
			nn_n_checks, nn_additive_update, nn_direct_samples, 
			nn_ssm_sigma_prec, debug_mode);
		return new NN<AMType, SSMType>(nn_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "nn3")){// 3 layers of NN
		vector<SMType*> trackers;
		trackers.resize(3);
		nn_ssm_sigma[0] = 0.06;
		nn_ssm_sigma[1] = 0.04;
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		nn_ssm_sigma[0] = 0.03;
		nn_ssm_sigma[1] = 0.02;
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		nn_ssm_sigma[0] = 0.015;
		nn_ssm_sigma[1] = 0.01;
		trackers[2] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "gnn")){
		GNNParams *gnn_params = new GNNParams(nn_max_iters, nn_n_samples, gnn_k, epsilon,
			nn_corner_sigma_d, nn_corner_sigma_t, nn_additive_update, nn_direct_samples, debug_mode);
		return new GNN<AMType, SSMType>(gnn_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "nnic")){// NNIC with 1 layer of NN		
		vector<SMType*> trackers;
		trackers.resize(2);
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params));
		CascadeSMParams * casc_params = new CascadeSMParams(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, casc_params);
	} else if(!strcmp(sm_type, "nn3ic")){// NNIC with 3 layers of NN
		vector<SMType*> trackers;
		trackers.resize(4);
		nn_ssm_sigma[0] = 0.06;
		nn_ssm_sigma[1] = 0.04;
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		nn_ssm_sigma[0] = 0.03;
		nn_ssm_sigma[1] = 0.02;
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		nn_ssm_sigma[0] = 0.015;
		nn_ssm_sigma[1] = 0.01;
		trackers[2] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));

		trackers[3] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnfc")){
		vector<SMType*> trackers;
		trackers.resize(2);
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("fc", am_params, ssm_params));
		CascadeSMParams * casc_params = new CascadeSMParams(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, casc_params);
	} else if(!strcmp(sm_type, "nnes")){
		vector<SMType*> trackers;
		trackers.resize(2);
		trackers[0] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params));
		trackers[1] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params));
		CascadeSMParams * casc_params = new CascadeSMParams(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, casc_params);
	} 
#endif
	//else if(!strcmp(sm_type, "haclk")){
	//	HACLKParams *haclk_params = new HACLKParams(max_iters, epsilon, rec_init_err_grad, 
	//		debug_mode, hess_type, conv_corners);
	//	return new HACLK<AMType, SSMType>(haclk_params, am_params, ssm_params);
	//} 
	else if(!strcmp(sm_type, "casc")){// general purpose cascaded tracker 
		FILE *fid = nullptr;
		vector<TrackerBase*> trackers;
		trackers.resize(casc_n_trackers);
		for(int i = 0; i < casc_n_trackers; i++){
			fid = readTrackerParams(fid, 1);
			ImgParams tracker_img_params(resx, resy, am_params->cv_img, grad_eps, hess_eps);
			if(!(trackers[i] = getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &tracker_img_params))){
				return nullptr;
			}
		}
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} 
	else if(!strcmp(sm_type, "casm")){// cascade of search methods
		FILE *fid = nullptr;
		vector<SMType*> trackers;
		trackers.resize(casc_n_trackers);
		for(int i = 0; i < casc_n_trackers; i++){
			fid = readTrackerParams(fid, 1);
			ImgParams tracker_img_params(resx, resy, am_params->cv_img, grad_eps, hess_eps);
			if(!(trackers[i] = dynamic_cast<SMType*>(getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &tracker_img_params)))){
				printf("Invalid search method provided for cascade SM tracker: %s\n", mtf_sm);
				return nullptr;
			}
		}
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "hrch")){ // hierarchical SSM tracker
		vector<TrackerBase*> trackers;
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "2", am_params));
		//trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "3", am_params));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "4", am_params));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "6", am_params));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "8", am_params));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	}
	// Grid Tracker
	else if(!strcmp(sm_type, "grid")){
		if(!strcmp(gt_sm, "cv")){
			GridTrackerCVParams grid_params(
				gt_grid_res, gt_grid_res, gt_patch_size, gt_patch_size,
				gt_pyramid_levels, gt_use_min_eig_vals, gt_min_eig_thresh,
				static_cast<GridTrackerCVParams::EstType>(gt_estimation_method),
				gt_ransac_reproj_thresh,  max_iters, epsilon, gt_show_trackers, 
				debug_mode);
			return new GridTrackerCV<SSMType>(am_params->cv_img, ssm_params, &grid_params);
		} else{
			vector<TrackerBase*> trackers;
			int gt_n_trackers = gt_grid_res*gt_grid_res;
			trackers.resize(gt_n_trackers);
			for(int i = 0; i < gt_n_trackers; i++){
				ImgParams tracker_img_params(gt_patch_size, gt_patch_size, am_params->cv_img, grad_eps, hess_eps);
				if(!(trackers[i] = getTrackerObj(gt_sm, gt_am, gt_ssm, &tracker_img_params))){
					return nullptr;
				}
			}
			GridTrackerParams grid_params(
				gt_grid_res, gt_grid_res, gt_patch_size, gt_patch_size,
				static_cast<GridTrackerParams::EstType>(gt_estimation_method),
				gt_ransac_reproj_thresh, gt_init_at_each_frame,
				gt_dyn_patch_size, gt_use_tbb, max_iters, epsilon,
				gt_show_trackers, gt_show_tracker_edges, debug_mode);
			return new GridTracker<SSMType>(am_params->cv_img, ssm_params, &grid_params, trackers);
		}
	} 
	else if(!strcmp(sm_type, "gric")){// Grid + ICLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("iclk", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "grfc")){// Grid + FCLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("fclk", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "gres")){// Grid + ESM
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "rklt") || !strcmp(sm_type, "rkl")){// Grid + Template tracker with SPI
		GridBase *grid_tracker = dynamic_cast<GridBase*>(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		//if(rkl_enable_spi){
		//	printf("Setting sampling resolution of the template tracker equal to the grid size: %d x %d so that SPI can be enabled.",
		//		grid_tracker->getResX(), grid_tracker->getResY());
		//	am_params->resx = grid_tracker->getResX();
		//	am_params->resy = grid_tracker->getResY();
		//}
		SMType *templ_tracker = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>(rkl_sm, am_params, ssm_params));
		if(!templ_tracker){
			// invalid or third party tracker has been specified in 'rkl_sm'
			throw std::invalid_argument("Search method provided is not compatible with RKLT");
		}
		RKLTParams rkl_params(rkl_enable_spi, rkl_enable_feedback, 
			rkl_failure_detection, rkl_failure_thresh, debug_mode);
		return new RKLT<AMType, SSMType>(&rkl_params, grid_tracker, templ_tracker);
	}

	// Parallel Tracker
	else if(!strcmp(sm_type, "prl") || !strcmp(sm_type, "prlt")) { // general purpose parallel tracker 
		vector<TrackerBase*> trackers;
		trackers.resize(prl_n_trackers);
		FILE *fid = nullptr;
		for(int tracker_id = 0; tracker_id < prl_n_trackers; tracker_id++) {
			fid = readTrackerParams(fid, 1);
			ImgParams tracker_img_params(resx, resy, am_params->cv_img, grad_eps, hess_eps);
			if(!(trackers[tracker_id] = getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &tracker_img_params))) {
				return nullptr;
			}
		}
		ParallelParams prl_params(
			static_cast<ParallelParams::EstimationMethod>(prl_estimation_method),
			prl_reset_to_mean, prl_auto_reinit, prl_reinit_err_thresh);
		return new ParallelTracker(trackers, &prl_params);
	} else if(!strcmp(sm_type, "prls") || !strcmp(sm_type, "prsm")) {// SM specific parallel tracker
		vector<SMType*> trackers;
		trackers.resize(prl_n_trackers);
		//char prl_am[strlen(mtf_am) + 1], prl_ssm[strlen(mtf_ssm) + 1];
		//strcpy(mtf_am, prl_am);
		//strcpy(mtf_ssm, prl_ssm);
		FILE *fid = nullptr;
		for(int tracker_id = 0; tracker_id < prl_n_trackers; tracker_id++) {
			fid = readTrackerParams(fid, 1);
			ImgParams tracker_img_params(resx, resy, am_params->cv_img, grad_eps, hess_eps);
			if(!(trackers[tracker_id] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>(mtf_sm, am_params, ssm_params)))) {
				printf("Invalid search method provided for parallel SM tracker: %s\n", mtf_sm);
				return nullptr;
			}
		}
		ParallelSMParams prl_params(
			static_cast<ParallelSMParams::EstimationMethod>(prl_estimation_method),
			prl_reset_to_mean, prl_auto_reinit, prl_reinit_err_thresh);
		return new ParallelSM<AMType, SSMType>(am_params->cv_img, trackers, &prl_params, resx, resy, ssm_params);
	}

	//else if(!strcmp(sm_type, "esmh")){
	//	ESMHParams *esm_params = new ESMHParams(max_iters, epsilon,
	//		static_cast<ESMHParams::JacType>(jac_type),
	//		static_cast<ESMHParams::HessType>(hess_type),
	//		sec_ord_hess, spi_enable, spi_thresh, debug_mode);
	//	return new ESMH<AMType, SSMType>(esm_params, am_params, ssm_params);
	//}
	//else if(!strcmp(sm_type, "ialk2")){
	//	IALK2Params *ialk_params = new IALK2Params(max_iters, epsilon,
	//		static_cast<IALK2Params::HessType>(hess_type), sec_ord_hess, debug_mode);
	//	return new IALK2<AMType, SSMType>(ialk_params, am_params, ssm_params);
	//} else if(!strcmp(sm_type, "fesm")){
	//	return getFESMObj<AMType, SSMType>(am_params, ssm_params);
	//}
	else{
		printf("Invalid search method provided: %s\n", sm_type);
		return nullptr;
	}
	//if(am_params){ delete(am_params); }
	//if(ssm_params){ delete(ssm_params); }
}

template< class AMType >
TrackerBase *getTrackerObj(const char *sm_type, const char *ssm_type,
	typename AMType::ParamType *am_params = nullptr){
	if(!strcmp(ssm_type, "lie_hom") || !strcmp(ssm_type, "l8")){
		LieHomographyParams *lhomm_params = new LieHomographyParams();
		lhomm_params->normalized_init = hom_normalized_init;
		return getTrackerObj<AMType, LieHomography>(sm_type, am_params, lhomm_params);
	} else if(!strcmp(ssm_type, "chom") || !strcmp(ssm_type, "c8")){
		CornerHomographyParams *chom_params = new CornerHomographyParams();
		chom_params->normalized_init = hom_normalized_init;
		chom_params->grad_eps = grad_eps;
		return getTrackerObj<AMType, mtf::CornerHomography>(sm_type, am_params, chom_params);
	} else if(!strcmp(ssm_type, "lhom2") || !strcmp(ssm_type, "l82")){
		HomographySL3Params *hom_params = new HomographySL3Params(hom_normalized_init, debug_mode);
		return getTrackerObj<AMType, mtf::HomographySL3>(sm_type, am_params, hom_params);
	} else if(!strcmp(ssm_type, "hom") || !strcmp(ssm_type, "8")){
		HomographyParams *hom_params = new HomographyParams(hom_normalized_init, hom_corner_based_sampling, debug_mode);
		return getTrackerObj<AMType, mtf::Homography>(sm_type, am_params, hom_params);
	} else if(!strcmp(ssm_type, "aff") || !strcmp(ssm_type, "6")){
		AffineParams *aff_params = new AffineParams();
		aff_params->normalized_init = hom_normalized_init;
		return getTrackerObj<AMType, mtf::Affine>(sm_type, am_params, aff_params);
	} else if(!strcmp(ssm_type, "sim") || !strcmp(ssm_type, "4")){
		return getTrackerObj<AMType, mtf::Similitude>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "iso") || !strcmp(ssm_type, "3")){
		return getTrackerObj<AMType, mtf::Isometry>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "trs") || !strcmp(ssm_type, "3s")){
		return getTrackerObj<AMType, mtf::Transcaling>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "trans") || !strcmp(ssm_type, "2")){
		return getTrackerObj<AMType, mtf::Translation>(sm_type, am_params);
	} else{
		printf("Invalid state space model provided: %s\n", ssm_type);
		return nullptr;
	}
}
inline TrackerBase *getTrackerObj(const char *sm_type, const char *am_type, const char *ssm_type,
	ImgParams *img_params){

	// check for 3rd party trackers
	TrackerBase *third_party_tracker = getTrackerObj(sm_type, img_params);
	if(third_party_tracker)
		return third_party_tracker;

	if(!strcmp(am_type, "ssd")){
		return getTrackerObj<SSD>(sm_type, ssm_type, img_params);
	} else if(!strcmp(am_type, "nssd")){
		NSSDParams *nssd_params = new NSSDParams(img_params, norm_pix_max, norm_pix_min, debug_mode);
		return getTrackerObj<NSSD>(sm_type, ssm_type, nssd_params);
	} else if(!strcmp(am_type, "zncc")){
		return getTrackerObj<ZNCC>(sm_type, ssm_type, img_params);
	} else if(!strcmp(am_type, "scv")){
		SCVParams *scv_params = new SCVParams(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, debug_mode);
		return getTrackerObj<SCV>(sm_type, ssm_type, scv_params);
	} else if(!strcmp(am_type, "lscv")){
		LSCVParams *lscv_params = new LSCVParams(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return getTrackerObj<LSCV>(sm_type, ssm_type, lscv_params);
	} else if(!strcmp(am_type, "rscv")){
		RSCVParams *rscv_params = new RSCVParams(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, debug_mode);
		return getTrackerObj<RSCV>(sm_type, ssm_type, rscv_params);
	} else if(!strcmp(am_type, "lrscv") || !strcmp(am_type, "lrsc")){
		LRSCVParams *lscv_params = new LRSCVParams(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping,  lscv_show_subregions, debug_mode);
		return getTrackerObj<LRSCV>(sm_type, ssm_type, lscv_params);
	} else if(!strcmp(am_type, "kld")){
		KLDParams *kld_params = new KLDParams(img_params, mi_n_bins, mi_pre_seed, mi_pou, debug_mode);
		return getTrackerObj<KLD>(sm_type, ssm_type, kld_params);
	} else if(!strcmp(am_type, "lkld")){
		LKLDParams *lkld_params = new LKLDParams(img_params, lkld_sub_regions, lkld_sub_regions,
			lkld_spacing, lkld_spacing, lkld_n_bins, lkld_pre_seed, lkld_pou, debug_mode);
		return getTrackerObj<LKLD>(sm_type, ssm_type, lkld_params);
	} else if(!strcmp(am_type, "mi")){
		MIParams *mi_params = new MIParams(img_params, mi_n_bins, mi_pre_seed, mi_pou,
			getPixMapperObj(pix_mapper, img_params), debug_mode);
		return getTrackerObj<MI>(sm_type, ssm_type, mi_params);
	} else if(!strcmp(am_type, "spss")){
		SPSSParams *spss_params = new SPSSParams(img_params, ssim_k1,
			getPixMapperObj(pix_mapper, img_params));
		return getTrackerObj<SPSS>(sm_type, ssm_type, spss_params);
	} else if(!strcmp(am_type, "ssim")){
		SSIMParams *ssim_params = new SSIMParams(img_params, ssim_k1, ssim_k2);
		return getTrackerObj<SSIM>(sm_type, ssm_type, ssim_params);
	} else if(!strcmp(am_type, "ncc")){
		NCCParams *ncc_params = new NCCParams(img_params, ncc_fast_hess);
		return getTrackerObj<NCC>(sm_type, ssm_type, ncc_params);
	} else if(!strcmp(am_type, "ccre")){
		CCREParams *ccre_params = new CCREParams(img_params, ccre_n_bins, ccre_pou, ccre_pre_seed,
			ccre_symmetrical_grad, ccre_n_blocks, debug_mode);
		return getTrackerObj<CCRE>(sm_type, ssm_type, ccre_params);
	} else if(!strcmp(am_type, "fmaps")){
		return getTrackerObj<FMaps>(sm_type, ssm_type, img_params);
	} else{
		printf("Invalid appearance model provided: %s\n", am_type);
		return nullptr;
	}
}
// Third Party Trackers
inline TrackerBase *getTrackerObj(const char *tracker_type, ImgParams *img_params){
#ifndef DISABLE_LEARNING_TRACKERS
	// 3rd party trackers
	if(!strcmp(tracker_type, "dsst")){
		DSSTParams *dsst_params = new DSSTParams(dsst_padding, dsst_sigma, dsst_scale_sigma, dsst_lambda,
			dsst_learning_rate, dsst_number_scales, dsst_scale_step,
			dsst_resize_factor, dsst_is_scaling, dsst_bin_size);
		return new DSSTTracker(img_params->cv_img, dsst_params);
	} else if(!strcmp(tracker_type, "kcf")){
		KCFParams *kcf_params = new KCFParams(
			kcf_padding,
			kcf_lambda,
			kcf_output_sigma_factor,
			kcf_interp_factor,
			kcf_kernel_sigma,
			kcf_number_scales,
			kcf_scale_step,
			kcf_scale_model_max_area,
			kcf_scale_sigma_factor,
			kcf_scale_learning_rate,
			kcf_enableScaling,
			kcf_resize_factor
			);
		return new KCFTracker(img_params->cv_img, kcf_params);
	} else if(!strcmp(tracker_type, "cmt")){
		CMTParams * cmt_params = new CMTParams(cmt_estimate_scale, cmt_estimate_rotation,
			cmt_feat_detector, cmt_desc_extractor, cmt_resize_factor);
		return new cmt::CMT(img_params->cv_img, cmt_params);
	} else if(!strcmp(tracker_type, "tld")){
		TLDParams * tld_params = new TLDParams(tld_tracker_enabled, tld_detector_enabled,
			tld_learning_enabled, tld_alternating);
		return new tld::TLD(img_params->cv_img, tld_params);
	} else if(!strcmp(tracker_type, "rct")){
		RCTParams *rct_params = new RCTParams(rct_min_n_rect, rct_max_n_rect, rct_n_feat,
			rct_rad_outer_pos, rct_rad_search_win, rct_learning_rate);
		return new CompressiveTracker(img_params->cv_img, rct_params);
	} else if(!strcmp(tracker_type, "strk")){
		struck::StruckParams strk_params(strk_config_path);
		return new struck::Struck(&strk_params);
	}
#ifndef DISABLE_PFSL3
	else if(!strcmp(tracker_type, "pfsl3")){
		PFSL3Params *pfsl3_params = new PFSL3Params(pfsl3_p_x, pfsl3_p_y,
			pfsl3_state_std, pfsl3_rot, pfsl3_ncc_std, pfsl3_pca_std, pfsl3_ar_p,
			pfsl3_n, pfsl3_n_c, pfsl3_n_iter, pfsl3_sampling, pfsl3_capture,
			pfsl3_mean_check, pfsl3_outlier_flag, pfsl3_len,
			pfsl3_init_size, pfsl3_update_period, pfsl3_ff,
			pfsl3_basis_thr, pfsl3_max_num_basis, pfsl3_max_num_used_basis,
			pfsl3_show_weights, pfsl3_show_templates);
		return new PFSL3(img_params->cv_img, pfsl3_params);
	}
#endif
#ifndef DISABLE_VISP
	else if(!strcmp(tracker_type, "visp")){
		ViSPParams::SMType vp_tracker_type = ViSPParams::SMType::FCLK;
		if(!strcmp(visp_sm, "fclk")){
			vp_tracker_type = ViSPParams::SMType::FCLK;
		} else if(!strcmp(visp_sm, "iclk")){
			vp_tracker_type = ViSPParams::SMType::ICLK;
		} else if(!strcmp(visp_sm, "falk")){
			vp_tracker_type = ViSPParams::SMType::FALK;
		} else if(!strcmp(visp_sm, "esm")){
			vp_tracker_type = ViSPParams::SMType::ESM;
		}
		ViSPParams::AMType vp_am_type = ViSPParams::AMType::SSD;
		if(!strcmp(visp_am, "ssd")){
			vp_am_type = ViSPParams::AMType::SSD;
		} else if(!strcmp(visp_am, "zncc")){
			vp_am_type = ViSPParams::AMType::ZNCC;
		} else if(!strcmp(visp_am, "mi")){
			vp_am_type = ViSPParams::AMType::MI;
		}

		ViSPParams::SSMType vp_stracker_type = ViSPParams::SSMType::Homography;
		if(!strcmp(visp_ssm, "8")){
			vp_stracker_type = ViSPParams::SSMType::Homography;
		} else if(!strcmp(visp_ssm, "l8")){
			vp_stracker_type = ViSPParams::SSMType::HomographySL3;
		} else if(!strcmp(visp_ssm, "6")){
			vp_stracker_type = ViSPParams::SSMType::Affine;
		} else if(!strcmp(visp_ssm, "4")){
			vp_stracker_type = ViSPParams::SSMType::Similarity;
		} else if(!strcmp(visp_ssm, "3")){
			vp_stracker_type = ViSPParams::SSMType::Isometry;
		} else if(!strcmp(visp_ssm, "2")){
			vp_stracker_type = ViSPParams::SSMType::Translation;
		}

		ViSPParams *visp_params = new ViSPParams(
			vp_tracker_type, vp_am_type, vp_stracker_type,
			visp_max_iters, visp_res, visp_res, visp_lambda,
			visp_thresh_grad, visp_pyr_n_levels, 
			visp_pyr_level_to_stop
			);
		return new ViSP(img_params->cv_img, visp_params);
	}
#endif
#endif
#ifndef DISABLE_XVISION
	else if(strstr(tracker_type, "xv")){
		using_xv_tracker = true;
		XVParams *xv_params = new XVParams(show_xv_window, steps_per_frame, false, false);
		if(!strcmp(tracker_type, "xv1r")){
			return new XVSSDRotate(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv1s")){
			return new XVSSDScaling(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv2")){
			return new XVSSDTrans(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv3")){
			return new XVSSDRT(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv4")){
			return new XVSSDSE2(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv6")){
			return new XVSSDAffine(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xv1p")){
			return new XVSSDPyramidRotate(img_params->cv_img, xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv2p")){
			return new XVSSDPyramidTrans(img_params->cv_img, xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv3p")){
			return new XVSSDPyramidRT(img_params->cv_img, xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv4p")){
			return new XVSSDPyramidSE2(img_params->cv_img, xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv6p")){
			return new XVSSDPyramidAffine(img_params->cv_img, xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xve")){
			return new XVEdgeTracker(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xvc")){
			return new XVColor(img_params->cv_img, xv_params);
		} else if(!strcmp(tracker_type, "xvg")){
			return new XVSSDGrid(img_params->cv_img, xv_params,
				xv_tracker_type, grid_size_x, grid_size_y, patch_size,
				reset_pos, reset_template, sel_reset_thresh, reset_wts,
				adjust_lines, update_wts, debug_mode);
		} else if(!strcmp(tracker_type, "xvgl")){
			return new XVSSDGridLine(img_params->cv_img, xv_params,
				xv_tracker_type, grid_size_x, grid_size_y,
				patch_size, use_constant_slope, use_ls, inter_alpha_thresh,
				intra_alpha_thresh, reset_pos, reset_template, debug_mode);
		} else{
			stringstream err_msg;
			err_msg << "Invalid Xvision tracker type type provided : " << tracker_type << "\n";
			throw std::invalid_argument(err_msg.str());
		}
	}
#endif
	return nullptr;
}
//template< class AMType, class SSMType >
//TrackerBase *getFESMObj(
//	typename AMType::ParamType *am_params = nullptr,
//	typename SSMType::ParamType *ssm_params = nullptr){
//	FESMParams *esm_params = new FESMParams(max_iters, epsilon,
//		sec_ord_hess, spi_enable, spi_thresh, debug_mode);
//	switch(static_cast<FESMParams::JacType>(jac_type)){
//	case FESMParams::JacType::Original:
//		switch(static_cast<FESMParams::HessType>(hess_type)){
//		case FESMParams::HessType::Original:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::Original,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfStd:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfStd,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::InitialSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::InitialSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::CurrentSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::Std:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		default:
//			throw std::invalid_argument("Invalid FESM Hessian type provided");
//		}
//	case FESMParams::JacType::DiffOfJacs:
//		switch(static_cast<FESMParams::HessType>(hess_type)){
//		case FESMParams::HessType::Original:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::Original,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfStd:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfStd,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::InitialSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::InitialSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::CurrentSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::Std:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		default:
//			throw std::invalid_argument("Invalid FESM Hessian type provided");
//		}
//	default:
//		throw std::invalid_argument("Invalid FESM Jacobian type provided");
//	}
//}

_MTF_END_NAMESPACE

#endif