# ----------------------------------------------------------------------------- #
# --------------------------------- Utilities --------------------------------- #
# ----------------------------------------------------------------------------- #

UTILITIES = histUtils warpUtils imgUtils miscUtils
MTF_UTIL_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${UTILITIES}))
MTF_UTIL_HEADERS = $(addprefix Utilities/, $(addsuffix .h, ${UTILITIES}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${MTF_UTIL_HEADERS})  

HIST_FLAGS = 

htbb ?= 0
ifeq (${htbb}, 1)
HIST_FLAGS += -D ENABLE_HIST_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

${BUILD_DIR}/warpUtils.o: Utilities/warpUtils.cc Utilities/warpUtils.h Macros/common.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FLAGS64} $< -o $@
	
${BUILD_DIR}/imgUtils.o: Utilities/imgUtils.cc Utilities/imgUtils.h Utilities/warpUtils.h Macros/common.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FLAGS64} $< -o $@
	
${BUILD_DIR}/histUtils.o: Utilities/histUtils.cc Utilities/histUtils.h Utilities/miscUtils.h Macros/common.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${HIST_FLAGS} ${FLAGS64} $< -o $@	
	
${BUILD_DIR}/miscUtils.o: Utilities/miscUtils.cc Utilities/miscUtils.h Utilities/miscUtils.h Macros/common.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${HIST_FLAGS} ${FLAGS64} $< -o $@	