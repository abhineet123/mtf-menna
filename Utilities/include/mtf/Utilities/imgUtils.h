#ifndef IMG_UTILS_H
#define IMG_UTILS_H

#include "mtf/Macros/common.h"

#ifndef HESS_INTERP_TYPE
#define HESS_INTERP_TYPE utils::Linear
#endif

#ifndef GRAD_INTERP_TYPE
#define GRAD_INTERP_TYPE utils::Linear
#endif

#ifndef PIX_INTERP_TYPE
#define PIX_INTERP_TYPE utils::Linear
#endif

_MTF_BEGIN_NAMESPACE

namespace utils{
	// types of interpolation
	enum{ Nearest, Linear, Cubic, Cubic2, CubicBSpl };

	/******* functions for extracting pixel values from image ***********/

	// check if the given x, y coordinates are outside the extents of an image with the given height and width
	inline bool checkOverflow(double x, double y, int h, int w){
		return ((x < 0) || (x >= w) || (y < 0) || (y >= h)) ? true : false;
	}
	// computes the pixel value at the given location expressed in (real) x, y, coordinates
	template<int interp_type>
	inline double getPixVal(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val = 128.0){
		printf("interpolation type: %d\n", interp_type);
		throw std::invalid_argument("getPixVal :: Invalid interpolation type specified");
	}
	// using nearest neighbor interpolation
	template<>
	inline double getPixVal<Nearest>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val){
		assert(img.rows() == h && img.cols() == w);

		if(checkOverflow(x, y, h, w)){
			return overflow_val;
		}
		int nx = static_cast<int>(rint(x));
		int ny = static_cast<int>(rint(y));
		return checkOverflow(nx, ny, h, w) ? overflow_val : img(ny, nx);
	}
	// using bilinear interpolation
	template<>
	inline double getPixVal<Linear>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val){
		assert(img.rows() == h && img.cols() == w);
		if(checkOverflow(x, y, h, w)){
			return overflow_val;
		}
		int lx = static_cast<int>(x);
		int ly = static_cast<int>(y);
		double dx = x - lx;
		double dy = y - ly;
		int ux = dx == 0 ? lx : lx + 1;
		int uy = dy == 0 ? ly : ly + 1;

		if(checkOverflow(lx, ly, h, w) || checkOverflow(ux, uy, h, w)){
			return overflow_val;
		}
		return 	img(ly, lx) * (1 - dx)*(1 - dy) +
			img(ly, ux) * dx*(1 - dy) +
			img(uy, lx) * (1 - dx)*dy +
			img(uy, ux) * dx*dy;
	}
	// using bi cubic interpolation
	template<>
	double getPixVal<Cubic>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val);
	// using cubic BSpline  interpolation - an alternate formulation that uses polynomial coefficients
	template<>
	double getPixVal<Cubic2>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val);
	// using cubic BSpline  interpolation
	template<>
	double getPixVal<CubicBSpl>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val);

	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals,
		const EigImgT &img, const PtsT &pts, int n_pix, int h, int w,
		double norm_mult=1, double norm_add=0);

	/*********** functions for mapping pixel values *******************/

	//! map pixel values using the given intensity map
	template<int interp_type>
	inline double mapPixVal(double x, const VectorXd &intensity_map){
		printf("mapPixVal:: invalid interpolation type specified: %d\n", interp_type);
		return 0;
	}
	//using nearest neighbor interpolation
	template<>
	inline double mapPixVal<Nearest>(double x, const VectorXd &intensity_map){
		return intensity_map(static_cast<int>(rint(x)));
	}
	//using linear interpolation
	template<>
	inline double mapPixVal<Linear>(double x, const VectorXd &intensity_map){
		int lx = static_cast<int>(x);
		double dx = x - lx;
		double mapped_x = dx == 0 ? intensity_map(lx) : (1 - dx)*intensity_map(lx) + dx*intensity_map(lx + 1);;
		//printf("Weighted mapping: x=%f lx=%d mapped x=%f\n", x, lx, mapped_x);
		//printMatrix(intensity_map, "intensity_map");
		return mapped_x;
	}
	// maps a vector of pixel values using the given intensity map; templated on the type of mapping to be done
	template<int interp_type>
	inline void mapPixVals(VectorXd &dst_pix_vals, VectorXd &src_pix_vals, 
		const VectorXd &intensity_map, int n_pix){
		for(int i = 0; i < n_pix; i++){
			dst_pix_vals(i) = mapPixVal<interp_type>(src_pix_vals(i), intensity_map);
		}
	}

	/*********** compute image gradient at a given pixel location *******************/

	// takes homogeneous points
	inline double getPixGrad(const EigImgT &img,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w);
		double pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixGrad(const EigImgT &img,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, pt_inc(0), pt_inc(1), h, w);
		double pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, pt_dec(0), pt_dec(1), h, w);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// uses an intensity map to remap the pixel values before computing their difference; 
	// templated on the type of mapping to be done: either floor or linear weighting
	// takes homogeneous points
	template<int mapping_type>
	inline double getPixGrad(const EigImgT &img, const VectorXd &intensity_map,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w), intensity_map);
		double pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w), intensity_map);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// takes non-homogeneous points
	template<int mapping_type>
	inline double getPixGrad(const EigImgT &img, const VectorXd &intensity_map,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, pt_inc(0), pt_inc(1), h, w), intensity_map);
		double pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, pt_dec(0), pt_dec(1), h, w), intensity_map);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}

	/*********** compute image Hessian at a given pixel location ***********/

	// compute diagonal entries of the pixel Hessian matrix
	// takes homogeneous points
	inline double getPixHess(const EigImgT &img,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double pix_val, double hess_mult_factor, int h, int w){
		double pix_val_inc = getPixVal<HESS_INTERP_TYPE>(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w);
		double pix_val_dec = getPixVal<HESS_INTERP_TYPE>(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w);
		return (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixHess(const EigImgT &img,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double pix_val, double hess_mult_factor, int h, int w){
		double pix_val_inc = getPixVal<HESS_INTERP_TYPE>(img, pt_inc(0), pt_inc(1), h, w);
		double pix_val_dec = getPixVal<HESS_INTERP_TYPE>(img, pt_dec(0), pt_dec(1), h, w);
		return (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;
	}
	// compute off diagonal entries of the pixel Hessian matrix
	// takes homogeneous points
	inline double getPixHess(const EigImgT &img,
		const Vector3d &pt_inc1, const Vector3d &pt_dec1,
		const Vector3d &pt_inc2, const Vector3d &pt_dec2,
		double hess_mult_factor, int h, int w){
		double pix_val_inc1 = getPixVal<HESS_INTERP_TYPE>(img, pt_inc1(0) / pt_inc1(2),
			pt_inc1(1) / pt_inc1(2), h, w);
		double pix_val_dec1 = getPixVal<HESS_INTERP_TYPE>(img, pt_dec1(0) / pt_dec1(2),
			pt_dec1(1) / pt_dec1(2), h, w);
		double pix_val_inc2 = getPixVal<HESS_INTERP_TYPE>(img, pt_inc2(0) / pt_inc2(2),
			pt_inc2(1) / pt_inc2(2), h, w);
		double pix_val_dec2 = getPixVal<HESS_INTERP_TYPE>(img, pt_dec2(0) / pt_dec2(2),
			pt_dec2(1) / pt_dec2(2), h, w);
		return ((pix_val_inc1 + pix_val_dec1) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixHess(const EigImgT &img,
		const Vector2d &pt_inc1, const Vector2d &pt_dec1,
		const Vector2d &pt_inc2, const Vector2d &pt_dec2,
		double hess_mult_factor, int h, int w){
		double pix_val_inc1 = getPixVal<HESS_INTERP_TYPE>(img, pt_inc1(0), pt_inc1(1), h, w);
		double pix_val_dec1 = getPixVal<HESS_INTERP_TYPE>(img, pt_dec1(0), pt_dec1(1), h, w);
		double pix_val_inc2 = getPixVal<HESS_INTERP_TYPE>(img, pt_inc2(0), pt_inc2(1), h, w);
		double pix_val_dec2 = getPixVal<HESS_INTERP_TYPE>(img, pt_dec2(0), pt_dec2(1), h, w);
		return ((pix_val_inc1 + pix_val_dec1) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
	}
	// mapped versions for homogeneous points
	template<int mapping_type>
	inline double getPixHess(const EigImgT &img, const VectorXd &intensity_map,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double pix_val, double hess_mult_factor, int h, int w){
		double pix_val_inc = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w), intensity_map);
		double pix_val_dec = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w), intensity_map);
		return (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;
	}
	template<int mapping_type>
	inline double getPixHess(const EigImgT &img, const VectorXd &intensity_map, 
		const Vector3d &pt_inc1, const Vector3d &pt_dec1,
		const Vector3d &pt_inc2, const Vector3d &pt_dec2,
		double hess_mult_factor, int h, int w){
		double pix_val_inc1 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_inc1(0) / pt_inc1(2),
			pt_inc1(1) / pt_inc1(2), h, w), intensity_map);
		double pix_val_dec1 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_dec1(0) / pt_dec1(2),
			pt_dec1(1) / pt_dec1(2), h, w), intensity_map);
		double pix_val_inc2 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_inc2(0) / pt_inc2(2),
			pt_inc2(1) / pt_inc2(2), h, w), intensity_map);
		double pix_val_dec2 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, pt_dec2(0) / pt_dec2(2),
			pt_dec2(1) / pt_dec2(2), h, w), intensity_map);
		return ((pix_val_inc1 + pix_val_dec1) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
	}

	/*********** functions for bicubic interpolation *******************/

	// extracts pixel values from a 4x4 grid of integral locations around the given location;
	// this grid extends in the range [x1-1, x1+2] and [y1-1, y1+2]; 
	// uses pixel duplication for points in the grid that lie outside the image extents
	// these pixels can be used for bicubic interpolation
	template<typename PixVecT>
	inline double cubicBSplInterpolate(const PixVecT &pix_vec, double dx) {
		double dx2 = dx*dx;
		double dx3 = dx2*dx;
		double dxi = 1 - dx;
		double dxi2 = dxi*dxi;
		double dxi3 = dxi2*dxi;
		return (pix_vec(0)*dxi3 + pix_vec(1)*(4 - 6 * dx2 + 3 * dx3) + pix_vec(2)*(4 - 6 * dxi2 + 3 * dxi3) + pix_vec(3)*dx3) / 6;
	}
	template<typename PixVecT>
	inline double cubicInterpolate(const PixVecT &pix_vec, double x) {
		return pix_vec(1) + 0.5 * x*(pix_vec(2) - pix_vec(0) + x*(2.0*pix_vec(0) - 5.0*pix_vec(1) + 4.0*pix_vec(2) - pix_vec(3) + x*(3.0*(pix_vec(1) - pix_vec(2)) + pix_vec(3) - pix_vec(0))));
	}
	bool getNeighboringPixGrid(Matrix4d &pix_grid, const EigImgT &img, double x, double y,
		int h, int w);
	void getBiCubicCoefficients(Matrix4d &bicubic_coeff, const Matrix4d &pix_grid);
	//evaluates the cubic polynomial surface defines by the given parameters at the given location
	double biCubic(const Matrix4d &bicubic_coeff, double x, double y);
	//evaluates the gradient of cubic polynomial surface defines by the given parameters at the given location
	double biCubicGradX(Vector2d &grad, const Matrix4d &bicubic_coeff, double x, double y);
	double biCubicGradY(Vector2d &grad, const Matrix4d &bicubic_coeff, double x, double y);
	//evaluates the hessian of the cubic polynomial surface defines by the given parameters at the given location
	double biCubicHessXX(const Matrix4d &bicubic_coeff, double x, double y);
	double biCubicHessYY(const Matrix4d &bicubic_coeff, double x, double y);
	double biCubicHessYX(const Matrix4d &bicubic_coeff, double x, double y);
	double biCubicHessXY(const Matrix4d &bicubic_coeff, double x, double y);

	/************ functions defined in img_utils.cc ************/

	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const Matrix3Xd &warped_pts,
		const ProjWarpT &warp, double grad_eps,
		int n_pix, int h, int w, double pix_mult_factor = 1.0);
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const Matrix8Xd &warped_offset_pts,
		double grad_eps, int n_pix,
		int h, int w, double pix_mult_factor=1.0);

	template<int type>
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const Matrix3Xd &warped_pts, const ProjWarpT &warp,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor = 1.0);

	template<int mapping_type>
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const Matrix8Xd &warped_offset_pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor=1.0);

	void getImgGrad(PixGradT &img_grad,
		const EigImgT &img, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor = 1.0);
	// mapping enabled version
	template<int mapping_type>
	void getImgGrad(PixGradT &img_grad, const EigImgT &img,
		const VectorXd &intensity_map, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor = 1.0);

	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const Matrix3Xd &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps,
		int n_pix, int h, int w, double pix_mult_factor = 1.0);

	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const PtsT &warped_pts,
		const Matrix16Xd &warped_offset_pts, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor = 1.0);
	// mapped version
	template<int mapping_type>
	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map,
		const PtsT &warped_pts, const Matrix16Xd &warped_offset_pts,
		double hess_eps, int n_pix, int h, int w, double pix_mult_factor=1.0);

	template<int mapping_type>
	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, const Matrix3Xd &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor = 1.0);

	void getImgHess(PixHessT &img_hess, const EigImgT &img,
		const PtsT &pts, double hess_eps,
		int n_pix, int h, int w, double pix_mult_factor = 1.0);
	template<int mapping_type>
	void getImgHess(PixHessT &img_hess, const EigImgT &img, const VectorXd &intensity_map,
		const PtsT &pts, double hess_eps,int n_pix, int h, int w, 
		double pix_mult_factor = 1.0);

	// fits a bi cubic polynomial surface at each pixel location and computes the analytical hessian
	// of this surface thus eliminating the need for finite difference approximations
	void getImgHess(PixHessT &img_hess, const EigImgT &img,
		const PtsT &pts, int n_pix, int h, int w);
}
_MTF_END_NAMESPACE
#endif
