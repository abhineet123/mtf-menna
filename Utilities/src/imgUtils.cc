#include "mtf/Utilities/imgUtils.h"
#include "mtf/Utilities/miscUtils.h"

#ifdef USE_TBB
#include "tbb/tbb.h" 
#endif

_MTF_BEGIN_NAMESPACE

namespace utils{
	char *interp_types[] = { "Nearest", "Linear", "Cubic", "Cubic2", "CubicBSpl" };

	// using cubic BSpline  interpolation
	inline double cubicBSplInterpolate(double p0, double p1, double p2, double p3, double dx) {
		double dx2 = dx*dx;
		double dx3 = dx2*dx;
		double dxi = 1 - dx;
		double dxi2 = dxi*dxi;
		double dxi3 = dxi2*dxi;
		return (p0*dxi3 + p1*(4 - 6 * dx2 + 3 * dx3) + p2*(4 - 6 * dxi2 + 3 * dxi3) + p3*dx3) / 6;
	}
	template<>
	inline double getPixVal<CubicBSpl>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val){
		assert(img.rows() == h && img.cols() == w);
		//printf("----------Cubic BSpline Interpolation------------\n\n");
		if(checkOverflow(x, y, h, w)){
			return overflow_val;
		}
		int x1 = static_cast<int>(x);
		int y1 = static_cast<int>(y);

		Matrix4d neigh_pix_grid;
		if(!getNeighboringPixGrid(neigh_pix_grid, img, x1, y1, h, w))
			return overflow_val;
		double dx = x - x1;
		double dy = y - y1;
		Vector4d col_interp_pix;
		col_interp_pix(0) = cubicBSplInterpolate(neigh_pix_grid.row(0), dy);
		col_interp_pix(1) = cubicBSplInterpolate(neigh_pix_grid.row(1), dy);
		col_interp_pix(2) = cubicBSplInterpolate(neigh_pix_grid.row(2), dy);
		col_interp_pix(3) = cubicBSplInterpolate(neigh_pix_grid.row(3), dy);
		return cubicBSplInterpolate(col_interp_pix, dx);
	}

	// using bicubic interpolation
	inline double cubicInterpolate(double p0, double p1, double p2, double p3, double x) {
		return p1 + 0.5 * x*(p2 - p0 + x*(2.0*p0 - 5.0*p1 + 4.0*p2 - p3 + x*(3.0*(p1 - p2) + p3 - p0)));
	}
	template<>
	inline double getPixVal<Cubic>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val){
		assert(img.rows() == h && img.cols() == w);
		//printf("----------Bicubic Interpolation------------\n\n");
		if(checkOverflow(x, y, h, w)){
			return overflow_val;
		}
		Matrix4d neigh_pix_grid;
		if(!getNeighboringPixGrid(neigh_pix_grid, img, x, y, h, w))
			return overflow_val;

		double dx = x - static_cast<int>(x);
		double dy = y - static_cast<int>(y);

		Vector4d col_interp_pix;
		col_interp_pix(0) = cubicInterpolate(neigh_pix_grid.row(0), dy);
		col_interp_pix(1) = cubicInterpolate(neigh_pix_grid.row(1), dy);
		col_interp_pix(2) = cubicInterpolate(neigh_pix_grid.row(2), dy);
		col_interp_pix(3) = cubicInterpolate(neigh_pix_grid.row(3), dy);
		return cubicInterpolate(col_interp_pix, dx);
	}
	template<>
	inline double getPixVal<Cubic2>(const EigImgT &img, double x, double y,
		int h, int w, double overflow_val){
		assert(img.rows() == h && img.cols() == w);
		//printf("----------Bi cubic Interpolation using coefficients------------\n\n");		
		Matrix4d neigh_pix_grid, bicubic_coeff;
		if(!getNeighboringPixGrid(neigh_pix_grid, img, x, y, h, w))
			return overflow_val;
		getBiCubicCoefficients(bicubic_coeff, neigh_pix_grid);
		double dx = x - static_cast<int>(x);
		double dy = y - static_cast<int>(y);
		return  biCubic(bicubic_coeff, dx, dy);
	}

	/****************************************************************/
	/******************** Gradient of Warped Image********************/
	/****************************************************************/

#ifdef USE_TBB
#include "imgUtils_tbb.cc"
#else

	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals,
		const EigImgT &img, const PtsT &pts, int n_pix, int h, int w,
		double norm_mult, double norm_add){
		//printf("n_pix: %d\t pix_vals.size(): %l\t pts.cols(): %l", n_pix, pix_vals.size(),  pts.cols());
		assert(pix_vals.size() == n_pix && pts.cols() == n_pix);

		for(int i = 0; i < n_pix; i++){
			pix_vals(i) = norm_mult * getPixVal<PIX_INTERP_TYPE>(img, pts(0, i), pts(1, i), h, w) + norm_add;
		}
	}

	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const HomPtsT &warped_pts, 
		const ProjWarpT &warp, double grad_eps, int n_pix, 
		int h, int w, double pix_mult_factor){
		assert(warped_img_grad.rows() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);

		Vector3d diff_vec_x_warped = warp.col(0) * grad_eps;
		Vector3d diff_vec_y_warped = warp.col(1) * grad_eps;

		//utils::printMatrixToFile(warped_pts, "warped_pts", "log/mtf_log.txt", "%15.9f", "a");
		//utils::printMatrixToFile(warp, "warp", "log/mtf_log.txt", "%15.9f", "a");
		//utils::printMatrixToFile(diff_vec_x_warped, "diff_vec_x_warped", "log/mtf_log.txt", "%15.9f", "a");
		//utils::printMatrixToFile(diff_vec_y_warped, "diff_vec_y_warped", "log/mtf_log.txt", "%15.9f", "a");

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warped_pts.col(pix_id) + diff_vec_x_warped;
			pt_dec_warped = warped_pts.col(pix_id) - diff_vec_x_warped;
			warped_img_grad(pix_id, 0) = getPixGrad(img, pt_inc_warped, pt_dec_warped,
				grad_mult_factor, h, w);

			pt_inc_warped = warped_pts.col(pix_id) + diff_vec_y_warped;
			pt_dec_warped = warped_pts.col(pix_id) - diff_vec_y_warped;
			warped_img_grad(pix_id, 1) = getPixGrad(img, pt_inc_warped, pt_dec_warped,
				grad_mult_factor, h, w);
		}
		//utils::printMatrixToFile(warped_img_grad, "warped_img_grad", "log/mtf_log.txt", "%15.9f", "a");

	}
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const Matrix8Xd &warped_offset_pts,
		double grad_eps, int n_pix,
		int h, int w, double pix_mult_factor){
		assert(warped_img_grad.rows() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);
		double pix_val_inc, pix_val_dec;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, warped_offset_pts(0, pix_id),
				warped_offset_pts(1, pix_id), h, w);
			pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, warped_offset_pts(2, pix_id),
				warped_offset_pts(3, pix_id), h, w);
			warped_img_grad(pix_id, 0) = (pix_val_inc - pix_val_dec)*grad_mult_factor;

			pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, warped_offset_pts(4, pix_id),
				warped_offset_pts(5, pix_id), h, w);
			pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, warped_offset_pts(6, pix_id),
				warped_offset_pts(7, pix_id), h, w);
			warped_img_grad(pix_id, 1) = (pix_val_inc - pix_val_dec)*grad_mult_factor;
		}
		//utils::printMatrixToFile(warped_img_grad, "warped_img_grad", "log/mtf_log.txt", "%15.9f", "a");

	}

	// mapped version
	template<int mapping_type>
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const Matrix8Xd &warped_offset_pts,	double grad_eps, 
		int n_pix, int h, int w, double pix_mult_factor){
		assert(warped_img_grad.rows() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);
		double pix_val_inc, pix_val_dec;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, 
				warped_offset_pts(0, pix_id), warped_offset_pts(1, pix_id), h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img,
				warped_offset_pts(2, pix_id), warped_offset_pts(3, pix_id), h, w), intensity_map);
			warped_img_grad(pix_id, 0) = (pix_val_inc - pix_val_dec)*grad_mult_factor;

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img,
				warped_offset_pts(4, pix_id), warped_offset_pts(5, pix_id), h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img,
				warped_offset_pts(6, pix_id), warped_offset_pts(7, pix_id), h, w), intensity_map);
			warped_img_grad(pix_id, 1) = (pix_val_inc - pix_val_dec)*grad_mult_factor;
		}
	}
	/***************************************************************/
	/******************** Warp of Image Gradient********************/
	/***************************************************************/

	void getImgGrad(PixGradT &img_grad,
		const EigImgT &img, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor){
		assert(img_grad.rows() == n_pix && pts.cols() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);
		double pix_val_inc, pix_val_dec;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			double curr_x = pts(0, pix_id), curr_y = pts(1, pix_id);

			pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, curr_x + grad_eps, curr_y, h, w);
			pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, curr_x - grad_eps, curr_y, h, w);
			img_grad(pix_id, 0) = (pix_val_inc - pix_val_dec)*grad_mult_factor;

			pix_val_inc = getPixVal<GRAD_INTERP_TYPE>(img, curr_x, curr_y + grad_eps, h, w);
			pix_val_dec = getPixVal<GRAD_INTERP_TYPE>(img, curr_x, curr_y - grad_eps, h, w);

			img_grad(pix_id, 1) = (pix_val_inc - pix_val_dec)*grad_mult_factor;
		}
	}
	/***************************************************************/
	/******************** Hessian of Warped Image ******************/
	/***************************************************************/

	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const HomPtsT &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor){
		assert(warped_img_hess.cols() == n_pix && warped_pts_hm.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		Vector3d diff_vec_xx_warped = warp.col(0) * hess_eps2;
		Vector3d diff_vec_yy_warped = warp.col(1) * hess_eps2;
		Vector3d diff_vec_xy_warped = (warp.col(0) + warp.col(1)) * hess_eps;
		Vector3d diff_vec_yx_warped = (warp.col(0) - warp.col(1)) * hess_eps;

		Vector3d pt_warped, pt_inc_warped, pt_dec_warped, pt_inc_warped2, pt_dec_warped2;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			pt_warped = warped_pts_hm.col(pix_id);

			double pix_val = getPixVal<HESS_INTERP_TYPE>(img, pt_warped(0) / pt_warped(2),
				pt_warped(1) / pt_warped(2), h, w);

			pt_inc_warped = pt_warped + diff_vec_xx_warped;
			pt_dec_warped = pt_warped - diff_vec_xx_warped;
			warped_img_hess(0, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pix_val, hess_mult_factor, h, w);

			pt_inc_warped = pt_warped + diff_vec_yy_warped;
			pt_dec_warped = pt_warped - diff_vec_yy_warped;
			warped_img_hess(3, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pix_val, hess_mult_factor, h, w);

			pt_inc_warped = pt_warped + diff_vec_xy_warped;
			pt_dec_warped = pt_warped - diff_vec_xy_warped;
			pt_inc_warped2 = pt_warped + diff_vec_yx_warped;
			pt_dec_warped2 = pt_warped - diff_vec_yx_warped;
			warped_img_hess(1, pix_id) = warped_img_hess(2, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pt_inc_warped2, pt_dec_warped2, hess_mult_factor, h, w);
		}
	}

	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const PtsT &warped_pts,
		const HessPtsT &warped_offset_pts, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor){
		assert(warped_img_hess.cols() == n_pix && warped_pts.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		double pix_val_inc, pix_val_dec, pix_val_inc2, pix_val_dec2;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			double pix_val = getPixVal<HESS_INTERP_TYPE>(img, warped_pts(0, pix_id), warped_pts(1, pix_id), h, w);

			pix_val_inc = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(0, pix_id), warped_offset_pts(1, pix_id), h, w);
			pix_val_dec = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(2, pix_id), warped_offset_pts(3, pix_id), h, w);
			warped_img_hess(0, pix_id) = (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;

			pix_val_inc = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(4, pix_id), warped_offset_pts(5, pix_id), h, w);
			pix_val_dec = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(6, pix_id), warped_offset_pts(7, pix_id), h, w);
			warped_img_hess(3, pix_id) = (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;

			pix_val_inc = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(8, pix_id), warped_offset_pts(9, pix_id), h, w);
			pix_val_dec = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(10, pix_id), warped_offset_pts(11, pix_id), h, w);
			pix_val_inc2 = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(12, pix_id), warped_offset_pts(13, pix_id), h, w);
			pix_val_dec2 = getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(14, pix_id), warped_offset_pts(15, pix_id), h, w);
			warped_img_hess(1, pix_id) = warped_img_hess(2, pix_id) = ((pix_val_inc + pix_val_dec) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
		}
	}
	// mapped version
	template<int mapping_type>
	void getWarpedImgHess(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, 
		const PtsT &warped_pts, const HessPtsT &warped_offset_pts, 
		double hess_eps, int n_pix,	int h, int w, double pix_mult_factor){
		assert(warped_img_hess.cols() == n_pix && warped_pts.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		double pix_val_inc, pix_val_dec, pix_val_inc2, pix_val_dec2;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			double pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_pts(0, pix_id), 
				warped_pts(1, pix_id), h, w), intensity_map);

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(0, pix_id), 
				warped_offset_pts(1, pix_id), h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(2, pix_id), 
				warped_offset_pts(3, pix_id), h, w), intensity_map);
			warped_img_hess(0, pix_id) = (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(4, pix_id),
				warped_offset_pts(5, pix_id), h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(6, pix_id), 
				warped_offset_pts(7, pix_id), h, w), intensity_map);
			warped_img_hess(3, pix_id) = (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(8, pix_id), 
				warped_offset_pts(9, pix_id), h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(10, pix_id), 
				warped_offset_pts(11, pix_id), h, w), intensity_map);
			pix_val_inc2 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(12, pix_id), 
				warped_offset_pts(13, pix_id), h, w), intensity_map);
			pix_val_dec2 = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, warped_offset_pts(14, pix_id), 
				warped_offset_pts(15, pix_id), h, w), intensity_map);
			warped_img_hess(1, pix_id) = warped_img_hess(2, pix_id) = ((pix_val_inc + pix_val_dec) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
		}
	}

	/***************************************************************/
	/******************** Warp of Image Hessian********************/
	/***************************************************************/

	void getImgHess(PixHessT &img_hess, const EigImgT &img,
		const PtsT &pts, double hess_eps,
		int n_pix, int h, int w, double pix_mult_factor){
		assert(img_hess.cols() == n_pix && pts.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			double curr_x = pts(0, pix_id), curr_y = pts(1, pix_id);
			double curr_pix_val = getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y, h, w);

			double ix_pix_val = getPixVal<HESS_INTERP_TYPE>(img, curr_x + hess_eps2, curr_y, h, w);
			double dx_pix_val = getPixVal<HESS_INTERP_TYPE>(img, curr_x - hess_eps2, curr_y, h, w);
			img_hess(0, pix_id) = (ix_pix_val + dx_pix_val - 2 * curr_pix_val) * hess_mult_factor;

			double iy_pix_val = getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y + hess_eps2, h, w);
			double dy_pix_val = getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y - hess_eps2, h, w);
			img_hess(3, pix_id) = (iy_pix_val + dy_pix_val - 2 * curr_pix_val) * hess_mult_factor;

			double inc_x = curr_x + hess_eps, dec_x = curr_x - hess_eps;
			double inc_y = curr_y + hess_eps, dec_y = curr_y - hess_eps;
			double ixiy_pix_val = getPixVal<HESS_INTERP_TYPE>(img, inc_x, inc_y, h, w);
			double dxdy_pix_val = getPixVal<HESS_INTERP_TYPE>(img, dec_x, dec_y, h, w);
			double ixdy_pix_val = getPixVal<HESS_INTERP_TYPE>(img, inc_x, dec_y, h, w);
			double iydx_pix_val = getPixVal<HESS_INTERP_TYPE>(img, dec_x, inc_y, h, w);
			img_hess(1, pix_id) = img_hess(2, pix_id) = ((ixiy_pix_val + dxdy_pix_val) - (ixdy_pix_val + iydx_pix_val)) * hess_mult_factor;
		}
	}

#endif

	// mapped version
	template<int mapping_type>
	void getWarpedImgGrad(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const HomPtsT &warped_pts, const ProjWarpT &warp,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor){
		assert(warped_img_grad.rows() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);

		Vector3d diff_vec_x_warped = warp.col(0) * grad_eps;
		Vector3d diff_vec_y_warped = warp.col(1) * grad_eps;

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warped_pts.col(pix_id) + diff_vec_x_warped;
			pt_dec_warped = warped_pts.col(pix_id) - diff_vec_x_warped;
			warped_img_grad(pix_id, 0) = getPixGrad<mapping_type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, grad_mult_factor, h, w);

			pt_inc_warped = warped_pts.col(pix_id) + diff_vec_y_warped;
			pt_dec_warped = warped_pts.col(pix_id) - diff_vec_y_warped;
			warped_img_grad(pix_id, 1) = getPixGrad<mapping_type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, grad_mult_factor, h, w);
		}
	}
	// mapped version
	template<int mapping_type>
	void getImgGrad(PixGradT &img_grad, const EigImgT &img,
		const VectorXd &intensity_map, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor){
		assert(img_grad.rows() == n_pix && pts.cols() == n_pix);

		double grad_mult_factor = pix_mult_factor / (2 * grad_eps);
		double pix_val_inc, pix_val_dec;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			double curr_x = pts(0, pix_id), curr_y = pts(1, pix_id);

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, curr_x + grad_eps, curr_y, h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, curr_x - grad_eps, curr_y, h, w), intensity_map);
			img_grad(pix_id, 0) = (pix_val_inc - pix_val_dec)*grad_mult_factor;

			pix_val_inc = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, curr_x, curr_y + grad_eps, h, w), intensity_map);
			pix_val_dec = mapPixVal<mapping_type>(getPixVal<GRAD_INTERP_TYPE>(img, curr_x, curr_y - grad_eps, h, w), intensity_map);

			img_grad(pix_id, 1) = (pix_val_inc - pix_val_dec)*grad_mult_factor;
		}
	}


	// mapped version
	template<int mapping_type>
	void getWarpedImgHess(PixHessT &warped_img_hess, const EigImgT &img, 
		const VectorXd &intensity_map, const HomPtsT &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor){
		assert(warped_img_hess.cols() == n_pix && warped_pts_hm.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		Vector3d diff_vec_xx_warped = warp.col(0) * hess_eps2;
		Vector3d diff_vec_yy_warped = warp.col(1) * hess_eps2;
		Vector3d diff_vec_xy_warped = (warp.col(0) + warp.col(1)) * hess_eps;
		Vector3d diff_vec_yx_warped = (warp.col(0) - warp.col(1)) * hess_eps;

		Vector3d pt_warped, pt_inc_warped, pt_dec_warped, pt_inc_warped2, pt_dec_warped2;

		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			pt_warped = warped_pts_hm.col(pix_id);

			double pix_val = getPixVal<HESS_INTERP_TYPE>(img, pt_warped(0) / pt_warped(2),
				pt_warped(1) / pt_warped(2), h, w);

			pt_inc_warped = pt_warped + diff_vec_xx_warped;
			pt_dec_warped = pt_warped - diff_vec_xx_warped;
			warped_img_hess(0, pix_id) = getPixHess<mapping_type>(img, intensity_map, pt_inc_warped, pt_dec_warped,
				pix_val, hess_mult_factor, h, w);

			pt_inc_warped = pt_warped + diff_vec_yy_warped;
			pt_dec_warped = pt_warped - diff_vec_yy_warped;
			warped_img_hess(3, pix_id) = getPixHess<mapping_type>(img, intensity_map, pt_inc_warped, pt_dec_warped,
				pix_val, hess_mult_factor, h, w);

			pt_inc_warped = pt_warped + diff_vec_xy_warped;
			pt_dec_warped = pt_warped - diff_vec_xy_warped;
			pt_inc_warped2 = pt_warped + diff_vec_yx_warped;
			pt_dec_warped2 = pt_warped - diff_vec_yx_warped;
			warped_img_hess(1, pix_id) = warped_img_hess(2, pix_id) = getPixHess<mapping_type>(img, intensity_map, pt_inc_warped, pt_dec_warped,
				pt_inc_warped2, pt_dec_warped2, hess_mult_factor, h, w);
		}
	}
	// mapped version
	template<int mapping_type>
	void getImgHess(PixHessT &img_hess, const EigImgT &img,
		const VectorXd &intensity_map, const PtsT &pts, double hess_eps,
		int n_pix, int h, int w, double pix_mult_factor){
		assert(img_hess.cols() == n_pix && pts.cols() == n_pix);

		double hess_eps2 = 2 * hess_eps;
		double hess_mult_factor = pix_mult_factor / (hess_eps2 * hess_eps2);

		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			double curr_x = pts(0, pix_id), curr_y = pts(1, pix_id);
			double curr_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y, h, w), intensity_map);

			double ix_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, curr_x + hess_eps2, curr_y, h, w), intensity_map);
			double dx_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, curr_x - hess_eps2, curr_y, h, w), intensity_map);
			img_hess(0, pix_id) = (ix_pix_val + dx_pix_val - 2 * curr_pix_val) * hess_mult_factor;

			double iy_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y + hess_eps2, h, w), intensity_map);
			double dy_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, curr_x, curr_y - hess_eps2, h, w), intensity_map);
			img_hess(3, pix_id) = (iy_pix_val + dy_pix_val - 2 * curr_pix_val) * hess_mult_factor;

			double inc_x = curr_x + hess_eps, dec_x = curr_x - hess_eps;
			double inc_y = curr_y + hess_eps, dec_y = curr_y - hess_eps;
			double ixiy_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, inc_x, inc_y, h, w), intensity_map);
			double dxdy_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, dec_x, dec_y, h, w), intensity_map);
			double ixdy_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, inc_x, dec_y, h, w), intensity_map);
			double iydx_pix_val = mapPixVal<mapping_type>(getPixVal<HESS_INTERP_TYPE>(img, dec_x, inc_y, h, w), intensity_map);
			img_hess(1, pix_id) = img_hess(2, pix_id) = ((ixiy_pix_val + dxdy_pix_val) - (ixdy_pix_val + iydx_pix_val)) * hess_mult_factor;
		}
	}

	// computes image hessian analytically by fitting a cubic polynomial surface to a 4x4 grid of pixels around each location
	void getImgHess(PixHessT &img_hess,
		const EigImgT &img, const PtsT &pts,
		int n_pix, int h, int w){
		assert(img_hess.cols() == n_pix && pts.cols() == n_pix);
		Matrix4d neigh_pix_grid, bicubic_coeff;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			double x = pts(0, pix_id), y = pts(1, pix_id);

			if(!getNeighboringPixGrid(neigh_pix_grid, img, x, y, h, w)){
				img_hess(0, pix_id) = img_hess(1, pix_id) = img_hess(2, pix_id) = img_hess(3, pix_id) = 0;
				continue;
			}
			getBiCubicCoefficients(bicubic_coeff, neigh_pix_grid);
			double dx = x - static_cast<int>(x);
			double dy = y - static_cast<int>(y);
			img_hess(0, pix_id) = biCubicHessXX(bicubic_coeff, dx, dy);
			img_hess(3, pix_id) = biCubicHessYY(bicubic_coeff, dx, dy);
			img_hess(1, pix_id) = img_hess(2, pix_id) = biCubicHessYX(bicubic_coeff, dx, dy);

			//img_hess(2, pix_id) = biCubicHessXY(bicubic_coeff, dx, dy);
			//printMatrix(neigh_pix_grid, "neigh_pix_grid");
			//printMatrix(bicubic_coeff, "bicubic_coeff");
			//printMatrix(Map<Matrix2d>((double*)img_hess.col(pix_id).data()), "img_hess");
		}
		//printScalarToFile(hess_eps, "hess_eps", "./log/mtf_log.txt", "%e", "a");
		//printMatrixToFile(img_hess, "img_hess", "./log/mtf_log.txt", "%e", "a");
	}


	bool getNeighboringPixGrid(Matrix4d &pix_grid, const EigImgT &img, double x, double y,
		int h, int w){
		assert(img.rows() == h && img.cols() == w);

		if(checkOverflow(x, y, h, w)){
			return false;
		}
		int x1 = static_cast<int>(x);
		int y1 = static_cast<int>(y);

		int x2 = x1 + 1;
		int y2 = y1 + 1;

		if(checkOverflow(x1, y1, h, w) || checkOverflow(x2, y2, h, w)){
			return false;
		}
		pix_grid(1, 1) = img(y1, x1), pix_grid(1, 2) = img(y1, x2);
		pix_grid(2, 1) = img(y2, x1), pix_grid(2, 2) = img(y2, x2);

		int x0 = x1 - 1, x3 = x2 + 1;
		int y0 = y1 - 1, y3 = y2 + 1;

		bool ul = true, ur = true, lr = true, ll = true;

		if(y0 < 0){
			ul = ur = false;
			pix_grid(0, 1) = pix_grid(1, 1);
			pix_grid(0, 2) = pix_grid(1, 2);
		} else{
			pix_grid(0, 1) = img(y0, x1);
			pix_grid(0, 2) = img(y0, x2);
		}
		if(y3 >= h){
			ll = lr = false;
			pix_grid(3, 1) = pix_grid(2, 1);
			pix_grid(3, 2) = pix_grid(2, 2);
		} else{
			pix_grid(3, 1) = img(y3, x1);
			pix_grid(3, 2) = img(y3, x2);
		}
		if(x0 < 0){
			ul = ll = false;
			pix_grid(1, 0) = pix_grid(1, 1);
			pix_grid(2, 0) = pix_grid(2, 1);
		} else{
			pix_grid(1, 0) = img(y1, x0);
			pix_grid(2, 0) = img(y2, x0);
		}
		if(x3 >= w){
			ur = lr = false;
			pix_grid(1, 3) = pix_grid(1, 2);
			pix_grid(2, 3) = pix_grid(2, 2);
		} else{
			pix_grid(1, 3) = img(y1, x3);
			pix_grid(2, 3) = img(y2, x3);
		}

		pix_grid(0, 0) = ul ? img(y0, x0) : pix_grid(1, 1);
		pix_grid(0, 3) = ur ? img(y0, x3) : pix_grid(1, 2);
		pix_grid(3, 3) = lr ? img(y3, x3) : pix_grid(2, 2);
		pix_grid(3, 0) = ll ? img(y3, x0) : pix_grid(2, 1);

		return true;
	}
	void getBiCubicCoefficients(Matrix4d &bicubic_coeff, const Matrix4d &pix_grid){
		bicubic_coeff(0, 0) = pix_grid(1, 1);
		bicubic_coeff(0, 1) = -.5*pix_grid(1, 0) + .5*pix_grid(1, 2);
		bicubic_coeff(0, 2) = pix_grid(1, 0) - 2.5*pix_grid(1, 1) + 2 * pix_grid(1, 2) - .5*pix_grid(1, 3);
		bicubic_coeff(0, 3) = -.5*pix_grid(1, 0) + 1.5*pix_grid(1, 1) - 1.5*pix_grid(1, 2) + .5*pix_grid(1, 3);
		bicubic_coeff(1, 0) = -.5*pix_grid(0, 1) + .5*pix_grid(2, 1);
		bicubic_coeff(1, 1) = .25*pix_grid(0, 0) - .25*pix_grid(0, 2) - .25*pix_grid(2, 0) + .25*pix_grid(2, 2);
		bicubic_coeff(1, 2) = -.5*pix_grid(0, 0) + 1.25*pix_grid(0, 1) - pix_grid(0, 2) + .25*pix_grid(0, 3) + .5*pix_grid(2, 0) - 1.25*pix_grid(2, 1) + pix_grid(2, 2) - .25*pix_grid(2, 3);
		bicubic_coeff(1, 3) = .25*pix_grid(0, 0) - .75*pix_grid(0, 1) + .75*pix_grid(0, 2) - .25*pix_grid(0, 3) - .25*pix_grid(2, 0) + .75*pix_grid(2, 1) - .75*pix_grid(2, 2) + .25*pix_grid(2, 3);
		bicubic_coeff(2, 0) = pix_grid(0, 1) - 2.5*pix_grid(1, 1) + 2 * pix_grid(2, 1) - .5*pix_grid(3, 1);
		bicubic_coeff(2, 1) = -.5*pix_grid(0, 0) + .5*pix_grid(0, 2) + 1.25*pix_grid(1, 0) - 1.25*pix_grid(1, 2) - pix_grid(2, 0) + pix_grid(2, 2) + .25*pix_grid(3, 0) - .25*pix_grid(3, 2);
		bicubic_coeff(2, 2) = pix_grid(0, 0) - 2.5*pix_grid(0, 1) + 2 * pix_grid(0, 2) - .5*pix_grid(0, 3) - 2.5*pix_grid(1, 0) + 6.25*pix_grid(1, 1) - 5 * pix_grid(1, 2) + 1.25*pix_grid(1, 3) + 2 * pix_grid(2, 0) - 5 * pix_grid(2, 1) + 4 * pix_grid(2, 2) - pix_grid(2, 3) - .5*pix_grid(3, 0) + 1.25*pix_grid(3, 1) - pix_grid(3, 2) + .25*pix_grid(3, 3);
		bicubic_coeff(2, 3) = -.5*pix_grid(0, 0) + 1.5*pix_grid(0, 1) - 1.5*pix_grid(0, 2) + .5*pix_grid(0, 3) + 1.25*pix_grid(1, 0) - 3.75*pix_grid(1, 1) + 3.75*pix_grid(1, 2) - 1.25*pix_grid(1, 3) - pix_grid(2, 0) + 3 * pix_grid(2, 1) - 3 * pix_grid(2, 2) + pix_grid(2, 3) + .25*pix_grid(3, 0) - .75*pix_grid(3, 1) + .75*pix_grid(3, 2) - .25*pix_grid(3, 3);
		bicubic_coeff(3, 0) = -.5*pix_grid(0, 1) + 1.5*pix_grid(1, 1) - 1.5*pix_grid(2, 1) + .5*pix_grid(3, 1);
		bicubic_coeff(3, 1) = .25*pix_grid(0, 0) - .25*pix_grid(0, 2) - .75*pix_grid(1, 0) + .75*pix_grid(1, 2) + .75*pix_grid(2, 0) - .75*pix_grid(2, 2) - .25*pix_grid(3, 0) + .25*pix_grid(3, 2);
		bicubic_coeff(3, 2) = -.5*pix_grid(0, 0) + 1.25*pix_grid(0, 1) - pix_grid(0, 2) + .25*pix_grid(0, 3) + 1.5*pix_grid(1, 0) - 3.75*pix_grid(1, 1) + 3 * pix_grid(1, 2) - .75*pix_grid(1, 3) - 1.5*pix_grid(2, 0) + 3.75*pix_grid(2, 1) - 3 * pix_grid(2, 2) + .75*pix_grid(2, 3) + .5*pix_grid(3, 0) - 1.25*pix_grid(3, 1) + pix_grid(3, 2) - .25*pix_grid(3, 3);
		bicubic_coeff(3, 3) = .25*pix_grid(0, 0) - .75*pix_grid(0, 1) + .75*pix_grid(0, 2) - .25*pix_grid(0, 3) - .75*pix_grid(1, 0) + 2.25*pix_grid(1, 1) - 2.25*pix_grid(1, 2) + .75*pix_grid(1, 3) + .75*pix_grid(2, 0) - 2.25*pix_grid(2, 1) + 2.25*pix_grid(2, 2) - .75*pix_grid(2, 3) - .25*pix_grid(3, 0) + .75*pix_grid(3, 1) - .75*pix_grid(3, 2) + .25*pix_grid(3, 3);
	}
	//evaluates the cubic polynomial surface defines by the given parameters at the given location
	double biCubic(const Matrix4d &bicubic_coeff, double x, double y) {
		double x2 = x * x;
		double x3 = x2 * x;
		double y2 = y * y;
		double y3 = y2 * y;
		return (bicubic_coeff(0, 0) + bicubic_coeff(0, 1) * y + bicubic_coeff(0, 2) * y2 + bicubic_coeff(0, 3) * y3) +
			(bicubic_coeff(1, 0) + bicubic_coeff(1, 1) * y + bicubic_coeff(1, 2) * y2 + bicubic_coeff(1, 3) * y3) * x +
			(bicubic_coeff(2, 0) + bicubic_coeff(2, 1) * y + bicubic_coeff(2, 2)* y2 + bicubic_coeff(2, 3) * y3) * x2 +
			(bicubic_coeff(3, 0) + bicubic_coeff(3, 1) * y + bicubic_coeff(3, 2) * y2 + bicubic_coeff(3, 3) * y3) * x3;
	}
	//evaluates the gradient of cubic polynomial surface defines by the given parameters at the given location
	double biCubicGradX(Vector2d &grad, const Matrix4d &bicubic_coeff, double x, double y) {
		double x2 = x * x;
		double y2 = y * y;
		double y3 = y2 * y;
		return (bicubic_coeff(1, 0) + bicubic_coeff(1, 1) * y + bicubic_coeff(1, 2) * y2 + bicubic_coeff(1, 3) * y3) +
			(bicubic_coeff(2, 0) + bicubic_coeff(2, 1) * y + bicubic_coeff(2, 2)* y2 + bicubic_coeff(2, 3) * y3) * 2 * x +
			(bicubic_coeff(3, 0) + bicubic_coeff(3, 1) * y + bicubic_coeff(3, 2) * y2 + bicubic_coeff(3, 3) * y3) * 3 * x2;
	}
	double biCubicGradY(Vector2d &grad, const Matrix4d &bicubic_coeff, double x, double y) {
		double x2 = x * x;
		double x3 = x2 * x;
		double y2 = y * y;
		return (bicubic_coeff(0, 1) + bicubic_coeff(1, 1) * x + bicubic_coeff(2, 1) * x2 + bicubic_coeff(3, 1) * x3) +
			(bicubic_coeff(0, 2) + bicubic_coeff(1, 2) * x + bicubic_coeff(2, 2)* x2 + bicubic_coeff(3, 2) * x3) * 2 * y +
			(bicubic_coeff(0, 3) + bicubic_coeff(1, 3) * x + bicubic_coeff(2, 3) * x2 + bicubic_coeff(3, 3) * x3) * 3 * y2;
	}
	//evaluates the hessian of the cubic polynomial surface defines by the given parameters at the given location
	double biCubicHessXX(const Matrix4d &bicubic_coeff, double x, double y) {
		double y2 = y * y;
		double y3 = y2 * y;
		return 2 * (bicubic_coeff(2, 0) + bicubic_coeff(2, 1) * y + bicubic_coeff(2, 2)* y2 + bicubic_coeff(2, 3) * y3) +
			6 * x*(bicubic_coeff(3, 0) + bicubic_coeff(3, 1) * y + bicubic_coeff(3, 2) * y2 + bicubic_coeff(3, 3) * y3);
	}
	double biCubicHessYY(const Matrix4d &bicubic_coeff, double x, double y) {
		double x2 = x * x;
		double x3 = x2 * x;
		return 2 * (bicubic_coeff(0, 2) + bicubic_coeff(1, 2) * x + bicubic_coeff(2, 2)* x2 + bicubic_coeff(3, 2) * x3) +
			6 * y*(bicubic_coeff(0, 3) + bicubic_coeff(1, 3) * x + bicubic_coeff(2, 3) * x2 + bicubic_coeff(3, 3) * x3);
	}
	double biCubicHessYX(const Matrix4d &bicubic_coeff, double x, double y) {
		double x2 = x * x;
		double y2 = y * y;
		double y2d = 2 * y, y3d = 3 * y2;
		return (bicubic_coeff(1, 1) + bicubic_coeff(1, 2) * y2d + bicubic_coeff(1, 3) * y3d) +
			(bicubic_coeff(2, 1) + bicubic_coeff(2, 2)* y2d + bicubic_coeff(2, 3) * y3d) * 2 * x +
			(bicubic_coeff(3, 1) + bicubic_coeff(3, 2) * y2d + bicubic_coeff(3, 3) * y3d) * 3 * x2;
	}
	double biCubicHessXY(const Matrix4d &bicubic_coeff, double x, double y) {
		double y2 = y * y;
		double y3 = y * y2;
		double x2 = x * x;
		double x2d = 2 * x, x3d = 3 * x2;
		return (bicubic_coeff(1, 1) + bicubic_coeff(2, 1) * x2d + bicubic_coeff(3, 1) * x3d) +
			(bicubic_coeff(1, 2) + bicubic_coeff(2, 2)* x2d + bicubic_coeff(3, 2) * x3d) * 2 * y +
			(bicubic_coeff(1, 3) + bicubic_coeff(2, 3) * x2d + bicubic_coeff(3, 3) * x3d) * 3 * y2;
	}

	/******************** Explicit Template Specializations ******************/

	template
	void getPixVals<PtsT>(VectorXd &pix_vals,
		const EigImgT &img, const PtsT &pts, int n_pix, int h, int w,
		double norm_mult, double norm_add);
	template
		void getWarpedImgGrad<Nearest>(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const HomPtsT &warped_pts, const ProjWarpT &warp,
		double grad_eps, int n_pix, int h, int w, double pix_mult_factor);
	template
		void getWarpedImgGrad<Linear>(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const HomPtsT &warped_pts, const ProjWarpT &warp,
		double grad_eps, int n_pix, int h, int w, double pix_mult_factor);

	template
		void getWarpedImgGrad<Nearest>(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const Matrix8Xd &warped_offset_pts,
		double grad_eps, int n_pix, int h, int w, double pix_mult_factor);
	template
		void getWarpedImgGrad<Linear>(PixGradT &warped_img_grad,
		const EigImgT &img, const VectorXd &intensity_map,
		const Matrix8Xd &warped_offset_pts,
		double grad_eps, int n_pix, int h, int w, double pix_mult_factor);


	template
		void getImgGrad<Nearest>(PixGradT &img_grad, const EigImgT &img,
		const VectorXd &intensity_map, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w,
		double pix_mult_factor);
	template
		void getImgGrad<Linear>(PixGradT &img_grad, const EigImgT &img,
		const VectorXd &intensity_map, const PtsT &pts,
		double grad_eps, int n_pix, int h, int w, double pix_mult_factor);

	template
	void getWarpedImgHess<Nearest>(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, const HomPtsT &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor);
	template
	void getWarpedImgHess<Linear>(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, const HomPtsT &warped_pts_hm,
		const ProjWarpT &warp, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor);

	template
		void getWarpedImgHess<Nearest>(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, const PtsT &warped_pts, 
		const HessPtsT &warped_offset_pts, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor);
	template
		void getWarpedImgHess<Linear>(PixHessT &warped_img_hess,
		const EigImgT &img, const VectorXd &intensity_map, const PtsT &warped_pts, 
		const HessPtsT &warped_offset_pts, double hess_eps, int n_pix,
		int h, int w, double pix_mult_factor);

	template
	void getImgHess<Nearest>(PixHessT &img_hess, const EigImgT &img, const VectorXd &intensity_map,
		const PtsT &pts, double hess_eps, int n_pix, int h, int w,
		double pix_mult_factor);
	template
	void getImgHess<Linear>(PixHessT &img_hess, const EigImgT &img, const VectorXd &intensity_map,
		const PtsT &pts, double hess_eps, int n_pix, int h, int w,
		double pix_mult_factor);

}

_MTF_END_NAMESPACE
