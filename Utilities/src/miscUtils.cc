#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	void cornersToPoint2D(cv::Point2d(&cv_corners)[4],
		const cv::Mat &cv_corners_mat) {
		for(int i = 0; i < 4; i++) {
			cv_corners[i].x = cv_corners_mat.at<double>(0, i);
			cv_corners[i].y = cv_corners_mat.at<double>(1, i);
		}
	}

	void cornersToMat(cv::Mat &cv_corners_mat ,
		const cv::Point2d (&cv_corners)[4]) {
		for(int i = 0; i < 4; i++) {
			cv_corners_mat.at<double>(0, i) = cv_corners[i].x;
			cv_corners_mat.at<double>(1, i) = cv_corners[i].y;
		}
	}
	// mask a vector, i.e. retain only those entries where the given mask is true
	void maskVector(VectorXd &masked_vec, const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		masked_vec.resize(masked_size);
		int mask_id = 0;
		for(int i = 0; i < in_size; i++){
			if(!mask(i)){ masked_vec(mask_id++) = in_vec(i); }
		}
	}
	// returning version
	VectorXd maskVector(const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		VectorXd masked_vec(masked_size);
		maskVector(masked_vec, in_vec, mask, masked_size, in_size);
		return masked_vec;
	}	
	// specialization with loop unrolling for copying a warp matrix from OpenCV to Eigen
	template<>
	void copyCVToEigen<double, Matrix3d>(Matrix3d &eig_mat, const cv::Mat &cv_mat){
		assert(eig_mat.rows() == cv_mat.rows && eig_mat.cols() == cv_mat.cols);
		eig_mat(0, 0) = cv_mat.at<double>(0, 0);
		eig_mat(0, 1) = cv_mat.at<double>(0, 1);
		eig_mat(0, 2) = cv_mat.at<double>(0, 2);

		eig_mat(1, 0) = cv_mat.at<double>(1, 0);
		eig_mat(1, 1) = cv_mat.at<double>(1, 1);
		eig_mat(1, 2) = cv_mat.at<double>(1, 2);

		eig_mat(2, 0) = cv_mat.at<double>(2, 0);
		eig_mat(2, 1) = cv_mat.at<double>(2, 1);
		eig_mat(2, 2) = cv_mat.at<double>(2, 2);
	}	
	// specialization for copying corners
	template<>
	void copyEigenToCV<double, CornersT>(cv::Mat &cv_mat, 
		const CornersT &eig_mat){
		assert(cv_mat.rows == 2 && cv_mat.cols == 4);
		cv_mat.at<double>(0, 0) = eig_mat(0, 0);
		cv_mat.at<double>(1, 0) = eig_mat(1, 0);
		cv_mat.at<double>(0, 1) = eig_mat(0, 1);
		cv_mat.at<double>(1, 1) = eig_mat(1, 1);
		cv_mat.at<double>(0, 2) = eig_mat(0, 2);
		cv_mat.at<double>(1, 2) = eig_mat(1, 2);
		cv_mat.at<double>(0, 3) = eig_mat(0, 3);
		cv_mat.at<double>(1, 3) = eig_mat(1, 3);
	}
	int writeTimesToFile(vector<double> &proc_times, 
		vector<char*> &proc_labels, char *time_fname, int iter_id){
		MatrixXd iter_times(proc_times.size(), 2);
		for(int proc_id = 0; proc_id<proc_times.size(); proc_id++){
			iter_times(proc_id, 0) = proc_times[proc_id];
		}
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time) * 100;
		utils::printScalarToFile(iter_id, "iteration", time_fname, "%6d", "a");
		//char **row_label_ptr = &proc_labels[0];
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", &proc_labels[0]);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		return total_iter_time;
	}
}
_MTF_END_NAMESPACE
