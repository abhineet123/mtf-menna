Input parameters can be specified in 4 text files called **mtf.cfg**, **modules.cfg**, **multi.cfg** and **nn.cfg** (present in this folder) where each line specifies the value of one parameter as: `<param_name><tab><param_val>`. If these files are present in some other folder, its path can be specified at runtime as `./runMTF config_dir <directory containing the cfg files>`

Note that all parameters described here or present in **mtf.cfg** or **modules.cfg** can be specified in either of these files since both are read sequentially;

this split is done only for convenience with the former containing parameters common to MTF in general and the latter having module specific parameters;

**multi.cfg** specifies configurations for individual trackers in multi tracker setups like CascadeTracker/ParallelTracker for single object tracking or when tracking multiple objects simultaneously.
The parameters that can be specified here are same as in the last two files and will override the values specified there for each specific tracker thus enabling different trackers to have independent settings.
The settings for two trackers should be separated by an empty line.  Also note that the changes made by specifying parameters in **multi.cfg** are global, i.e. if the value specified for a specific parameter for one tracker will be used for all subsequent trackers too unless overridden again.

**nn.cfg** specifies parameters specific to different types of indices used by the NN tracker.

Following are some of important parameters, their brief descriptions and possible values:

Input/Output related parameters:
================================
	 Parameter:	'pipeline'
		Description:
			input video pipeline
		Possible Values:
			c: OpenCV
		If Xvision is enabled during compilation:
			x: Xvision
			
	 Parameter:	'img_source'
		Description:
			input video source/stream
		Possible Values:
			m: MPEG video file(OpenCV can read AVI files too)
			j: JPEG image files (some other common formats line PNG and BMP are supported too)
			u: USB camera
			f: Firewire camera (only Xvision pipeline; the USB camera option(u) can be used to access Firewire cameras with OpenCV as long as no USB cameras are attached)
			
	 Parameter:	'actor_id'
		Description:
			integral index of the dataset (or "actor") to use out of the possible datasets hard coded in datasets.h;
			used in conjunction with the parameter 'source_id'  to get the source name;
			only matters if both are non negative
		Possible Values:
				0:	TMT
				1:	UCSB
				2:	LinTrack
				3:	PAMI
				4:	METAIO
				5:	CMT
				6:	VOT
				7:	VTB
				8:	VIVID
				9:	MALIS
				10: TrakMark
				11: Synthetic
				12: Live
			Note: the first 8 datasets in a ready to use form can be downloaded from the MTF website: http://webdocs.cs.ualberta.ca/~vis/mtf/
			
	 Parameter:	'root_path'
		Description:
			location of the root directory that contains the files for all datasets (or 'actors');
			for JPEG file the full path is constructed as: root_path/actor/source_name/*.source_fmt;
			for MPEG/Video file the full path is constructed as: root_path/actor/source_name.source_fmt;
				
	 Parameter:	'source_id'
		Description:
			integral index of the source name to use out of the sources hard coded in parameters.h;
			used in conjunction with the parameter 'actor_id' to get the source name;
			only matters if both are non negative
		Possible Values:
			refer parameters.h for details of what each index means for each actor type; 
			following are the valid (inclusive) ranges for different actors:
				TMT:	0-108
				UCSB:	0-95
				LinTrack:	0-2
				PAMI:	0-24
				METAIO:	0-39
				CMT:	0-19				
				VOT:	0-99	
				VTB:	0-24	
				VIVID:	0-8
				Synthetic:	0-2
				Live:	0-1
				
	 Parameter:	'source_name'
		Description:
			name of the input video file (for MPEG source) or folder (for JPEG source); 
				overridden if both the parameters 'source_id' and 'actor_id' are non-negative;
				does not matter if a camera stream is being used
		Possible Values:
			#: use default values hard coded in inputBase.h 
			
	 Parameter:	'source_path'
		Description:
			only matters for Xvision pipeline and camera streams; 
			specifies the path of the camera device to use
		Possible Values:
			depend on the number and types of camera attached to the system;
			following are common values for the two camera types:
				/dev/video0:	USB
				/dev/fw1:	Firewire
			special value #:	use default values hard coded in inputBase.h
			
	 Parameter:	'source_fmt'
		Description:
			file extension for image and video file streams;
			any special formatting strings to be passed to the Xvision initializer for camera streams;
		Possible Values:
			jpg:	JPEG image files
			mpg:	MPEG video file
			avi:	AVI	video file (only OpenCV pipeline)
			#:	use default values hard coded in inputBase.h
			
	 Parameter:	'init_frame_id'
		Description:
			id of the frame at which to initialize the tracker in case tracking is desired to be started in the middle of the sequence rather than the beginning;
		Possible Values:
			should be between 0 and no_of_frames-1			
			
	 Parameter:	'read_objs'
		Description:
			read initial location of the object to be tracked from the text file specified by 'read_obj_fname' where they were previously written to by enabling 'write_objs';
			this is meant to avoid having to specify a custom initialization locations for one or more trackers repeatedly
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'read_obj_fname'
		Description:
			name of the text file where the initial location of the object to be tracked will be read from;
			only matters if read_objs is 1			
			
	 Parameter:	'write_objs'
		Description:
			write the manually selected initial object location to the text file specified by 'write_obj_fname';
			only matters if manual selection is enabled by disabling both 'read_objs' and 'read_obj_from_gt';
			this is meant to avoid having to specify a custom initialization locations for one or more trackers repeatedly
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'write_obj_fname'
		Description:
			name of the text file where the initial location of the object to be tracked will be written to;
			only matters if manual selection is enabled by disabling both 'read_objs' and 'read_obj_from_gt' and enabling 'write_objs'
			
	 Parameter:	'read_obj_from_gt'
		Description:
			read initial object location from a ground truth file present in the same directory as the input source file;
			matters only if a file stream is being used; 
			the format of this file should be identical to the ground truth files for the TMT dataset available here:
				http://webdocs.cs.ualberta.ca/~vis/trackDB/firstpage.html
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'show_cv_window'
		Description:
			show the result of tracking from frame to frame in an OpenCV window;
			disabling it can speed up the overall tracking speed by eliminating the delay caused by drawing the object locations on the current frame;
			useful for benchmarking
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'show_tracking_error'
		Description:
			show the the tracking error in terms of the mean corner distance between the tracking result and the ground truth in the OpenCV window; 
			only matters if read_objs_from_gt is enabled and a file input source (video or image) is used; 
			a valid text file containing the show_tracking_error for all the frames in the source should also be present;
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'record_frames'
		Description:
			record the tracked frames into a video file called Tracked_video.avi; 
			enabling this may significantly decrease the overall tracking speed
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'pause_after_frame'
		Description:
			pause tracking after each frame; 
			pressing space bar will resume tracking; 
			pressing any other key (except Esc) will move to next frame (Esc will exit the program);
		Possible Values:
			0: Disable
			1: Enable		
			
	 Parameter:	'reinit_from_gt'
		Description:
			reinitialize tracker when it fails, i.e. when its MCD error goes above err_thresh; 
			only works when a dataset sequence is used and full ground truth is available

MTF Tracker specific parameters:
================================
	 Parameter:	'mtf_sm'
		Description:
			Search method to use for the MTF tracker or the name of the detection based tracker
		Possible Values:
			esm:	Efficient Second-order Minimization
			aesm:	Additive formulation of ESM 
			ic/iclk:	Inverse Compositional Lucas Kanade
			fc/fclk:	Forwards Compositional Lucas Kanade
			fa/falk:	Forwards Additive Lucas Kanade
			ia/ialk:	Inverse Additive Lucas Kanade
			pf:	Particle filter tracker
			gnn:	Graph based NN tracker
			hrch:	Hierarchical SSM tracker - uses same SM ('hrch_sm') and AM with four different SSMs - 2, 4, 6 and 8 dof that are run in a cascade
			casc:	general cascade tracker whose configuration is read from multi.cfg
			grid:	Grid Tracker
			rkl/rklt:	RKLT (Grid tracker + template tracker with SPI and failure detection)
			prl/prlt:	Parallel tracker whose configuration is read from multi.cfg
			if NN is not disabled during compilation:
				nn:	Nearest Neighbour tracker
				nnic:	cascade tracker with NN+ICLK
				nnfc:	cascade tracker with NN+FCLK
				nnes:	cascade tracker with NN+NESM
				nn3ic:	3 layer NN with IC
			if learning based trackers are not disabled during compilation:
				dsst:	Discriminative Scale Space Tracker 
				kfc:	Kernelized Correlation Filter Tracker 
				cmt:	Consensus-based Tracker 
				tld:	Tracking-Learning-Detection Tracker
				rct:	Realtime Compressive Tracker
				strk:	Struck: structured output tracking with kernels
			if ViSP template tracker module is enabled during compilation:
				visp:	ViSP template tracker		
			If Xvision is enabled during compilation:			
				xv1 / xv1p:	XVSSD Rotate / Pyramidal version
				xv2 / xv1p:	XVSSD Translation / Pyramidal version
				xv3 / xv1p:	XVSSD RT / Pyramidal version
				xv4 / xv1p:	XVSSD SE2 / Pyramidal version
				xv6 / xv1p:	XVSSD Affine / Pyramidal version
				xvc:	XVColor tracker
				xve:	XVEdge tracker
				xvg:	XV Grid tracker			
				xvgl:	XV Grid Line tracker			
			Note: tracker specific parameters including those for the learning based trackers are specified in modules.cfg		
			
	 Parameter:	'mtf_am'
		Description:
			Appearance model to use for the MTF tracker
		Possible Values:
			ssd:	Sum of Squared Differences
			zncc:	Zero mean Normalized Cross-Correlation
			nssd:	Normalized SSD
			ncc:	Normalized Cross-Correlation
			scv:	Sum of Conditional Variance
			rscv:	Reversed Sum of Conditional Variance
			lscv:	Localized SCV
			lrscv:	Localized RSCV
			mi:	Mutual Information
			ccre:	Cross Cumulative Residual Entropy
			ssim:	Structural Similarity
			spss:	Sum of Pixel wise Structural Similarity
			kld:	Kullback–Leibler Divergence (does not work well)
			lkld:	Localized KLD (incomplete/does not work well yet)
			
	 Parameter:	'mtf_ssm'
		Description:
			State space model to use for the MTF tracker
		Possible Values:
			lhom or l8:	Lie Homography (8 dof)
			hom or 8:	Homography (8 dof)
			chom or c8:	Corner based Homography (8 dof)
			aff or 6:	Affine (6 dof)	
			sim or 4:	Similarity (translation + rotation + isotropic scaling)(4 dof)
			iso or 3:	Isometry (translation + rotation)(3 dof)
			trs or 3s:	Transcaling (Translation + isotropic scaling)
			trans or 2:	Translation (2 dof)		
			
	 Parameter:	'resx' / 'resy'
		Description:
			horizontal and vertical sampling resolutions for extracting pixel values from the object patch;
			object pixel values are sampled from a rectangular grid of size resx x resy;
			higher values usually lead to better tracking performance but also slow it down; 
			these options only matter if mtf_res<=0	
			
	 Parameter:	'mtf_res':
		Description:
			single value for both vertical and horizontal sampling resolutions, if this is >=0, then resx=resy=mtf_res
			
	 Parameter:	'res_from_size'
		Description:
			set the horizontal and vertical sampling resolutions equal to the actual size of the object selected for tracking

For NSSD AM:
============
	 Parameter:	'norm_pix_min' / 'norm_pix_max'
		Description:
			minimum and maximum values within which to normalize the pixel values

For SCV/RSCV/LSCV/LRSCV AMs:
=========================== 
	 Parameter:	'scv_use_bspl'
		Description:
			use BSpline kernel of order 3 while computing the joint histogram that is used for computing the sum of conditional variance; the Dirac Delta function is used otherwise;
			
	 Parameter:	'scv_n_bins'
		Description:
			number of bins in the joint histogram
			
	 Parameter:	'scv_preseed'
		Description:
			value with which to preseed the histograms; this can be set to non zero values while using BSpline histograms to avoid some numerical issues associated with empty bins that can sometimes occur;
			
	 Parameter:	'scv_pou'
		Description:
			strictly enforce the partition of unity constraint for border bins while computing the BSpline joint histogram
			
For MI AM:
==========
	 Parameter:	'mi_n_bins' / 'mi_preseed' / 'mi_pou'
		Description:
			only for MI appearance model; 
			meaning is same as the corresponding parameters for SCV		

For GridTracker and RKLT:
========================= 
	 Parameter:	'gt_sm' / 'gt_am' / 'gt_ssm'
		Description:
			Search method, appearance model and state space model for the individual patch trackers used by the Grid Tracker
			
	 Parameter:	'gt_grid_res'
		Description:
			resolution of the grid into which the object is divided so that no. of patch trackers = gt_grid_res*gt_grid_res
			
	 Parameter:	'gt_patch_size'
		Description:
			sampling resolution for each patch tracker	
			
	 Parameter:	'gt_dyn_patch_size'
		Description:
			set to 1 to dynamically adjust the patch sizes based on the size of the overall object bounding box by dividing it evenly; 
			the individual patches in this case are no longer rectangular;
			
	 Parameter:	'gt_estimation_method'
		Description:
			method used to estimate the best fit SSM parameters between the two sets of points representing the centroids of the locations of the patch trackers in two consecutive frames
		Possible Values:
			0: RANSAC
			1: Least Median
			2: LeastSquares
			
	 Parameter:	'gt_ransac_reproj_thresh'
		Description:
			reprojection error threshold for a point to be considered an outlier by the OpenCV RANSAC  algorithm; 
			only matters if this method is selected for gt_estimation_method;
		
	 Parameter:	'gt_init_at_each_frame'
		Description:
			set to 1 to reinitialize the patch trackers at each frame based on the estimated location of the larger bounding box
			
	 Parameter:	'gt_show_trackers'
		Description:
			set to 1 to show the locations of all the patch trackers within the larger object patch	where each is marked by the location of its centroid
			
	 Parameter:	'gt_show_tracker_edges'
		Description:
			set to 1 to also show the edges of the bounding box representing each patch tracker (in addition to its centroid)
			
	 Parameter:	'gt_use_tbb'
		Description:
			set to 1 to enable parallelization 	of the patch trackers using Intel TBB library
			
For RKLT:
=========
	 Parameter:	'rkl_sm'
		Description:
			SM for the template tracker used by RKLT; the corresponding AM and SSM are specified by 'mtf_am' and 'mtf_ssm' respectively;
			
	 Parameter:	'rkl_enable_spi'
		Description:
			enable selective pixel integration where the template tracker is updated using only those pixels that are deemed inliers by the robust estimation method;
			this only works if both the AM and SSM of the template tracker support SPI;	
			if enabled, the sampling resolution of the template tracker is set equal to the grid size (as specified by 'gt_grid_res');
			
	 Parameter:	'rkl_enable_feedback'
		Description:
			reset the grid tracker to the location of the template tracker at each frame;
			
	 Parameter:	'rkl_failure_detection'
		Description:
			set to 1 to check if the template tracker has failed and ignore its output if so; 
			this check is done by comparing the L2 norm of the difference in corners provided by the template tracker and the grid tracker with the failure threshold (specified by 'rkl_failure_thresh') 			
	 
	 Parameter:	'rkl_failure_thresh'
		Description:
			threshold to decide if the template tracker has failed; only matters if 'rkl_failure_detection' is enabled
 
The parameters can also be specified from the command line through a list of argument pairs as follows:

	./runMTFs <arg_name_1> <arg_val_1> <arg_name_2> <arg_val2> .... <arg_name_n> <arg_val_n>
	
where the valid values of `arg_name` and `arg_val` are same as in the cfg files; 
these arguments will override the values specified in those files in case both are provided;
any invalid values for `arg_name` will be ignored along with its `arg_val`;
			