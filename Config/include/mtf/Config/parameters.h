#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#define CASCADE_MAX_TRACKERS 10

#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "opencv2/core/core.hpp"

namespace mtf{
	namespace params{
		// folder where the config files mtf.cfg, modules.cfg and multi.cfg are located
		std::string config_dir = "Config";

		/* default parameters */
		int source_id = 0;
		int actor_id = 0;
		int n_trackers = 1;
		char pipeline = 'c';
		char img_source = 'j';
		int buffer_count = 10;
		int buffer_id = 0;

		// flags//
		int show_xv_window = 0;
		int show_cv_window = 1;
		int show_ground_truth = 0;
		int read_objs = 0;
		int read_obj_from_gt = 1;
		bool sel_quad_obj = false;
		int line_thickness = 2;
		int record_frames = 0;
		int write_frame_data = 0;
		int write_tracking_data = 0;
		int write_pts = 1;
		int show_warped_img = 0;
		int show_template_img = 0;
		int write_tracker_states = 0;
		int write_objs = 0;
		int reinit_from_gt = false;
		double reinit_err_thresh = 5.0;
		int reinit_frame_skip = 5;
		bool use_opt_gt = false;
		std::string opt_gt_ssm = "2";

		char* read_obj_fname = "sel_objs/selected_objects.txt";
		char* write_obj_fname = "sel_objs/selected_objects.txt";
		char* tracking_data_fname = nullptr;

		char *source_name = nullptr;
		char *source_fmt = nullptr;
		char *source_path = nullptr;
		char *root_path = "../../../Datasets";
		char *actor = nullptr;

		// for Xvision trackers
		int steps_per_frame = 1;
		int patch_size = 0;

		//char frame_dir[200];
		//char pts_dir[200];
		//char states_dir[200];

		/* only for pyramidal trackers */
		int no_of_levels = 2;
		double scale = 0.5;
		/* only for color tracker; enabling it causes tracker to hang and crash */
		bool color_resample = false;

		int search_width = 50;
		int search_angle = 0;
		int line_width = 4;

		/* for xvision grid tracker */
		int grid_size_x = 4;
		int grid_size_y = -1;
		int xv_tracker_type = 't';
		int reset_pos = 0;
		// also used by runMTF to reset the template
		int reset_template = 0;
		int reset_wts = 0;
		double sel_reset_thresh = 1.0;

		int pause_after_line = 0;
		int pause_after_frame = 0;
		int show_tracked_pts = 0;
		int debug_mode = 0;
		int use_constant_slope = 0;
		int use_ls = 0;
		int update_wts = 0;
		double inter_alpha_thresh = 0.10;
		double intra_alpha_thresh = 0.05;

		// flags//
		int adjust_grid = 0;
		int adjust_lines = 1;

		// for MTF
		unsigned int mtf_res = 50;
		unsigned int resx = 50;
		unsigned int resy = 50;
		int init_frame_id = 0;
		int end_frame_id = 0;
		int max_iters = 10;
		double epsilon = 0.01;
		char* mtf_sm = "esm";
		char* mtf_am = "ssd";
		char* mtf_ssm = "8";

		bool ic_update_ssm = true;
		bool ic_chained_warp = true;
		bool fc_chained_warp = false;
		double norm_pix_min = 0.0;
		double norm_pix_max = 1.0;
		int res_from_size = 0;
		int show_tracking_error = 0;
		int hess_type = 0;
		bool sec_ord_hess = false;
		int jac_type = 0;
		char *pix_mapper = nullptr;

		int hom_normalized_init = 0;
		int update_templ = 0;
		double grad_eps = 1e-8;
		double hess_eps = 1.0;

		// Homograhy
		bool hom_corner_based_sampling = true;

		// SCV
		bool scv_use_bspl = 0;
		int scv_n_bins = 256;
		double scv_preseed = 0;
		bool scv_pou = 1;
		bool scv_weighted_mapping = 1;
		bool scv_mapped_gradient = 1;
		bool scv_affine_mapping = 0;
		bool scv_once_per_frame = 0;

		// LSCV
		int lscv_sub_regions = 3;
		int lscv_spacing = 10;
		bool lscv_show_subregions = false;

		// LKLD
		int lkld_n_bins = 8;
		double lkld_pre_seed = 0.1;
		bool lkld_pou = 1;
		int lkld_sub_regions = 0;
		int lkld_spacing = 1;

		//NCC
		bool ncc_fast_hess = false;

		//SSIM
		int ssim_pix_proc_type = 0;
		double ssim_k1 = 0.01;
		double ssim_k2 = 0.03;

		// MI & CCRE
		int mi_n_bins = 8;
		double mi_pre_seed = 10;
		bool mi_pou = false;



		// CCRE
		int ccre_n_bins = 8;
		double ccre_pre_seed = 10;
		bool ccre_pou = false;
		bool ccre_symmetrical_grad = false;
		int ccre_n_blocks = 0;

		// NN and GNN
		int nn_max_iters = 10;
		int nn_n_samples = 1000;
		std::vector<double> nn_ssm_sigma;
		double nn_corner_sigma_d = 0.04;
		double nn_corner_sigma_t = 0.06;
		double nn_pix_sigma = 0;
		int nn_n_trees = 6;
		int nn_n_checks = 50;
		double nn_ssm_sigma_prec = 1.1;
		int nn_index_type = 1;
		std::string nn_index_config_file = "Config/nn.cfg";
		bool nn_additive_update = false;
		bool nn_direct_samples = false;

		// GNN 
		int gnn_k = 250;

		//Gradient Descent
		double gd_learning_rate = 0.1;

		// Particle Filter
		int pf_max_iters = 1;
		int pf_n_particles = 100;
		int pf_dyn_model = 1;
		int pf_upd_type = 0;
		int pf_likelihood_func = 0;
		int pf_resampling_type = 0;
		bool pf_reset_to_mean = false;
		bool pf_mean_of_corners = false;
		std::vector<double> pf_ssm_sigma;
		double pf_measurement_sigma = 0.1;
		double pf_pix_sigma = 0;

		// Hierarchical SSM tracker
		char* hrch_sm = "iclk";
		char* hrch_am = "ssd";

		// Cascade tracker
		int casc_n_trackers = 2;
		bool casc_enable_feedback = 1;

		// Grid tracker
		char* gt_sm = "iclk";
		char* gt_am = "ssd";
		char* gt_ssm = "2";
		int gt_grid_res = 10;
		int gt_patch_size = 10;
		int gt_patch_size_y = 10;
		int gt_estimation_method = 0;
		double gt_ransac_reproj_thresh = 10;
		bool gt_init_at_each_frame = true;
		bool gt_dyn_patch_size = false;
		bool gt_show_trackers = false;
		bool gt_show_tracker_edges = false;
		bool gt_use_tbb = true;
		// OpenCV grid tracker
		int gt_pyramid_levels = 2;
		bool gt_use_min_eig_vals = 0;
		double gt_min_eig_thresh = 1e-4;

		//RKL Tracker
		char* rkl_sm = "iclk";
		bool rkl_enable_spi = true;
		bool rkl_enable_feedback = true;
		bool rkl_failure_detection = true;
		double rkl_failure_thresh = 15.0;

		//Parallel Tracker
		int prl_n_trackers = 1;
		int prl_estimation_method = 0;
		bool prl_reset_to_mean = false;
		bool prl_auto_reinit = false;
		double prl_reinit_err_thresh = 1.0;

		// MTF Diagnostics
		char* diag_am = "ssd";
		char* diag_ssm = "2";
		int diag_frame_gap = 0;
		double diag_range = 0;
		std::vector<double> diag_ssm_range;
		char* diag_gen_norm = "00";// Norm,FeatNorm
		char* diag_gen_jac = "000";// Std,ESM,Diff
		char* diag_gen_hess = "0000";// Std,ESM,InitSelf,CurrSelf
		char* diag_gen_hess2 = "0000";// Std2,ESM2,InitSelf2,CurrSelf2
		char* diag_gen_hess_sum = "0000";// Std, Std2, Self, Self2
		char* diag_gen_num = "000"; // Jac, Hess, NHess
		char* diag_gen_ssm = "0";// ssm params

		bool diag_bin = true;
		bool diag_inv = true;
		bool diag_show_corners = true;
		bool diag_show_patches = true;
		bool diag_verbose = false;

		double diag_grad_diff = 0.1;
		int diag_res = 50;
		int diag_update = 0;

		bool diag_enable_validation = false;
		double diag_validation_prec = 1e-20;

		bool spi_enable = false;
		double spi_thresh = 10;

		// DSST 
		double dsst_padding = 1;
		double dsst_sigma = 1.0 / 16;
		double dsst_scale_sigma = 1.0 / 4;
		double dsst_lambda = 1e-2;
		double dsst_learning_rate = 0.025;
		int dsst_number_scales = 33;
		double dsst_scale_step = 1.02;
		int dsst_resize_factor = 4;
		int dsst_is_scaling = 1;
		int dsst_bin_size = 1;

		// KCF 
		double kcf_padding; //extra area surrounding the target
		double kcf_lambda; //regularization
		double kcf_output_sigma_factor; //spatial bandwidth (proportional to target)
		double kcf_interp_factor; //linear interpolation factor for adaptation
		double kcf_kernel_sigma; //gaussian kernel bandwidth
		//for scaling
		int kcf_number_scales;
		double kcf_scale_step;
		double kcf_scale_model_max_area;
		double kcf_scale_sigma_factor;
		double kcf_scale_learning_rate;
		bool kcf_enableScaling;
		int kcf_resize_factor;

		// CMT
		bool cmt_estimate_scale = true;
		bool cmt_estimate_rotation = false;
		char* cmt_feat_detector = "FAST";
		char* cmt_desc_extractor = "BRISK";
		double cmt_resize_factor = 0.5;

		// TLD
		bool tld_tracker_enabled = true;
		bool tld_detector_enabled = true;
		bool tld_learning_enabled = true;
		bool tld_alternating = false;

		//RCT
		int rct_min_n_rect = 2;
		int rct_max_n_rect = 4;
		int rct_n_feat = 50;
		int rct_rad_outer_pos = 4;
		int rct_rad_search_win = 25;
		double rct_learning_rate = 0.85;

		std::string strk_config_path = "Config/struck.cfg";

		//ViSP Template Tracker
		char* visp_sm = "fclk";
		char* visp_am = "ssd";
		char* visp_ssm = "8";
		int visp_max_iters = 30;
		int visp_res = 50;
		double visp_lambda = 0.001;
		double visp_thresh_grad = 60;
		int visp_pyr_n_levels = 0;
		int visp_pyr_level_to_stop = 1;

		// PFSL3
		int pfsl3_p_x = 40;
		int pfsl3_p_y = 40;
		double pfsl3_rot = 0;
		double pfsl3_ncc_std = 0.1;
		double pfsl3_pca_std = 10;
		std::vector<double> pfsl3_state_std;
		double pfsl3_ar_p = 0.5;
		int pfsl3_n = 40;
		int pfsl3_n_c = 10;
		int pfsl3_n_iter = 5;
		int pfsl3_sampling = 0;
		int pfsl3_capture = 0;
		int pfsl3_mean_check = 0;
		int pfsl3_outlier_flag = 0;
		int pfsl3_len = 100;
		int pfsl3_init_size = 15;
		int pfsl3_update_period = 5;
		float pfsl3_ff = 0.99;
		double pfsl3_basis_thr = 0.95;
		int pfsl3_max_num_basis = 30;
		int pfsl3_max_num_used_basis = 10;
		bool pfsl3_show_weights = false;
		bool pfsl3_show_templates = false;


		// HACLK
		//std::vector<cv::Mat> conv_corners;

		inline int readParams(std::vector<char*> &fargv, const char* fname = "params.txt"){
			FILE *fid = fopen(fname, "r");
			if(!fid){
				printf("\n Parameter file: %s not found\n", fname);
				return 0;
			}
			// one extra entry at the beginning to maintain compatibilty with the C/C++
			// command line argument convention
			fargv.push_back(nullptr);
			int arg_id = 0;
			while(!feof(fid)){
				char *temp = new char[500];
				fgets(temp, 500, fid);
				strtok(temp, "\n");
				strtok(temp, "\r");
				if(strlen(temp) <= 1 || temp[0] == '#'){
					delete(temp);
					continue;
				}
				fargv.push_back(temp);
				++arg_id;
				//printf("arg %d: %s\n", arg_id, fargv[arg_id]);
			}
			fclose(fid);
			return arg_id + 1;
		}

		inline void processStringParam(char* &str_out, const char* param){
			//printf("-----------------------\n");
			//printf("param: %s\n", param);
			//printf("use_default: %c\n", use_default);
			if(!strcmp(param, "#")){
				// use default value
				return;
			}
			//if((strlen(param) == 2) && (param[0] == '0') && (param[1] == '0')){
			//	return;
			//}
			//if(str_out){ delete(str_out); }

			str_out = new char[strlen(param) + 1];
			strcpy(str_out, param);
			//printf("str_out: %s\n", str_out);
		}
		// convert a string of comma (or other specified character) separated values 
		// into a vector of doubles
		inline std::vector<double> atof_arr(char *str, const char *sep = ","){
			char *param_str = strtok(str, sep);
			std::vector<double> param_arr;
			//printf("atof_arr::str: %s\n", str);
			//printf("atof_arr::param_arr:\n");
			while(param_str){
				double param_num = atof(param_str);
				param_arr.push_back(param_num);
				//printf("%f\t", param_num);
				param_str = strtok(nullptr, sep);
			}
			//printf("\n");
			//std::string str_temp(str);
			//for(int i = 0; i < str_temp.length(); ++i){
			//	if(str_temp[i] == sep)
			//		str_temp[i] = ' ';
			//}
			//printf("atof_arr::str_temp: %s\n", str_temp.c_str());	
			//std::stringstream ss(str);
			//double temp;
			//while(ss >> temp)
			//	param_arr.push_back(temp);
			return param_arr;
		}

		inline void processAgrument(char *arg_name, char *arg_val){
			if(!strcmp(arg_name, "n_trackers")){
				n_trackers = atoi(arg_val);
			} else if(!strcmp(arg_name, "source_id")){
				source_id = atoi(arg_val);
			} else if(!strcmp(arg_name, "actor_id")){
				actor_id = atoi(arg_val);
			} else if(!strcmp(arg_name, "pipeline")){
				pipeline = arg_val[0];
			} else if(!strcmp(arg_name, "img_source")){
				img_source = arg_val[0];
			} else if(!strcmp(arg_name, "write_frame_data")){
				write_frame_data = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "write_tracking_data")){
				write_tracking_data = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "tracking_data_fname")){
				processStringParam(tracking_data_fname, arg_val);
			} else if(!strcmp(arg_name, "write_pts")){
				write_pts = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "show_cv_window")){
				show_cv_window = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "show_ground_truth")){
				show_ground_truth = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "show_xv_window")){
				show_xv_window = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "read_obj_from_gt")){
				read_obj_from_gt = atoi(arg_val);
			} else if(!strcmp(arg_name, "sel_quad_obj")){
				sel_quad_obj = atoi(arg_val);
			} else if(!strcmp(arg_name, "line_thickness")){
				line_thickness = atoi(arg_val);
			} else if(!strcmp(arg_name, "read_objs")){
				read_objs = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "write_objs")){
				write_objs = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "record_frames")){
				record_frames = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "source_name")){
				processStringParam(source_name, arg_val);
				/*			source_name = new char[strlen(arg_val)+1];
				strcpy(source_name, arg_val);*/
			} else if(!strcmp(arg_name, "source_path")){
				processStringParam(source_path, arg_val);
			} else if(!strcmp(arg_name, "patch_size")){
				patch_size = atoi(arg_val);
			} else if(!strcmp(arg_name, "read_obj_fname")){
				processStringParam(read_obj_fname, arg_val);
			} else if(!strcmp(arg_name, "write_obj_fname")){
				processStringParam(write_obj_fname, arg_val);
			} else if(!strcmp(arg_name, "source_fmt")){
				processStringParam(source_fmt, arg_val);
			} else if(!strcmp(arg_name, "root_path")){
				processStringParam(root_path, arg_val);
			} else if(!strcmp(arg_name, "grid_size_x")){
				grid_size_x = atoi(arg_val);
			} else if(!strcmp(arg_name, "grid_size_y")){
				grid_size_y = atoi(arg_val);
			} else if(!strcmp(arg_name, "adjust_grid")){
				adjust_grid = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "adjust_lines")){
				adjust_lines = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "reset_pos")){
				reset_pos = atoi(arg_val);
			} else if(!strcmp(arg_name, "reset_template")){
				reset_template = atoi(arg_val);
			} else if(!strcmp(arg_name, "reset_wts")){
				reset_wts = atoi(arg_val);
			} else if(!strcmp(arg_name, "pause_after_line")){
				pause_after_line = atoi(arg_val);
			} else if(!strcmp(arg_name, "debug_mode")){
				debug_mode = atoi(arg_val);
			} else if(!strcmp(arg_name, "use_constant_slope")){
				use_constant_slope = atoi(arg_val);
			} else if(!strcmp(arg_name, "use_ls")){
				use_ls = atoi(arg_val);
			} else if(!strcmp(arg_name, "inter_alpha_thresh")){
				inter_alpha_thresh = atof(arg_val);
			} else if(!strcmp(arg_name, "intra_alpha_thresh")){
				intra_alpha_thresh = atof(arg_val);
			} else if(!strcmp(arg_name, "pause_after_frame")){
				pause_after_frame = atoi(arg_val);
			} else if(!strcmp(arg_name, "show_tracked_pts")){
				show_tracked_pts = atoi(arg_val);
			} else if(!strcmp(arg_name, "show_warped_img")){
				show_warped_img = atoi(arg_val);
			} else if(!strcmp(arg_name, "show_template_img")){
				show_template_img = atoi(arg_val);
			} else if(!strcmp(arg_name, "write_tracker_states")){
				write_tracker_states = atoi(arg_val);
			} else if(!strcmp(arg_name, "update_wts")){
				update_wts = atoi(arg_val);
			} else if(!strcmp(arg_name, "sel_reset_thresh")){
				sel_reset_thresh = atof(arg_val);
			} else if(!strcmp(arg_name, "res_from_size")){
				res_from_size = arg_val[0] - '0';
			} else if(!strcmp(arg_name, "mtf_res")){
				mtf_res = atoi(arg_val);
				if(mtf_res > 0){
					resx = resy = mtf_res;
				}
			} else if(!strcmp(arg_name, "resx") && mtf_res <= 0){
				resx = atoi(arg_val);
			} else if(!strcmp(arg_name, "resy") && mtf_res <= 0){
				resy = atoi(arg_val);
			} else if(!strcmp(arg_name, "epsilon")){
				epsilon = atof(arg_val);
			} else if(!strcmp(arg_name, "max_iters")){
				max_iters = atoi(arg_val);
			} else if(!strcmp(arg_name, "mtf_sm")){
				processStringParam(mtf_sm, arg_val);
				//mtf_sm = new char[strlen(arg_val)+1];
				//strcpy(mtf_sm, arg_val);
			} else if(!strcmp(arg_name, "mtf_am")){
				processStringParam(mtf_am, arg_val);
				//mtf_am = new char[strlen(arg_val)+1];
				//strcpy(mtf_am, arg_val);
			} else if(!strcmp(arg_name, "mtf_ssm")){
				processStringParam(mtf_ssm, arg_val);
				//mtf_ssm = new char[strlen(arg_val)+1];
				//strcpy(mtf_ssm, arg_val);
			} else if(!strcmp(arg_name, "ic_update_ssm")){
				ic_update_ssm = atoi(arg_val);
			} else if(!strcmp(arg_name, "ic_chained_warp")){
				ic_chained_warp = atoi(arg_val);
			} else if(!strcmp(arg_name, "fc_chained_warp")){
				fc_chained_warp = atoi(arg_val);
			} else if(!strcmp(arg_name, "buffer_count")){
				buffer_count = atoi(arg_val);
			} else if(!strcmp(arg_name, "norm_pix_max")){
				norm_pix_max = atof(arg_val);
			} else if(!strcmp(arg_name, "norm_pix_min")){
				norm_pix_min = atof(arg_val);
			} else if(!strcmp(arg_name, "init_frame_id")){
				init_frame_id = atoi(arg_val);
			} else if(!strcmp(arg_name, "end_frame_id")){
				end_frame_id = atoi(arg_val);
			} else if(!strcmp(arg_name, "show_tracking_error")){
				show_tracking_error = atoi(arg_val);
			} else if(!strcmp(arg_name, "update_templ")){
				update_templ = atoi(arg_val);
			} else if(!strcmp(arg_name, "grad_eps")){
				grad_eps = strtod(arg_val, nullptr);
			} else if(!strcmp(arg_name, "hess_eps")){
				hess_eps = strtod(arg_val, nullptr);
			} else if(!strcmp(arg_name, "reinit_from_gt")){
				reinit_from_gt = atoi(arg_val);
			} else if(!strcmp(arg_name, "reinit_err_thresh")){
				reinit_err_thresh = atof(arg_val);
			} else if(!strcmp(arg_name, "reinit_frame_skip")){
				reinit_frame_skip = atoi(arg_val);
			} else if(!strcmp(arg_name, "use_opt_gt")){
				use_opt_gt = atoi(arg_val);
			} else if(!strcmp(arg_name, "opt_gt_ssm")){
				opt_gt_ssm = std::string(arg_val);
			}
			// Homography
			else if(!strcmp(arg_name, "hom_corner_based_sampling")){
				hom_corner_based_sampling = atoi(arg_val);
			}
			// SCV
			else if(!strcmp(arg_name, "scv_use_bspl")){
				scv_use_bspl = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_n_bins")){
				scv_n_bins = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_preseed")){
				scv_preseed = atof(arg_val);
			} else if(!strcmp(arg_name, "scv_pou")){
				scv_pou = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_weighted_mapping")){
				scv_weighted_mapping = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_mapped_gradient")){
				scv_mapped_gradient = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_affine_mapping")){
				scv_affine_mapping = atoi(arg_val);
			} else if(!strcmp(arg_name, "scv_once_per_frame")){
				scv_once_per_frame = atoi(arg_val);
			}

			// LSCV
			else if(!strcmp(arg_name, "lscv_sub_regions")){
				lscv_sub_regions = atoi(arg_val);
			} else if(!strcmp(arg_name, "lscv_spacing")){
				lscv_spacing = atoi(arg_val);
			} else if(!strcmp(arg_name, "lscv_show_subregions")){
				lscv_show_subregions = atoi(arg_val);
			}

			// LKLD
			else if(!strcmp(arg_name, "lkld_pre_seed")){
				lkld_pre_seed = atof(arg_val);
			} else if(!strcmp(arg_name, "lkld_pou")){
				lkld_pou = atoi(arg_val);
			} else if(!strcmp(arg_name, "lkld_n_bins")){
				lkld_n_bins = atoi(arg_val);
			} else if(!strcmp(arg_name, "lkld_sub_regions")){
				lkld_sub_regions = atoi(arg_val);
			} else if(!strcmp(arg_name, "lkld_spacing")){
				lkld_spacing = atoi(arg_val);
			}

			// NCC
			else if(!strcmp(arg_name, "ncc_fast_hess")){
				ncc_fast_hess = atoi(arg_val);
			}

			// SSIM
			else if(!strcmp(arg_name, "ssim_pix_proc_type")){
				ssim_pix_proc_type = atoi(arg_val);
			} else if(!strcmp(arg_name, "ssim_k1")){
				ssim_k1 = atof(arg_val);
			} else if(!strcmp(arg_name, "ssim_k2")){
				ssim_k2 = atof(arg_val);
			}

			else if(!strcmp(arg_name, "hom_normalized_init")){
				hom_normalized_init = atoi(arg_val);
			} else if(!strcmp(arg_name, "hess_type")){
				hess_type = atoi(arg_val);
			} else if(!strcmp(arg_name, "sec_ord_hess")){
				sec_ord_hess = atoi(arg_val);
			} else if(!strcmp(arg_name, "jac_type")){
				jac_type = atoi(arg_val);
			}

			// MI and CCRE
			else if(!strcmp(arg_name, "mi_pre_seed")){
				mi_pre_seed = atof(arg_val);
			} else if(!strcmp(arg_name, "mi_pou")){
				mi_pou = atoi(arg_val);
			} else if(!strcmp(arg_name, "mi_n_bins")){
				mi_n_bins = atoi(arg_val);
			}
			// CCRE
			else if(!strcmp(arg_name, "ccre_pre_seed")){
				ccre_pre_seed = atof(arg_val);
			} else if(!strcmp(arg_name, "ccre_pou")){
				ccre_pou = atoi(arg_val);
			} else if(!strcmp(arg_name, "ccre_n_bins")){
				ccre_n_bins = atoi(arg_val);
			} else if(!strcmp(arg_name, "ccre_symmetrical_grad")){
				ccre_symmetrical_grad = atoi(arg_val);
			} else if(!strcmp(arg_name, "ccre_n_blocks")){
				ccre_n_blocks = atoi(arg_val);
			}


			// diagnostics
			else if(!strcmp(arg_name, "diag_am")){
				processStringParam(diag_am, arg_val);
			} else if(!strcmp(arg_name, "diag_ssm")){
				processStringParam(diag_ssm, arg_val);
			} else if(!strcmp(arg_name, "diag_range")){
				diag_range = atof(arg_val);
			} else if(!strcmp(arg_name, "diag_ssm_range")){
				diag_ssm_range = atof_arr(arg_val);
			} else if(!strcmp(arg_name, "diag_res")){
				diag_res = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_update")){
				diag_update = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_grad_diff")){
				diag_grad_diff = atof(arg_val);
			} else if(!strcmp(arg_name, "diag_gen_norm")){
				processStringParam(diag_gen_norm, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_jac")){
				processStringParam(diag_gen_jac, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_hess")){
				processStringParam(diag_gen_hess, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_hess2")){
				processStringParam(diag_gen_hess2, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_hess_sum")){
				processStringParam(diag_gen_hess_sum, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_num")){
				processStringParam(diag_gen_num, arg_val);
			} else if(!strcmp(arg_name, "diag_gen_ssm")){
				processStringParam(diag_gen_ssm, arg_val);
			} else if(!strcmp(arg_name, "diag_bin")){
				diag_bin = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_inv")){
				diag_inv = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_frame_gap")){
				diag_frame_gap = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_show_corners")){
				diag_show_corners = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_show_patches")){
				diag_show_patches = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_verbose")){
				diag_verbose = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_enable_validation")){
				diag_enable_validation = atoi(arg_val);
			} else if(!strcmp(arg_name, "diag_validation_prec")){
				diag_validation_prec = atof(arg_val);
			}

			//Selective Pixel Integration
			else if(!strcmp(arg_name, "spi_enable")){
				spi_enable = atoi(arg_val);
			} else if(!strcmp(arg_name, "spi_thresh")){
				spi_thresh = atof(arg_val);
			}

			else if(!strcmp(arg_name, "pix_mapper")){
				processStringParam(pix_mapper, arg_val);;
			}
			// NN and GNN
			else if(!strcmp(arg_name, "nn_max_iters")){
				nn_max_iters = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_n_samples")){
				nn_n_samples = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_ssm_sigma_prec")){
				nn_ssm_sigma_prec = atof(arg_val);
			} else if(!strcmp(arg_name, "nn_additive_update")){
				nn_additive_update = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_direct_samples")){
				nn_direct_samples = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_ssm_sigma")){
				nn_ssm_sigma = atof_arr(arg_val);
			} else if(!strcmp(arg_name, "nn_corner_sigma_d")){
				nn_corner_sigma_d = atof(arg_val);
			} else if(!strcmp(arg_name, "nn_corner_sigma_t")){
				nn_corner_sigma_t = atof(arg_val);
			} else if(!strcmp(arg_name, "nn_pix_sigma")){
				nn_pix_sigma = atof(arg_val);
			} else if(!strcmp(arg_name, "nn_n_trees")){
				nn_n_trees = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_n_checks")){
				nn_n_checks = atoi(arg_val);
			}  else if(!strcmp(arg_name, "nn_index_type")){
				nn_index_type = atoi(arg_val);
			} else if(!strcmp(arg_name, "nn_index_config_file")){
				nn_index_config_file = std::string(arg_val);
			}
			// GNN
			else if(!strcmp(arg_name, "gnn_k")){
				gnn_k = atoi(arg_val);
			}

			else if(!strcmp(arg_name, "hrch_sm")){
				processStringParam(hrch_sm, arg_val);
			} else if(!strcmp(arg_name, "hrch_am")){
				processStringParam(hrch_am, arg_val);
			}

			// Cascade
			if(!strcmp(arg_name, "casc_n_trackers")){
				casc_n_trackers = atoi(arg_val);
			} else 	if(!strcmp(arg_name, "casc_enable_feedback")){
				casc_enable_feedback = atoi(arg_val);
			}
			// Grid
			else if(!strcmp(arg_name, "gt_sm")){
				processStringParam(gt_sm, arg_val);
			} else 	if(!strcmp(arg_name, "gt_am")){
				processStringParam(gt_am, arg_val);
			} else 	if(!strcmp(arg_name, "gt_ssm")){
				processStringParam(gt_ssm, arg_val);
			} else if(!strcmp(arg_name, "gt_grid_res")){
				gt_grid_res = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_patch_size")){
				gt_patch_size = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_estimation_method")){
				gt_estimation_method = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_ransac_reproj_thresh")){
				gt_ransac_reproj_thresh = atof(arg_val);
			} else if(!strcmp(arg_name, "gt_init_at_each_frame")){
				gt_init_at_each_frame = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_dyn_patch_size")){
				gt_dyn_patch_size = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_show_trackers")){
				gt_show_trackers = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_show_tracker_edges")){
				gt_show_tracker_edges = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_use_tbb")){
				gt_use_tbb = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_pyramid_levels")){
				gt_pyramid_levels = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_use_min_eig_vals")){
				gt_use_min_eig_vals = atoi(arg_val);
			} else if(!strcmp(arg_name, "gt_min_eig_thresh")){
				gt_min_eig_thresh = atof(arg_val);
			}
			// RKLT
			else if(!strcmp(arg_name, "rkl_sm")){
				processStringParam(rkl_sm, arg_val);
			} else if(!strcmp(arg_name, "rkl_enable_spi")){
				rkl_enable_spi = atoi(arg_val);
			} else if(!strcmp(arg_name, "rkl_enable_feedback")){
				rkl_enable_feedback = atoi(arg_val);
			} else if(!strcmp(arg_name, "rkl_failure_detection")){
				rkl_failure_detection = atoi(arg_val);
			} else if(!strcmp(arg_name, "rkl_failure_thresh")){
				rkl_failure_thresh = atof(arg_val);
			}
			// Parallel Tracker
			else if(!strcmp(arg_name, "prl_n_trackers")) {
				prl_n_trackers = atoi(arg_val);
			} else if(!strcmp(arg_name, "prl_estimation_method")) {
				prl_estimation_method = atoi(arg_val);
			} else if(!strcmp(arg_name, "prl_reset_to_mean")) {
				prl_reset_to_mean = atoi(arg_val);
			} else if(!strcmp(arg_name, "prl_auto_reinit")) {
				prl_auto_reinit = atoi(arg_val);
			} else if(!strcmp(arg_name, "prl_reinit_err_thresh")) {
				prl_reinit_err_thresh = atof(arg_val);
			}
			// Gradient Descent
			else if(!strcmp(arg_name, "gd_learning_rate")){
				gd_learning_rate = atof(arg_val);
			}
			// Particle Filter
			else if(!strcmp(arg_name, "pf_max_iters")){
				pf_max_iters = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_n_particles")){
				pf_n_particles = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_dyn_model")){
				pf_dyn_model = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_upd_type")){
				pf_upd_type = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_likelihood_func")){
				pf_likelihood_func = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_resampling_type")){
				pf_resampling_type = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_reset_to_mean")){
				pf_reset_to_mean = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_mean_of_corners")){
				pf_mean_of_corners = atoi(arg_val);
			} else if(!strcmp(arg_name, "pf_ssm_sigma")){
				pf_ssm_sigma = atof_arr(arg_val);
				//printf("pf_ssm_sigma: \n");
				//for(int state_id = 0; state_id < pf_ssm_sigma.size(); state_id++){
				//	printf("%f\t", pf_ssm_sigma[state_id]);
				//}
				//printf("\n");
			} else if(!strcmp(arg_name, "pf_measurement_sigma")){
				pf_measurement_sigma = atof(arg_val);
			} else if(!strcmp(arg_name, "pf_pix_sigma")){
				pf_pix_sigma = atof(arg_val);
			}
			// CMT
			else if(!strcmp(arg_name, "cmt_estimate_scale")){
				cmt_estimate_scale = atoi(arg_val);
			} else if(!strcmp(arg_name, "cmt_estimate_rotation")){
				cmt_estimate_rotation = atoi(arg_val);
			} else if(!strcmp(arg_name, "cmt_feat_detector")){
				processStringParam(cmt_feat_detector, arg_val);
			} else if(!strcmp(arg_name, "cmt_desc_extractor")){
				processStringParam(cmt_desc_extractor, arg_val);
			} else if(!strcmp(arg_name, "cmt_resize_factor")){
				cmt_resize_factor = atof(arg_val);
			}
			// DSST
			else if(!strcmp(arg_name, "dsst_sigma")){
				dsst_sigma = atof(arg_val);
			} else if(!strcmp(arg_name, "dsst_scale_sigma")){
				dsst_scale_sigma = atof(arg_val);
			} else if(!strcmp(arg_name, "dsst_lambda")){
				dsst_lambda = atof(arg_val);
			} else if(!strcmp(arg_name, "dsst_learning_rate")){
				dsst_learning_rate = atof(arg_val);
			} else if(!strcmp(arg_name, "dsst_number_scales")){
				dsst_number_scales = atoi(arg_val);
			} else if(!strcmp(arg_name, "dsst_scale_step")){
				dsst_scale_step = atof(arg_val);
			} else if(!strcmp(arg_name, "dsst_padding")){
				dsst_padding = atoi(arg_val);
			} else if(!strcmp(arg_name, "dsst_resize_factor")){
				dsst_resize_factor = atoi(arg_val);
			} else if(!strcmp(arg_name, "dsst_is_scaling")){
				dsst_is_scaling = atoi(arg_val);
			} else if(!strcmp(arg_name, "dsst_bin_size")){
				dsst_bin_size = atoi(arg_val);
			}

			else if(!strcmp(arg_name, "kcf_output_sigma_factor")){
				kcf_output_sigma_factor = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_interp_factor")){
				kcf_interp_factor = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_lambda")){
				kcf_lambda = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_kernel_sigma")){
				kcf_kernel_sigma = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_number_scales")){
				kcf_number_scales = atoi(arg_val);
			} else if(!strcmp(arg_name, "kcf_scale_step")){
				kcf_scale_step = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_padding")){
				kcf_padding = atoi(arg_val);
			} else if(!strcmp(arg_name, "kcf_resize_factor")){
				kcf_resize_factor = atoi(arg_val);
			} else if(!strcmp(arg_name, "kcf_scale_model_max_area")){
				kcf_scale_model_max_area = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_scale_sigma_factor")){
				kcf_scale_sigma_factor = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_scale_learning_rate")){
				kcf_scale_learning_rate = atof(arg_val);
			} else if(!strcmp(arg_name, "kcf_enableScaling")){
				kcf_enableScaling = atoi(arg_val);
			}

			//TLD
			else if(!strcmp(arg_name, "tld_detector_enabled")){
				tld_detector_enabled = atoi(arg_val);
			} else if(!strcmp(arg_name, "tld_learning_enabled")){
				tld_learning_enabled = atoi(arg_val);
			} else if(!strcmp(arg_name, "tld_tracker_enabled")){
				tld_tracker_enabled = atoi(arg_val);
			} else if(!strcmp(arg_name, "tld_alternating")){
				tld_alternating = atoi(arg_val);
			}
			//RCT
			else if(!strcmp(arg_name, "rct_min_n_rect")){
				rct_min_n_rect = atoi(arg_val);
			} else if(!strcmp(arg_name, "rct_max_n_rect")){
				rct_max_n_rect = atoi(arg_val);
			} else if(!strcmp(arg_name, "rct_n_feat")){
				rct_n_feat = atoi(arg_val);
			} else if(!strcmp(arg_name, "rct_rad_outer_pos")){
				rct_rad_outer_pos = atoi(arg_val);
			} else if(!strcmp(arg_name, "rct_rad_search_win")){
				rct_rad_search_win = atoi(arg_val);
			} else if(!strcmp(arg_name, "rct_learning_rate")){
				rct_learning_rate = atof(arg_val);
			}
			//Struck
			else if(!strcmp(arg_name, "strk_config_path")){
				strk_config_path = std::string(arg_val);
			}

			//ViSP
			else if(!strcmp(arg_name, "visp_sm")){
				processStringParam(visp_sm, arg_val);
			} else if(!strcmp(arg_name, "visp_am")){
				processStringParam(visp_am, arg_val);
			} else if(!strcmp(arg_name, "visp_ssm")){
				processStringParam(visp_ssm, arg_val);
			} else if(!strcmp(arg_name, "visp_max_iters")){
				visp_max_iters = atoi(arg_val);
			} else if(!strcmp(arg_name, "visp_res")){
				visp_res = atoi(arg_val);
			} else if(!strcmp(arg_name, "visp_pyr_n_levels")){
				visp_pyr_n_levels = atoi(arg_val);
			} else if(!strcmp(arg_name, "visp_pyr_level_to_stop")){
				visp_pyr_level_to_stop = atoi(arg_val);
			} else if(!strcmp(arg_name, "visp_lambda")){
				visp_lambda = atof(arg_val);
			} else if(!strcmp(arg_name, "visp_thresh_grad")){
				visp_thresh_grad = atof(arg_val);
			}
			// PFSL3
			else if(!strcmp(arg_name, "pfsl3_p_x")){
				pfsl3_p_x = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_p_y")){
				pfsl3_p_y = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_rot")){
				pfsl3_rot = atof(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_ncc_std")){
				pfsl3_ncc_std = atof(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_pca_std")){
				pfsl3_pca_std = atof(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_state_std")){
				pfsl3_state_std = atof_arr(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_ar_p")){
				pfsl3_ar_p = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_n")){
				pfsl3_n = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_n_c")){
				pfsl3_n_c = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_n_iter")){
				pfsl3_n_iter = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_sampling")){
				pfsl3_sampling = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_capture")){
				pfsl3_capture = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_mean_check")){
				pfsl3_mean_check = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_outlier_flag")){
				pfsl3_outlier_flag = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_len")){
				pfsl3_len = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_init_size")){
				pfsl3_init_size = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_update_period")){
				pfsl3_update_period = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_ff")){
				pfsl3_ff = atof(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_basis_thr")){
				pfsl3_basis_thr = atof(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_max_num_basis")){
				pfsl3_max_num_basis = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_max_num_used_basis")){
				pfsl3_max_num_used_basis = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_show_weights")){
				pfsl3_show_weights = atoi(arg_val);
			} else if(!strcmp(arg_name, "pfsl3_show_templates")){
				pfsl3_show_templates = atoi(arg_val);
			}

		}

		inline FILE* readTrackerParams(FILE * fid = nullptr, int print_args = 0){
			// reads parameters for a single tracker in a multi tracker setup
			// a blank line signals the end of the current tracker's parameters
			// a line starting with # is treated as comment and ignored
			if(!fid){
				const char *fname = (config_dir + "/multi.cfg").c_str();
				printf("Opening file: %s...\n", fname);
				if(!(fid = fopen(fname, "r"))){
					printf("readTrackerParams :: Error: File could not be opened\n");
					return nullptr;
				}
			}
			char curr_line[500];
			char arg_name[500], arg_val[500];
			while(!feof(fid)){
				fgets(curr_line, 500, fid);
				strtok(curr_line, "\n");
				strtok(curr_line, "\r");
				// ignore comments 
				if(curr_line[0] == '#')
					continue;
				// empty line signals the end of current tracker's parameters
				if(strlen(curr_line) <= 1)
					return fid;
				sscanf(curr_line, "%s%s", arg_name, arg_val);
				strtok(arg_name, "\n");
				strtok(arg_name, "\r");
				strtok(arg_val, "\n");
				strtok(arg_val, "\r");
				if(print_args)
					printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);
				processAgrument(arg_name, arg_val);
			}
			fclose(fid);
			return nullptr;
		}

		inline void parseArgumentPairs(char * argv[], int argc,
			int parse_type = 0, int print_args = 0){
			// parse_type = 0 means that each element of argv contains 
			// an argument's name and its value separated by a space
			// parse_type = 1 means that consecutive elements of argv contain 
			// the name and value of each argument

			//printf("Parsing %d argument pairs with argv=%d...\n", argc, argv);
			char arg_name[500], arg_val[500];
			int max_arg = parse_type ? argc / 2 + 1 : argc;
			if(print_args)
				printf("argc: %d max_arg: %d\n", argc, max_arg);
			for(int i = 1; i < max_arg; i++){
				//printf("i=%d\n", i);
				if(parse_type){
					sscanf(argv[2 * i - 1], "%s", arg_name);
					sscanf(argv[2 * i], "%s", arg_val);
				} else{
					sscanf(argv[i], "%s%s", arg_name, arg_val);
				}
				if(print_args)
					printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);
				strtok(arg_name, "\n");
				strtok(arg_name, "\r");
				strtok(arg_val, "\n");
				strtok(arg_val, "\r");
				processAgrument(arg_name, arg_val);
			}
		}

		inline void freeParams(){
			if(source_name){
				delete(source_name);
			}
			if(read_obj_fname){
				delete(read_obj_fname);
			}
			if(source_fmt){
				delete(source_fmt);
			}
			if(root_path){
				delete(root_path);
			}
		}
	}
}



#endif
