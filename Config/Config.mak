# --------------------------------------------------------------------------------- #
# ---------------------------------- Config ---------------------------------- #
# --------------------------------------------------------------------------------- #
CONFIG = parameters datasets
CONFIG_HEADERS =  $(addprefix Config/, $(addsuffix .h, ${CONFIG}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${CONFIG_HEADERS})  
