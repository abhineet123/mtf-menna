#ifndef MTF_REGISTER_H
#define MTF_REGISTER_H

//! include statements and macros for registering different combinations of the various modules

// Appearance Models
#include "mtf/AM/SSD.h"
#include "mtf/AM//NSSD.h"
#include "mtf/AM//ZNCC.h"
#include "mtf/AM//SCV.h"
#include "mtf/AM//LSCV.h"
#include "mtf/AM//RSCV.h"
#include "mtf/AM//LRSCV.h"
#include "mtf/AM//KLD.h"
#include "mtf/AM//LKLD.h"	
#include "mtf/AM//MI.h"
#include "mtf/AM//SPSS.h"
#include "mtf/AM//SSIM.h"
#include "mtf/AM//NCC.h"
#include "mtf/AM//CCRE.h"
#include "mtf/AM//FMaps.h"

// State Space Models
#include "mtf/SSM/LieHomography.h"
#include "mtf/SSM/CornerHomography.h"
#include "mtf/SSM/HomographySL3.h"
#include "mtf/SSM/Homography.h"
#include "mtf/SSM/Affine.h"
#include "mtf/SSM/Similitude.h"
#include "mtf/SSM/Isometry.h"
#include "mtf/SSM/Transcaling.h"
#include "mtf/SSM/Translation.h"	

#define _REGISTER_TRACKERS_AM(SM, SSM) \
	template class mtf::SM< mtf::SSD,  mtf::SSM >;\
	template class mtf::SM< mtf::NSSD,  mtf::SSM >;\
	template class mtf::SM< mtf::ZNCC,  mtf::SSM >;\
	template class mtf::SM< mtf::SCV,  mtf::SSM >;\
	template class mtf::SM< mtf::RSCV,  mtf::SSM >;\
	template class mtf::SM< mtf::LSCV,  mtf::SSM >;\
	template class mtf::SM< mtf::LRSCV,  mtf::SSM >;\
	template class mtf::SM< mtf::KLD,  mtf::SSM >;\
	template class mtf::SM< mtf::LKLD,  mtf::SSM >;\
	template class mtf::SM< mtf::SPSS,  mtf::SSM >;\
	template class mtf::SM< mtf::SSIM,  mtf::SSM >;\
	template class mtf::SM< mtf::NCC,  mtf::SSM >;\
	template class mtf::SM< mtf::CCRE,  mtf::SSM >;\
	template class mtf::SM< mtf::MI,  mtf::SSM >;\
	template class mtf::SM< mtf::FMaps,  mtf::SSM >;


#define _REGISTER_TRACKERS(SM) \
	_REGISTER_TRACKERS_AM(SM, LieHomography)\
	_REGISTER_TRACKERS_AM(SM, CornerHomography)\
	_REGISTER_TRACKERS_AM(SM, HomographySL3)\
	_REGISTER_TRACKERS_AM(SM, Homography)\
	_REGISTER_TRACKERS_AM(SM, Affine)\
	_REGISTER_TRACKERS_AM(SM, Similitude)\
	_REGISTER_TRACKERS_AM(SM, Isometry)\
	_REGISTER_TRACKERS_AM(SM, Transcaling)\
	_REGISTER_TRACKERS_AM(SM, Translation)\

#define _REGISTER_TRACKERS_SSM(SM) \
	template class mtf::SM< mtf::LieHomography>;\
	template class mtf::SM< mtf::CornerHomography>;\
	template class mtf::SM< mtf::HomographySL3>;\
	template class mtf::SM< mtf::Homography>;\
	template class mtf::SM< mtf::Affine>;\
	template class mtf::SM< mtf::Similitude>;\
	template class mtf::SM< mtf::Isometry>;\
	template class mtf::SM< mtf::Transcaling>;\
	template class mtf::SM< mtf::Translation>;

//registration macros for hierarchical trackers with 2 SSMs
#define _REGISTER_HTRACKERS_AM(SM, SSM, SSM2) \
	template class mtf::SM< mtf::SSD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::NSSD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::ZNCC,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::SCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::RSCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::LSCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::LRSCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::KLD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::LKLD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::SPSS,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::SSIM,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::NCC,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::CCRE,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::MI,  mtf::SSM,  mtf::SSM2 >;

#define _REGISTER_HTRACKERS_SSM(SM, SSM) \
	_REGISTER_HTRACKERS_AM(SM, SSM, LieHomography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, CornerHomography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, HomographySL3)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Homography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Affine)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Similitude)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Isometry)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Transcaling)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Translation)\

#define _REGISTER_HTRACKERS(SM) \
	_REGISTER_HTRACKERS_SSM(SM, LieHomography)\
	_REGISTER_HTRACKERS_SSM(SM, CornerHomography)\
	_REGISTER_HTRACKERS_SSM(SM, HomographySL3)\
	_REGISTER_HTRACKERS_SSM(SM, Homography)\
	_REGISTER_HTRACKERS_SSM(SM, Affine)\
	_REGISTER_HTRACKERS_SSM(SM, Similitude)\
	_REGISTER_HTRACKERS_SSM(SM, Isometry)\
	_REGISTER_HTRACKERS_SSM(SM, Transcaling)\
	_REGISTER_HTRACKERS_SSM(SM, Translation)\

#endif

#define _REGISTER_ESM_AM(HT, JT, SSM) \
	template class mtf::FESM< mtf::SSD,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::NSSD,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::ZNCC,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::SCV,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::RSCV,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::LSCV,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::LRSCV,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::KLD,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::LKLD,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::SPSS,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::SSIM,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::NCC,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::CCRE,  mtf::SSM, mtf::HT, mtf::JT >;\
	template class mtf::FESM< mtf::MI,  mtf::SSM, mtf::HT, mtf::JT >;

#define _REGISTER_ESM(HT, JT) \
	_REGISTER_ESM_AM(HT, JT, LieHomography)\
	_REGISTER_ESM_AM(HT, JT, CornerHomography)\
	_REGISTER_ESM_AM(HT, JT, HomographySL3)\
	_REGISTER_ESM_AM(HT, JT, Homography)\
	_REGISTER_ESM_AM(HT, JT, Affine)\
	_REGISTER_ESM_AM(HT, JT, Similitude)\
	_REGISTER_ESM_AM(HT, JT, Isometry)\
	_REGISTER_ESM_AM(HT, JT, Transcaling)\
	_REGISTER_ESM_AM(HT, JT, Translation)\




