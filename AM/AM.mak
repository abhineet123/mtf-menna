# ------------------------------------------------------------------------------------- #
# --------------------------------- Appearance Models --------------------------------- #
# ------------------------------------------------------------------------------------- #
BASE_CLASSES += AM/AppearanceModel
DIAG_BASE_CLASSES += AM/AppearanceModel
APPEARANCE_MODELS = FMaps ImageBase SSDBase SSD NSSD ZNCC SCV LSCV RSCV LRSCV CCRE LKLD MI SSIM NCC SPSS KLD
APPEARANCE_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${APPEARANCE_MODELS}))
APPEARANCE_HEADERS = $(addprefix AM/, $(addsuffix .h, ${APPEARANCE_MODELS}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${APPEARANCE_HEADERS}) 

MI_FLAGS = 
CCRE_FLAGS = 
LSCV_FLAGS = 
SSD_FLAGS = 

mil ?= 0
mid ?= 0
mitbb ?= 0
miomp ?= 0
ccretbb ?= 0
ccreomp ?= 0
pip ?= -1
gip ?= -1
hip ?= -1
sg ?= 0
lscd ?= 0

ifeq (${mitbb}, 1)
MI_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${miomp}, 1)
MI_FLAGS += -D ENABLE_OMP -fopenmp
RUN_MTF_FLAGS += -D ENABLE_PARALLEL -fopenmp
LIB_MTF_LIBS += -fopenmp
endif

ifeq (${ccretbb}, 1)
CCRE_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${ccreomp}, 1)
CCRE_FLAGS += -D ENABLE_OMP -fopenmp
RUN_MTF_FLAGS += -D ENABLE_PARALLEL -fopenmp
LIB_MTF_LIBS += -fopenmp
endif

ifneq (${pip}, -1)
CT_FLAGS += -D PIX_INTERP_TYPE=${pip}
endif
ifneq (${gip}, -1)
CT_FLAGS += -D GRAD_INTERP_TYPE=${gip}
endif
ifneq (${hip}, -1)
CT_FLAGS += -D HESS_INTERP_TYPE=${hip}
endif

ifeq (${mid}, 1)
MI_FLAGS = -D LOG_MI_DATA
endif

ifeq (${mil}, 1)
MI_FLAGS = -D LOG_MI_DATA -D LOG_MI_TIMES 
endif

ifeq (${sg}, 1)
SSD_FLAGS += -D USE_SLOW_GRAD
endif

ifeq (${lscd}, 1)
LSCV_FLAGS += -D LOG_LSCV_DATA
endif

${BUILD_DIR}/ImageBase.o: AM/ImageBase.cc AM/ImageBase.h Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/SSIM.o: AM/SSIM.cc AM/SSIM.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSIM_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/SPSS.o: AM/SPSS.cc AM/SPSS.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SPSS_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/NCC.o: AM/NCC.cc AM/NCC.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${NCC_FLAGS}$< ${FLAGS64} -o $@
	
${BUILD_DIR}/SSDBase.o: AM/SSDBase.cc AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@	
	
${BUILD_DIR}/SSD.o: AM/SSD.cc AM/SSD.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/NSSD.o: AM/NSSD.cc AM/NSSD.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/ZNCC.o: AM/ZNCC.cc AM/ZNCC.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/SCV.o: AM/SCV.cc AM/SCV.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/LSCV.o: AM/LSCV.cc AM/LSCV.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ${LSCV_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/RSCV.o: AM/RSCV.cc AM/RSCV.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@	
	
${BUILD_DIR}/LRSCV.o: AM/LRSCV.cc AM/LRSCV.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ${LRSCV_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/FMaps.o: AM/FMaps.cc AM/FMaps.h AM/SSDBase.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/KLD.o: AM/KLD.cc AM/KLD.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/histUtils.h Utilities/imgUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${KLD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/LKLD.o: AM/LKLD.cc AM/LKLD.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/histUtils.h Utilities/imgUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${LKLD_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/MI.o: AM/MI.cc AM/MI.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/histUtils.h Utilities/imgUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${MI_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/CCRE.o: AM/CCRE.cc AM/CCRE.h AM/AppearanceModel.h AM/ImageBase.h  Macros/common.h Utilities/histUtils.h Utilities/imgUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${CCRE_FLAGS} $< ${FLAGS64} -o $@		

	
