#include "mtf/AM/ImageBase.h"
#include "mtf/Utilities/imgUtils.h"

_MTF_BEGIN_NAMESPACE

ImageBase::ImageBase(ImgParams *img_params) :
	curr_img(nullptr, 0, 0), img_height(0), img_width(0),
	resx(0), resy(0), n_pix(0), frame_count(0),
	pix_norm_add(0.0), pix_norm_mult(1.0),
	grad_eps(GRAD_EPS), hess_eps(HESS_EPS) {
	if(img_params) {
		if(img_params->resx <= 0 || img_params->resy <= 0) {
			throw std::invalid_argument("ImageBase::Invalid sampling resolution provided");
		}
		resx = img_params->resx;
		resy = img_params->resy;
		n_pix = resx*resy;
		grad_eps = img_params->grad_eps;
		hess_eps = img_params->hess_eps;

		img_height = img_params->cv_img.rows;
		img_width = img_params->cv_img.cols;
		if(img_height>0 && img_width>0) {
			new (&curr_img) EigImgT((EigPixT*)((img_params->cv_img).data), img_height, img_width);
		}
	}
}

void ImageBase::initializePixVals(const Matrix2Xd& init_pts){
	if(!isInitialized()->pix_vals){
		init_pix_vals.resize(n_pix);
		curr_pix_vals.resize(n_pix);
	}
	++frame_count;
	utils::getPixVals(init_pix_vals, curr_img, init_pts, n_pix,
		img_height, img_width, pix_norm_mult, pix_norm_add);

	if(!isInitialized()->pix_vals){
		curr_pix_vals = init_pix_vals;
		isInitialized()->pix_vals = true;
	}
}

void ImageBase::initializePixGrad(const Matrix3Xd& init_pts_hm,
	const Matrix3d &init_warp){

	if(!isInitialized()->pix_grad){
		init_pix_grad.resize(n_pix, Eigen::NoChange);
		curr_pix_grad.resize(n_pix, Eigen::NoChange);
	}
	utils::getWarpedImgGrad(init_pix_grad,
		curr_img, init_pts_hm, init_warp,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}
void ImageBase::initializePixGrad(const Matrix2Xd &init_pts){

	if(!isInitialized()->pix_grad){
		init_pix_grad.resize(n_pix, Eigen::NoChange);
		curr_pix_grad.resize(n_pix, Eigen::NoChange);
	}

	utils::getImgGrad(init_pix_grad, curr_img, init_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);


	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}

void ImageBase::initializePixGrad(const Matrix8Xd &warped_offset_pts){

	if(!isInitialized()->pix_grad){
		init_pix_grad.resize(n_pix, Eigen::NoChange);
		curr_pix_grad.resize(n_pix, Eigen::NoChange);
	}
	utils::getWarpedImgGrad(init_pix_grad,
		curr_img, warped_offset_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}

void ImageBase::initializePixHess(const Matrix3Xd& init_pts_hm,
	const Matrix3d &init_warp){
	if(!isInitialized()->pix_hess){
		init_pix_hess.resize(Eigen::NoChange, n_pix);
		curr_pix_hess.resize(Eigen::NoChange, n_pix);
	}
	utils::getWarpedImgHess(init_pix_hess, curr_img, init_pts_hm,
		init_warp, hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}

}

void ImageBase::initializePixHess(const Matrix2Xd& init_pts,
	const Matrix16Xd &warped_offset_pts){
	if(!isInitialized()->pix_hess){
		init_pix_hess.resize(Eigen::NoChange, n_pix);
		curr_pix_hess.resize(Eigen::NoChange, n_pix);
	}

	utils::getWarpedImgHess(init_pix_hess, curr_img, init_pts, warped_offset_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}
}


void ImageBase::initializePixHess(const Matrix2Xd &init_pts){
	if(!isInitialized()->pix_hess){
		init_pix_hess.resize(Eigen::NoChange, n_pix);
		curr_pix_hess.resize(Eigen::NoChange, n_pix);
	}
	utils::getImgHess(init_pix_hess, curr_img, init_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}
}


void ImageBase::updatePixVals(const Matrix2Xd& curr_pts){
	utils::getPixVals(curr_pix_vals, curr_img, curr_pts, n_pix, img_height, img_width, 
		pix_norm_mult, pix_norm_add);
}

void ImageBase::updatePixGrad(const Matrix2Xd &curr_pts){
	utils::getImgGrad(curr_pix_grad, curr_img, curr_pts,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixHess(const Matrix2Xd &curr_pts){
	utils::getImgHess(curr_pix_hess, curr_img, curr_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixGrad(const Matrix8Xd &warped_offset_pts){
	utils::getWarpedImgGrad(curr_pix_grad, curr_img, warped_offset_pts,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixHess(const Matrix2Xd& curr_pts,
	const Matrix16Xd &warped_offset_pts){
	utils::getWarpedImgHess(curr_pix_hess, curr_img, curr_pts, warped_offset_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updateTemplate(const Matrix2Xd& curr_pts){
	++frame_count;
	// update the template, aka init_pix_vals with the running average of patch corresponding to the provided points
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double pix_val = pix_norm_mult * utils::getPixVal<PIX_INTERP_TYPE>(curr_img, curr_pts(0, pix_id), curr_pts(1, pix_id), 
			img_height, img_width) + pix_norm_add;
		init_pix_vals(pix_id) += (pix_val - init_pix_vals(pix_id)) / frame_count;
	}
}

_MTF_END_NAMESPACE