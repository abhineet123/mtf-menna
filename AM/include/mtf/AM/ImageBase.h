#ifndef IMAGE_BASE_H
#define IMAGE_BASE_H

#define WARPED_GRAD_EPS 1e-8
#define GRAD_EPS 1e-8
#define HESS_EPS 1

#define PIX_MAX 255.0
#define PIX_MIN 0.0

#define RES_X 50
#define RES_Y 50

#include "mtf/Macros/common.h"
#include <stdexcept>

_MTF_BEGIN_NAMESPACE

struct ImgParams{
	// horizontal and vertical sampling resolutions
	int resx, resy;
	//! numerical increment/decrement used for computing image hessian and gradient
	//! using the method of finite differences
	double grad_eps, hess_eps;
	// optional OpenCV image where all images in the tracking sequence will be put
	// this is provided to avoid passing the image again each time it is updated
	cv::Mat cv_img;
	ImgParams(int _resx, int _resy,
		double _grad_eps = GRAD_EPS,
		double _hess_eps = HESS_EPS) : 
		resx(_resx), resy(_resy),
		grad_eps(_grad_eps), hess_eps(_hess_eps){}
	ImgParams(int _resx, int _resy, const cv::Mat &_cv_img,
		double _grad_eps = GRAD_EPS,
		double _hess_eps = HESS_EPS) :
		resx(_resx), resy(_resy),
		grad_eps(_grad_eps), hess_eps(_hess_eps),
		cv_img(_cv_img){}
	ImgParams(ImgParams *img_params=nullptr) :
		resx(RES_X),
		resy(RES_Y),
		grad_eps(RES_X),
		hess_eps(RES_Y){
		if(img_params){
			resx = img_params->resx;
			resy = img_params->resy;
			grad_eps = img_params->grad_eps;
			hess_eps = img_params->hess_eps;
			cv_img = img_params->cv_img;
		}
	}
};

// set of indicator variables to keep track of which dependent state variables need updating and executing
// the update expression for any variable only when at least one of the other variables it depends on has 
// been updated; this can help to increase speed by avoiding repeated computations
struct PixStatus{
	bool pix_vals, pix_grad, pix_hess;

	PixStatus(){ clearPixState(); }

	void setPixState(){
		pix_vals = pix_grad = pix_hess = true;
	}
	void clearPixState(){
		pix_vals = pix_grad = pix_hess = false;
	}
};
class ImageBase{

public:
	enum{ ColorSpace = SingleChannel };

	typedef EigImgT ImageT;

	ImageT curr_img;
	int img_height, img_width; // height and width of the input images
	
	int resx, resy; // horizontal and vertical sampling resolutions
	int n_pix; //! no. of pixels in the sampled image patch to be tracked	

	PixValT init_pix_vals, curr_pix_vals; //! pix values in the object patch being tracked (flattened as a vector)
	// let A = err_vec_size = dimensionality of error vector and N = n_pix = no. of pixels

	PixGradT init_pix_grad, curr_pix_grad; //! N x 2 jacobian of pixel values in the warped image w.r.t. 
	//! (homogeneous) pixel coordinate locations aka gradient of the warped image or warp of the gradient image depending on the search method

	PixHessT init_pix_hess, curr_pix_hess;//! 4 x N Hessian of pixel values in the warped image w.r.t. pixel coordinate locations;
	//! each column of this matrix contains the 2x2 hessian for the respective location flasttened in the column major order;

	double pix_norm_add, pix_norm_mult; //! additive and multiplicative factors for normalizing pix values

	explicit ImageBase(ImgParams *img_params = nullptr);
	virtual ~ImageBase(){}

	// return true if the AM requires the raw RGB image
	virtual bool rgbInput() const { return false; }

	virtual void setCurrImg(const cv::Mat &cv_img){
		img_height = cv_img.rows;
		img_width = cv_img.cols;
		new (&curr_img) EigImgT((EigPixT*)(cv_img.data), img_height, img_width);
	}
	virtual const ImageT& getCurrImg(){ return curr_img; }
	virtual int getImgHeight(){ return img_height; }
	virtual int getImgWidth(){ return img_width; }

	virtual int getResX(){ return resx; }
	virtual int getResY(){ return resy; }
	virtual int getPixCount(){ return n_pix; }

	// accessor methods; these are not defined as 'const' since an appearance model may like to
	// make some last moment changes to the variable being accessed before returning it to can avoid any 
	// unnecessary computations concerning the variable (e.g. in its 'update' function) unless it is actually accessed;
	virtual const PixValT& getInitPixVals(){ return init_pix_vals; }
	virtual const PixGradT& getInitPixGrad(){ return init_pix_grad; }
	virtual const PixHessT& getInitPixHess(){ return init_pix_hess; }

	virtual const PixValT& getCurrPixVals(){ return curr_pix_vals; }
	virtual const PixGradT& getCurrPixGrad(){ return curr_pix_grad; }
	virtual const PixHessT& getCurrPixHess(){ return curr_pix_hess; }

	// modifier methods;
	virtual void setInitPixVals(const PixValT &pix_vals){ init_pix_vals = pix_vals; }
	virtual void setInitPixGrad(const PixGradT &pix_grad){ init_pix_grad = pix_grad; }
	virtual void setInitPixHess(const PixHessT &pix_hess){ init_pix_hess = pix_hess; }

	virtual void setCurrPixVals(const PixValT &pix_vals){ curr_pix_vals = pix_vals; }
	virtual void setCurrPixGrad(const PixGradT &pix_grad){ curr_pix_grad = pix_grad; }
	virtual void setCurrPixHess(const PixHessT &pix_hess){ curr_pix_hess = pix_hess; }

	virtual void initializePixVals(const PtsT& init_pts);

	// functions to compute image differentials (gradient and hessian) are overloaded since there are two ways to define them:
	// 1. differential of the warped image: the image is warped first, then its differential is computed at the base (unwarped) locations
	// 2. warp of the image differential:  differential is computed in the image coordinates and evaluated at the given (presumably warped) locations
	//1. gradient of the warped image
	virtual void initializePixGrad(const HomPtsT& init_pts_hm, const ProjWarpT &init_warp);
	virtual void initializePixGrad(const GradPtsT &warped_offset_pts);
	// 2. warp of the image gradient
	virtual void initializePixGrad(const PtsT &init_pts);

	//1. hessian of the warped image
	virtual void initializePixHess(const HomPtsT& init_pts_hm, const ProjWarpT &init_warp);
	virtual void initializePixHess(const PtsT& init_pts, const HessPtsT &warped_offset_pts);
	// 2. warp of the image hessian
	virtual void initializePixHess(const PtsT &init_pts);

	// -------- functions for updating state variables when a new image arrives -------- //

	virtual void updatePixVals(const PtsT& curr_pts);

	virtual void updatePixGrad(const GradPtsT &warped_offset_pts);
	virtual void updatePixGrad(const PtsT &curr_pts);

	virtual void updatePixHess(const PtsT &curr_pts);
	virtual void updatePixHess(const PtsT& curr_pts, const HessPtsT &warped_offset_pts);

	// optional function to incorporate online learning or adaptation of the object template being tracked
	// this should be called with the final location of the object (obtained by the search process) in the curent image
	// by default it updates the template with the running average of all the object patches seen so far
	virtual void updateTemplate(const PtsT& curr_pts);
	int frame_count;// incremented once during initialization and thereafter everytime the template is updated

	virtual PixStatus* isInitialized() = 0;
	
	double grad_eps, hess_eps;
	virtual double getGradOffset(){ return grad_eps; }
	virtual double getHessOffset(){ return hess_eps; }

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

_MTF_END_NAMESPACE

#endif



