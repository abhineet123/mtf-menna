#ifndef FMAPs_H
#define FMAPs_H

#include "AppearanceModel.h"
#include "SSD.h"

_MTF_BEGIN_NAMESPACE


class FMaps: public SSD
{
public:
    FMaps(ImgParams *img_params = nullptr);
};

_MTF_END_NAMESPACE

#endif
