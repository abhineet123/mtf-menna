Welcome to the home of **Modular Tracking Framework (MTF)** - a highly efficient and extensible library for
[**registration based tracking**](https://en.wikipedia.org/wiki/Kanade%E2%80%93Lucas%E2%80%93Tomasi_feature_tracker)
that utilizes a modular decomposition of trackers in this domain.
Each tracker within this framework comprises the following 3 modules:

1. **Search Method (SM)**: [ESM](http://far.in.tum.de/pub/benhimane2007ijcv/benhimane2007ijcv.pdf), IC, IA, FC, FA, NN, PF or RKLT
2. **Appearance Model (AM)**: SSD, ZNCC, SCV, NCC, MI, CCRE KLD, SSIM or SPSS
3. **State Space Model (SSM)**: Homography (8 dof), Affine (6 dof), Similitude(4 dof), Isometery (3 dof) or pure Translation (2 dof)

Please refer
[this paper](http://webdocs.cs.ualberta.ca/~asingh1/docs/Modular%20Tracking%20Framework%20A%20Uni%EF%AC%81ed%20Approach%20to%20Registration%20based%20Tracking%20(CRV%202016).pdf)
for more details on the system design and
[this one](http://webdocs.cs.ualberta.ca/~asingh1/docs/Modular%20Decomposition%20and%20Analysis%20of%20Registration%20based%20Trackers%20(CRV%202016).pdf)
for some preliminary results. There is also an [**official website**](http://webdocs.cs.ualberta.ca/~vis/mtf/) where Doxygen documentation will soon be available along with detailed tutorials and examples. It also provides several datasets formatted to work with MTF.

The library is implemented entirely in C++ though a Python interface called `pyMTF` also exists and works seamlessly with our [Python Tracking Framework](https://bitbucket.org/abhineet123/ptf). 
A Matlab interface similar to [Mexvision](http://ugweb.cs.ualberta.ca/~vis/courses/CompVis/lab/mexVision/) is currently under development too.

We also provide a simple interface for [ROS](http://www.ros.org/) called [mtf_bridge](https://gitlab.com/vis/mtf_bridge) for seamless integration with robotics applications.

Installation:
-------------
* Prerequisites:
    *  MTF uses some C++11 features so a supporting compiler is needed ([GCC 4.7](https://gcc.gnu.org/projects/cxx0x.html) or newer)
    * [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) should be installed and added to the C/C++ include paths. This can be done, for instance, by running `echo "export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` and `echo "export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` assuming that Eigen is installed in _/usr/include/eigen3_
    * [OpenCV](http://opencv.org/) should be installed.
    * [FLANN library](http://www.cs.ubc.ca/research/flann/) and its dependency [HDF5](https://www.hdfgroup.org/HDF5/release/obtain5.html) should be installed for the NN search method
	    - NN can be disabled at compile time using nn=0 if these are not available (see below)
	* [Boost Library](http://www.boost.org/) should be installed
    * [Intel TBB](https://www.threadingbuildingblocks.org/) / [OpenMP](http://openmp.org/wp/) should be installed if parallelization is to be enabled.
    * [ViSP library](https://visp.inria.fr/) should be installed if its [template tracker module](https://visp.inria.fr/template-tracking/) is enabled during compilation (see below).
	    - Note that [version 3.0.0](http://gforge.inria.fr/frs/download.php/latestfile/475/visp-3.0.0.zip)+ is required. The Ubuntu apt package is 2.8 and is therefore incompatible.
    * [Xvision](https://bitbucket.org/abhineet123/xvision2) should be installed if it is enabled during compilation (see below).
* Download the source code using `git clone https://abhineet123@bitbucket.org/abhineet123/mtf.git`.
* Use one of the following make commands to compile and install the library and the demo application:
    * `make` or `make mtf` : compiles the shared library (_libmtf.so_) to the build directory (_Build/Release_)
    * `make install` : compiles the shared library if needed and copies it to _/usr/lib_; also copies the headers to _/usr/include/mtf_; this needs administrative (sudo) privilege.
	    - if this is not available, then the variables `MTF_LIB_INSTALL_DIR` and `MTF_HEADER_INSTALL_DIR` in _mtf_flags.mak_ can be modified to install elsewhere. This can be done either by editing the file itself or providing these with the make command as: `make install MTF_LIB_INSTALL_DIR=<library_installation_dir> MTF_HEADER_INSTALL_DIR=<header_installation_dir>`
		    - these folders should be present in LD_LIBRARY_PATH and C_INCLUDE_PATH/CPLUS_INCLUDE_PATH environment variables respectively
    * `make mtfe` compiles the example file _Examples/runMTF.cc_ to create an executable called _runMTF_ that uses this library to track objects. This too is placed in the build directory.
	* `make install_exec`: creates _runMTF_ if needed and copies it to _/usr/bin_; this needs administrative privilege too - change `MTF_EXEC_INSTALL_DIR` as above if this is not available
    * **`make mtfi`** : all of the above - **recommended command that compiles and installs the library and the executable**
    * `make mtfp` : compile the Python interface to MTF - this creates a Python module called _pyMTF.so_ that serves as a front end for running these trackers from Python.
Usage of this module is fully demonstrated in the `mtfTracker.py` file in our [Python Tracking Framework](https://bitbucket.org/abhineet123/ptf)
    * Compile time switches for all of the above commands:
	    - `nn=0` will disable NN search method (enabled by default).
		    - should be specified if FLANN is not available
	    - `lt=0` will disable the third party open source learning based trackers - [DSST](http://www.cvl.isy.liu.se/en/research/objrec/visualtracking/scalvistrack/index.html), [KCF](http://home.isr.uc.pt/~henriques/circulant/), [CMT](http://www.gnebehay.com/cmt/), [TLD](http://www.gnebehay.com/tld/), [RCT](http://www4.comp.polyu.edu.hk/~cslzhang/CT/CT.htm) and [Struck](http://www.samhare.net/research/struck) - that are also bundled with this library (in _ThirdParty_ subfolder) (enabled by default).
	    - `vp=1` will enable ViSP template trackers (disabled by default).
	    - `xv=1` will enable Xvision trackers and pipeline (disabled by default and not recommended).
	    - `o=0` will compile the library in debug mode (_libmtf_debug.so_) and disable optimization - the corresponding executable is called _runMTFd_
    * Clean up commands:
	    * `make clean` : removes all the .o files and the .so file created during compilation from the Build folder folder
        * `make mtfc` : also removes the executable

Setting Parameters:
-------------------
MTF parameters can be specified either in the _cfg_ files present in the _Config_ sub folder or from the command line.
Please refer the ReadMe in the **_Config_** sub folder for detailed instructions.

Running the demo application:
-----------------------------

Use either `make run` or `runMTF` to start the tracking application using the settings specified as above.


**For Developers**
==================

Adding a new Appearance Model (AM) or State Space Model (SSM):
-------------------------------------------------------------

1. Add rule to "modules.mak" in the respective sub directory to compile the .o of the new AM/SSM that is dependent on its source and header files - simplest method would be to copy an existing command and change the names of the model.
2. Modify "modules.mak" in the respective sub directory to add the name of the AM/SSM to the variable `APPEARANCE_MODELS` or `STATE_SPACE_MODELS` respectively
3. Modify makefile to add any extra dependencies that the new files need to include under variable `FLAGS64`
and extra libraries to link against under `LIB_MTF_LIBS`
4. Modify "mtf.h" to add a new AM/SSM option in the overloaded function `getTrackerObj` (starting lines 498 and 533 respectively) that can be used with your config file to pick the AM/SSM, and create an object with this selected model.
5. Modify "Macros/register.h" to add the new AM/SSM class name under `_REGISTER_TRACKERS_AM/_REGISTER_HTRACKERS_AM` or `_REGISTER_TRACKERS/_REGISTER_TRACKERS_SSM/_REGISTER_HTRACKERS/_REGISTER_HTRACKERS_SSM` respectively

All steps are identical for adding a new Search Method (SM) too except the last one which is not needed. Instead this header needs to be included and the appropriate macro needs to be called from its source file to register the SM with all existing AMs and SSMs. Refer to the last 2 lines of the .cc file of any existing SM to see how to do this.

Example: Implementing a minimalistic AM that can be used with NN search method:
-------------------------------------------------------------------------------

You need to create a new derived class from AppearanceModel.
Implement the following functions:

1. `initializePixVals/updatePixVals` : takes the current location of the object as a 2xN matrix where N = no. of sampled points and each column contains the x,y coordinates of one point. By default, it is simply supposed to extract the pixel values at these locations from the current image but there is no restriction by design and you are free to compute anything from these pixel values.
2. `initializeDistFeat/updateDistFeat` : these compute a distance feature transform from the current patch.
3. `getDistFeat`: returns a pointer to an array containing the distance feature vector computed by the above function. There is also an overloaded variant of updateDistFeat  that takes a pointer as input and directly writes this feature vector to the pre-allocated array pointed to by this pointer.
4. distance functor (`operator()`): computes a scalar that measures the dissimilarity or distance between two feature vectors (obtained using the previous two functions).
