# -------------------------------------------------------------------------------------- #
# --------------------------------- State Space Models --------------------------------- #
# -------------------------------------------------------------------------------------- #
BASE_CLASSES += SSM/StateSpaceModel
DIAG_BASE_CLASSES += SSM/StateSpaceModel
STATE_SPACE_MODELS = ProjectiveBase LieHomography CornerHomography Homography HomographySL3 Affine Similitude Isometry Transcaling Translation
STATE_SPACE_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${STATE_SPACE_MODELS}))	
STATE_SPACE_HEADERS = $(addprefix SSM/, $(addsuffix .h, ${STATE_SPACE_MODELS}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${STATE_SPACE_HEADERS})  

${BUILD_DIR}/ProjectiveBase.o: SSM/ProjectiveBase.cc SSM/ProjectiveBase.h SSM/StateSpaceModel.h Macros/common.h Utilities/warpUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/LieHomography.o: SSM/LieHomography.cc SSM/LieHomography.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Macros/common.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@

${BUILD_DIR}/CornerHomography.o: SSM/CornerHomography.cc SSM/CornerHomography.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Macros/common.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/HomographySL3.o: SSM/HomographySL3.cc SSM/HomographySL3.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Macros/common.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/Homography.o: SSM/Homography.cc SSM/Homography.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Macros/common.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/Affine.o: SSM/Affine.cc SSM/Affine.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Macros/common.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/Similitude.o: SSM/Similitude.cc SSM/Similitude.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Utilities/warpUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} $< ${FLAGS64} -o $@	
	
${BUILD_DIR}/Isometry.o: SSM/Isometry.cc SSM/Isometry.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Utilities/warpUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} $< ${FLAGS64} -o $@
	
${BUILD_DIR}/Transcaling.o: SSM/Transcaling.cc SSM/Transcaling.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Utilities/warpUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} $< ${FLAGS64} -o $@	
	
${BUILD_DIR}/Translation.o: SSM/Translation.cc SSM/Translation.h SSM/StateSpaceModel.h SSM/ProjectiveBase.h Utilities/warpUtils.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${TRANS_FLAGS} $< ${FLAGS64} -o $@