#ifndef _INC_RANDOMINPUTBASE_
#define _INC_RANDOMINPUTBASE_

#include <stdio.h>
#include <stdlib.h>

#include "opencv2/core/core.hpp"

#define NCHANNELS 3

#define SRC_VID 'm'
#define SRC_USB_CAM 'u'
#define SRC_DIG_CAM 'f'
#define SRC_IMG 'j'

#define VID_FNAME "nl_bookI_s3"
#define VID_FMT "mpg"

#define IMG_FOLDER "nl_bookI_s3"
#define IMG_FMT "jpg"

#define ROOT_FOLDER "../../Datasets/TMT"

#define DIG_DEV_NAME "firewire_cam"
#define DIG_DEV_PATH "/dev/fw1"
#define DIG_DEV_FORMAT "i1V1f7o3r150"

#define USB_DEV_NAME "usb_cam"
#define USB_DEV_PATH "/dev/video0"
#define USB_DEV_FORMAT "I1B4r0N0"

#define CV_CAM_ID 0

#define MAX_PATH_SIZE 500

using namespace std;

class InputBase {

public:
	vector<cv::Mat> cv_buffer;

	int init_success;

	int img_width;
	int img_height;
	int n_buffers;
	int n_channels;
	int buffer_id;

	int n_frames;

	char file_path[MAX_PATH_SIZE];
	char *dev_name = nullptr;
	char *dev_fmt = nullptr;
	char *dev_path = nullptr;


	InputBase(char img_source = SRC_VID, char* _dev_name = nullptr, char *_dev_fmt = nullptr,
		char* _dev_path = nullptr, int _n_buffers = 1){

		init_success = 0;	

		dev_name = _dev_name;
		dev_fmt = _dev_fmt;
		dev_path = _dev_path;

		n_buffers = _n_buffers;

		fprintf(stdout, "Starting InputBase constructor\n");
		fprintf(stdout, "img_source=%c\n", img_source);

		/*fprintf(stdout, "before: dev_name=%d\n", dev_name);
		fprintf(stdout, "before: dev_fmt=%d\n", dev_fmt);*/

		n_frames = 0;

		switch(img_source) {
		case SRC_VID: {
			if(!dev_fmt){
				dev_fmt = static_cast<char*>(VID_FMT);
			}
			if(!dev_name){
				dev_name = static_cast<char*>(VID_FNAME);
			}
			if(!dev_path){
				dev_path = static_cast<char*>(ROOT_FOLDER);
			}
			snprintf(file_path, MAX_PATH_SIZE, "%s/%s.%s", dev_path, dev_name, dev_fmt);
			break;
		}
		case SRC_IMG: {
			if(!dev_fmt){
				dev_fmt = static_cast<char*>(IMG_FMT);
			}
			if(!dev_name){
				dev_name = static_cast<char*>(IMG_FOLDER);
			}
			if(!dev_path){
				dev_path = static_cast<char*>(ROOT_FOLDER);
			}
			snprintf(file_path, MAX_PATH_SIZE, "%s/%s/frame%%05d.%s", dev_path, dev_name, dev_fmt);
			n_frames = getNumberOfFrames(file_path);
			break;
		}
		case SRC_USB_CAM: {
			if(!dev_fmt){
				dev_fmt = static_cast<char*>(USB_DEV_FORMAT);
			}
			if(!dev_name){
				dev_name = static_cast<char*>(USB_DEV_NAME);
			}
			if(!dev_path){
				dev_path = static_cast<char*>(USB_DEV_PATH);
			}
			break;
		}
		case SRC_DIG_CAM: {
			if(!dev_fmt){
				dev_fmt = static_cast<char*>(DIG_DEV_FORMAT);
			}
			if(!dev_name){
				dev_name = static_cast<char*>(DIG_DEV_NAME);
			}
			if(!dev_path){
				dev_path = static_cast<char*>(DIG_DEV_PATH);
			}
			break;
		}
		default: {
			fprintf(stdout, "Invalid image source provided\n");
			exit(0);
		}
		}
		fprintf(stdout, "dev_name=%s\n", dev_name);
		fprintf(stdout, "dev_path=%s\n", dev_path);
		fprintf(stdout, "dev_fmt=%s\n", dev_fmt);
	}

	virtual ~InputBase(){}
	virtual bool updateFrame() = 0;
	virtual void remapBuffer(uchar **new_addr) = 0;
	virtual int getFameID() = 0;
	virtual const cv::Mat& getFrame() = 0;
	virtual cv::Mat& getFrameMutable() = 0;

	int getNumberOfFrames(char *file_template){
		FILE* fid;
		int frame_id = 0;
		char fname[MAX_PATH_SIZE];
		snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
		while(fid = fopen(fname, "r")){
			fclose(fid);
			snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
		}
		//printf("Could not find file: %s\n", fname);
		return frame_id - 1;
	}
};
#endif