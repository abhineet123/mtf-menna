#ifndef INPUT_CV
#define INPUT_CV

#include "inputBase.h"

class InputCV : public InputBase {

public:
	cv::VideoCapture *cap_obj = nullptr;
	InputCV(char img_source = SRC_VID,
		char *dev_name_in = nullptr, char *dev_fmt_in = nullptr, char* dev_path_in = nullptr,
		int n_buffers = 1, int img_type = CV_8UC3);
	~InputCV();
	void updateFrame() override;
	void remapBuffer(uchar** new_addr) override;
};

#endif



