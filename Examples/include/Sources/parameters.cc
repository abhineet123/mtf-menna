#include "parameters.h"

int readParams(char* fname){
	FILE *fid = fopen(fname, "r");
	if(!fid){
		printf("\nerror in readParams: input file could not be opened: %s\n", fname);
		exit(0);
	}
	int arg_id = 0;
	while(!feof(fid)){
		char *temp = new char[500];
		fgets(temp, 500, fid);
		strtok(temp, "\n");
		strtok(temp, "\r");
		if(strlen(temp) <= 1 || temp[0] == '#'){
			delete(temp);
			continue;
		}
		fargv[++arg_id] = temp;
		//printf("arg %d: %s\n", arg_id, fargv[arg_id]);
	}
	fclose(fid);
	return arg_id + 1;
}

void processStringParam(char* &str_out, const char* param){
	//printf("-----------------------\n");
	//printf("param: %s\n", param);
	//printf("use_default: %c\n", use_default);
	if(!strcmp(param, "#")){
		// use default value
		return;
	}
	//if((strlen(param) == 2) && (param[0] == '0') && (param[1] == '0')){
	//	return;
	//}
	str_out = new char[strlen(param) + 1];
	strcpy(str_out, param);
	//printf("str_out: %s\n", str_out);
}
// convert a string of comma (or other specified character) separated values 
// into a vector of doubles
std::vector<double> atof_arr(char *str, char sep){
	std::string str_temp(str);
	printf("atof_arr::str: %s\n", str);
	printf("atof_arr::str_temp: %s\n", str_temp.c_str());
	for(int i = 0; i < str_temp.length(); i++){
		if(str_temp[i] == sep)
			str_temp[i] = ' ';
	}
	std::vector<double> param_arr;
	std::stringstream ss(str);
	double temp;
	while(ss >> temp)
		param_arr.push_back(temp);
	return param_arr;
}

void processAgrument(char *arg_name, char *arg_val){
	if(!strcmp(arg_name, "n_trackers")){
		n_trackers = atoi(arg_val);
	} else if(!strcmp(arg_name, "source_id")){
		source_id = atoi(arg_val);
	} else if(!strcmp(arg_name, "actor_id")){
		actor_id = atoi(arg_val);
	} else if(!strcmp(arg_name, "pipeline")){
		pipeline = arg_val[0];
	} else if(!strcmp(arg_name, "img_source")){
		img_source = arg_val[0];
	} else if(!strcmp(arg_name, "write_frame_data")){
		write_frame_data = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "write_tracking_data")){
		write_tracking_data = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "tracking_data_fname")){
		processStringParam(tracking_data_fname, arg_val);
	} else if(!strcmp(arg_name, "write_pts")){
		write_pts = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "show_cv_window")){
		show_cv_window = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "show_xv_window")){
		show_xv_window = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "read_obj_from_gt")){
		read_obj_from_gt = atoi(arg_val);
	} else if(!strcmp(arg_name, "read_objs")){
		read_objs = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "write_objs")){
		write_objs = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "record_frames")){
		record_frames = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "source_name")){
		processStringParam(source_name, arg_val);
		/*			source_name = new char[strlen(arg_val)+1];
		strcpy(source_name, arg_val);*/
	} else if(!strcmp(arg_name, "source_path")){
		processStringParam(source_path, arg_val);
	} else if(!strcmp(arg_name, "patch_size")){
		patch_size = atoi(arg_val);
	} else if(!strcmp(arg_name, "read_obj_fname")){
		processStringParam(read_obj_fname, arg_val);
	} else if(!strcmp(arg_name, "write_obj_fname")){
		processStringParam(write_obj_fname, arg_val);
	} else if(!strcmp(arg_name, "source_fmt")){
		processStringParam(source_fmt, arg_val);
	} else if(!strcmp(arg_name, "root_path")){
		processStringParam(root_path, arg_val);
	} else if(!strcmp(arg_name, "grid_size_x")){
		grid_size_x = atoi(arg_val);
	} else if(!strcmp(arg_name, "grid_size_y")){
		grid_size_y = atoi(arg_val);
	} else if(!strcmp(arg_name, "adjust_grid")){
		adjust_grid = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "adjust_lines")){
		adjust_lines = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "reset_pos")){
		reset_pos = atoi(arg_val);
	} else if(!strcmp(arg_name, "reset_template")){
		reset_template = atoi(arg_val);
	} else if(!strcmp(arg_name, "reset_wts")){
		reset_wts = atoi(arg_val);
	} else if(!strcmp(arg_name, "pause_after_line")){
		pause_after_line = atoi(arg_val);
	} else if(!strcmp(arg_name, "debug_mode")){
		debug_mode = atoi(arg_val);
	} else if(!strcmp(arg_name, "use_constant_slope")){
		use_constant_slope = atoi(arg_val);
	} else if(!strcmp(arg_name, "use_ls")){
		use_ls = atoi(arg_val);
	} else if(!strcmp(arg_name, "inter_alpha_thresh")){
		inter_alpha_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "intra_alpha_thresh")){
		intra_alpha_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "pause_after_frame")){
		pause_after_frame = atoi(arg_val);
	} else if(!strcmp(arg_name, "show_tracked_pts")){
		show_tracked_pts = atoi(arg_val);
	} else if(!strcmp(arg_name, "show_warped_img")){
		show_warped_img = atoi(arg_val);
	} else if(!strcmp(arg_name, "show_template_img")){
		show_template_img = atoi(arg_val);
	} else if(!strcmp(arg_name, "write_tracker_states")){
		write_tracker_states = atoi(arg_val);
	} else if(!strcmp(arg_name, "update_wts")){
		update_wts = atoi(arg_val);
	} else if(!strcmp(arg_name, "sel_reset_thresh")){
		sel_reset_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "res_from_size")){
		res_from_size = arg_val[0] - '0';
	} else if(!strcmp(arg_name, "mtf_res")){
		mtf_res = atoi(arg_val);
	} else if(!strcmp(arg_name, "resx")){
		resx = atoi(arg_val);
	} else if(!strcmp(arg_name, "resy")){
		resy = atoi(arg_val);
	} else if(!strcmp(arg_name, "upd_thresh")){
		upd_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "max_iters")){
		max_iters = atoi(arg_val);
	} else if(!strcmp(arg_name, "mtf_sm")){
		processStringParam(mtf_sm, arg_val);
		//mtf_sm = new char[strlen(arg_val)+1];
		//strcpy(mtf_sm, arg_val);
	} else if(!strcmp(arg_name, "mtf_am")){
		processStringParam(mtf_am, arg_val);
		//mtf_am = new char[strlen(arg_val)+1];
		//strcpy(mtf_am, arg_val);
	} else if(!strcmp(arg_name, "mtf_ssm")){
		processStringParam(mtf_ssm, arg_val);
		//mtf_ssm = new char[strlen(arg_val)+1];
		//strcpy(mtf_ssm, arg_val);
	} else if(!strcmp(arg_name, "rec_init_err_grad")){
		rec_init_err_grad = atoi(arg_val);
	} else if(!strcmp(arg_name, "buffer_count")){
		buffer_count = atoi(arg_val);
	} else if(!strcmp(arg_name, "norm_pix_max")){
		norm_pix_max = atof(arg_val);
	} else if(!strcmp(arg_name, "norm_pix_min")){
		norm_pix_min = atof(arg_val);
	} else if(!strcmp(arg_name, "init_frame_id")){
		init_frame_id = atoi(arg_val);
	} else if(!strcmp(arg_name, "end_frame_id")){
		end_frame_id = atoi(arg_val);
	} else if(!strcmp(arg_name, "show_tracking_error")){
		show_tracking_error = atoi(arg_val);
	} else if(!strcmp(arg_name, "update_templ")){
		update_templ = atoi(arg_val);
	} else if(!strcmp(arg_name, "grad_eps")){
		grad_eps = strtod(arg_val, nullptr);
	} else if(!strcmp(arg_name, "hess_eps")){
		hess_eps = strtod(arg_val, nullptr);
	}
	// Homography
	else if(!strcmp(arg_name, "hom_direct_samples")){
		hom_direct_samples = atoi(arg_val);
	}
	// SCV
	else if(!strcmp(arg_name, "scv_use_bspl")){
		scv_use_bspl = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_n_bins")){
		scv_n_bins = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_preseed")){
		scv_preseed = atof(arg_val);
	} else if(!strcmp(arg_name, "scv_pou")){
		scv_pou = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_wt_map")){
		scv_wt_map = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_map_grad")){
		scv_map_grad = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_affine_mapping")){
		scv_affine_mapping = atoi(arg_val);
	} else if(!strcmp(arg_name, "scv_once_per_frame")){
		scv_once_per_frame = atoi(arg_val);
	}

	// LSCV
	else if(!strcmp(arg_name, "lscv_sub_regions")){
		lscv_sub_regions = atoi(arg_val);
	} else if(!strcmp(arg_name, "lscv_spacing")){
		lscv_spacing = atoi(arg_val);
	} else if(!strcmp(arg_name, "lscv_show_subregions")){
		lscv_show_subregions = atoi(arg_val);
	}

	// LKLD
	else if(!strcmp(arg_name, "lkld_pre_seed")){
		lkld_pre_seed = atof(arg_val);
	} else if(!strcmp(arg_name, "lkld_pou")){
		lkld_pou = atoi(arg_val);
	} else if(!strcmp(arg_name, "lkld_n_bins")){
		lkld_n_bins = atoi(arg_val);
	} else if(!strcmp(arg_name, "lkld_sub_regions")){
		lkld_sub_regions = atoi(arg_val);
	} else if(!strcmp(arg_name, "lkld_spacing")){
		lkld_spacing = atoi(arg_val);
	}

	// SSIM
	else if(!strcmp(arg_name, "ssim_pix_proc_type")){
		ssim_pix_proc_type = atoi(arg_val);
	} else if(!strcmp(arg_name, "ssim_k1")){
		ssim_k1 = atof(arg_val);
	} else if(!strcmp(arg_name, "ssim_k2")){
		ssim_k2 = atof(arg_val);
	}

	else if(!strcmp(arg_name, "normalized_init")){
		normalized_init = atoi(arg_val);
	} else if(!strcmp(arg_name, "hess_type")){
		hess_type = atoi(arg_val);
	} else if(!strcmp(arg_name, "sec_ord_hess")){
		sec_ord_hess = atoi(arg_val);
	} else if(!strcmp(arg_name, "jac_type")){
		jac_type = atoi(arg_val);
	}

	// MI and CCRE
	else if(!strcmp(arg_name, "mi_pre_seed")){
		mi_pre_seed = atof(arg_val);
	} else if(!strcmp(arg_name, "mi_pou")){
		mi_pou = atoi(arg_val);
	} else if(!strcmp(arg_name, "mi_rscv_mapping")){
		mi_rscv_mapping = atoi(arg_val);
	} else if(!strcmp(arg_name, "mi_n_bins")){
		mi_n_bins = atoi(arg_val);
	}
	// CCRE
	else if(!strcmp(arg_name, "ccre_symmetrical_grad")){
		ccre_symmetrical_grad = atoi(arg_val);
	} else if(!strcmp(arg_name, "ccre_n_blocks")){
		ccre_n_blocks = atoi(arg_val);
	}


	// diagnostics
	else if(!strcmp(arg_name, "diag_am")){
		processStringParam(diag_am, arg_val);
	} else if(!strcmp(arg_name, "diag_ssm")){
		processStringParam(diag_ssm, arg_val);
	} else if(!strcmp(arg_name, "diag_range")){
		diag_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_trans_range")){
		diag_trans_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_rot_range")){
		diag_rot_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_scale_range")){
		diag_scale_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_shear_range")){
		diag_shear_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_proj_range")){
		diag_proj_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_hom_range")){
		diag_hom_range = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_res")){
		diag_res = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_update")){
		diag_update = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_grad_diff")){
		diag_grad_diff = atof(arg_val);
	} else if(!strcmp(arg_name, "diag_gen_norm")){
		processStringParam(diag_gen_norm, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_jac")){
		processStringParam(diag_gen_jac, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_hess")){
		processStringParam(diag_gen_hess, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_hess2")){
		processStringParam(diag_gen_hess2, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_hess_sum")){
		processStringParam(diag_gen_hess_sum, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_num")){
		processStringParam(diag_gen_num, arg_val);
	} else if(!strcmp(arg_name, "diag_gen_ssm")){
		processStringParam(diag_gen_ssm, arg_val);
	} else if(!strcmp(arg_name, "diag_bin")){
		diag_bin = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_inv")){
		diag_inv = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_frame_gap")){
		diag_frame_gap = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_show_corners")){
		diag_show_corners = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_show_patches")){
		diag_show_patches = atoi(arg_val);
	} else if(!strcmp(arg_name, "diag_verbose")){
		diag_verbose = atoi(arg_val);
	}

	//Selective Pixel Integration
	else if(!strcmp(arg_name, "spi_enable")){
		spi_enable = atoi(arg_val);
	} else if(!strcmp(arg_name, "spi_thresh")){
		spi_thresh = atof(arg_val);
	}

	else if(!strcmp(arg_name, "pix_mapper")){
		processStringParam(pix_mapper, arg_val);;
	}
	// NN and GNN
	else if(!strcmp(arg_name, "nn_max_iters")){
		nn_max_iters = atoi(arg_val);
	} else if(!strcmp(arg_name, "nn_n_samples")){
		nn_n_samples = atoi(arg_val);
	} else if(!strcmp(arg_name, "nn_ssm_sigma_prec")){
		nn_ssm_sigma_prec = atof(arg_val);
	} else if(!strcmp(arg_name, "nn_additive_update")){
		nn_additive_update = atoi(arg_val);
	} else if(!strcmp(arg_name, "nn_direct_samples")){
		nn_direct_samples = atoi(arg_val);
	} else if(!strcmp(arg_name, "nn_ssm_sigma")){
		nn_ssm_sigma = atof_arr(arg_val, ',');
	} else if(!strcmp(arg_name, "nn_corner_sigma_d")){
		nn_corner_sigma_d = atof(arg_val);
	} else if(!strcmp(arg_name, "nn_corner_sigma_t")){
		nn_corner_sigma_t = atof(arg_val);
	} else if(!strcmp(arg_name, "nn_pix_sigma")){
		nn_pix_sigma = atof(arg_val);
	} else if(!strcmp(arg_name, "nn_n_trees")){
		nn_n_trees = atoi(arg_val);
	} else if(!strcmp(arg_name, "nn_n_checks")){
		nn_n_checks = atoi(arg_val);
	} else if(!strcmp(arg_name, "reinit_from_gt")){
		reinit_from_gt = atoi(arg_val);
	} else if(!strcmp(arg_name, "err_thresh")){
		err_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "nn_index_type")){
		nn_index_type = atoi(arg_val);
	}
	// GNN
	else if(!strcmp(arg_name, "gnn_k")){
		gnn_k = atoi(arg_val);
	}

	// Cascade
	if(!strcmp(arg_name, "casc_n_trackers")){
		casc_n_trackers = atoi(arg_val);
	} else 	if(!strcmp(arg_name, "casc_enable_feedback")){
		casc_enable_feedback = atoi(arg_val);
	}
	// Grid
	else if(!strcmp(arg_name, "gt_sm")){
		processStringParam(gt_sm, arg_val);
	} else 	if(!strcmp(arg_name, "gt_am")){
		processStringParam(gt_am, arg_val);
	} else 	if(!strcmp(arg_name, "gt_ssm")){
		processStringParam(gt_ssm, arg_val);
	} else if(!strcmp(arg_name, "gt_grid_res")){
		gt_grid_res = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_patch_size")){
		gt_patch_size = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_estimation_method")){
		gt_estimation_method = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_ransac_reproj_thresh")){
		gt_ransac_reproj_thresh = atof(arg_val);
	} else if(!strcmp(arg_name, "gt_init_at_each_frame")){
		gt_init_at_each_frame = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_dyn_patch_size")){
		gt_dyn_patch_size = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_show_trackers")){
		gt_show_trackers = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_show_tracker_edges")){
		gt_show_tracker_edges = atoi(arg_val);
	} else if(!strcmp(arg_name, "gt_use_tbb")){
		gt_use_tbb = atoi(arg_val);
	}
	// RKLT
	else if(!strcmp(arg_name, "rkl_sm")){
		processStringParam(rkl_sm, arg_val);
	} else if(!strcmp(arg_name, "rkl_enable_spi")){
		rkl_enable_spi = atoi(arg_val);
	} else if(!strcmp(arg_name, "rkl_enable_feedback")){
		rkl_enable_feedback = atoi(arg_val);
	} else if(!strcmp(arg_name, "rkl_failure_detection")){
		rkl_failure_detection = atoi(arg_val);
	} else if(!strcmp(arg_name, "rkl_failure_thresh")){
		rkl_failure_thresh = atof(arg_val);
	}
	// Gradient Descent
	else if(!strcmp(arg_name, "gd_learning_rate")){
		gd_learning_rate = atof(arg_val);
	}
	// Particle Filter
	else if(!strcmp(arg_name, "pf_n_particles")){
		pf_n_particles = atoi(arg_val);
	} else if(!strcmp(arg_name, "pf_dyn_model")){
		pf_dyn_model = atoi(arg_val);
	} else if(!strcmp(arg_name, "pf_enable_resampling")){
		pf_enable_resampling = atoi(arg_val);
	} else if(!strcmp(arg_name, "pf_reset_to_mean")){
		pf_reset_to_mean = atoi(arg_val);
	} else if(!strcmp(arg_name, "pf_mean_of_corners")){
		pf_mean_of_corners = atoi(arg_val);
	} else if(!strcmp(arg_name, "pf_ssm_sigma")){
		pf_ssm_sigma = atof_arr(arg_val, ',');
		printf("pf_ssm_sigma: \n");
		for(int state_id = 0; state_id < pf_ssm_sigma.size(); state_id++){
			printf("%f\t", pf_ssm_sigma[state_id]);
		}
		printf("\n");
	} else if(!strcmp(arg_name, "pf_measurement_sigma")){
		pf_measurement_sigma = atof(arg_val);
	} else if(!strcmp(arg_name, "pf_pix_sigma")){
		pf_pix_sigma = atof(arg_val);
	}
	// CMT
	else if(!strcmp(arg_name, "cmt_estimate_scale")){
		cmt_estimate_scale = atoi(arg_val);
	} else if(!strcmp(arg_name, "cmt_estimate_rotation")){
		cmt_estimate_rotation = atoi(arg_val);
	} else if(!strcmp(arg_name, "cmt_feat_detector")){
		processStringParam(cmt_feat_detector, arg_val);
	} else if(!strcmp(arg_name, "cmt_desc_extractor")){
		processStringParam(cmt_desc_extractor, arg_val);
	} else if(!strcmp(arg_name, "cmt_resize_factor")){
		cmt_resize_factor = atof(arg_val);
	}
	// DSST
	else if(!strcmp(arg_name, "dsst_resize_factor")){
		dsst_resize_factor = atoi(arg_val);
	} else if(!strcmp(arg_name, "dsst_is_scaling")){
		dsst_is_scaling = atoi(arg_val);
	} else if(!strcmp(arg_name, "dsst_bin_size")){
		dsst_bin_size = atoi(arg_val);
	}
	//TLD
	else if(!strcmp(arg_name, "tld_detector_enabled")){
		tld_detector_enabled = atoi(arg_val);
	} else if(!strcmp(arg_name, "tld_learning_enabled")){
		tld_learning_enabled = atoi(arg_val);
	} else if(!strcmp(arg_name, "tld_tracker_enabled")){
		tld_tracker_enabled = atoi(arg_val);
	} else if(!strcmp(arg_name, "tld_alternating")){
		tld_alternating = atoi(arg_val);
	}

	else if(!strcmp(arg_name, "rct_min_n_rect")){
		rct_min_n_rect = atoi(arg_val);
	} else if(!strcmp(arg_name, "rct_max_n_rect")){
		rct_max_n_rect = atoi(arg_val);
	} else if(!strcmp(arg_name, "rct_n_feat")){
		rct_n_feat = atoi(arg_val);
	} else if(!strcmp(arg_name, "rct_rad_outer_pos")){
		rct_rad_outer_pos = atoi(arg_val);
	} else if(!strcmp(arg_name, "rct_rad_search_win")){
		rct_rad_search_win = atoi(arg_val);
	} else if(!strcmp(arg_name, "rct_learning_rate")){
		rct_learning_rate = atof(arg_val);
	}
}

FILE* readTrackerParams(FILE * fid, int print_args){
	// reads parameters for a single tracker in a multi tracker setup
	// a blank line signals the end of the current tracker's parameters
	// a line starting with # is treated as comment and ignored
	if(!fid){
		char *fname = "Tools/multi.cfg";
		printf("Opening file: %s...\n", fname);
		if(!(fid = fopen(fname, "r"))){
			printf("readTrackerParams :: Error: File could not be opened\n");
			return nullptr;
		}
	}
	char curr_line[500];
	char arg_name[500], arg_val[500];
	while(!feof(fid)){
		fgets(curr_line, 500, fid);
		strtok(curr_line, "\n");
		strtok(curr_line, "\r");
		// ignore comments 
		if(curr_line[0] == '#')
			continue;
		// empty line signals the end of current tracker's parameters
		if(strlen(curr_line) <= 1)
			return fid;
		sscanf(curr_line, "%s%s", arg_name, arg_val);
		strtok(arg_name, "\n");
		strtok(arg_name, "\r");
		strtok(arg_val, "\n");
		strtok(arg_val, "\r");
		if(print_args)
			printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);
		processAgrument(arg_name, arg_val);
	}
	fclose(fid);
	return nullptr;
}

void parseArgumentPairs(char * argv[], int argc,
	int parse_type, int print_args){
	// parse_type = 0 means that each element of argv contains 
	// an argument's name and its value separated by a space
	// parse_type = 1 means that consecutive elements of argv contain 
	// the name and value of each argument

	//printf("Parsing %d argument pairs with argv=%d...\n", argc, argv);
	char arg_name[500], arg_val[500];
	int max_arg = parse_type ? argc / 2 + 1 : argc;
	printf("argc: %d max_arg: %d\n", argc, max_arg);
	for(int i = 1; i < max_arg; i++){
		//printf("i=%d\n", i);
		if(parse_type){
			sscanf(argv[2 * i - 1], "%s", arg_name);
			sscanf(argv[2 * i], "%s", arg_val);
		} else{
			sscanf(argv[i], "%s%s", arg_name, arg_val);
		}
		if(print_args)
			printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);
		strtok(arg_name, "\n");
		strtok(arg_name, "\r");
		strtok(arg_val, "\n");
		strtok(arg_val, "\r");
		processAgrument(arg_name, arg_val);
	}
}

void freeParams(){
	if(source_name){
		delete(source_name);
	}
	if(read_obj_fname){
		delete(read_obj_fname);
	}
	if(source_fmt){
		delete(source_fmt);
	}
	if(root_path){
		delete(root_path);
	}
}
