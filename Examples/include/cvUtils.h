#include<iostream>
#include<fstream>
#include<sstream>
#include <stdio.h>
#include <stdlib.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAX_HOVER_TEXT_SIZE 100

////using namespace cv;
using namespace std;

struct obj_struct {
	cv::Point min_point;
	cv::Point max_point;

	double size_x;
	double size_y;
	double pos_x;
	double pos_y;

	cv::Mat corners;

	obj_struct(){
		corners.create(2, 4, CV_64FC1);
		size_x = size_y = 0;
		pos_x = pos_y = 0;
	}
	void updateCornerMat(){
		corners.at<double>(0, 0) = min_point.x;
		corners.at<double>(0, 1) = max_point.x;
		corners.at<double>(0, 2) = max_point.x;
		corners.at<double>(0, 3) = min_point.x;
		corners.at<double>(1, 0) = min_point.y;
		corners.at<double>(1, 1) = min_point.y;
		corners.at<double>(1, 2) = max_point.y;
		corners.at<double>(1, 3) = max_point.y;
	}
	void updateCornerPoints(){
		min_point.x = corners.at<double>(0, 0);
		min_point.y = corners.at<double>(1, 0);
		max_point.x = corners.at<double>(0, 2);
		max_point.y = corners.at<double>(1, 2);
		size_x = max_point.x - min_point.x;
		size_y = max_point.y - min_point.y;
		pos_x = cv::mean(corners.row(0))[0];
		pos_y = cv::mean(corners.row(1))[0];
		//pos_x=(ulx+urx+llx+lrx)/4.0;
		//pos_y=(uly+ury+lly+lry)/4.0;
		//size_x = ((urx-ulx)+(lrx-llx))/2.0;
		//size_y = ((lly-uly)+(lry-ury))/2.0;
	}
};


int clicked_point_count;
cv::Point mouse_click_point;
bool point_selected;
bool left_button_clicked;
bool right_button_clicked;
cv::Point mouse_hover_point;
bool mouse_hover_event;
double hover_font_size = 0.50;
cv::Scalar hover_color(0, 255, 0);

/* callback function for mouse clicks */
inline void getClickedPoint(int mouse_event, int x, int y, int flags, void* param) {

	if(mouse_event == CV_EVENT_LBUTTONDOWN) {

		//cout<<"\nReceived a left click at "<<x<<"\t"<<y<<"\n";

		mouse_click_point.x = x;
		mouse_click_point.y = y;
		left_button_clicked = true;
		point_selected = true;

	} else if(mouse_event == CV_EVENT_RBUTTONDOWN) {

		//cout<<"\nReceived a right click at "<<x<<"\t"<<y<<"\n";

		point_selected = true;
		right_button_clicked = true;
	} else if(mouse_event == CV_EVENT_MOUSEMOVE) {

		mouse_hover_point.x = x;
		mouse_hover_point.y = y;
		mouse_hover_event = true;
	}
}


class CVUtils {
public:
	vector<cv::Scalar> obj_cols;
	int no_of_cols;
	vector<cv::Mat> ground_truth;

	CVUtils() {
		obj_cols.push_back(cv::Scalar(0, 0, 255));
		obj_cols.push_back(cv::Scalar(0, 255, 0));
		obj_cols.push_back(cv::Scalar(255, 0, 0));
		obj_cols.push_back(cv::Scalar(255, 255, 0));
		obj_cols.push_back(cv::Scalar(255, 0, 255));
		obj_cols.push_back(cv::Scalar(0, 255, 255));
		obj_cols.push_back(cv::Scalar(255, 255, 255));
		obj_cols.push_back(cv::Scalar(0, 0, 0));

		no_of_cols = obj_cols.size();
	}
	/*allows the user to select a rectangle by clicking on its opposite corners*/
	obj_struct* getRectObject(cv::Mat&selection_image, string selection_window, int col_id = 0,
		int line_thickness = 2, int patch_size = 0) {
		//cout<<"Start getObject\n";

		obj_struct *new_obj = new obj_struct;
		cv::Mat hover_image;
		cv::Scalar color(0, 0, 255);

		cv::setMouseCallback(selection_window, getClickedPoint, nullptr);
		imshow(selection_window, selection_image);

		clicked_point_count = 0;
		while(clicked_point_count < 2) {

			point_selected = false;
			while((!point_selected) && (!mouse_hover_event)) {
				//cout<<"Inside the loop\n";
				//cvWaitKey(1);
				int pressed_key = cv::waitKey(1);
				if(pressed_key == 27) {
					exit(0);
				}
			}
			if(left_button_clicked) {
				left_button_clicked = false;
				if(clicked_point_count == 0) {
					//printf("Adding min point: %d %d\n", mouse_click_point.x, mouse_click_point.y);
					if(patch_size > 0){
						new_obj->min_point.x = mouse_click_point.x - patch_size / 2.0;
						new_obj->min_point.y = mouse_click_point.y - patch_size / 2.0;
						new_obj->max_point.x = mouse_click_point.x + patch_size / 2.0;
						new_obj->max_point.y = mouse_click_point.y + patch_size / 2.0;
						break;
					} else{
						new_obj->min_point.x = mouse_click_point.x;
						new_obj->min_point.y = mouse_click_point.y;
					}

				} else if(clicked_point_count == 1) {
					//printf("Adding max point: %d %d\n", mouse_click_point.x, mouse_click_point.y);
					new_obj->max_point.x = mouse_click_point.x;
					new_obj->max_point.y = mouse_click_point.y;

					if(new_obj->min_point.x > new_obj->max_point.x) {
						int temp = new_obj->min_point.x;
						new_obj->min_point.x = new_obj->max_point.x;
						new_obj->max_point.x = temp;
					}
					if(new_obj->min_point.y > new_obj->max_point.y) {
						int temp = new_obj->min_point.y;
						new_obj->min_point.y = new_obj->max_point.y;
						new_obj->max_point.y = temp;
					}
					break;
				}
			} else if(mouse_hover_event) {
				mouse_hover_event = false;
				if((new_obj) && (clicked_point_count == 1)) {
					hover_image = selection_image.clone();
					int hover_width = abs(mouse_hover_point.x - new_obj->min_point.x);
					int hover_height = abs(mouse_hover_point.y - new_obj->min_point.y);
					char hover_text[MAX_HOVER_TEXT_SIZE];
					snprintf(hover_text, MAX_HOVER_TEXT_SIZE, "(%d, %d)", hover_width, hover_height);
					cv::rectangle(hover_image, new_obj->min_point, mouse_hover_point, obj_cols[col_id], line_thickness);
					cv::putText(hover_image, hover_text, mouse_hover_point, cv::FONT_HERSHEY_SIMPLEX, hover_font_size, hover_color);
					imshow(selection_window, hover_image);
				}
			}
			if(point_selected) {
				clicked_point_count++;
				//cout<<"\nclicked_point_count="<<clicked_point_count<<"\n";
			}
		}
		new_obj->size_x = abs(new_obj->max_point.x - new_obj->min_point.x);
		new_obj->size_y = abs(new_obj->max_point.y - new_obj->min_point.y);

		new_obj->pos_x = (new_obj->min_point.x + new_obj->max_point.x) / 2;
		new_obj->pos_y = (new_obj->min_point.y + new_obj->max_point.y) / 2;
		new_obj->updateCornerMat();

		//cout<<"Done getObject\n";
		return new_obj;
	}

	/*allows the user to select a quadrilateral by clicking on its 4 corners*/
	obj_struct* getQuadObject(cv::Mat&selection_image, string selection_window, int col_id = 0,
		int line_thickness = 2, int patch_size = 0) {
		//cout<<"Start getObject\n";

		obj_struct *new_obj = new obj_struct;
		cv::Mat hover_image;

		cv::Point clicked_pts[4];

		cv::setMouseCallback(selection_window, getClickedPoint, nullptr);
		imshow(selection_window, selection_image);

		clicked_point_count = 0;
		while(clicked_point_count < 4) {

			point_selected = false;
			while((!point_selected) && (!mouse_hover_event)) {
				//cout<<"Inside the loop\n";
				//cvWaitKey(1);
				int pressed_key = cv::waitKey(1);
				if(pressed_key == 27) {
					exit(0);
				}
			}
			if(left_button_clicked) {
				left_button_clicked = false;
				clicked_pts[clicked_point_count] = mouse_click_point;

				new_obj->corners.at<double>(0, clicked_point_count) = mouse_click_point.x;
				new_obj->corners.at<double>(1, clicked_point_count) = mouse_click_point.y;
				++clicked_point_count;
				if(clicked_point_count == 4){
					break;
				}

			} else if(mouse_hover_event) {
				mouse_hover_event = false;
				if(new_obj && (clicked_point_count > 0)) {
					hover_image = selection_image.clone();
					for(int pt_id = 0; pt_id < clicked_point_count - 1; ++pt_id){
						cv::line(hover_image, clicked_pts[pt_id], clicked_pts[pt_id + 1], obj_cols[col_id], line_thickness);
					}
					cv::line(hover_image, clicked_pts[clicked_point_count - 1], mouse_hover_point, obj_cols[col_id], line_thickness);
					if(clicked_point_count == 3){
						cv::line(hover_image, clicked_pts[0], mouse_hover_point, obj_cols[col_id], line_thickness);
					}
					imshow(selection_window, hover_image);
				}
			}
		}
		new_obj->min_point.x = new_obj->corners.at<double>(0, 0);
		new_obj->min_point.y = new_obj->corners.at<double>(1, 0);
		new_obj->max_point.x = new_obj->corners.at<double>(0, 2);
		new_obj->max_point.y = new_obj->corners.at<double>(1, 2);

		new_obj->size_x = ((new_obj->corners.at<double>(0, 1) - new_obj->corners.at<double>(0, 0)) +
			(new_obj->corners.at<double>(0, 2) - new_obj->corners.at<double>(0, 3))) / 2;
		new_obj->size_y = ((new_obj->corners.at<double>(1, 3) - new_obj->corners.at<double>(1, 0)) +
			(new_obj->corners.at<double>(1, 2) - new_obj->corners.at<double>(1, 1))) / 2;

		new_obj->pos_x = (new_obj->min_point.x + new_obj->max_point.x) / 2;
		new_obj->pos_y = (new_obj->min_point.y + new_obj->max_point.y) / 2;

		return new_obj;
	}

	vector<obj_struct*> getMultipleObjects(const cv::Mat &selection_image, int no_of_objs,
		int patch_size = 0, int line_thickness = 1, int write_objs = 0, bool sel_quad_obj = false,
		char* filename = "selected_objects.txt") {
		vector<obj_struct*> objects;
		cv::Mat temp_image = selection_image.clone();
		stringstream temp_stream;
		if(no_of_objs > 1) {
			temp_stream << "Please select " << no_of_objs << " objects to track";
			if(patch_size > 0){
				temp_stream << " by clicking at the center of each to add a " << patch_size << "x" << patch_size << " patch";
			} else if(sel_quad_obj){
				temp_stream << " by clicking at the four corners of each";
			} else{
				temp_stream << " by clicking at the two opposite corners of each";
			}
		} else {
			temp_stream << "Please select the object to track";
			if(patch_size > 0){
				temp_stream << " by clicking at its center to add a " << patch_size << "x" << patch_size << " patch";
			} else if(sel_quad_obj){
				temp_stream << " by clicking at its four corners";
			} else{
				temp_stream << " by clicking at its two opposite corners";
			}
		}
		string window_title = temp_stream.str();
		cv::namedWindow(window_title, 1);
		cv::Scalar color(0, 0, 255);

		for(int i = 0; i < no_of_objs; i++) {
			int col_id = i % no_of_cols;
			obj_struct *new_obj;
			if(sel_quad_obj){
				printf("selecting quadrilateral object...\n");
				new_obj = getQuadObject(temp_image, window_title, col_id, line_thickness, patch_size);
				for(int pt_id = 0; pt_id < 3; ++pt_id){
					cv::Point pt1(new_obj->corners.at<double>(0, pt_id), new_obj->corners.at<double>(1, pt_id));
					cv::Point pt2(new_obj->corners.at<double>(0, pt_id + 1), new_obj->corners.at<double>(1, pt_id + 1));
					cv::line(temp_image, pt1, pt2, color, line_thickness);
				}
				cv::Point pt1(new_obj->corners.at<double>(0, 0), new_obj->corners.at<double>(1, 0));
				cv::Point pt2(new_obj->corners.at<double>(0, 3), new_obj->corners.at<double>(1, 3));
				cv::line(temp_image, pt1, pt2, color, line_thickness);
			} else{
				new_obj = getRectObject(temp_image, window_title, col_id, line_thickness, patch_size);
				cv::rectangle(temp_image, new_obj->min_point, new_obj->max_point, obj_cols[col_id], line_thickness);
			}
			objects.push_back(new_obj);
		}
		cv::destroyWindow(window_title);
		if(write_objs){
			writeObjectsToFile(objects, no_of_objs, filename);
		}
		return objects;
	}

	void writeObjectsToFile(vector<obj_struct*> objs, int no_of_objs,
		char* filename = "sel_objs/selected_objects.txt"){
		ofstream fout;
		cout << "Writing object locations to file: " << filename << "\n";
		fout.open(filename, ios::out);
		if(!fout) {
			cout << "Could not open file for writing object locations.\n";
		}
		for(int i = 0; i < no_of_objs; i++) {
			obj_struct *obj = objs[i];
			fout << obj->max_point.x << "\t";
			fout << obj->max_point.y << "\t";
			fout << obj->min_point.x << "\t";
			fout << obj->min_point.y << "\t";
			fout << obj->size_x << "\t";
			fout << obj->size_y << "\n";
		}
		fout.close();
	}
	obj_struct* readObjectFromGT(string source_name, string source_path,
		int n_frames = 1, int init_frame_id = 0, int debug_mode = 0,
		bool use_opt_gt = false, string opt_gt_ssm = "2"){
		if(n_frames <= 0){
			printf("Error while reading objects from ground truth: n_frames: %d is too small\n", n_frames);
			return nullptr;
		}
		string gt_filename;
		if(use_opt_gt){
			gt_filename = source_path + "/OptGT/" + source_name + "_" + opt_gt_ssm + ".txt";
		} else{
			gt_filename = source_path + "/" + source_name + ".txt";
		}

		//char gt_filename[500];
		//snprintf(gt_filename, 500, "%s/%s.txt", source_path, source_name);
		ifstream fin;
		cout << "Reading object location from ground truth file: " << gt_filename << "\n";
		fin.open(gt_filename, ios::in);
		if(!fin) {
			printf("Could not open ground truth file for reading object location.\n");
			return nullptr;
		}
		char header[500];
		fin.getline(header, 500);
		//cout << "header: "<<header<<"\n";
		cout << "n_frames: " << n_frames << "\n";
		cout << "init_frame_id: " << init_frame_id << "\n";

		int frame_id;
		for(frame_id = 0; frame_id < n_frames; frame_id++) {
			fin >> header;
			if(!fin.good()){
				printf("Ground truth file has ended unexpectedly - only %d out of %d entries were read\n",
					frame_id + 1, n_frames);
				break;
			}
			//cout << "header2: "<<header<<"\n";
			float ulx, uly, urx, ury, lrx, lry, llx, lly;
			fin >> ulx >> uly >> urx >> ury >> lrx >> lry >> llx >> lly;

			if(!fin){
				printf("Invalid formatting in line %d of the ground truth file. Aborting...\n", frame_id + 1);
				cout << ulx << "\t" << uly << "\t" << urx << "\t" << ury << "\t" << lrx << "\t" << lry << "\t" << llx << "\t" << lly << "\n";
				return nullptr;
			}

			cv::Mat curr_gt(2, 4, CV_64FC1);
			curr_gt.at<double>(0, 0) = ulx;
			curr_gt.at<double>(0, 1) = urx;
			curr_gt.at<double>(0, 2) = lrx;
			curr_gt.at<double>(0, 3) = llx;
			curr_gt.at<double>(1, 0) = uly;
			curr_gt.at<double>(1, 1) = ury;
			curr_gt.at<double>(1, 2) = lry;
			curr_gt.at<double>(1, 3) = lly;
			ground_truth.push_back(curr_gt);

			//if(debug_mode){
			//	cout<<"ulx: "<<ulx<<"\n";
			//	cout<<"uly: "<<uly<<"\n";
			//	cout<<"urx: "<<urx<<"\n";
			//	cout<<"ury: "<<ury<<"\n";
			//	cout<<"lrx: "<<lrx<<"\n";
			//	cout<<"lry: "<<lry<<"\n";
			//	cout<<"llx: "<<llx<<"\n";
			//	cout<<"lly: "<<lly<<"\n";
			//}
			if(frame_id == init_frame_id){
				//obj->corners.at<double>(0, 0)=ulx;
				//obj->corners.at<double>(0, 1)=urx;
				//obj->corners.at<double>(0, 2)=lrx;
				//obj->corners.at<double>(0, 3)=llx;
				//obj->corners.at<double>(1, 0)=uly;
				//obj->corners.at<double>(1, 1)=ury;
				//obj->corners.at<double>(1, 2)=lry;
				//obj->corners.at<double>(1, 3)=lly;
				//obj->updateCornerPoints();
				//objs.push_back(obj);
				//if(debug_mode){
				//	cout<<"min_x: "<<obj->min_point.x<<"\n";
				//	cout<<"min_y: "<<obj->min_point.y<<"\n";
				//	cout<<"max_x: "<<obj->max_point.x<<"\n";
				//	cout<<"max_y: "<<obj->max_point.y<<"\n";

				//	cout<<"pos_x: "<<obj->pos_x<<"\n";
				//	cout<<"pos_y: "<<obj->pos_y<<"\n";
				//	cout<<"size_x: "<<obj->size_x<<"\n";
				//	cout<<"size_y: "<<obj->size_y<<"\n";
				//}
			}
		}
		fin.close();

		if(init_frame_id >= frame_id){
			printf("Error while reading objects from ground truth: init_frame_id: %d is larger than n_frames: %d\n",
				init_frame_id, n_frames);
			return nullptr;
		}

		obj_struct *obj = new obj_struct;
		obj->corners = ground_truth[init_frame_id];
		obj->updateCornerPoints();
		//printf("Done reading objects from ground truth\n");
		return obj;
	}

	vector<obj_struct*> readObjectsFromFile(int no_of_objs,
		char* filename = "sel_objs/selected_objects.txt", int debug_mode = 0){
		vector<obj_struct*> objs;
		obj_struct *obj = nullptr;
		ifstream fin;
		cout << "Reading object locations from file: " << filename << "\n";
		fin.open(filename, ios::in);
		if(!fin) {
			printf("Could not open file for reading object locations.\n");
			return objs;
		}
		for(int i = 0; i < no_of_objs; i++) {
			obj = new obj_struct;
			fin >> obj->max_point.x;
			fin >> obj->max_point.y;
			fin >> obj->min_point.x;
			fin >> obj->min_point.y;
			fin >> obj->size_x;
			fin >> obj->size_y;

			obj->pos_x = (obj->min_point.x + obj->max_point.x) / 2;
			obj->pos_y = (obj->min_point.y + obj->max_point.y) / 2;

			obj->updateCornerMat();

			objs.push_back(obj);

			//if(debug_mode){
			//	cout<<"Object "<<i<<":\n\t";
			//	cout<<"pos_x="<<obj->pos_x<<"\n\t";
			//	cout<<"pos_y="<<obj->pos_y<<"\n\t";
			//	cout<<"size_x="<<obj->size_x<<"\n\t";
			//	cout<<"size_y="<<obj->size_y<<"\n";
			//}
		}
		return objs;

	}
	cv::Point getMeanPoint(cv::Point *pt_array, int no_of_points){
		long sum_x = 0;
		long sum_y = 0;
		for(int i = 0; i < no_of_points; i++){
			sum_x += pt_array[i].x;
			sum_y += pt_array[i].y;
		}
		return cv::Point(sum_x / no_of_points, sum_y / no_of_points);
	}

	void cornersToPoint2D(cv::Point2d(&cv_corners)[4], const cv::Mat &cv_corners_mat) {
		for(int corner_id = 0; corner_id < 4; corner_id++) {
			cv_corners[corner_id].x = cv_corners_mat.at<double>(0, corner_id);
			cv_corners[corner_id].y = cv_corners_mat.at<double>(1, corner_id);
		}
	}
};

