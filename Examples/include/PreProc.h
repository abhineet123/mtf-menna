#ifndef PRE_PROC_H
#define PRE_PROC_H
// some basic functions for preprocessing the raw input image before using them for tracking
struct PreProc{
	PreProc(){}
	virtual ~PreProc(){}
	virtual void apply(cv::Mat &img_gs) const = 0;
};

struct GaussianSmoothing : public PreProc{
	cv::Size kernel_size;
	double sigma_x;
	double sigma_y;
	GaussianSmoothing(int _kernel_size = 5, 
		double _sigma_x = 3.0, double _sigma_y = 0) :
		PreProc(){
		kernel_size = cv::Size(_kernel_size, _kernel_size);
		sigma_x = _sigma_x;
		sigma_y = _sigma_y;
	}
	void apply(cv::Mat &img_gs) const override{
		cv::GaussianBlur(img_gs, img_gs, kernel_size, sigma_x, sigma_y);
	}
};

#endif