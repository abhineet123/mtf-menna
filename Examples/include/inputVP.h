#ifndef INPUT_VP
#define INPUT_VP

#include "inputBase.h"

class InputVP : public InputBase {

public:
	cv::VideoCapture cap_obj;
	int frame_id;


	InputVP(char img_source = SRC_VID,
		char *dev_name_in = nullptr, char *dev_fmt_in = nullptr,
		char* dev_path_in = nullptr,
		int n_buffers = 1, int img_type = CV_8UC3) :
		InputBase(img_source, dev_name_in, dev_fmt_in, dev_path_in, n_buffers){
		fprintf(stdout, "Starting InputVP constructor\n");

		n_channels = 3;

		if(img_source == SRC_VID || img_source == SRC_IMG) {
			if(img_source == SRC_VID){
				fprintf(stdout, "Opening video file: %s\n", file_path);
				//n_frames = cap_obj.get(CV_CAP_PROP_FRAME_COUNT);
			} else{
				fprintf(stdout, "Opening jpeg files at %s with %d frames\n", file_path, n_frames);
			}
			cap_obj.open(file_path);
			if(img_source == SRC_VID){
				n_frames = cap_obj.get(CV_CAP_PROP_FRAME_COUNT);
			}
		} else if(img_source == SRC_USB_CAM) {
			cap_obj.open(atoi(dev_path));
		} else {
			std::cout << "==========Invalid source provided for OpenCV Pipeline==========\n";
			exit(0);
		}
		if(!(cap_obj.isOpened())) {
			printf("OpenCV stream could not be initialized successfully\n");
			return;
		} else{
			printf("OpenCV stream initialized successfully\n");
		}

		//Mat temp_frame;
		//*cap_obj>>temp_frame;
		//frames_captured++;

		//img_height=temp_frame.rows;
		//img_width=temp_frame.cols;

		img_height = cap_obj.get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width = cap_obj.get(CV_CAP_PROP_FRAME_WIDTH);

		/*img_height=cap_obj.get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width=cap_obj.get(CV_CAP_PROP_FRAME_WIDTH);*/

		fprintf(stdout, "InputVP :: img_height=%d\n", img_height);
		fprintf(stdout, "InputVP :: img_width=%d\n", img_width);

		cv_buffer.resize(n_buffers);

		for(int i = 0; i < n_buffers; i++){
			cv_buffer[i].create(img_height, img_width, img_type);
		}
		buffer_id = -1;

		frame_id = 0;
		buffer_id = (buffer_id + 1) % n_buffers;
		init_success = cap_obj.read(cv_buffer[buffer_id]);
	}

	~InputVP(){
		cv_buffer.clear();
		cap_obj.release();
	}

	const cv::Mat& getFrame() override{
		return cv_buffer[buffer_id];
	}
	cv::Mat& getFrameMutable() override{
		return cv_buffer[buffer_id];
	}
	bool updateFrame() override{
		buffer_id = (buffer_id + 1) % n_buffers;
		++frame_id;
		return cap_obj.read(cv_buffer[buffer_id]);
	}
	int getFameID() override{ return frame_id; }

	void remapBuffer(uchar** new_addr) override{
		for(int i = 0; i < n_buffers; i++){
			//printf("Remapping CV buffer %d to: %lu\n", i, (unsigned long)new_addr[i]);
			cv_buffer[i].data = new_addr[i];
		}
		buffer_id = -1;
		updateFrame();
	}


};

#endif



