set(MTF_EXEC_INSTALL_DIR /usr/local/bin CACHE PATH "Directory to install the executable")
set(MTF_PY_INSTALL_DIR ../../CModules/ CACHE PATH "Directory to install the Python interface module (normally the CModules sub directory of PTF)") 
set(MTF_PY_LIB_NAME pyMTF.so)


# set(WARNING_FLAGS -Wfatal-errors -Wno-write-strings -Wno-unused-result)
# set(CT_FLAGS -std=c++11)
# set(MTF_TOOLS inputCV inputBase cvUtils PreProc)
# addPrefixAndSuffix("${MTF_TOOLS}" "Tools/" ".h" MTF_TOOLS_HEADERS)
# message(STATUS "MTF_TOOLS_HEADERS: ${MTF_TOOLS_HEADERS}")

# set(MTF_INCLUDE_DIRS ${MTF_INCLUDE_DIRS} Examples/include PARENT_SCOPE)

find_package(Boost REQUIRED COMPONENTS filesystem system)
message(STATUS "Boost_LIBRARIES: ${Boost_LIBRARIES}")
# message(STATUS "Examples: MTF_RUNTIME_FLAGS: ${MTF_RUNTIME_FLAGS}")

add_executable(runMTF Examples/src/runMTF.cc)
add_custom_target(mtfe DEPENDS runMTF)
target_compile_options(runMTF PUBLIC ${MTF_RUNTIME_FLAGS})
target_include_directories(runMTF PUBLIC  Examples/include ${MTF_INCLUDE_DIRS} ${MTF_EXT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
target_link_libraries(runMTF mtf ${MTF_LIBS} ${Boost_LIBRARIES})
install(TARGETS runMTF RUNTIME DESTINATION ${MTF_EXEC_INSTALL_DIR} COMPONENT exe)
add_custom_target(install_exe
  ${CMAKE_COMMAND}
  -D "CMAKE_INSTALL_COMPONENT=exe"
  -P "${MTF_BINARY_DIR}/cmake_install.cmake"
  )
add_executable(trackUAVTrajectory Examples/src/trackUAVTrajectory.cc)
target_compile_options(trackUAVTrajectory PUBLIC ${MTF_RUNTIME_FLAGS})
target_include_directories(trackUAVTrajectory PUBLIC  Examples/include ${MTF_INCLUDE_DIRS} ${MTF_EXT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
target_link_libraries(trackUAVTrajectory mtf ${MTF_LIBS} ${Boost_LIBRARIES})
install(TARGETS trackUAVTrajectory RUNTIME DESTINATION ${MTF_EXEC_INSTALL_DIR} COMPONENT uav)
add_custom_target(mtfu DEPENDS trackUAVTrajectory)
add_custom_target(install_uav
  ${CMAKE_COMMAND}
  -D "CMAKE_INSTALL_COMPONENT=uav"
  -P "${MTF_BINARY_DIR}/cmake_install.cmake"
  )
find_package(PythonLibs)
if(PYTHONLIBS_FOUND)
	add_library (pyMTF MODULE Examples/src/pyMTF.cc)
	set_target_properties(pyMTF PROPERTIES PREFIX "")
	add_custom_target(mtfp DEPENDS pyMTF)
	target_compile_options(pyMTF PUBLIC ${MTF_RUNTIME_FLAGS})
	target_include_directories(pyMTF PUBLIC Examples/include ${MTF_INCLUDE_DIRS} ${MTF_EXT_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS})
	target_link_libraries(pyMTF mtf ${MTF_LIBS} ${PYTHON_LIBRARIES} ${PYTHON_LIBS} ${Boost_LIBRARIES})	
	install(TARGETS pyMTF LIBRARY DESTINATION ${MTF_PY_INSTALL_DIR} COMPONENT py)
	add_custom_target(install_py
	  ${CMAKE_COMMAND}
	  -D "CMAKE_INSTALL_COMPONENT=py"
	  -P "${MTF_BINARY_DIR}/cmake_install.cmake"
	  )
else(PYTHONLIBS_FOUND)
	message(STATUS "Python library not found so PyMTF is disabled")
endif(PYTHONLIBS_FOUND)

add_executable(testMTF Examples/src/testMTF.cc)
# add_custom_target(mtft DEPENDS testMTF)
target_compile_options(testMTF PUBLIC ${MTF_RUNTIME_FLAGS})
target_include_directories(testMTF PUBLIC Examples/include ${MTF_INCLUDE_DIRS} ${MTF_EXT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
target_link_libraries(testMTF mtf_test mtf ${MTF_LIBS} ${Boost_LIBRARIES})
install(TARGETS testMTF RUNTIME DESTINATION ${MTF_EXEC_INSTALL_DIR} COMPONENT test_exe)
add_custom_target(install_test_exe
  ${CMAKE_COMMAND}
  -D "CMAKE_INSTALL_COMPONENT=test_exe"
  -P "${MTF_BINARY_DIR}/cmake_install.cmake"
  )

