#include "mtf/mtf.h"

#include "cvUtils.h"
#include "inputCV.h"
#ifndef DISABLE_XVISION
#include "inputXV.h"
#endif
// classes for preprocessing the image
#include "PreProc.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config/datasets.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

#ifdef _WIN32
#define start_input_timer() \
	clock_t start_time_with_input = clock()
#define start_tracking_timer() \
	clock_t start_time = clock()
#define end_both_timers(fps, fps_win) \
	clock_t end_time = clock();\
	fps = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time);\
	fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time_with_input)
#else
#define start_input_timer() \
	timespec start_time_with_input;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_with_input)
#define start_tracking_timer() \
	timespec start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time)
#define end_both_timers(fps, fps_win) \
	timespec end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);\
	fps = 1.0 / ((double)(end_time.tv_sec - start_time.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time.tv_nsec));\
	fps_win = 1.0 / ((double)(end_time.tv_sec - start_time_with_input.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time_with_input.tv_nsec))
#endif

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	printf("Starting MTF...\n");
	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// parse command line arguments
	parseArgumentPairs(argv, argc, 1, 1);
	if(actor_id >= 0){
		int n_actors = sizeof(actors) / sizeof(actors[0]);
		printf("n_actors: %d\n", n_actors);
		if(actor_id >= n_actors){
			printf("Invalid actor id specified: %d\n", actor_id);
			return 0;
		}
		actor = actors[actor_id];
		if(source_id >= 0){
			source_name = combined_sources[actor_id][source_id];
		}
	}
#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif
	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name);	
	printf("actor: %s\n", actor);
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("show_cv_window: %d\n", show_cv_window);
	//printf("read_obj_from_gt: %d\n", read_obj_from_gt);
	//printf("write_tracking_data: %d\n", write_tracking_data);
	//printf("mtf_sm: %s\n", mtf_sm);
	//printf("mtf_am: %s\n", mtf_am);
	//printf("mtf_ssm: %s\n", mtf_ssm);
	printf("*******************************\n");

	InputBase *input = nullptr;
	GaussianSmoothing pre_proc_obj;

	cv::Point fps_origin(10, 20);
	double fps_font_size = 1.00;
	cv::Scalar fps_color(0, 255, 0);
	cv::Point err_origin(10, 40);
	double err_font_size = 1.00;
	cv::Scalar err_color(0, 255, 0);
	cv::Scalar gt_color(0, 255, 0);

	if((img_source == SRC_USB_CAM) || (img_source == SRC_DIG_CAM)){
		source_name = nullptr;
		show_tracking_error = read_obj_from_gt = read_objs = 0;

	} else {
		source_path = new char[500];
		snprintf(source_path, 500, "%s/%s", root_path, actor);
	}

	if(source_path && !strcmp(source_path, "#")){
		source_path = nullptr;
	}
	if(source_fmt && !strcmp(source_fmt, "#")){
		source_fmt = nullptr;
	}

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif

	else {
		printf("Invalid video pipeline provided: %c\n", pipeline);
		return 1;
	}
	if(!input->init_success){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return 1;
	}

	source_name = input->dev_name;
	source_fmt = input->dev_fmt;
	source_path = input->dev_path;
	int n_frames = input->n_frames;

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", n_frames);
	printf("source_path: %s\n", source_path);

	//printf("done initializing pipeline\n");
	string satellite_img_file = string(source_path) + "/" + string(source_name) + "/YJ_map_right.bmp";
	printf("satellite_img_file: %s\n", satellite_img_file.c_str());

	cv::Mat satellite_img = cv::imread(satellite_img_file);

	cv::Mat uav_rgb, uav_gs;
	cv::Mat satellite_rgb, satellite_gs;
	mtf::initializeFrame(uav_rgb, uav_gs, input->getFrame());
	mtf::initializeFrame(satellite_rgb, satellite_gs, satellite_img);

	//conv_corners = cv_utils.ground_truth;
	mtf::updateFrame<GaussianSmoothing>(uav_rgb, uav_gs, input->getFrame(), pre_proc_obj);
	mtf::updateFrame<GaussianSmoothing>(satellite_rgb, satellite_gs, satellite_img, pre_proc_obj);

	cv::Mat entire_image(2, 4, CV_64FC1);
	entire_image.at<double>(0, 0) = 0;
	entire_image.at<double>(0, 1) = input->getFrame().cols-1;
	entire_image.at<double>(0, 2) = input->getFrame().cols - 1;
	entire_image.at<double>(0, 3) = 0;
	entire_image.at<double>(1, 0) = 0;
	entire_image.at<double>(1, 1) = 0;
	entire_image.at<double>(1, 2) = input->getFrame().rows - 1;
	entire_image.at<double>(1, 3) = input->getFrame().rows - 1;

	mtf::ImgParams uav_img_params(resx, resy, uav_gs, grad_eps, hess_eps);
	//mtf::MIParams uav_mi_params(&uav_img_params, mi_n_bins, mi_pre_seed, mi_pou,
	//	nullptr, debug_mode);
	//mtf::MI template_extractor_am(&uav_mi_params);
	mtf::SSD template_extractor_am(&uav_img_params);
	mtf::Translation template_extractor_ssm(resx, resy);
	template_extractor_ssm.initialize(entire_image);
	template_extractor_am.initializePixVals(template_extractor_ssm.getPts());

	if(init_frame_id > 0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
		if(!input->updateFrame()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return 1;
		}
	}

	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}
	
	/*********************************** initialize tracker ***********************************/

	ifstream fin("initial_location.txt");
	float ulx, uly, urx, ury, lrx, lry, llx, lly;
	fin >> ulx >> uly >> urx >> ury >> lrx >> lry >> llx >> lly;
	cout << ulx << "\t" << uly << "\t" << urx << "\t" << ury << "\t" << lrx << "\t" << lry << "\t" << llx << "\t" << lly << "\n";

	cv::Mat init_location(2, 4, CV_64FC1);
	init_location.at<double>(0, 0) = ulx;
	init_location.at<double>(0, 1) = urx;
	init_location.at<double>(0, 2) = lrx;
	init_location.at<double>(0, 3) = llx;
	init_location.at<double>(1, 0) = uly;
	init_location.at<double>(1, 1) = ury;
	init_location.at<double>(1, 2) = lry;
	init_location.at<double>(1, 3) = lly;

	mtf::ImgParams img_params(resx, resy, satellite_gs, grad_eps, hess_eps);
	mtf::FCLKParams fclk_params(max_iters, epsilon,
		static_cast<mtf::FCLKParams::HessType>(hess_type), sec_ord_hess,
		update_templ, fc_chained_warp, debug_mode);
	//mtf::MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
	//	getPixMapperObj(pix_mapper, &img_params), debug_mode);
	mtf::FCLK<mtf::SSD, mtf::Similitude> uav_tracker(&fclk_params, &img_params);
	uav_tracker.initialize(init_location);
	
	char cv_win_name[100];
	snprintf(cv_win_name, 100, "OpenCV Window :: %s", source_name);
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}
	// if reading object from ground truth did not succeed then the 
	// ground truth is invalid and computing tracking error is not possible 
	// nor is it possible to reinitialize the object from ground truth
	show_tracking_error = show_tracking_error && read_obj_from_gt;
	reinit_from_gt = reinit_from_gt && read_obj_from_gt;
	show_ground_truth = show_ground_truth && read_obj_from_gt;

	double fps = 0, fps_win = 0;
	double avg_fps = 0, avg_fps_win = 0;
	int fps_count = 0;
	double avg_err = 0;

	double tracking_err = 0;
	int failure_count = 0;
	bool is_initialized = true;
	int valid_frame_count = 0;
	cv::Point2d corners[4];

	CVUtils cv_utils;
	cv::Mat satellite_img_copy, satellite_img_small;
	satellite_img_small.create(1000, 900, satellite_img.type());


	/*********************************** update tracker ***********************************/
	while(true) {
		
		satellite_img.copyTo(satellite_img_copy);		

		if(record_frames || show_cv_window) {
			/* draw tracker positions to OpenCV window */
			for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {				
				int col_id = tracker_id % cv_utils.no_of_cols;
				cv_utils.cornersToPoint2D(corners, uav_tracker.getRegion());
				line(satellite_img_copy, corners[0], corners[1], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[1], corners[2], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[2], corners[3], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[3], corners[0], cv_utils.obj_cols[col_id], line_thickness);
				putText(satellite_img_copy, uav_tracker.name, corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, cv_utils.obj_cols[col_id]);
			}
			//if(show_ground_truth){			
			//	cv_utils.cornersToPoint2D(corners, cv_utils.ground_truth[input->getFameID()]);
			//	line(input->getFrameMutable(), corners[0], corners[1], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[1], corners[2], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[2], corners[3], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[3], corners[0], gt_color, line_thickness);
			//	putText(input->getFrameMutable(), "ground_truth", corners[0],
			//		cv::FONT_HERSHEY_SIMPLEX, fps_font_size, gt_color);
			//}
			char fps_text[100];
			snprintf(fps_text, 100, "frame: %d c: %9.3f a: %9.3f cw: %9.3f aw: %9.3f fps",
				input->getFameID() + 1, fps, avg_fps, fps_win, avg_fps_win);
			putText(satellite_img_copy, fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

			if(show_tracking_error){
				char err_text[100];
				snprintf(err_text, 100, "ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				putText(satellite_img_copy, err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

			if(show_cv_window){
				cv::resize(satellite_img_copy, satellite_img_small, cv::Size(satellite_img_small.cols, satellite_img_small.rows));
				imshow(cv_win_name, satellite_img_small);
				imshow("UAV", input->getFrame());
				int pressed_key = cv::waitKey(1 - pause_after_frame);
				if(pressed_key == 27){
					break;
				}
				if(pressed_key == 32){
					pause_after_frame = 1 - pause_after_frame;
				}
			}
		}
		if(!show_cv_window && (input->getFameID() + 1) % 50 == 0){
			printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
				input->getFameID() + 1, avg_fps, avg_fps_win, avg_err);
		}
		if(n_frames > 0 && input->getFameID() >= n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}

		start_input_timer();
		// update frame
		if (!input->updateFrame()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			break;
		}
		mtf::updateFrame<GaussianSmoothing>(uav_rgb, uav_gs,
			input->getFrame(), pre_proc_obj);
		// extract template
		template_extractor_am.updatePixVals(template_extractor_ssm.getPts());
		uav_tracker.getAM()->setInitPixVals(template_extractor_am.getCurrPixVals());

		cv::Mat curr_img_cv = cv::Mat(uav_tracker.getAM()->getResY(), uav_tracker.getAM()->getResX(), CV_64FC1,
			const_cast<double*>(uav_tracker.getAM()->getInitPixVals().data()));
		cv::Mat  curr_img_cv_uchar(uav_tracker.getAM()->getResY(), uav_tracker.getAM()->getResX(), CV_8UC1);
		curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
		imshow("Template", curr_img_cv_uchar);

		//update tracker		
		start_tracking_timer();
		uav_tracker.update();
		end_both_timers(fps, fps_win);

		if(!std::isinf(fps) && fps<MAX_FPS){
			++fps_count;
			avg_fps += (fps - avg_fps) / fps_count;
			// if fps is not inf then fps_win too must be non inf
			avg_fps_win += (fps_win - avg_fps_win) / fps_count;
		}
	}

	//double avg_fps = accumulate( fps_vector.begin(), fps_vector.end(), 0.0 )/ fps_vector.size();
	//double avg_fps_win = accumulate( fps_win_vector.begin(), fps_win_vector.end(), 0.0 )/ fps_win_vector.size();
	printf("Average FPS: %15.10f\n", avg_fps);
	printf("Average FPS with Input: %15.10f\n", avg_fps_win);
	if(show_tracking_error){
		printf("Average Tracking Error: %15.10f\n", avg_err);
		printf("Frames used for computing the average: %d\n", valid_frame_count);
	}
	return 0;
}
