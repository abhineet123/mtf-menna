#include "mtf/mtf.h"

#include "cvUtils.h"
#include "inputCV.h"
#ifndef DISABLE_XVISION
#include "inputXV.h"
#endif
// classes for preprocessing the image
#include "PreProc.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config/datasets.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

#ifdef _WIN32
#define start_input_timer() \
	clock_t start_time_with_input = clock()
#define start_tracking_timer() \
	clock_t start_time = clock()
#define end_both_timers(fps, fps_win) \
	clock_t end_time = clock();\
	fps = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time);\
	fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time_with_input)
#else
#define start_input_timer() \
	timespec start_time_with_input;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_with_input)
#define start_tracking_timer() \
	timespec start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time)
#define end_both_timers(fps, fps_win) \
	timespec end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);\
	fps = 1.0 / ((double)(end_time.tv_sec - start_time.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time.tv_nsec));\
	fps_win = 1.0 / ((double)(end_time.tv_sec - start_time_with_input.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time_with_input.tv_nsec))
#endif

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	printf("Starting MTF...\n");
	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// parse command line arguments
	parseArgumentPairs(argv, argc, 1, 1);
	if(actor_id >= 0){
		int n_actors = sizeof(actors) / sizeof(actors[0]);
		printf("n_actors: %d\n", n_actors);
		if(actor_id >= n_actors){
			printf("Invalid actor id specified: %d\n", actor_id);
			return 0;
		}
		actor = actors[actor_id];
		if(source_id >= 0){
			source_name = combined_sources[actor_id][source_id];
		}
	}
#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif
	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name);
	printf("actor: %s\n", actor);
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("show_cv_window: %d\n", show_cv_window);
	printf("read_obj_from_gt: %d\n", read_obj_from_gt);
	printf("write_tracking_data: %d\n", write_tracking_data);
	printf("mtf_sm: %s\n", mtf_sm);
	printf("mtf_am: %s\n", mtf_am);
	printf("mtf_ssm: %s\n", mtf_ssm);
	printf("*******************************\n");

	vector<mtf::TrackerBase*> trackers;
	InputBase *input = nullptr;
	cv::VideoWriter output_obj;
	GaussianSmoothing pre_proc_obj;

	cv::Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	cv::Scalar fps_color(0, 255, 0);
	cv::Point err_origin(10, 40);
	double err_font_size = 0.50;
	cv::Scalar err_color(0, 255, 0);
	cv::Scalar gt_color(0, 255, 0);

	if((img_source == SRC_USB_CAM) || (img_source == SRC_DIG_CAM)){
		source_name = nullptr;
		show_tracking_error = read_obj_from_gt = read_objs = 0;

	} else {
		source_path = new char[500];
		snprintf(source_path, 500, "%s/%s", root_path, actor);
	}

	if(source_path && !strcmp(source_path, "#")){
		source_path = nullptr;
	}
	if(source_fmt && !strcmp(source_fmt, "#")){
		source_fmt = nullptr;
	}

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif

	else {
		printf("Invalid video pipeline provided: %c\n", pipeline);
		return 1;
	}
	if(!input->init_success){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return 1;
	}
	//printf("done initializing pipeline\n");

	cv::Mat cv_frame_rgb, cv_frame_gs;
	mtf::initializeFrame(cv_frame_rgb, cv_frame_gs, input->getFrame());

	source_name = input->dev_name;
	source_fmt = input->dev_fmt;
	source_path = input->dev_path;
	int n_frames = input->n_frames;

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", n_frames);

	if(init_frame_id > 0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
		if(!input->updateFrame()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return 1;
		}
	}
	bool init_obj_read = false;
	/*get objects to be tracked*/
	CVUtils cv_utils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = cv_utils.readObjectFromGT(source_name, source_path, n_frames,
			init_frame_id, debug_mode, use_opt_gt, opt_gt_ssm);
		if(init_object){
			init_obj_read = true;
			for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
				init_objects.push_back(init_object);
			}
		} else{
			printf("Failed to read initial object from ground truth; using manual selection...\n");
		}
	}
	if(!init_obj_read && read_objs) {
		init_objects = cv_utils.readObjectsFromFile(n_trackers, read_obj_fname, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		//printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
		init_objects = cv_utils.getMultipleObjects(input->getFrame(), n_trackers,
			patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname);
		//printf("done calling getMultipleObjects\n");
	}

	//conv_corners = cv_utils.ground_truth;

	mtf::updateFrame<GaussianSmoothing>(cv_frame_rgb, cv_frame_gs,
		input->getFrame(), pre_proc_obj);

	//ofstream eig_img_out;
	//ofstream cv_img_out;
	//eig_img_out.open("log/eig_img.txt", ios::out);
	//cv_img_out.open("log/cv_img.txt", ios::out);

	//printf("Using norm_pix_max: %f\n norm_pix_min: %f\n", norm_pix_max, norm_pix_min);


	/*********************************** initialize trackers ***********************************/
	if(n_trackers > 1){
		printf("Multi tracker setup enabled\n");
		write_tracking_data = 0;
		reinit_from_gt = 0;
		show_ground_truth = 0;
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}
	mtf::ImgParams img_params(resx, resy, cv_frame_gs, grad_eps, hess_eps);
	FILE *multi_fid = nullptr;
	bool pre_proc_enabled = false;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(n_trackers > 1){
			multi_fid = readTrackerParams(multi_fid);
		}
		printf("Initializing tracker %d with object of size %f x %f\n", tracker_id,
			init_objects[tracker_id]->size_x, init_objects[tracker_id]->size_y);
		if(res_from_size){
			img_params.resx = init_objects[tracker_id]->size_x;
			img_params.resy = init_objects[tracker_id]->size_y;
		}
		mtf::TrackerBase *new_tracker = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &img_params);
		//delete(new_tracker);
		//new_tracker = nullptr;
		//return 0;
		if(!new_tracker){
			printf("Tracker could not be initialized successfully\n");
			exit(1);
		}
		if(new_tracker->rgbInput()){
			new_tracker->initialize(input->getFrame(), init_objects[tracker_id]->corners);
		} else{
			new_tracker->initialize(init_objects[tracker_id]->corners);
		}
		if(!new_tracker->rgbInput()){ pre_proc_enabled = true; }
		trackers.push_back(new_tracker);
	}
	char cv_win_name[100];
	snprintf(cv_win_name, 100, "OpenCV Window :: %s", source_name);
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}
	// if reading object from ground truth did not succeed then the 
	// ground truth is invalid and computing tracking error is not possible 
	// nor is it possible to reinitialize the object from ground truth
	show_tracking_error = show_tracking_error && read_obj_from_gt;
	reinit_from_gt = reinit_from_gt && read_obj_from_gt;
	show_ground_truth = show_ground_truth && read_obj_from_gt;


	if(show_tracking_error || reinit_from_gt){
		int valid_gt_frames = cv_utils.ground_truth.size();
		if(n_frames <= 0 || valid_gt_frames < n_frames){
			if(n_frames <= 0){
				printf("Disabling tracking error computation\n");
			} else if(valid_gt_frames>0){
				printf("Disabling tracking error computation since ground truth is only available for %d out of %d frames\n",
					valid_gt_frames, n_frames);
			} else{
				printf("Disabling tracking error computation since ground truth is not available\n");
			}
			show_tracking_error = 0;
			reinit_from_gt = 0;
			show_ground_truth = 0;
		}
	}

	if(record_frames){
		output_obj.open("Tracked_video.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input->getFrame().size());
	}
	FILE *tracking_data_fid = nullptr;
	if(write_tracking_data){
		char tracking_data_dir[500];
		char tracking_data_path[500];

		if(reinit_from_gt){
			snprintf(tracking_data_dir, 500, "log/tracking_data/reinit_%d_%d/%s/%s",
				static_cast<int>(reinit_err_thresh), reinit_frame_skip, actor, source_name);
		} else{
			snprintf(tracking_data_dir, 500, "log/tracking_data/%s/%s", actor, source_name);
		}

		if(!fs::exists(tracking_data_dir)){
			printf("Tracking data directory: %s does not exist. Creating it...\n", tracking_data_dir);
			fs::create_directories(tracking_data_dir);
		}
		if(!tracking_data_fname){
			tracking_data_fname = new char[500];
			snprintf(tracking_data_fname, 500, "%s_%s_%s_%d", mtf_sm, mtf_am, mtf_ssm, 1 - hom_normalized_init);
		}
		snprintf(tracking_data_path, 500, "%s/%s.txt", tracking_data_dir, tracking_data_fname);
		printf("Writing tracking data to: %s\n", tracking_data_path);
		tracking_data_fid = fopen(tracking_data_path, "w");
		fprintf(tracking_data_fid, "frame ulx uly urx ury lrx lry llx lly\n");
	}

	ofstream fout;

	double fps = 0, fps_win = 0;
	double avg_fps = 0, avg_fps_win = 0;
	int fps_count = 0;
	double avg_err = 0;
	if(reset_template){
		printf("Template resetting is enabled\n");
	}
	if(reinit_from_gt){
		printf("Tracker reinitialization on failure is enabled with error threshold: %f and skipped frames: %d\n",
			reinit_err_thresh, reinit_frame_skip);
	}

	double tracking_err = 0;
	int failure_count = 0;
	bool is_initialized = true;
	int valid_frame_count = 0;
	cv::Point2d corners[4];

	/*********************************** update trackers ***********************************/
	while(true) {
		if(show_tracking_error || reinit_from_gt){
			//printf("computing error for frame: %d\n", frame_id - 1);	

			// compute the mean corner distance between the tracking result and the ground truth
			tracking_err = 0;
			for(int corner_id = 0; corner_id < 4; ++corner_id){
				double x_diff = cv_utils.ground_truth[input->getFameID()].at<double>(0, corner_id) - trackers[0]->getRegion().at<double>(0, corner_id);
				double y_diff = cv_utils.ground_truth[input->getFameID()].at<double>(1, corner_id) - trackers[0]->getRegion().at<double>(1, corner_id);
				//printf("x_diff: %f\n", x_diff);
				//printf("y_diff: %f\n", y_diff);
				tracking_err += sqrt((x_diff*x_diff) + (y_diff*y_diff));
			}
			tracking_err /= 4.0;
			//printf("tracking_err: %f\n", tracking_err);
			//printf("done computing error\n");
			if(reinit_from_gt && tracking_err > reinit_err_thresh){
				++failure_count;
				printf("Tracking failure %4d detected in frame %5d with error: %10.6f. ",
					failure_count, input->getFameID() + 1, tracking_err);
				if(write_tracking_data){
					fprintf(tracking_data_fid, "frame%05d.jpg tracker_failed\n", input->getFameID() + 1);
				}
				if(input->getFameID() + reinit_frame_skip >= n_frames){
					printf("Reinitialization is not possible as insufficient frames (%d) are left to skip. Exiting...\n",
						n_frames - input->getFameID() - 1);
					break;
				}
				bool skip_success = true;
				for(int skip_id = 0; skip_id < reinit_frame_skip; ++skip_id) {
					if(!input->updateFrame()){
						printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
						skip_success = false;
						break;
					}
				}
				if(!skip_success){ break; }
				printf("Reinitializing in frame %5d...\n", input->getFameID() + 1);
				//cout << "Using ground truth:\n" << cv_utils.ground_truth[input->getFameID()] << "\n";
				if(pre_proc_enabled){
					mtf::updateFrame<GaussianSmoothing>(cv_frame_rgb, cv_frame_gs,
						input->getFrame(), pre_proc_obj);
				}
				if(trackers[0]->rgbInput()){
					trackers[0]->initialize(input->getFrame(), cv_utils.ground_truth[input->getFameID()]);
				} else{
					trackers[0]->initialize(cv_utils.ground_truth[input->getFameID()]);
				}
				is_initialized = true;
			}
			if(is_initialized){
				is_initialized = false;
			} else{// exclude initialization frames for computing the average error				
				if(!std::isinf(fps)){
					++valid_frame_count;
					avg_err += (tracking_err - avg_err) / valid_frame_count;
					//fps_vector.push_back(fps);	
				}
			}
		}
		if(record_frames || show_cv_window) {
			/* draw tracker positions to OpenCV window */
			for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {				
				int col_id = tracker_id % cv_utils.no_of_cols;
				cv_utils.cornersToPoint2D(corners, trackers[tracker_id]->getRegion());
				line(input->getFrameMutable(), corners[0], corners[1], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[1], corners[2], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[2], corners[3], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[3], corners[0], cv_utils.obj_cols[col_id], line_thickness);
				putText(input->getFrameMutable(), trackers[tracker_id]->name, corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, cv_utils.obj_cols[col_id]);
			}
			if(show_ground_truth){			
				cv_utils.cornersToPoint2D(corners, cv_utils.ground_truth[input->getFameID()]);
				line(input->getFrameMutable(), corners[0], corners[1], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[1], corners[2], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[2], corners[3], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[3], corners[0], gt_color, line_thickness);
				putText(input->getFrameMutable(), "ground_truth", corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, gt_color);
			}
			char fps_text[100];
			snprintf(fps_text, 100, "frame: %d c: %9.3f a: %9.3f cw: %9.3f aw: %9.3f fps",
				input->getFameID() + 1, fps, avg_fps, fps_win, avg_fps_win);
			putText(input->getFrameMutable(), fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

			if(show_tracking_error){
				char err_text[100];
				snprintf(err_text, 100, "ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				putText(input->getFrameMutable(), err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

			if(record_frames){
				output_obj.write(input->getFrame());
			}
			if(show_cv_window){
				imshow(cv_win_name, input->getFrame());
				int pressed_key = cv::waitKey(1 - pause_after_frame);
				if(pressed_key == 27){
					break;
				}
				if(pressed_key == 32){
					pause_after_frame = 1 - pause_after_frame;
				}
			}
		}
		if(!show_cv_window && (input->getFameID() + 1) % 50 == 0){
			printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
				input->getFameID() + 1, avg_fps, avg_fps_win, avg_err);
		}
		if(write_tracking_data){
			fprintf(tracking_data_fid, "frame%05d.jpg %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f\n", input->getFameID() + 1,
				trackers[0]->getRegion().at<double>(0, 0), trackers[0]->getRegion().at<double>(1, 0),
				trackers[0]->getRegion().at<double>(0, 1), trackers[0]->getRegion().at<double>(1, 1),
				trackers[0]->getRegion().at<double>(0, 2), trackers[0]->getRegion().at<double>(1, 2),
				trackers[0]->getRegion().at<double>(0, 3), trackers[0]->getRegion().at<double>(1, 3));
		}

		if(n_frames > 0 && input->getFameID() >= n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}

		start_input_timer();
		// update frame
		if (!input->updateFrame()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			break;
		}
		if(pre_proc_enabled){
			mtf::updateFrame<GaussianSmoothing>(cv_frame_rgb, cv_frame_gs,
				input->getFrame(), pre_proc_obj);
		}
		//update trackers		
		for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
			if(trackers[tracker_id]->rgbInput()){
				start_tracking_timer();
				trackers[tracker_id]->update(input->getFrame());
				end_both_timers(fps, fps_win);
				if(reset_template){
					trackers[tracker_id]->initialize(input->getFrame(), trackers[tracker_id]->getRegion());
				}
			} else{
				start_tracking_timer();
				trackers[tracker_id]->update();
				end_both_timers(fps, fps_win);
				if(reset_template){
					trackers[tracker_id]->initialize(trackers[tracker_id]->getRegion());
				}
			}
		}
		if(!std::isinf(fps) && fps<MAX_FPS){
			++fps_count;
			avg_fps += (fps - avg_fps) / fps_count;
			// if fps is not inf then fps_win too must be non inf
			avg_fps_win += (fps_win - avg_fps_win) / fps_count;
		}
	}

	//double avg_fps = accumulate( fps_vector.begin(), fps_vector.end(), 0.0 )/ fps_vector.size();
	//double avg_fps_win = accumulate( fps_win_vector.begin(), fps_win_vector.end(), 0.0 )/ fps_win_vector.size();
	printf("Average FPS: %15.10f\n", avg_fps);
	printf("Average FPS with Input: %15.10f\n", avg_fps_win);
	if(show_tracking_error){
		printf("Average Tracking Error: %15.10f\n", avg_err);
		printf("Frames used for computing the average: %d\n", valid_frame_count);
	}
	if(reinit_from_gt){
		printf("Number of failures: %d\n", failure_count);
	}
	if(write_tracking_data){
		fclose(tracking_data_fid);
		FILE *tracking_stats_fid = fopen("log/tracking_stats.txt", "a");
		fprintf(tracking_stats_fid, "%s\t %s\t %s\t %s\t %d\t %s\t %15.9f\t %15.9f",
			source_name, mtf_sm, mtf_am, mtf_ssm, hom_normalized_init, tracking_data_fname, avg_fps, avg_fps_win);
		if(show_tracking_error){
			fprintf(tracking_stats_fid, "\t %15.9f", avg_err);
		}
		if(reinit_from_gt){
			fprintf(tracking_stats_fid, "\t %d", failure_count);
		}
		fprintf(tracking_stats_fid, "\n");
		fclose(tracking_stats_fid);
	}
	if(record_frames){
		output_obj.release();
	}
	//for(int i = 0; i < n_trackers; i++) {
	//	delete(trackers[i]);
	//}
	return 0;
}
