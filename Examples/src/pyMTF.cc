#include <Python.h>
#include <numpy/arrayobject.h>


#include "mtf/mtf.h"
#include "mtf/Config/parameters.h"
#include "mtf/Config/datasets.h"
#include "cvUtils.h"

#include <time.h>
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace mtf::params;


static PyObject* initialize(PyObject* self, PyObject* args);
static PyObject* update(PyObject* self, PyObject* args);
static PyObject* setRegion(PyObject* self, PyObject* args);
static PyObject* getRegion(PyObject* self, PyObject* args);

static PyArrayObject *img_py;
static PyArrayObject *init_corners_py;
static PyArrayObject *out_corners_py;
static double* out_corners_data;

static vector<mtf::TrackerBase*> trackers;
static cv::Mat cv_frame_rgb, cv_frame_gs;
static int img_height, img_width;
static vector<cv::Scalar> obj_cols;

static cv::Point fps_origin(10, 20);
static double fps_font_size = 0.50;
static cv::Scalar fps_color(0, 255, 0);
static char fps_text[100];
static int frame_id;

static char* config_root_dir = "C++/MTF/Config";

static PyMethodDef pyMTFMethods[] = {
	{ "initialize", initialize, METH_VARARGS },
	{ "update", update, METH_VARARGS },
	{ "setRegion", setRegion, METH_VARARGS },
	{ "getRegion", getRegion, METH_VARARGS },
	{ NULL, NULL }     /* Sentinel - marks the end of this structure */
};

/* ==== Initialize the C_test functions ====================== */
// Module name must be pyMTF in compile and linked
PyMODINIT_FUNC initpyMTF()  {
	(void)Py_InitModule("pyMTF", pyMTFMethods);
	import_array();  // Must be present for NumPy.  Called first after above line.
}

/* ==== initialize tracker ==== */

static PyObject* initialize(PyObject* self, PyObject* args) {

	/*parse first input array*/
	if(!PyArg_ParseTuple(args, "O!O!z", &PyArray_Type, &img_py, &PyArray_Type, &init_corners_py, &config_root_dir)) {
		printf("\n----pyMTF::initialize: Input arguments could not be parsed----\n\n");
		return NULL;
	}

	if(img_py == NULL) {
		printf("\n----pyMTF::initialize::init_img is NULL----\n\n");
		return NULL;
	}
	if(init_corners_py == NULL) {
		printf("\n----pyMTF::initialize::init_corners is NULL----\n\n");
		return NULL;
	}

	if(init_corners_py->dimensions[0] != 2 || init_corners_py->dimensions[1] != 4){
		printf("pyMTF::Initial corners matrix has incorrect dimensions: %ld, %ld\n",
			init_corners_py->dimensions[0], init_corners_py->dimensions[1]);
		return NULL;
	}
	if(!config_root_dir){
		config_root_dir = "C++/MTF/Config";
		printf("Using default configuration folder: %s\n", config_root_dir);
	}else{
		printf("Reading MTF configuration files from: %s\n", config_root_dir);
	}
#ifdef USE_TBB
	Eigen::initParallel();
#endif

	img_height = img_py->dimensions[0];
	img_width = img_py->dimensions[1];

	printf("img_height: %d\n", img_height);
	printf("img_width: %d\n", img_width);

	cv::Mat init_img_cv(img_height, img_width, CV_8UC3, img_py->data);

	cv::Mat temp(2, 4, CV_64FC1, init_corners_py->data);
	cv::Mat init_corners_cv(2, 4, CV_64FC1);

	//double *in_corners_data = (double*)init_corners_py->data;
	//for(int i = 0; i < 4; i++) {
	//	init_corners_cv.at<double>(0, i) = in_corners_data[i];
	//	init_corners_cv.at<double>(1, i) = in_corners_data[i + init_corners_py->strides[1]];
	//}
	init_corners_cv.at<double>(0, 0) = temp.at<double>(0, 0);
	init_corners_cv.at<double>(1, 0) = temp.at<double>(0, 1);
	init_corners_cv.at<double>(0, 1) = temp.at<double>(0, 2);
	init_corners_cv.at<double>(1, 1) = temp.at<double>(0, 3);
	init_corners_cv.at<double>(0, 2) = temp.at<double>(1, 0);
	init_corners_cv.at<double>(1, 2) = temp.at<double>(1, 1);
	init_corners_cv.at<double>(0, 3) = temp.at<double>(1, 2);
	init_corners_cv.at<double>(1, 3) = temp.at<double>(1, 3);


	printf("init_corners_cv:\n");
	for(int i = 0; i < 4; i++) {
		printf("%d: (%f, %f)\n", i, init_corners_cv.at<double>(0, i), init_corners_cv.at<double>(1, i));
	}

	double min_x = init_corners_cv.at<double>(0, 0);
	double min_y = init_corners_cv.at<double>(1, 0);
	double max_x = init_corners_cv.at<double>(0, 2);
	double max_y = init_corners_cv.at<double>(1, 2);
	double size_x = max_x - min_x;
	double size_y = max_y - min_y;

	vector<char*> fargv;
	char config_path[500];
	snprintf(config_path, 500, "%s/modules.cfg", config_root_dir);
	// read general parameters
	int fargc = readParams(fargv, config_path);
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// read parameters specific to different modules
	snprintf(config_path, 500, "%s/modules.cfg", config_root_dir);
	fargc = readParams(fargv, config_path);
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	if(actor_id >= 0){
		actor = actors[actor_id];
		if(source_id >= 0){
			source_name = combined_sources[actor_id][source_id];
		}
	}
	cout << "*******************************\n";
	cout << "Using parameters:\n";
	cout << "n_trackers: " << n_trackers << "\n";
	cout << "source_id: " << source_id << "\n";
	cout << "source_name: " << source_name << "\n";
	cout << "actor: " << actor << "\n";
	cout << "steps_per_frame: " << steps_per_frame << "\n";
	cout << "pipeline: " << pipeline << "\n";
	cout << "img_source: " << img_source << "\n";
	cout << "show_cv_window: " << show_cv_window << "\n";
	cout << "read_objs: " << read_objs << "\n";
	cout << "record_frames: " << record_frames << "\n";
	cout << "patch_size: " << patch_size << "\n";
	cout << "read_obj_fname: " << read_obj_fname << "\n";
	cout << "read_obj_from_gt: " << read_obj_from_gt << "\n";
	cout << "show_warped_img: " << show_warped_img << "\n";
	cout << "pause_after_frame: " << pause_after_frame << "\n";
	cout << "write_tracking_data: " << write_tracking_data << "\n";
	cout << "mtf_sm: " << mtf_sm << "\n";
	cout << "mtf_am: " << mtf_am << "\n";
	cout << "mtf_ssm: " << mtf_ssm << "\n";

	cout << "*******************************\n";

	cv_frame_rgb.create(img_height, img_width, CV_32FC3);
	cv_frame_gs.create(img_height, img_width, CV_32FC1);

	init_img_cv.convertTo(cv_frame_rgb, cv_frame_rgb.type());
	cv::cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);
	cv::GaussianBlur(cv_frame_gs, cv_frame_gs, cv::Size(5, 5), 3);

	/*********************************** initialize trackers ***********************************/
	if(n_trackers > 1){
		printf("Multi tracker setup enabled\n");
		write_tracking_data = 0;
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}
	mtf::ImgParams sm_params(resx, resy, cv_frame_gs);
	FILE *multi_fid = NULL;
	for(int i = 0; i < n_trackers; i++) {
		if(n_trackers > 1){
			multi_fid = readTrackerParams(multi_fid);
		}
		printf("Initializing tracker %d with object of size %f x %f\n", i,
			size_x, size_y);
		if(res_from_size){
			sm_params.resx = size_x;
			sm_params.resy = size_y;
		}
		mtf::TrackerBase *new_tracker = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &sm_params);
		if(!new_tracker){
			printf("Tracker could not be initialized successfully\n");
			exit(0);
		}
		if(new_tracker->rgbInput()){
			new_tracker->initialize(init_img_cv, init_corners_cv);
		} else{
			new_tracker->initialize(init_corners_cv);
		}

		trackers.push_back(new_tracker);
	}
	if(show_cv_window) {
		cv::namedWindow("OpenCV Window", cv::WINDOW_AUTOSIZE);
	}
	obj_cols.push_back(cv::Scalar(0, 0, 255));
	obj_cols.push_back(cv::Scalar(0, 255, 0));
	obj_cols.push_back(cv::Scalar(255, 0, 0));
	obj_cols.push_back(cv::Scalar(255, 255, 0));
	obj_cols.push_back(cv::Scalar(255, 0, 255));
	obj_cols.push_back(cv::Scalar(0, 255, 255));
	obj_cols.push_back(cv::Scalar(255, 255, 255));
	obj_cols.push_back(cv::Scalar(0, 0, 0));


	int dims[] = {2, 4};
	out_corners_py = (PyArrayObject *)PyArray_FromDims(2, dims, NPY_DOUBLE);
	out_corners_data = (double*)out_corners_py->data;

	frame_id = 0;

	return Py_BuildValue("i", 1);
}

static PyObject* update(PyObject* self, PyObject* args) {

	/*parse first input array*/
	if(!PyArg_ParseTuple(args, "O!", &PyArray_Type, &img_py)) {
		printf("\n----pyMTF::update: Input arguments could not be parsed----\n\n");
		return NULL;
	}

	if(img_py == NULL) {
		printf("\n----pyMTF::img_py is NULL----\n\n");
		return NULL;
	}
	frame_id++;

	Mat curr_img_cv(img_height, img_width, CV_8UC3, img_py->data);

	curr_img_cv.convertTo(cv_frame_rgb, cv_frame_rgb.type());
	cv::cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);
	cv::GaussianBlur(cv_frame_gs, cv_frame_gs, cv::Size(5, 5), 3);

	//update trackers
	timespec start_time, end_time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time);
	for(int i = 0; i < n_trackers; i++) {
		if(trackers[i]->rgbInput()){
			trackers[i]->update(curr_img_cv);
		} else{
			trackers[i]->update();
			if(update_templ){
				trackers[i]->initialize(trackers[i]->getRegion());
			}		
		}
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);
	double fps = 1.0/((double)(end_time.tv_sec - start_time.tv_sec) + 1e-9*(double)(end_time.tv_nsec - start_time.tv_nsec));
	CVUtils cv_utils;
	cv::Point2d corners[4];
	if(record_frames || show_cv_window) {
		/* draw tracker positions to OpenCV window */
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			int col_id = tracker_id % obj_cols.size();
			cv_utils.cornersToPoint2D(corners, trackers[tracker_id]->getRegion());
			line(curr_img_cv, corners[0], corners[1], obj_cols[col_id], line_thickness);
			line(curr_img_cv, corners[1], corners[2], obj_cols[col_id], line_thickness);
			line(curr_img_cv, corners[2], corners[3], obj_cols[col_id], line_thickness);
			line(curr_img_cv, corners[3], corners[0], obj_cols[col_id], line_thickness);
			putText(curr_img_cv, trackers[tracker_id]->name, corners[0],
				cv::FONT_HERSHEY_SIMPLEX, fps_font_size, obj_cols[col_id]);
		}
		snprintf(fps_text, 100, "frame: %d c: %12.6f", frame_id, fps);
		putText(curr_img_cv, fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

		if(show_cv_window){
			imshow("OpenCV Window", curr_img_cv);
			cv::waitKey(1);
		}
	}
	cv_utils.cornersToPoint2D(corners, trackers[0]->getRegion());
	//mtf::utils::printMatrix<double>(trackers[0]->getRegion(), "mtf corners");
	for(int corner_id = 0; corner_id<4; corner_id++) {		
		out_corners_data[corner_id] = corners[corner_id].x;
		out_corners_data[corner_id + 4] = corners[corner_id].y;
	}
	return Py_BuildValue("O", out_corners_py);
}

static PyObject* setRegion(PyObject* self, PyObject* args) {
	/*parse first input array*/
	if(!PyArg_ParseTuple(args, "O!", &PyArray_Type, &init_corners_py)) {
		printf("\n----pyMTF::initialize: Input arguments could not be parsed----\n\n");
		return Py_BuildValue("i", 0);
	}

	if(init_corners_py == NULL) {
		printf("\n----pyMTF::initialize::init_corners is NULL----\n\n");
		return Py_BuildValue("i", 0);
	}

	if(init_corners_py->dimensions[0] != 2 || init_corners_py->dimensions[1] != 4){
		printf("pyMTF::Initial corners matrix has incorrect dimensions: %ld, %ld\n",
			init_corners_py->dimensions[0], init_corners_py->dimensions[1]);
		return Py_BuildValue("i", 0);
	}
	Mat corners(2, 4, CV_64FC1, init_corners_py->data);
	trackers[0]->setRegion(corners);
	return Py_BuildValue("i", 1);

}
static PyObject* getRegion(PyObject* self, PyObject* args){
	return Py_BuildValue("O", out_corners_py);
}
