MTF_LIB_INSTALL_DIR ?= /usr/local/lib
MTF_HEADER_INSTALL_DIR ?= /usr/local/include/mtf
EIGEN_INCLUDE_DIR ?= /usr/local/include/eigen3

o ?= 1
spi ?= 1
prf ?= 0

FLAGSCV = `pkg-config --cflags opencv`
FLAGS64 = -I/usr/include
WARNING_FLAGS = -Wfatal-errors -Wno-write-strings -Wno-unused-result
LIBSCV = `pkg-config --libs opencv`
LIB_MTF_LIBS =  -L/usr/local/lib -lstdc++ -lgnn -lboost_random
CT_FLAGS = -std=c++11 -I${EIGEN_INCLUDE_DIR}
PROF_FLAGS =
# Optimization
ifeq (${o}, 1)
OPT_FLAGS = -O3 -D NDEBUG
MTF_LIB_NAME=libmtf.so
MTF_LIB_LINK=-lmtf
BUILD_DIR = Build/Release
else
OPT_FLAGS += -g -O0
MTF_LIB_NAME=libmtf_debug.so
MTF_LIB_LINK=-lmtf_debug
BUILD_DIR = Build/Debug
endif
# Profiling
ifeq (${prf}, 1)
PROF_FLAGS += -pg
endif
# Selective Pixel Integration
ifeq (${spi}, 0)
CT_FLAGS += -D DISABLE_SPI
endif

#@echo installing $^ to ${MTF_HEADER_INSTALL_DIR}...
BASE_CLASSES += TrackerBase 
# MTF_MISC_INCLUDES = SM/GNN/build_graph.h SM/GNN/utility.h
BASE_HEADERS += $(addsuffix .h, ${BASE_CLASSES})
#MTF_HEADERS= $(addprefix ${MTF_HEADER_DIR}/, ${BASE_HEADERS} ${SEARCH_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${MTF_UTIL_HEADERS} ${COMPOSITE_HEADERS} ${TOOL_HEADERS} ${DIAG_BASE_HEADERS} ${DIAG_HEADERS} ${LEARNING_HEADERS})  
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, mtf.h ${BASE_HEADERS})
# read make commands and definitions for each component of MTF
MTF_SUB_DIRS = AM SM SSM Utilities Macros Config ThirdParty Examples Test
# include $(addsuffix /make.mk, ${MTF_SUB_DIRS})
include $(foreach SUB_DIR,${MTF_SUB_DIRS},${SUB_DIR}/${SUB_DIR}.mak)

.PHONY: mtf
.PHONY: install install_lib install_header
.PHONY: clean

h1:
	@echo ${MTF_HEADERS}
all: mtf
mtf: ${BUILD_DIR}/${MTF_LIB_NAME}
install: install_lib install_header
install_lib: ${MTF_LIB_INSTALL_DIR}/${MTF_LIB_NAME}
install_header: ${MTF_HEADER_INSTALL_DIR} ${MTF_HEADERS}
clean: 
	rm -f ${BUILD_DIR}/*.o ${BUILD_DIR}/*.so

${BUILD_DIR}:
		mkdir -p $@			
${MTF_LIB_INSTALL_DIR}/${MTF_LIB_NAME}: ${BUILD_DIR}/${MTF_LIB_NAME}
	sudo cp -f ${BUILD_DIR}/${MTF_LIB_NAME} $@
${MTF_HEADER_INSTALL_DIR}:
		sudo mkdir -p $@	
#@find . -type f -name '*.h' | cpio -p -d -v ${MTF_HEADER_INSTALL_DIR}/		
${MTF_HEADER_INSTALL_DIR}/%: %
	sudo cp --parents $^ ${MTF_HEADER_INSTALL_DIR}
	
${BUILD_DIR}/${MTF_LIB_NAME}: | ${BUILD_DIR}
${BUILD_DIR}/${MTF_LIB_NAME}:  ${MTF_UTIL_OBJS} ${APPEARANCE_OBJS} ${STATE_SPACE_OBJS} ${SEARCH_OBJS} ${COMPOSITE_OBJS} ${LEARNING_OBJS} ${LEARNING_TRACKERS_SO}
	${CXX} -shared -o $@  ${APPEARANCE_OBJS} ${SEARCH_OBJS} ${COMPOSITE_OBJS} ${STATE_SPACE_OBJS} ${MTF_UTIL_OBJS} ${LEARNING_OBJS} ${LIBSCV} ${LIB_MTF_LIBS} ${LIBS_VISP} ${LIBS_LEARNING}
