# --------------------------------------------------------------------------------- #
# ---------------------------------- Diagnostics ---------------------------------- #
# --------------------------------------------------------------------------------- #
DIAG_BASE_CLASSES += Test/DiagBase
DIAG_TOOLS = Diagnostics
DIAG_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${DIAG_TOOLS})) 
DIAG_BASE_HEADERS = $(addsuffix .h, ${DIAG_BASE_CLASSES})
DIAG_HEADERS = $(addprefix Test/,$(addsuffix .h, ${DIAG_TOOLS}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, Test/mtf_diag.h Test/DiagBase.h ${DIAG_HEADERS})  

ifeq (${o}, 1)
MTF_DIAG_LIB_NAME=libmtf_test.so
MTF_DIAG_LIB_LINK=-lmtf_test
else
MTF_DIAG_LIB_NAME=libmtf_test_debug.so
MTF_DIAG_LIB_LINK=-lmtf_test_debug
endif

.PHONY: mtft
.PHONY: install_diag

mtft: install install_diag install_test

install_diag: ${MTF_LIB_INSTALL_DIR}/${MTF_DIAG_LIB_NAME}
${MTF_LIB_INSTALL_DIR}/${MTF_DIAG_LIB_NAME}: ${BUILD_DIR}/${MTF_DIAG_LIB_NAME}
	sudo cp -f ${BUILD_DIR}/${MTF_DIAG_LIB_NAME} $@
	
${BUILD_DIR}/Diagnostics.o: Test/Diagnostics.cc Test/DiagAnalytic.cc Test/DiagInvAnalytic.cc Test/DiagNumeric.cc Test/DiagHelper.cc ${DIAG_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${DIAG_BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/${MTF_DIAG_LIB_NAME}: | ${BUILD_DIR}
${BUILD_DIR}/${MTF_DIAG_LIB_NAME}:  ${DIAG_OBJS}
	${CXX} -shared -o $@  ${DIAG_OBJS} ${MTF_LIB_LINK}	