#include "mtf/Test/Diagnostics.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateAnalyticalData(VectorXd &param_range_vec,
	int n_pts, ADT data_type, const char* fname){
	assert(param_range_vec.size() == ssm_state_size);

	//const char *data_name = getADTName(data_type);
	//printf("Computing analytical %s %s data for %d points with ", update_name, data_name, n_pts);
	//utils::printMatrix(param_range_vec, "parameter range");
	//utils::printMatrixToFile(init_dist_vec.transpose().eval(), "init_dist_vec", "log/diag_log.txt");

	//VectorXd param_range_vec = param_range_vec.array() / ssm_grad_norm_mean.array();
	VectorXd param_range_vec_norm = param_range_vec;

	utils::printMatrix(param_range_vec_norm.transpose(), "param_range_vec_norm");
	//VectorXd base_state = ssm->getInitState();
	VectorXd min_state = - param_range_vec_norm;
	VectorXd max_state = param_range_vec_norm;

	//utils::printMatrix(param_range_vec_norm, "param_range_vec_norm");
	//utils::printMatrix(min_state, "min_state");
	//utils::printMatrix(max_state, "max_state");


	VectorXd state_update(ssm_state_size);
	diagnostics_data.resize(n_pts, 2 * ssm_state_size);
	//if(data_type == iHessian){
	//	am->cmptCurrHessian(hessian, init_pix_jacobian, init_pix_hessian);		
	//}

	double data_val;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		//printf("Processing state parameter %d....\n", state_id);

		state_update.setZero();
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){

			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			//utils::printMatrix(ssm->getCorners(), "init_corners");
			updateSSM(state_update);
			//utils::printMatrix(state_update, "state_update");
			//utils::printMatrix(ssm->getCorners(), "curr_corners");

			switch(data_type){
			case  ADT::Norm:
				updateCurrSimilarity();
				data_val = am->getSimilarity();
				break;
			case  ADT::FeatNorm:
				am->updatePixVals(ssm->getPts());
				am->updateDistFeat();
				//utils::printMatrixToFile(am->updateDistFeat().transpose().eval(), "curr_dist_vec", "log/diag_log.txt");
				data_val = (*am)(init_dist_vec.data(), am->getDistFeat(), am_dist_size);
				break;
			case  ADT::StdJac:
				updateCurrGrad();
				updateCurrPixJacobian();
				am->cmptCurrJacobian(similarity_jacobian, curr_pix_jacobian);
				data_val = similarity_jacobian(state_id);
				break;
			case  ADT::ESMJac:
				updateCurrGrad();
				updateCurrPixJacobian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				am->cmptCurrJacobian(similarity_jacobian, mean_pix_jacobian);
				data_val = similarity_jacobian(state_id);
				break;
			case  ADT::DiffOfJacs:
				updateCurrGrad();
				am->updateInitGrad();
				updateCurrPixJacobian();
				am->cmptDifferenceOfJacobians(similarity_jacobian, init_pix_jacobian, curr_pix_jacobian);
				data_val = similarity_jacobian(state_id)*0.5;
				break;
			case  ADT::Std:
				updateCurrPixJacobian();
				updateCurrGrad();
				am->cmptCurrHessian(hessian, curr_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::ESM:
				updateCurrGrad();
				updateCurrPixJacobian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				am->cmptCurrHessian(hessian, mean_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::InitSelf:
				data_val = init_self_hessian(state_id, state_id);
				break;
			case  ADT::CurrSelf:
				updateCurrSelfHess();
				updateCurrPixJacobian();
				am->cmptSelfHessian(hessian, curr_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::Std2:
				updateCurrGrad();
				updateCurrPixJacobian();
				updateCurrPixHessian();
				am->cmptCurrHessian(hessian, curr_pix_jacobian,
					curr_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::ESM2:
				updateCurrGrad();
				updateCurrPixJacobian();
				updateCurrPixHessian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				mean_pix_hessian = (init_pix_hessian + curr_pix_hessian) / 2.0;
				am->cmptCurrHessian(hessian, mean_pix_jacobian,
					mean_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::InitSelf2:
				data_val = init_self_hessian2(state_id, state_id);
				break;
			case  ADT::CurrSelf2:
				updateCurrSelfHess();
				updateCurrPixJacobian();
				updateCurrPixHessian();
				am->cmptSelfHessian(hessian, curr_pix_jacobian,
					curr_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::SumOfStd:
				updateCurrGrad();
				am->updateInitGrad();
				updateCurrPixJacobian();
				am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian);
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			case  ADT::SumOfStd2:
				updateCurrGrad();
				am->updateInitGrad();
				updateCurrPixJacobian();
				updateCurrPixHessian();
				am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian,
					init_pix_hessian, curr_pix_hessian);
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			case  ADT::SumOfSelf:
				updateCurrSelfHess();
				updateCurrPixJacobian();
				am->cmptSelfHessian(hessian, curr_pix_jacobian);
				hessian += init_self_hessian;
				data_val = hessian(state_id, state_id)/2.0;
				break;
			case  ADT::SumOfSelf2:
				updateCurrSelfHess();
				updateCurrPixJacobian();
				updateCurrPixHessian();
				am->cmptSelfHessian(hessian, curr_pix_jacobian,
					curr_pix_hessian);
				hessian += init_self_hessian2;
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			default:
				throw std::invalid_argument("Diagnostics :: Invalid data type specified");
			}			
			if(params.show_patches){
				curr_patch.convertTo(curr_patch_uchar, curr_patch_uchar.type());
				imshow(curr_patch_win_name, curr_patch_uchar);
			}
			if(params.show_corners){
				curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
				char fps_text[100];
				snprintf(fps_text, 100, "param: %d range: %f", state_id, param_range_vec_norm(state_id));
				putText(curr_img_cv_uchar, fps_text, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.50, cv::Scalar(255, 255, 255));
				drawCurrCorners(curr_img_cv_uchar, state_id);
				imshow(curr_img_win_name, curr_img_cv_uchar);
				if(cv::waitKey(1) == 27)
					exit(0);
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;
			resetSSM(state_update);
			//if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	if(fname){
		printf("Writing diagnostics data to: %s\n", fname);
		utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
	}
}
_MTF_END_NAMESPACE
