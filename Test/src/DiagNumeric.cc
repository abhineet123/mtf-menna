#include "mtf/Test/Diagnostics.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateNumericalData(VectorXd &param_range_vec,
	int n_pts, NDT data_type, const char* fname, double grad_diff){
	assert(param_range_vec.size() == ssm_state_size);

	//printf("Computing numerical %s %s data for %d points with grad_diff: %f and ",
	//	update_name, data_name, n_pts, grad_diff);
	//utils::printMatrix(param_range_vec, "parameter range");


	//VectorXd param_range_vec = param_range_vec.array() / ssm_grad_norm_mean.array();
	VectorXd param_range_vec_norm = param_range_vec;

	//VectorXd base_state = ssm->getInitState();
	VectorXd min_state = -param_range_vec_norm;
	VectorXd max_state = param_range_vec_norm;

	VectorXd state_update(ssm_state_size), grad_update(ssm_state_size);
	diagnostics_data.resize(n_pts, 2 * ssm_state_size);

	double grad_mult_factor = 1.0 / (2 * grad_diff);
	double data_val;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		//printf("Processing state parameter %d....\n", state_id);

		state_update.setZero();
		grad_update.setZero();
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			updateSSM(state_update);

			switch(data_type){
			case NDT::Jacobian:
			{
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateCurrSimilarity();
				double norm_inc = am->getSimilarity();	
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateCurrSimilarity();
				double norm_dec = am->getSimilarity();
				resetSSM(grad_update);

				data_val = (norm_inc - norm_dec) * grad_mult_factor;
				break;
			}
			case NDT::Hessian:
			{
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateCurrPixJacobian();
				updateCurrGrad();
				am->cmptCurrJacobian(similarity_jacobian, curr_pix_jacobian);
				double jacobian_inc = similarity_jacobian(state_id);
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateCurrPixJacobian();
				updateCurrGrad();
				am->cmptCurrJacobian(similarity_jacobian, curr_pix_jacobian);
				double jacobian_dec = similarity_jacobian(state_id);
				resetSSM(grad_update);

				data_val = (jacobian_inc - jacobian_dec) * grad_mult_factor;
				break;
			}
			case NDT::NHessian:
			{
				updateCurrSimilarity();
				double norm = am->getSimilarity();

				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateCurrSimilarity();
				double norm_inc = am->getSimilarity();
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateCurrSimilarity();
				double norm_dec = am->getSimilarity();
				resetSSM(grad_update);

				data_val = (norm_inc + norm_dec - 2 * norm) / (grad_diff * grad_diff);
				break;
			}
			default:
				throw std::invalid_argument("Diagnostics :: Invalid numerical data type specified");
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;
			resetSSM(state_update);
			//if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	if(fname){
		printf("Writing diagnostics data to: %s\n", fname);
		utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
	}
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateInverseNumericalData(VectorXd &param_range_vec,
	int n_pts, NDT data_type, const char* fname, double grad_diff){
	assert(param_range_vec.size() == ssm_state_size);

	//printf("Computing numerical %s %s data for %d points with grad_diff: %f and ",
	//	update_name, data_name, n_pts, grad_diff);
	//utils::printMatrix(param_range_vec, "parameter range");

	am->updatePixVals(ssm->getPts());
	updateCurrPixJacobian();
	updateCurrPixHessian();

	ssm->initialize(init_corners);

	VectorXd param_range_vec_norm(ssm_state_size);
	for(int i = 0; i < ssm_state_size; i++){
		param_range_vec_norm(i) = param_range_vec(i) / ssm_grad_norm_mean(i);
	}

	//VectorXd base_state = ssm->getInitState();
	VectorXd min_state = -param_range_vec_norm;
	VectorXd max_state = param_range_vec_norm;

	VectorXd state_update(ssm_state_size), grad_update(ssm_state_size);
	diagnostics_data.resize(n_pts, 2 * ssm_state_size);

	double grad_mult_factor = 1.0 / (2 * grad_diff);
	double data_val;

	// backup the current image
	curr_img_cv = cv::Mat(am->getImgHeight(), am->getImgWidth(), CV_32FC1,
		const_cast<float*>(am->getCurrImg().data()));

	// all further updates need to be done using the initial image;
	// so the current image  is not needed anymore in this call
	am->setCurrImg(init_img_cv);

	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		//printf("Processing state parameter %d....\n", state_id);

		state_update.setZero();
		grad_update.setZero();
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			updateSSM(state_update);

			switch(data_type){
			case NDT::Jacobian:
			{
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateInitSimilarity();
				double norm_inc = am->getSimilarity();
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateInitSimilarity();
				double norm_dec = am->getSimilarity();
				resetSSM(grad_update);

				data_val = (norm_inc - norm_dec) * grad_mult_factor;
				break;
			}
			case NDT::Hessian:
			{
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateInitPixJacobian();
				updateInitSimilarity();
				updateInitGrad();
				am->cmptInitJacobian(similarity_jacobian, init_pix_jacobian);
				double jacobian_inc = similarity_jacobian(state_id);
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateInitPixJacobian();
				updateInitSimilarity();
				updateInitGrad();
				am->cmptInitJacobian(similarity_jacobian, init_pix_jacobian);
				double jacobian_dec = similarity_jacobian(state_id);
				resetSSM(grad_update);

				data_val = (jacobian_inc - jacobian_dec) * grad_mult_factor;
				break;
			}
			case NDT::NHessian:
			{
				updateInitSimilarity();
				double norm = am->getSimilarity();

				grad_update(state_id) = grad_diff;
				updateSSM(grad_update);
				updateInitSimilarity();
				double norm_inc = am->getSimilarity();
				resetSSM(grad_update);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update);
				updateInitSimilarity();
				double norm_dec = am->getSimilarity();
				resetSSM(grad_update);

				data_val = (norm_inc + norm_dec - 2 * norm) / (grad_diff * grad_diff);
				break;
			}
			default:
				throw std::invalid_argument("Diagnostics :: Invalid numerical data type specified");
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;
			resetSSM(state_update);
			//if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	// restore the current image
	am->setCurrImg(curr_img_cv);
	if(fname){
		printf("Writing diagnostics data to: %s\n", fname);
		utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
	}
}

_MTF_END_NAMESPACE
