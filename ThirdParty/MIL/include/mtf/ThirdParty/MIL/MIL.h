#ifndef STRUCK_H
#define STRUCK_H


#include "Tracker.h"
#include "Config.h"

#include "opencv2/core/core.hpp"

#include "mtf/TrackerBase.h"

#define STRUCK_CONFIG_PATH "Config/struck.cfg"
namespace struck{
	struct StruckParams{
		std::string config_path;
		StruckParams(std::string _config_path);
		StruckParams(StruckParams *params = nullptr);
	};


	class Struck : public mtf::TrackerBase{
	public:
		typedef StruckParams ParamType;
		ParamType params;
		Config conf;
		Tracker tracker;
		cv::Mat cv_img;
		cv::Mat cv_img_resized;
		float scaleW, scaleH;
		Struck();
		Struck(StruckParams *struck_params = nullptr);
		bool rgbInput() const override{ return true; }
		void initialize(const cv::Mat &img, const cv::Mat &corners) override;
		void initialize(const cv::Mat& corners) override;
		void update(const cv::Mat &img) override;
		void update() override;
		void updateCVCorners();
	};
}

#endif 
