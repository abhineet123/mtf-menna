/************************************************************************
* File:	CompressiveTracker.h
* Brief: C++ demo for paper: Kaihua Zhang, Lei Zhang, Ming-Hsuan Yang,"Real-Time Compressive Tracking," ECCV 2012.
* Version: 1.0
* Author: Yang Xian
* Email: yang_xian521@163.com
* Date:	2012/08/03
* History:
* Revised by Kaihua Zhang on 14/8/2012, 23/8/2012
* Email: zhkhua@gmail.com
* Homepage: http://www4.comp.polyu.edu.hk/~cskhzhang/
* Project Website: http://www4.comp.polyu.edu.hk/~cslzhang/CT/CT.htm
************************************************************************/
#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include "mtf/TrackerBase.h"

#define RCT_MIN_N_RECT 2
#define RCT_MAX_N_RECT 4
#define RCT_N_FEAT 50
#define RCT_RAD_OUTER_POS 4
#define RCT_RAD_SEARCH_WIN 25
#define RCT_LEARNING_RATE 0.85

using std::vector;

struct RCTParams{
	int min_n_rect;
	int max_n_rect;
	int n_feat;
	int rad_outer_pos;
	int rad_search_win ;
	double learning_rate;

	RCTParams(int _min_n_rect, int _max_n_rect,
		int _n_feat, int _rad_outer_pos,
		int _rad_search_win, double _learning_rate){
		this->min_n_rect = _min_n_rect;
		this->max_n_rect = _max_n_rect;
		this->n_feat = _n_feat;
		this->rad_outer_pos = _rad_outer_pos;
		this->rad_search_win = _rad_search_win;
		this->learning_rate = _learning_rate;

	}
	RCTParams(RCTParams *params = nullptr) : 
		min_n_rect(RCT_MIN_N_RECT), 
		max_n_rect(RCT_MAX_N_RECT),
		n_feat(RCT_N_FEAT), 
		rad_outer_pos(RCT_RAD_OUTER_POS),
		rad_search_win(RCT_RAD_SEARCH_WIN), 
		learning_rate(RCT_LEARNING_RATE){
		if(params){
			min_n_rect = params->min_n_rect;
			max_n_rect = params->max_n_rect;
			n_feat = params->n_feat;
			rad_outer_pos = params->rad_outer_pos;
			rad_search_win = params->rad_search_win;
			learning_rate = params->learning_rate;
		}
	}
};

//---------------------------------------------------
class CompressiveTracker : public mtf::TrackerBase
{
public:
	typedef RCTParams Paramtype;

	Paramtype params;

	CompressiveTracker(void);
	~CompressiveTracker(void);
	CompressiveTracker(const cv::Mat &img, Paramtype *rct_params);
	bool rgbInput() const{ return true; }

private:
	int featureMinNumRect ;
	int featureMaxNumRect ;
	int featureNum;
	vector< vector<cv::Rect > > features;
	vector< vector<float> > featuresWeight;
	int rOuterPositive;
	vector<cv::Rect > samplePositiveBox;
	vector<cv::Rect > sampleNegativeBox;
	int rSearchWindow;
	cv::Mat imageIntegral;
	cv::Mat samplePositiveFeatureValue;
	cv::Mat sampleNegativeFeatureValue;
	vector<float> muPositive;
	vector<float> sigmaPositive;
	vector<float> muNegative;
	vector<float> sigmaNegative;
	float learnRate;
	vector<cv::Rect > detectBox;
	cv::Mat detectFeatureValue;
	cv::RNG rng;
	cv::Mat cv_img;
	cv::Rect  curr_rect;
private:
	void HaarFeature(cv::Rect & _objectBox, int _numFeature);
	void sampleRect (cv::Mat& _image, cv::Rect & _objectBox, float _rInner, float _rOuter, int _maxSampleNum, vector<cv::Rect >& _sampleBox);
	void sampleRect (cv::Mat& _image, cv::Rect & _objectBox, float _srw, vector<cv::Rect >& _sampleBox);
	void getFeatureValue(cv::Mat& _imageIntegral, vector<cv::Rect >& _sampleBox, cv::Mat& _sampleFeatureValue);
	void classifierUpdate(cv::Mat& _sampleFeatureValue, vector<float>& _mu, vector<float>& _sigma, float _learnRate);
	void radioClassifier(vector<float>& _muPos, vector<float>& _sigmaPos, vector<float>& _muNeg, vector<float>& _sigmaNeg,
	                     cv::Mat& _sampleFeatureValue, float& _radioMax, int& _radioMaxIndex);
public:
	void processFrame(cv::Mat& _frame, cv::Rect & _objectBox);
	void init(cv::Mat& _frame, cv::Rect & _objectBox);
	void initialize(const cv::Mat &img, const cv::Mat &corners);
	void initialize(const cv::Mat& init_corners);
	void update(const cv::Mat &img){
		cv::cvtColor(img, cv_img, CV_BGR2GRAY);
		//printf("img_height: %d\n", cv_img.rows);
		//printf("img_width: %d\n", cv_img.cols);
		update();
	}
	void update(){
		processFrame(cv_img, curr_rect);
		updateCVCorners();
	}
	const cv::Mat& getRegion(){		
		return cv_corners_mat;
	}
	void updateCVCorners();

};
