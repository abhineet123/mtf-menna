/*
 * DSST.cpp
 *
 *  Created on: 22 May, 2015
 *      Author: Sara & Mennatullah
 */

#include "mtf/ThirdParty/DSST/DSST.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <math.h>
#include "mtf/ThirdParty/DSST/HOG.h"
#include <string>

#define eps 0.0001

// unit vectors used to compute gradient orientation
static double uu[9] = { 1.0000, 0.9397, 0.7660, 0.500, 0.1736, -0.1736, -0.5000, -0.7660, -0.9397 };
static double vv[9] = { 0.0000, 0.3420, 0.6428, 0.8660, 0.9848, 0.9848, 0.8660, 0.6428, 0.3420 };

/*static inline double round(double num)
{
return (num > 0.0) ? floor(num + 0.5) : ceil(num - 0.5);
}*/
static inline float min(float x, float y)
{
	return (x <= y ? x : y);
}
static inline float max(float x, float y)
{
	return (x <= y ? y : x);
}

/*static inline double round(double num)
{
return (num > 0.0) ? floor(num + 0.5) : ceil(num - 0.5);
}*/
/*static inline int min(int x, int y)
{
return (x <= y ? x : y);
}
static inline int max(int x, int y)
{
return (x <= y ? y : x);
}
*/

cv::Mat DSSTTracker::convertFloatImg(cv::Mat &img)
{
	cv::Mat imgU;
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;
	minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);

	img -= minVal;
	//img.convertTo(imgU,CV_8U,255.0/(maxVal-minVal));
	//cout<<"Converting float image to 8 unsigned char"<<endl;
	img.convertTo(imgU, CV_8U);
	return imgU;
}

DSSTTracker::DSSTTracker(const cv::Mat &img, DSSTParams *params) :
TrackerBase(), tParams(params)
{
	name = "dsst";
	tSetup.num_trans = 0;
	//tSetup.enableScaling= false;
	tSetup.num_scale = 0;

	//currFrame= convertFloatImg(img);
	currFrame = img;
	currFrame.data = img.data;
	//imshow("testing sth", currFrame);
	//waitKey();
}

void DSSTTracker::initialize(const cv::Mat& corners)
{
	//utils::convertEigenToCV<MatrixXuc, double>(curr_img, currFrame);
	//cv::Mat currFrame2= convertFloatImg(currFrame);
	//imshow("testing teet", currFrame2);
	//waitKey();

	int width = corners.at<double>(0, 1) - corners.at<double>(0, 0);
	int height = corners.at<double>(1, 2) - corners.at<double>(1, 0);
	cv::Rect rect;
	rect.x = corners.at<double>(0, 0) / tParams.resize_factor;
	rect.y = corners.at<double>(1, 0) / tParams.resize_factor;
	rect.width = width / tParams.resize_factor;
	rect.height = height / tParams.resize_factor;
	cv::Mat scaledCurrFrame;
	resize(currFrame, scaledCurrFrame, cv::Size(currFrame.cols / tParams.resize_factor, currFrame.rows / tParams.resize_factor));
	//cout<<"Resized the image to "<<currFrame.cols/tParams.resize_factor<<" "<<currFrame.rows/tParams.resize_factor<<endl;

	//cout<<"Before preprocess "<<rect.x<<" "<<rect.y<<" "<<rect.width<<" "<<rect.height<<endl;
	//imwrite("/home/mennatullah/test.jpg", currFrame2);
	preprocess(height, width, scaledCurrFrame, rect);

	currCorners = corners;
	cv_corners_mat = corners;
}

void DSSTTracker::update()
{
	//utils::convertEigenToCV<MatrixXuc, unsigned char>(curr_img, currFrame);
	//cv::Mat imgTemp= currFrame.clone();
	//cv::Mat currFrame2= convertFloatImg(currFrame);
	/*imshow("Init Testing ", imgTemp);
	waitKey();*/
	cv::Mat scaledCurrFrame;
	resize(currFrame, scaledCurrFrame, cv::Size(currFrame.cols / tParams.resize_factor, currFrame.rows / tParams.resize_factor));
	cv::Rect rect = processFrame(scaledCurrFrame, tParams.is_scaling);
	//cout<<"Finished processing"<<endl;
	cv::Mat corners(2, 4, CV_64FC1);
	corners.at<double>(0, 0) = rect.x*tParams.resize_factor;
	corners.at<double>(0, 1) = rect.x*tParams.resize_factor + rect.width*tParams.resize_factor;
	corners.at<double>(0, 2) = rect.x*tParams.resize_factor + rect.width*tParams.resize_factor;
	corners.at<double>(0, 3) = rect.x*tParams.resize_factor;
	corners.at<double>(1, 0) = rect.y*tParams.resize_factor;
	corners.at<double>(1, 1) = rect.y*tParams.resize_factor;
	corners.at<double>(1, 2) = rect.y*tParams.resize_factor + rect.height*tParams.resize_factor;
	corners.at<double>(1, 3) = rect.y*tParams.resize_factor + rect.height*tParams.resize_factor;
	currCorners = corners;
	cv_corners_mat = corners;
}

cv::Mat DSSTTracker::inverseFourier(cv::Mat original, int flag)
{
	cv::Mat output;
	cv::idft(original, output, cv::DFT_REAL_OUTPUT | cv::DFT_SCALE);  // Applying DFT without padding
	return output;
}

cv::Mat DSSTTracker::createFourier(cv::Mat original, int flag)
{
	cv::Mat planes[] = { cv::Mat_<double>(original), cv::Mat::zeros(original.size(), CV_64F) };
	cv::Mat complexI;
	cv::merge(planes, 2, complexI);
	cv::dft(complexI, complexI, flag);  // Applying DFT without padding
	return complexI;
}

cv::Mat DSSTTracker::hann(int size)
{
	cv::Mat arr(size, 1, CV_32FC1);
	float multiplier;
	for(int i = 0; i < size; i++)
	{
		multiplier = 0.5 * (1 - cos(2 * M_PI*i / (size - 1)));
		*((float *)(arr.data + i*arr.step[0])) = multiplier;
	}
	return arr;
}

cv::Mat DSSTTracker::convert2DImageFloat(double *arr, int w, int h)
{
	int k = 0;
	cv::Mat img(h, w, CV_32FC1);

	for(int i = 0; i < img.cols; i++)
		for(int j = 0; j < img.rows; j++)
		{
			img.at<float>(j, i) = (float)arr[k];
			k++;
		}

	return img;
}

cv::Mat DSSTTracker::convertNormalizedFloatImg(cv::Mat &img)
{
	cv::Mat imgU;
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;
	minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);

	img -= minVal;
	img.convertTo(imgU, CV_64FC1, 1.0 / (maxVal - minVal));
	return imgU;
}

double *DSSTTracker::computeMeanVariance(cv::Mat trans_response)
{
	double mean = 0;
	for(int i = 0; i < trans_response.rows; i++)
	{
		for(int j = 0; j < trans_response.cols; j++)
		{
			mean += trans_response.at<double>(i, j);
		}
	}
	mean = mean / (trans_response.rows*trans_response.cols);

	double variance = 0;
	for(int i = 0; i < trans_response.rows; i++)
	{
		for(int j = 0; j < trans_response.cols; j++)
		{
			variance += pow(trans_response.at<double>(i, j) - mean, 2);
		}
	}
	//cout<<"Variance "<<variance<<endl;
	variance = variance / (trans_response.cols*trans_response.rows);
	//cout<<"Variance again "<<variance<<endl;
	double *params = new double[2];
	params[0] = mean;
	params[1] = sqrt(variance);

	//cout<<"Variance last time"<<params[1]<<endl;

	return params;
}

double DSSTTracker::computeCorrelationVariance(double *arr, int arrW, int arrH)
{
	//Assert on the width and height of the image!
	if(arrW != tSetup.padded.width || arrH != tSetup.padded.height)
	{
		cout << "Error in the size of the patch" << endl;
		return -1;
	}

	//Convert 1D array to 2D patch
	cv::Mat patch = convert2DImageFloat(arr, arrW, arrH);
	cv::Mat roiGrayFlot = patch;
	//imshow("testing patch ", patch);
	//waitKey();

	int nDims = 0;
	roiGrayFlot = roiGrayFlot.mul((float)1 / 255);
	roiGrayFlot = roiGrayFlot - 0.5;
	//imshow("testing float ", roiGrayFlot);
	//waitKey();

	//Compute HOG  features
	int nChns;
	cv::Mat *featureMap = create_feature_map(patch, 1, nChns, roiGrayFlot, false);

	//Compute Correlation
	nDims = nChns;
	for(int i = 0; i < nChns; i++)
		featureMap[i] = featureMap[i].mul(tSetup.trans_cos_win);

	cv::Mat *feature_map_fourier = new cv::Mat[nDims];
	for(int i = 0; i < nDims; i++)
	{
		cv::Mat feature_map_double(featureMap[i].rows, featureMap[i].cols, CV_64FC1);
		featureMap[i].convertTo(feature_map_double, CV_64FC1);
		feature_map_fourier[i] = createFourier(feature_map_double);
	}

	cv::Mat* temp = new cv::Mat[nDims];
	for(int i = 0; i < nDims; i++)
		mulSpectrums(tSetup.num_trans[i], feature_map_fourier[i], temp[i], 0, false);

	int w = tSetup.num_trans[0].cols, h = tSetup.num_trans[0].rows;

	cv::Mat sumDen(h, w, CV_64F);

	for(int j = 0; j < h; j++)
		for(int k = 0; k < w; k++)
			sumDen.at<double>(j, k) = tSetup.den_trans.at<cv::Vec2d>(j, k)[0] + tParams.lambda;

	cv::Mat sumTemp(h, w, CV_64FC2);
	sumTemp = cv::Mat::zeros(sumTemp.size(), CV_64FC2);
	for(int j = 0; j < h; j++)
		for(int k = 0; k < w; k++)
		{
			for(int i = 0; i < nDims; i++)
				sumTemp.at<cv::Vec2d>(j, k) += temp[i].at<cv::Vec2d>(j, k);

			sumTemp.at<cv::Vec2d>(j, k) /= sumDen.at<double>(j, k);
		}

	//Compute Final Translation Response
	cv::Mat trans_response = cv::Mat::zeros(sumTemp.rows, sumTemp.cols, CV_64FC1);
	trans_response = inverseFourier(sumTemp);
	cv::Mat trans2 = convertNormalizedFloatImg(trans_response);
	//imshow("Fake Translation Response ", trans2);
	//waitKey();

	double *params = computeMeanVariance(trans2);
	double var = params[1];

	delete[] params;
	delete[] featureMap;
	delete[] feature_map_fourier;
	delete[] temp;

	return var;
}

float *DSSTTracker::convert1DArray(cv::Mat &patch)
{
	float *img = (float*)calloc(patch.rows*patch.cols, sizeof(float));

	int k = 0;
	for(int i = 0; i < patch.cols; i++)
		for(int j = 0; j < patch.rows; j++)
		{
			img[k] = (float)patch.at<float>(j, i);
			k++;
		}
	return img;
}

double *DSSTTracker::convert1DArrayDouble(cv::Mat &patch)
{
	double *img = (double*)calloc(patch.rows*patch.cols, sizeof(double));

	int k = 0;
	for(int i = 0; i < patch.cols; i++)
		for(int j = 0; j < patch.rows; j++)
		{
			img[k] = (double)patch.at<float>(j, i);
			k++;
		}
	return img;
}

cv::Mat DSSTTracker::convert2DImage(float *arr, int w, int h)
{
	int k = 0;
	cv::Mat img(h, w, CV_32F);

	for(int i = 0; i < img.cols; i++)
		for(int j = 0; j < img.rows; j++)
		{
			img.at<float>(j, i) = arr[k];
			k++;
		}

	cv::Mat imgU;
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;
	minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);
	img -= minVal;
	img.convertTo(imgU, CV_8U, 255.0 / (maxVal - minVal));

	return imgU;
}
cv::Point DSSTTracker::ComputeMaxDisplayfl(cv::Mat &img, string winName)
{
	cv::Mat imgU;
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;
	minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);

	return maxLoc;
}

cv::Mat *DSSTTracker::create_feature_map(cv::Mat& patch, int full, int &nChns, cv::Mat& Gray, bool scaling)
{
	int h = patch.rows, w = patch.cols;
	float* M = (float*)calloc(h*w, sizeof(float));
	float* O = (float*)calloc(h*w, sizeof(float));

	float *img = convert1DArray(patch);
	//patch= convert2DImage(img, w, h);
	//cv::Mat patch8U= convertFloatImg(patch);
	//cout<<"Before Computing Gradients"<<endl;
	gradMag(img, M, O, h, w, 1, full);
	//imshow("Patch testing", patch8U);
	//waitKey();
	//cout<<"After Computing Gradients"<<endl;
	if(!scaling)
	{
		hParams.binSize = tParams.bin_size;//1;
	} else
	{
		hParams.binSize = 4;
	}
	int hb = h / hParams.binSize; int wb = w / hParams.binSize;

	nChns = hParams.nOrients * 3 + 5;
	float *H = (float*)calloc(hb*wb*nChns, sizeof(float));
	//cout<<"Before FHOG SSE"<<endl;
	fhogSSE(M, O, H, h, w, hParams.binSize, hParams.nOrients, hParams.softBin, hParams.clipHog);
	//cout<<"After FHOG SSE"<<endl;
	cv::Mat GrayRes;
	if(!scaling)
		resize(Gray, GrayRes, cv::Size(wb, hb));
	//cout<<"Gray size patch "<<GrayRes.cols<<" "<<GrayRes.rows<<" "<<wb<<" "<<hb<<endl;
	int l = 0;
	cv::Mat *featureMap;
	if(!scaling)
	{
		nChns = 28;
		featureMap = new cv::Mat[nChns];
		for(int i = 0; i < nChns; i++)
			featureMap[i] = cv::Mat(hb, wb, CV_32FC1);

		GrayRes.convertTo(featureMap[0], CV_32FC1);
		for(int j = 0; j < wb; j++)
			for(int i = 0; i < hb; i++)
				for(int k = 0; k < nChns - 1; k++)
					featureMap[k + 1].at<float>(i, j) = H[k*(hb*wb) + j*hb + i];

		//cout<<"finished the feature map "<<endl;
	} else
	{
		nChns = 31;
		featureMap = new cv::Mat[nChns];
		for(int i = 0; i < nChns; i++)
			featureMap[i] = cv::Mat(hb, wb, CV_32FC1);

		for(int j = 0; j < wb; j++)
			for(int i = 0; i < hb; i++)
				for(int k = 0; k < nChns; k++)
					featureMap[k].at<float>(i, j) = H[k*(hb*wb) + j*hb + i];


	}

	free(img);
	free(H);
	free(M);
	free(O);

	return featureMap;
}

cv::Mat DSSTTracker::get_scale_sample(cv::Mat img, trackingSetup tSetup, DSSTParams tParams, int &nDims, bool display)
{
	cv::Mat featureMapScale;
	CvRect patchSize;

	cv::Mat roiGray;
	cv::Mat roiResized;
	cv::Mat feature_map_scale_fourier;
	for(int i = 0; i < tParams.number_scales; i++)
	{
		float pw = tSetup.scaleFactors[i] * tSetup.current_scale_factor*tSetup.original.width;
		float ph = tSetup.scaleFactors[i] * tSetup.current_scale_factor*tSetup.original.height;

		int tlX1, tlY1, w1, w2, h1, h2;
		tlX1 = max(0, (int)ceil(tSetup.centroid.x + 1 - pw / 2));
		int padToX = (int)ceil(tSetup.centroid.x + 1 - pw / 2) < 0 ? (int)ceil(tSetup.centroid.x + 1 - pw / 2) : 0;
		w1 = (padToX + pw);
		w2 = (tlX1 + w1) >= img.cols ? img.cols - tlX1 : w1;

		tlY1 = max(0, (int)ceil(tSetup.centroid.y + 1 - ph / 2));
		int padToY = (int)ceil(tSetup.centroid.y + 1 - ph / 2) < 0 ? (int)ceil(tSetup.centroid.y + 1 - ph / 2) : 0;
		h1 = (padToY + ph);
		h2 = (tlY1 + h1) >= img.rows ? img.rows - tlY1 : h1;

		cv::Rect rect(tlX1, tlY1, w2, h2);
		cv::Mat patch = img(rect);
		cv::Mat roi;

		copyMakeBorder(patch, roi, abs(padToY), h1 - h2, abs(padToX), w1 - w2, cv::BORDER_REPLICATE);
		cv::Rect patchSize = cv::Rect(tlX1, tlY1, roi.cols, roi.rows);

		int interpolation;
		if(tSetup.scale_model_sz.width > patchSize.width)
			interpolation = cv::INTER_LINEAR;
		else
			interpolation = cv::INTER_AREA;
		resize(roi, roiResized, cv::Size(tSetup.scale_model_sz.width, tSetup.scale_model_sz.height), 0, 0, interpolation);

		int nChns;
		cv::Mat m = cv::Mat();
		cv::Mat *featureMap = create_feature_map(roiResized, 1, nChns, m, true);

		float s = tSetup.scale_cos_win.at<float>(i, 0);

		if(featureMapScale.data == NULL)//i==0)
		{
			featureMapScale = cv::Mat(featureMap[0].rows*featureMap[0].cols*nChns, tParams.number_scales, CV_32FC1);
			feature_map_scale_fourier = cv::Mat(featureMap[0].rows*featureMap[0].cols*nChns, tParams.number_scales, CV_32FC1);
		}

		int k = 0;

		for(int j = 0; j < nChns; j++)
		{
			for(int m = 0; m < featureMap[j].cols; m++)
				for(int l = 0; l < featureMap[j].rows; l++)
				{
					featureMapScale.at<float>(k, i) = featureMap[j].at<float>(l, m) * s;
					k++;
				}
		}
		delete[]featureMap;
		if(display){
			imshow("roi", roi);
			cv::waitKey();
		}
	}
	cv::Mat featureMapTempDouble;
	featureMapScale.convertTo(featureMapTempDouble, CV_64FC1);

	cv::Mat feature_map_scale_fourier_temp = createFourier(featureMapTempDouble, cv::DFT_ROWS);

	nDims = tParams.number_scales;

	return feature_map_scale_fourier_temp;
}

cv::Mat *DSSTTracker::get_translation_sample(cv::Mat img, trackingSetup tSet, int &nDims)
{
	float pw = tSetup.padded.width*tSet.current_scale_factor;
	float ph = tSetup.padded.height*tSet.current_scale_factor;
	int centerX = tSetup.centroid.x + 1;
	int centerY = tSetup.centroid.y + 1;

	int tlX1, tlY1, w1, w2, h1, h2;
	tlX1 = max(0, (int)ceil(centerX - pw / 2));
	int padToX = (int)ceil(centerX - pw / 2) < 0 ? (int)ceil(centerX - pw / 2) : 0;
	w1 = (padToX + pw);
	w2 = (tlX1 + w1) >= img.cols ? img.cols - tlX1 : w1;

	tlY1 = max(0, (int)ceil(centerY - ph / 2));
	int padToY = (int)ceil(centerY - ph / 2) < 0 ? (int)ceil(centerY - ph / 2) : 0;
	h1 = (padToY + ph);
	h2 = (tlY1 + h1) >= img.rows ? img.rows - tlY1 : h1;

	cv::Rect rect(tlX1, tlY1, w2, h2);
	cv::Mat patch = img(rect);
	cv::Mat roi;
	copyMakeBorder(patch, roi, abs(padToY), h1 - h2, abs(padToX), w1 - w2, cv::BORDER_REPLICATE);
	//cv::Mat roiDisplay= convertFloatImg(roi);
	//imshow("testing " , roiDisplay);
	//waitKey();
	cv::Rect patchSize = cv::Rect(tlX1, tlY1, roi.cols, roi.rows);

	int interpolation;
	if(tSetup.padded.width > patchSize.width)
		interpolation = cv::INTER_LINEAR;
	else
		interpolation = cv::INTER_AREA;

	resize(roi, roi, cv::Size(tSetup.padded.width, tSetup.padded.height), 0, 0, interpolation);
	//cv::Mat roiGray= roi;
	//cv::cvtColor(roi, roiGray, CV_BGR2GRAY);

	//cv::Mat roiGrayFlot(roiGray.rows, roiGray.cols, CV_32FC1);
	//roiGray.convertTo(roiGrayFlot,CV_32FC1);
	cv::Mat roiGrayFlot = roi;
	roiGrayFlot = roiGrayFlot.mul((float)1 / 255);
	roiGrayFlot = roiGrayFlot - 0.5;

	int hb, wb, nChns;
	//cout<<"Before creating feature map"<<endl;
	cv::Mat *featureMap = create_feature_map(roi, 1, nChns, roiGrayFlot, false);
	//cout<<"After creating feature map"<<featureMap[0].cols<<" "<<featureMap[0].rows<<endl;
	nDims = nChns;

	for(int i = 0; i < nChns; i++)
		featureMap[i] = featureMap[i].mul(tSetup.trans_cos_win);
	//cout<<"Multiplied with cosine window "<<endl; 

	return featureMap;
}

void DSSTTracker::train(bool first, cv::Mat img)
{
	//cout<<"Entered training "<<endl;
	//Model update:
	//1- Extract samples ftrans and fscale from It at pt and st .
	//A- Extract translation sample
	int nDims = 0;
	cv::Mat *feature_map = get_translation_sample(img, tSetup, nDims);
	//cout<<"After translation sample "<<endl;

	//B- Compute Denominator Translation, Numerator Translation
	cv::Mat *feature_map_fourier = new cv::Mat[nDims];
	cv::Mat *num = new cv::Mat[nDims];

	cv::Mat den(feature_map[0].rows, feature_map[0].cols, CV_64FC2);
	den = cv::Mat::zeros(feature_map[0].rows, feature_map[0].cols, CV_64FC2);

	//cout<<"Feature Map Fourier Translation"<<endl;
	for(int i = 0; i < nDims; i++)
	{
		cv::Mat feature_map_double(feature_map[i].rows, feature_map[i].cols, CV_64FC1);
		feature_map[i].convertTo(feature_map_double, CV_64FC1);
		feature_map_fourier[i] = createFourier(feature_map_double);
		mulSpectrums(tSetup.transFourier, feature_map_fourier[i], num[i], 0, true);

		cv::Mat temp;
		mulSpectrums(feature_map_fourier[i], feature_map_fourier[i], temp, 0, true);
		den = den + temp;
	}
	//cout<<"Current Scale tany: "<<tSetup.current_scale_factor<<endl;

	int nDimsScale;
	cv::Mat *num_scale;
	cv::Mat den_scale;
	cv::Mat feature_map_scale_fourier;
	if(tParams.is_scaling)
	{
		feature_map_scale_fourier = get_scale_sample(img, tSetup, tParams, nDimsScale);//I have to convert featuremap to double first

		num_scale = new cv::Mat[feature_map_scale_fourier.rows];
		den_scale = cv::Mat(1, nDimsScale, CV_64FC2);
		den_scale = cv::Mat::zeros(1, nDimsScale, CV_64FC2);

		for(int i = 0; i < feature_map_scale_fourier.rows; i++)
		{
			cv::Mat temp(1, nDimsScale, CV_64FC2);
			for(int j = 0; j < nDimsScale; j++)
				temp.at<cv::Vec2d>(0, j) = feature_map_scale_fourier.at<cv::Vec2d>(i, j);

			mulSpectrums(tSetup.scaleFourier, temp, num_scale[i], 0, true);

		}

		cv::Mat temp;
		mulSpectrums(feature_map_scale_fourier, feature_map_scale_fourier, temp, 0, true);
		//cout<<"Temp "<<temp.rows<<" "<<temp.cols<<endl;
		/*cout<<"Feature scale Fourier"<<endl;
		for(int i = 0 ; i < feature_map_scale_fourier.rows ; i++){
		for(int j = 0 ; j < feature_map_scale_fourier.cols ; j++)
		cout<<feature_map_scale_fourier.at<cv::Vec2d>(i,j)<<"  ";
		cout<<endl;
		}*/
		for(int i = 0; i < temp.cols; i++)
		{
			for(int j = 0; j < temp.rows; j++)
			{
				den_scale.at<cv::Vec2d>(0, i)[0] += temp.at<cv::Vec2d>(j, i)[0];
				den_scale.at<cv::Vec2d>(0, i)[1] += temp.at<cv::Vec2d>(j, i)[1];
			}
		}
	}
	//cout<<"Updating Model "<<endl;

	//Update Our Model
	if(first)
	{
		tSetup.num_trans = num;
		tSetup.nNumTrans = nDims;
		tSetup.den_trans = den;

		if(tParams.is_scaling)
		{
			tSetup.num_scale = num_scale;
			tSetup.nNumScale = nDimsScale;
			tSetup.den_scale = den_scale;
		}
	} else
	{
		for(int i = 0; i < tSetup.nNumTrans; i++)
			tSetup.num_trans[i] = tSetup.num_trans[i].mul(1 - tParams.learning_rate) + num[i].mul(tParams.learning_rate);
		tSetup.den_trans = tSetup.den_trans.mul(1 - tParams.learning_rate) + den.mul(tParams.learning_rate);
		delete[] num;

		if(tParams.is_scaling)
		{
			for(int i = 0; i < feature_map_scale_fourier.rows; i++)
				tSetup.num_scale[i] = tSetup.num_scale[i].mul(1 - tParams.learning_rate) + num_scale[i].mul(tParams.learning_rate);
			tSetup.den_scale = tSetup.den_scale.mul(1 - tParams.learning_rate) + den_scale.mul(tParams.learning_rate);

			delete[] num_scale;
		}

	}
	delete[] feature_map;
	delete[] feature_map_fourier;
}

cv::Point DSSTTracker::updateCentroid(cv::Point oldC, int w, int h, int imgw, int imgh)
{
	bool outBorder = false;
	int left = oldC.x - w / 2;
	if(left <= 0)
	{
		left = 1;
		outBorder = true;
	}
	int top = oldC.y - h / 2;
	if(top <= 0)
	{
		top = 1;
		outBorder = true;
	}

	if((left + w) >= imgw)
	{
		left = imgw - w - 1;
		outBorder = true;
	}

	if((top + h) >= imgh)
	{
		top = imgh - h - 1;
		outBorder = true;
	}
	cv::Point newPt;
	if(outBorder)
	{
		newPt.x = left + w / 2;
		newPt.y = top + h / 2;
	} else
		newPt = oldC;
	return newPt;
}

cv::Rect DSSTTracker::processFrame(cv::Mat img, bool enableScaling)
{
	//ofstream fout("C://Users//mincosy//Desktop//Tracking//Final_DSST_C++//DSST//logzebala.txt",  std::ofstream::out | std::ofstream::app);
	//fout<<"entered process frame"<<endl;

	/* //Testing the Usage of ComputeCorrelationVariance
	cv::Point originalCentroid= tSetup.centroid;
	ofstream fout("/home/mennatullah/Desktop/test.txt");
	for(int sx= -100; sx<50; sx+=10)
	{
	for(int sy=-100; sy<50; sy+=10)
	{
	//Create Patch Array
	float pw= tSetup.padded.width*tSetup.current_scale_factor;
	float ph= tSetup.padded.height*tSetup.current_scale_factor;

	int centerX= originalCentroid.x + sx + 1;
	int centerY= originalCentroid.y + sy + 1;

	//Accounting for Borders
	int tlX1, tlY1, w1, w2, h1, h2;
	tlX1= max(0,(int)ceil(centerX-pw/2));
	int padToX = (int)ceil(centerX-pw/2) < 0 ? (int)ceil(centerX-pw/2) : 0;
	w1 = (padToX + pw);
	w2 = (tlX1 + w1) >= img.cols ? img.cols - tlX1 : w1;

	tlY1= max(0,(int)ceil(centerY-ph/2));
	int padToY = (int)ceil(centerY-ph/2) < 0 ? (int)ceil(centerY-ph/2) : 0;
	h1 = (padToY + ph);
	h2 = (tlY1 + h1) >= img.rows ? img.rows - tlY1 : h1;

	cv::Rect rect(tlX1,tlY1,w2,h2);
	cv::Mat patch = img(rect);
	cv::Mat roi;
	copyMakeBorder(patch, roi,abs(padToY),h1 - h2, abs(padToX), w1-w2, BORDER_REPLICATE);

	cv::Rect patchSize= Rect(tlX1, tlY1, roi.cols, roi.rows);
	int interpolation;
	if(tSetup.padded.width > patchSize.width)
	interpolation = INTER_LINEAR;
	else
	interpolation = INTER_AREA;
	resize(roi,roi,cv::Size(tSetup.padded.width, tSetup.padded.height),0,0,interpolation);
	//imshow("testing ", roi);
	//waitKey();

	double *arr= convert1DArrayDouble(roi);

	//Compute Variance for the correlation of patch with filter
	double variance= computeCorrelationVariance(arr, roi.cols, roi.rows);
	double cle= sqrt(pow(sx, 2) + pow(sy, 2));
	fout<<sx<<" "<<sy<<" "<<cle<<" "<<variance<<endl;
	}
	}
	fout.close();
	tSetup.centroid= originalCentroid;
	*/
	int nDims = 0;
	cv::Mat *feature_map = get_translation_sample(img, tSetup, nDims);
	cv::Mat *feature_map_fourier = new cv::Mat[nDims];

	for(int i = 0; i < nDims; i++)
	{
		cv::Mat feature_map_double(feature_map[i].rows, feature_map[i].cols, CV_64FC1);
		feature_map[i].convertTo(feature_map_double, CV_64FC1);
		feature_map_fourier[i] = createFourier(feature_map_double);
	}

	//response = real(ifft2(sum(hf_num .* xtf, 3) ./ (hf_den + lambda)));	
	cv::Mat* temp = new cv::Mat[nDims];
	for(int i = 0; i < nDims; i++)
		mulSpectrums(tSetup.num_trans[i], feature_map_fourier[i], temp[i], 0, false);

	int w = tSetup.num_trans[0].cols, h = tSetup.num_trans[0].rows;

	cv::Mat sumDen(h, w, CV_64F);

	for(int j = 0; j < h; j++)
		for(int k = 0; k < w; k++)
			sumDen.at<double>(j, k) = tSetup.den_trans.at<cv::Vec2d>(j, k)[0] + tParams.lambda;

	cv::Mat sumTemp(h, w, CV_64FC2);
	sumTemp = cv::Mat::zeros(sumTemp.size(), CV_64FC2);
	for(int j = 0; j < h; j++)
		for(int k = 0; k < w; k++)
		{
			for(int i = 0; i < nDims; i++)
				sumTemp.at<cv::Vec2d>(j, k) += temp[i].at<cv::Vec2d>(j, k);

			sumTemp.at<cv::Vec2d>(j, k) /= sumDen.at<double>(j, k);
		}

	cv::Mat trans_response = cv::Mat::zeros(sumTemp.rows, sumTemp.cols, CV_64FC1);
	trans_response = inverseFourier(sumTemp);
	//imshow("Translation Response ", trans_response);
	//waitKey();

	cv::Point maxLoc = ComputeMaxDisplayfl(trans_response);
	//imshow("trans_response", trans_response);
	//waitKey();
	maxLoc.x = maxLoc.x * (tSetup.padded.width / trans_response.cols);
	maxLoc.y = maxLoc.y * (tSetup.padded.height / trans_response.rows);

	tSetup.centroid.x += cvRound((maxLoc.x - tSetup.padded.width / 2 + 1)*tSetup.current_scale_factor);
	tSetup.centroid.y += cvRound((maxLoc.y - tSetup.padded.height / 2 + 1)*tSetup.current_scale_factor);

	tSetup.centroid = updateCentroid(tSetup.centroid, tSetup.original.width*tSetup.current_scale_factor, tSetup.original.height*tSetup.current_scale_factor, img.cols, img.rows);

	//fout<<"before scaling"<<endl;
	if(enableScaling)
	{
		int nDimsScale;

		cv::Mat feature_map_scale_fourier = get_scale_sample(img, tSetup, tParams, nDimsScale);//I have to convert featuremap to double first

		cv::Mat* tempScale = new cv::Mat[feature_map_scale_fourier.rows];

		for(int i = 0; i < feature_map_scale_fourier.rows; i++)
		{
			cv::Mat temp1(1, feature_map_scale_fourier.cols, CV_64FC2);
			for(int j = 0; j < feature_map_scale_fourier.cols; j++)
				temp1.at<cv::Vec2d>(0, j) = feature_map_scale_fourier.at<cv::Vec2d>(i, j);

			mulSpectrums(tSetup.num_scale[i], temp1, tempScale[i], 0, false);
		}
		w = nDimsScale;
		cv::Mat sumDenScale(1, w, CV_64F);
		for(int k = 0; k < w; k++)
			sumDenScale.at<double>(0, k) = tSetup.den_scale.at<cv::Vec2d>(0, k)[0] + tParams.lambda;
		cv::Mat sumTempScale(1, w, CV_64FC2);
		sumTempScale = cv::Mat::zeros(sumTempScale.size(), CV_64FC2);
		for(int k = 0; k < w; k++)
		{

			for(int i = 0; i < feature_map_scale_fourier.rows; i++)
				sumTempScale.at<cv::Vec2d>(0, k) += tempScale[i].at<cv::Vec2d>(0, k);

			sumTempScale.at<cv::Vec2d>(0, k) /= sumDenScale.at<double>(0, k);
		}

		cv::Mat scale_response = cv::Mat::zeros(1, nDimsScale, CV_64FC1);
		scale_response = inverseFourier(sumTempScale);

		cv::Point maxLocScale = ComputeMaxDisplayfl(scale_response);

		tSetup.current_scale_factor = tSetup.current_scale_factor* tSetup.scaleFactors[maxLocScale.x];
		if(tSetup.current_scale_factor< tSetup.min_scale_factor)
			tSetup.current_scale_factor = tSetup.min_scale_factor;
		if(tSetup.current_scale_factor> tSetup.max_scale_factor)
			tSetup.current_scale_factor = tSetup.max_scale_factor;

		delete[] tempScale;
	}

	train(false, img);

	/*ofstream fout("C://Users//mincosy//Desktop//Tracking//Final_DSST_C++//DSST//logzebala.txt",  std::ofstream::out | std::ofstream::app);
	fout<<"Centroid "<<tSetup.centroid.x<<" "<<tSetup.centroid.y<<endl;
	fout<<"Size "<<tSetup.original.width<<" "<<tSetup.original.height<<endl;
	*/
	tSetup.centroid = updateCentroid(tSetup.centroid, tSetup.original.width*tSetup.current_scale_factor, tSetup.original.height*tSetup.current_scale_factor, img.cols, img.rows);
	int left = tSetup.centroid.x - (tSetup.original.width / 2 * tSetup.current_scale_factor);
	int top = tSetup.centroid.y - (tSetup.original.height / 2 * tSetup.current_scale_factor);
	cv::Rect rect(left, top, tSetup.original.width*tSetup.current_scale_factor, tSetup.original.height*tSetup.current_scale_factor);
	//fout<<"Updated Centroid "<<tSetup.centroid.x<<" "<<tSetup.centroid.y<<" "<<rect.width<<"  "<<rect.height<<endl;
	//fout.close();

	delete[] feature_map;
	delete[] feature_map_fourier;
	delete[] temp;
	//fout<<"Finished deleting :D?hzhahahaha "<<tSetup.centroid.x<<" "<<tSetup.centroid.y<<endl;
	//fout.close();

	return rect;
}
/*DSSTTracker::~DSSTTracker()
{
if(tSetup.num_trans!=0)
{
cout<<"here "<<tSetup.num_trans<<endl;
delete[] tSetup.num_trans;
}
if (tSetup.enableScaling)
{
delete[] tSetup.num_scale;
delete[] tSetup.scaleFactors;
}
}*/

void DSSTTracker::preprocess(int rows, int cols, cv::Mat img, cv::Rect bb)
{
	//cout<<"Type "<<img.type()<<" "<<CV_8U<<endl;

	//tSetup.enableScaling= false;
	tSetup.centroid.x = bb.x + bb.width / 2;
	tSetup.centroid.y = bb.y + bb.height / 2;
	tSetup.original.width = bb.width + 1;
	tSetup.original.height = bb.height + 1;
	//cout<<"entered preprocessing "<<tSetup.centroid.x<<" "<<tSetup.centroid.y<<endl;	
	//cout<<"entered preprocessing "<<tSetup.original.width<<" "<<tSetup.original.height<<endl;	
	//0- Preprocessing
	//A- Create Translation Gaussian Filters
	tSetup.padded.width = floor(tSetup.original.width * (1 + tParams.padding));
	tSetup.padded.height = floor(tSetup.original.height * (1 + tParams.padding));
	int szPadding_w = tSetup.padded.width / tParams.bin_size;
	int szPadding_h = tSetup.padded.height / tParams.bin_size;


	//don't change to float cause it causes huge truncation errors :) .. asdna el transFilter
	float transSigma = sqrt(float(tSetup.original.width*tSetup.original.height))*tParams.output_sigma_factor;
	cv::Mat transFilter(szPadding_h, szPadding_w, CV_64FC1);
	for(int r = -szPadding_h / 2; r < ceil((double)szPadding_h / 2); r++)
		for(int c = -szPadding_w / 2; c < ceil((double)szPadding_w / 2); c++)
			transFilter.at<double>(r + szPadding_h / 2, c + szPadding_w / 2) = exp(-0.5 * ((double)((r + 1)*(r + 1) + (c + 1)*(c + 1)) / (transSigma*transSigma)));

	tSetup.transFourier = createFourier(transFilter);

	//B- Create Scale Gaussian Filters
	//We don't know why this equation?!
	double scaleSigma = tParams.number_scales / sqrtf(tParams.number_scales) * tParams.scale_sigma_factor;
	cv::Mat scaleFilter(1, tParams.number_scales, CV_64FC1);
	for(int r = -tParams.number_scales / 2; r < ceil((double)tParams.number_scales / 2); r++)
		scaleFilter.at<double>(0, r + tParams.number_scales / 2) = exp(-0.5 * ((double)(r*r) / (scaleSigma*scaleSigma)));
	tSetup.scaleFourier = createFourier(scaleFilter);

	//Create Cosine Windows to give less weight to boundarie
	cv::Mat trans_cosine_win(szPadding_h, szPadding_w, CV_32FC1);
	//cout<<"Size cosine window "<<szPadding_w<<" "<<szPadding_h<<endl;

	cv::Mat cos1 = hann(szPadding_h);
	cv::Mat cos2 = hann(szPadding_w);
	tSetup.trans_cos_win = cos1*cos2.t();
	cv::Mat scale_cosine_win(tParams.number_scales, 1, CV_32FC1);
	tSetup.scale_cos_win = hann(tParams.number_scales);

	//Create Scale Factors
	double *scaleFactors = new double[tParams.number_scales];
	for(int i = 1; i <= tParams.number_scales; i++)
		scaleFactors[i - 1] = pow(tParams.scale_step, (ceil((double)tParams.number_scales / 2) - i));
	tSetup.scaleFactors = scaleFactors;

	//compute the resize dimensions used for feature extraction in the scale estimation
	float scale_model_factor = 1;
	int area = tSetup.original.width*tSetup.original.height;
	if(area > tParams.scale_model_max_area)
		scale_model_factor = sqrt((double)tParams.scale_model_max_area / area);


	tSetup.scale_model_sz = cv::Size(floor(tSetup.original.width * scale_model_factor), floor(tSetup.original.height*scale_model_factor));

	// find maximum and minimum scales
	//why 
	tSetup.min_scale_factor = pow(tParams.scale_step, ceil(log(max(5.0 / szPadding_h, 5.0 / szPadding_w)) / log(tParams.scale_step)));
	tSetup.max_scale_factor = pow(tParams.scale_step, floor(log(min((float)rows / tSetup.original.height, (float)cols / tSetup.original.width)) / log(tParams.scale_step)));
	tSetup.current_scale_factor = 1;

	hParams.binSize = tParams.bin_size;
	hParams.nOrients = 9;
	hParams.clipHog = 0.2;
	hParams.softBin = -1;

	//cout<<"before train"<<endl;
	train(true, img);
	//cout<<"after train"<<endl;
}

cv::Mat DSSTTracker::visualize(cv::Rect rect, cv::Mat img, cv::Scalar scalar)
{
	cv::Mat retImg = img.clone();
	cv::rectangle(retImg, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), scalar, 2);
	return retImg;
}

