# ------------------------------------------------------------------------------------- #
# ------------------------------------ Third Party ------------------------------------ #
# ------------------------------------------------------------------------------------- #

XVISION_TRACKERS = xvSSDAffine xvSSDGrid xvSSDGridLine xvSSDHelper xvSSDMain xvSSDPyramidAffine xvSSDPyramidRotate xvSSDPyramidRT xvSSDPyramidSE2 xvSSDPyramidTrans xvSSDRotate xvSSDRT xvSSDScaling xvSSDSE2 xvSSDTR xvSSDTrans common xvColor xvEdgeTracker
_LEARNING_TRACKERS = DSST/DSST KCF/KCFTracker RCT/CompressiveTracker Struck/Struck
_LEARNING_TRACKERS_SO = cmt opentld cvblobs struck
TLD_INCLUDES = TLD.h MedianFlowTracker.h DetectorCascade.h DetectionResult.h ForegroundDetector.h VarianceFilter.h IntegralImage.h EnsembleClassifier.h Clustering.h NNClassifier.h NormalizedPatch.h
CMT_INCLUDES = CMT.h common.h logging/log.h Consensus.h Fusion.h Tracker.h gui.h Matcher.h fastcluster/fastcluster.h
DSST_INCLUDES = wrappers.h HOG.h Params.h sse.hpp
STRUCK_MODULES = Tracker Config Features HaarFeature HaarFeatures HistogramFeatures ImageRep LaRank MultiFeatures RawFeatures Sampler
STRUCK_INCLUDES = Kernels Rect Sample

LEARNING_TRACKERS =  $(addprefix ThirdParty/, ${_LEARNING_TRACKERS})
LEARNING_INCLUDES =  $(addprefix ThirdParty/, $(addprefix DSST/, ${DSST_INCLUDES}) $(addprefix CMT/, ${CMT_INCLUDES}) $(addprefix TLD/, ${TLD_INCLUDES}))
STRUCK_HEADERS = $(addprefix ThirdParty/Struck/src/,$(addsuffix .h, ${STRUCK_MODULES} ${STRUCK_INCLUDES}))
STRUCK_SRC = $(addprefix ThirdParty/Struck/src/,$(addsuffix .cpp, ${STRUCK_MODULES}))

LEARNING_TRACKERS_SO =  $(addprefix ${MTF_LIB_INSTALL_DIR}/lib, $(addsuffix .so, ${_LEARNING_TRACKERS_SO}))
LIBS_LEARNING =  $(addprefix -l, ${_LEARNING_TRACKERS_SO})
LEARNING_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${notdir ${LEARNING_TRACKERS}}))
# LEARNING_OBJS = $(addsuffix .o, ${LEARNING_TRACKERS})
LEARNING_HEADERS = $(addsuffix .h, ${LEARNING_TRACKERS})
LEARNING_HEADERS += ${LEARNING_INCLUDES} ${STRUCK_HEADERS}
XVISION_HEADERS =  $(addprefix ThirdParty/Xvision/, $(addsuffix .h, ${XVISION_TRACKERS}))

MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${LEARNING_HEADERS} ${XVISION_HEADERS})  


LIBSXV= -L/usr/X11R6/lib -lXVTrack -lXVDevs -lXVCons -lXVSeg -lXVTools \
	-lXVImages -ljpeg -lpng -ltiff -L/usr/X11R6/lib64 -lXext -lX11 -lavformat -lavcodec -lavutil -lpthread -lippi -lippcc -lipps  \
	-lraw1394 -ldc1394 -lmpeg /usr/lib/x86_64-linux-gnu/libXxf86dga.so.1 /usr/lib/x86_64-linux-gnu/libXxf86vm.so.1
FLAGSXV= -I/include/XVision2 -I/usr/include/dc1394/

lt ?= 1
vp ?= 0
xv ?= 0
pfsl3 ?= 0

ifeq (${o}, 1)
OPT_FLAGS_PFSL3 = -O3 -D NDEBUG
OPT_FLAGS_DSST = -O2 -msse2 -D NDEBUG
OPT_FLAGS_MTF += -msse2 
PFSL3_EXE_NAME= pfsl3
else
OPT_FLAGS_PFSL3 = -g -O0
OPT_FLAGS_DSST = -g -O0 -msse2
PFSL3_EXE_NAME= pfsl3d
endif

ifeq (${pfsl3}, 0)
RUN_MTF_FLAGS += -D DISABLE_PFSL3
else
_LEARNING_TRACKERS += PFSL3/PFSL3
# LEARNING_TRACKERS_SO += ${MTF_LIB_INSTALL_DIR}/libpfsl3.a
LIBS_LEARNING += -llapacke -llapack -lblas -ltmg -lf2c -lgfortran -lm
endif

ifeq (${lt}, 0)
LEARNING_INCLUDES=
LEARNING_TRACKERS=
LEARNING_TRACKERS_SO=
LEARNING_OBJS=
LEARNING_HEADERS=
FLAGS_LEARNING=
LIBS_LEARNING=
RUN_MTF_FLAGS += -D DISABLE_LEARNING_TRACKERS
endif

ifeq (${vp}, 1)
FLAGS_VISP = -I /usr/local/include
LIBS_VISP = -L/usr/local/lib/x86_64-linux-gnu -lvisp_core -lvisp_tt -lvisp_tt_mi
_LEARNING_TRACKERS += ViSP/ViSP
else
RUN_MTF_FLAGS += -D DISABLE_VISP
LIBS_VISP =
FLAGS_VISP = 
endif

ifeq (${xv}, 1)
MTF_FLAGSXV = ${FLAGSXV}
MTF_LIBSXV = ${LIBSXV}
TOOLS += inputXV
else
XVISION_HEADERS =
MTF_FLAGSXV = -D DISABLE_XVISION
MTF_LIBSXV = 
endif

PFSL3_EXE_INSTALL_DIR = /usr/local/bin
.PHONY: pfsl3
pfsl3: 	${PFSL3_EXE_INSTALL_DIR}/${PFSL3_EXE_NAME}

${BUILD_DIR}/ThirdParty/DSST:
	mkdir -p $@
${BUILD_DIR}/ThirdParty/DSST/DSST.o:  | ${BUILD_DIR}/ThirdParty/DSST
${BUILD_DIR}/DSST.o: ThirdParty/DSST/DSST.cc ThirdParty/DSST/DSST.h ThirdParty/DSST/Params.h ThirdParty/DSST/HOG.h ThirdParty/DSST/sse.hpp ThirdParty/DSST/wrappers.h TrackerBase.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS_DSST} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/KCFTracker.o: ThirdParty/KCF/KCFTracker.cc ThirdParty/KCF/KCFTracker.h ThirdParty/KCF/defines.h ThirdParty/DSST/HOG.h ThirdParty/DSST/sse.hpp ThirdParty/DSST/wrappers.h TrackerBase.h  | $(@D)
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS_DSST} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/CompressiveTracker.o: ThirdParty/RCT/CompressiveTracker.cc ThirdParty/RCT/CompressiveTracker.h TrackerBase.h | $(@D)
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/Struck.o: ThirdParty/Struck/Struck.cc ThirdParty/Struck/Struck.h TrackerBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
${MTF_LIB_INSTALL_DIR}/libstruck.so: ThirdParty/Struck/libstruck.so
	sudo cp -f $< $@	
ThirdParty/Struck/libstruck.so: ${STRUCK_SRC} ${STRUCK_HEADERS}
	cd ./ThirdParty/Struck; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ./ThirdParty/Struck --no-print-directory
	
${MTF_LIB_INSTALL_DIR}/libcmt.so: ThirdParty/CMT/libcmt.so
	sudo cp -f $< $@	
ThirdParty/CMT/libcmt.so: ThirdParty/CMT/CMT.cpp ThirdParty/CMT/CMT.h 
	cd ./ThirdParty/CMT; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ./ThirdParty/CMT --no-print-directory
	
${MTF_LIB_INSTALL_DIR}/libopentld.so: ThirdParty/TLD/libopentld.so
	sudo cp -f $< $@
ThirdParty/TLD/libopentld.so: ThirdParty/TLD/TLD.cpp ThirdParty/TLD/TLD.h
	cd ./ThirdParty/TLD; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .		
	$(MAKE) -C ./ThirdParty/TLD --no-print-directory	
${MTF_LIB_INSTALL_DIR}/libcvblobs.so: ThirdParty/TLD/3rdparty/cvblobs/libcvblobs.so
	sudo cp -f $< $@
ThirdParty/TLD/3rdparty/cvblobs/libcvblobs.so: ThirdParty/TLD/3rdparty/cvblobs/blob.h ThirdParty/TLD/3rdparty/cvblobs/blob.cpp
	cd ./ThirdParty/TLD/3rdparty/cvblobs; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ./ThirdParty/TLD/3rdparty/cvblobs --no-print-directory
	
${BUILD_DIR}/PFSL3.o: ThirdParty/PFSL3/PFSL3.cc ThirdParty/PFSL3/PFSL3.h TrackerBase.h
	${CXX} -fPIC -c -I/usr/include/clapack ${WARNING_FLAGS} ${OPT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o  $@	
ThirdParty/PFSL3/libpfsl3.a: ThirdParty/PFSL3/PFSL3.o
	ar rcs $@ $<
${MTF_LIB_INSTALL_DIR}/libpfsl3.a: ThirdParty/PFSL3/libpfsl3.a
	sudo cp -f $< $@
${PFSL3_EXE_INSTALL_DIR}/${PFSL3_EXE_NAME}: ${BUILD_DIR}/${PFSL3_EXE_NAME}
	sudo cp -f $< $@
${BUILD_DIR}/${PFSL3_EXE_NAME}: ThirdParty/PFSL3/tracking.cpp
	${CXX} -w ${WARNING_FLAGS} -o $@ -I/usr/include/clapack ${OPT_FLAGS_PFSL3} ${FLAGSCV} $< ${LIBSCV} -llapacke -llapack -lblas -ltmg -lf2c -lgfortran -lm	
	
${BUILD_DIR}/ViSP.o: ThirdParty/ViSP/ViSP.cc ThirdParty/ViSP/ViSP.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} ${FLAGS_VISP} -o $@
