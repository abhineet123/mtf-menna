_LEARNING_TRACKERS += CMT/CMT
_LEARNING_TRACKERS_SO += cmt 
CMT_ROOT_DIR = ThirdParty\CMT
CMT_INCLUDE_DIR = ${CMT_ROOT_DIR}\include\mtf\ThirdParty\CMT
CMT_SRC_DIR = ${CMT_ROOT_DIR}\src
MTF_INCLUDE_DIRS += ${CMT_ROOT_DIR}\include

${MTF_LIB_INSTALL_DIR}/libcmt.so: ThirdParty/CMT/libcmt.so
	sudo cp -f $< $@	
ThirdParty/CMT/libcmt.so: ${CMT_SRC_DIR}/*.cc ${CMT_INCLUDE_DIR}/*.h
	cd ThirdParty/CMT; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ThirdParty/CMT --no-print-directory
${BUILD_DIR}/CMT.o: ${CMT_SRC_DIR}/CMT.cc ${CMT_INCLUDE_DIR}/CMT.h include/TrackerBase.h
	${CXX} -fPIC -c ${WARNING_FLAGS} ${OPT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o  $@
	