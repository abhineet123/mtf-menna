#include "mtf/SM/GridTrackerCV.h"
#include <stdexcept>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"

_MTF_BEGIN_NAMESPACE


GridTrackerCVParams::GridTrackerCVParams(
int _grid_size_x, int _grid_size_y,
int _search_window_x, int _search_window_y,
int _pyramid_levels, bool _use_min_eig_vals,
double _min_eig_thresh, EstType _estimation_method,
double _ransac_reproj_thresh, 
int _max_iters, double _epsilon, bool _show_trackers, 
bool _debug_mode) :
grid_size_x(_grid_size_x),
grid_size_y(_grid_size_y),
search_window_x(_search_window_x),
search_window_y(_search_window_y),
pyramid_levels(_pyramid_levels),
use_min_eig_vals(_use_min_eig_vals),
min_eig_thresh(_min_eig_thresh),
estimation_method(_estimation_method),
ransac_reproj_thresh(_ransac_reproj_thresh),
max_iters(_max_iters),
epsilon(_epsilon),
show_trackers(_show_trackers),
debug_mode(_debug_mode){}

GridTrackerCVParams::GridTrackerCVParams(GridTrackerCVParams *params) :
grid_size_x(GTCV_GRID_SIZE_X),
grid_size_y(GTCV_GRID_SIZE_Y),
search_window_x(GTCV_SEARCH_WINDOW_X),
search_window_y(GTCV_SEARCH_WINDOW_Y),
pyramid_levels(GTCV_PYRAMID_LEVELS),
use_min_eig_vals(GTCV_USE_MIN_EIG_VALS),
min_eig_thresh(GTCV_MIN_EIG_THRESH),
estimation_method(static_cast<EstType>(GTCV_ESTIMATION_METHOD)),
ransac_reproj_thresh(GTCV_RANSAC_REPROJ_THRESH),
max_iters(GTCV_MAX_ITERS), epsilon(GTCV_EPSILON),
show_trackers(GTCV_SHOW_TRACKERS),
debug_mode(GTCV_DEBUG_MODE){
	if(params){
		grid_size_x = params->grid_size_x;
		grid_size_y = params->grid_size_y;
		search_window_x = params->search_window_x;
		search_window_y = params->search_window_y;
		pyramid_levels = params->pyramid_levels;
		use_min_eig_vals = params->use_min_eig_vals;
		min_eig_thresh = params->min_eig_thresh;
		estimation_method = params->estimation_method;
		ransac_reproj_thresh = params->ransac_reproj_thresh;
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		show_trackers = params->show_trackers;
		debug_mode = params->debug_mode;
	}
}
template<class SSM>
GridTrackerCV<SSM>::GridTrackerCV(const cv::Mat &cv_img,
	SSMParams *ssm_params, ParamType *grid_params) :
	GridBase(), params(grid_params){
	printf("\n");
	printf("Initializing OpenCV Grid tracker with:\n");
	printf("grid_size: %d x %d\n", params.grid_size_x, params.grid_size_y);
	printf("search window size: %d x %d\n", params.search_window_x, params.search_window_y);
	printf("pyramid_levels: %d\n", params.pyramid_levels);
	printf("use_min_eig_vals: %d\n", params.use_min_eig_vals);
	printf("min_eig_thresh: %f\n", params.min_eig_thresh);
	printf("estimation_method: %d\n", params.estimation_method);
	printf("ransac_reproj_thresh: %f\n", params.ransac_reproj_thresh);
	printf("max_iters: %d\n", params.max_iters);
	printf("show_trackers: %d\n", params.show_trackers);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("\n");

	name = "grid_cv";

	switch(params.estimation_method){
	case EstType::LeastSquares:
		printf("Using LeastSquares estimation\n");
		estimation_method_cv = 0;
		break;
	case EstType::RANSAC:
		printf("Using RANSAC estimation\n");
		estimation_method_cv = CV_RANSAC;
		break;
	case EstType::LeastMedian:
		printf("Using LeastMedian estimation\n");
		estimation_method_cv = CV_LMEDS;
		break;
	default:
		throw std::invalid_argument("GridTrackerCV :: Invalid estimation method specified");
	}

	ssm = new SSM(params.grid_size_x, params.grid_size_y, ssm_params);

	n_pts = params.grid_size_x *params.grid_size_y;
	search_window = cv::Size(params.search_window_x, params.search_window_x);
	lk_termination_criteria = cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS,
		params.max_iters, params.epsilon);

	curr_img_float = cv_img;
	prev_img.create(cv_img.rows, cv_img.cols, CV_8UC1);
	curr_img.create(cv_img.rows, cv_img.cols, CV_8UC1);


	patch_corners.create(2, 4, CV_64FC1);
	cv_corners_mat.create(2, 4, CV_64FC1);

	prev_pts_mat.create(n_pts, 2, CV_32FC1);
	curr_pts_mat.create(n_pts, 2, CV_32FC1);

	prev_pts.resize(n_pts);
	curr_pts.resize(n_pts);

	ssm_update.resize(ssm->getStateSize());
	lk_status.resize(n_pts);
	pix_mask.resize(n_pts);
	std::fill(pix_mask.begin(), pix_mask.end(), 1);
	pause_seq = 0;

	if(params.use_min_eig_vals){
		lk_flags = cv::OPTFLOW_LK_GET_MIN_EIGENVALS;
	} else{
		lk_flags = 0;
	}

	if(params.show_trackers){
		patch_img = cv_img;
		patch_img_uchar.create(cv_img.rows, cv_img.cols, CV_8UC3);
		patch_win_name = "Patch Trackers";
		cv::namedWindow(patch_win_name);
	}
}
template<class SSM>
void GridTrackerCV<SSM>::initialize(const cv::Mat &cv_img, const cv::Mat &corners) {
	curr_img_float = cv_img;
	initialize(corners);
}
template<class SSM>
void GridTrackerCV<SSM>::update(const cv::Mat &cv_img) {
	curr_img_float = cv_img;
	update();
}

template<class SSM>
void GridTrackerCV<SSM>::initialize(const cv::Mat &corners) {
	curr_img_float.convertTo(curr_img, curr_img.type());

	ssm->initialize(corners);

	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		Vector2d patch_centroid = ssm->getPts().col(pt_id);
		prev_pts[pt_id].x = patch_centroid(0);
		prev_pts[pt_id].y = patch_centroid(1);
	}
	curr_img.copyTo(prev_img);

	ssm->getCorners(cv_corners_mat);
	if(params.show_trackers){ showTrackers(); }
}
template<class SSM>
void GridTrackerCV<SSM>::update() {
	curr_img_float.convertTo(curr_img, curr_img.type());
	cv::calcOpticalFlowPyrLK(prev_img, curr_img,
		prev_pts, curr_pts, lk_status, lk_error,
		search_window,
		params.pyramid_levels, lk_termination_criteria,
		lk_flags, params.min_eig_thresh);

	ssm->estimateWarpFromPts(ssm_update, pix_mask, prev_pts, curr_pts,
		estimation_method_cv, params.ransac_reproj_thresh);

	Matrix24d opt_warped_corners;
	ssm->applyWarpToCorners(opt_warped_corners, ssm->getCorners(), ssm_update);
	ssm->setCorners(opt_warped_corners);

	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		prev_pts[pt_id].x = curr_pts[pt_id].x;
		prev_pts[pt_id].y = curr_pts[pt_id].y;
	}
	ssm->getCorners(cv_corners_mat);
	curr_img.copyTo(prev_img);

	if(params.show_trackers){ showTrackers(); }
}

template<class SSM>
void GridTrackerCV<SSM>::setRegion(const cv::Mat& corners) {
	ssm->setCorners(corners);
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		prev_pts[pt_id].x = ssm->getPts()(0, pt_id);
		prev_pts[pt_id].y = ssm->getPts()(1, pt_id);
	}
	ssm->getCorners(cv_corners_mat);
}

template<class SSM>
void GridTrackerCV<SSM>::showTrackers(){
	patch_img.convertTo(patch_img_uchar, patch_img_uchar.type());
	cv::cvtColor(patch_img_uchar, patch_img_uchar, CV_GRAY2BGR);
	cv::Scalar line_color(255, 0, 0);
	cv::Point2d ul(cv_corners_mat.at<double>(0, 0), cv_corners_mat.at<double>(1, 0));
	cv::Point2d ur(cv_corners_mat.at<double>(0, 1), cv_corners_mat.at<double>(1, 1));
	cv::Point2d lr(cv_corners_mat.at<double>(0, 2), cv_corners_mat.at<double>(1, 2));
	cv::Point2d ll(cv_corners_mat.at<double>(0, 3), cv_corners_mat.at<double>(1, 3));
	line(patch_img_uchar, ul, ur, line_color, 2);
	line(patch_img_uchar, ur, lr, line_color, 2);
	line(patch_img_uchar, lr, ll, line_color, 2);
	line(patch_img_uchar, ll, ul, line_color, 2);
	for(int tracker_id = 0; tracker_id < n_pts; tracker_id++) {
		cv::Scalar tracker_color;
		if(pix_mask[tracker_id]){
			tracker_color = cv::Scalar(0, 255, 0);
		} else{
			tracker_color = cv::Scalar(0, 0, 255);
		}
		circle(patch_img_uchar, curr_pts[tracker_id], 2, tracker_color, 2);
	}
	imshow(patch_win_name, patch_img_uchar);
	//int key = cv::waitKey(1 - pause_seq);
	//if(key == 32){
	//	pause_seq = 1 - pause_seq;
	//}
}

_MTF_END_NAMESPACE
#include "mtf/Macros/register.h"
_REGISTER_TRACKERS_SSM(GridTrackerCV);