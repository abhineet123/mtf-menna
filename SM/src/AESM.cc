#include "mtf/SM/AESM.h"

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
AESM<AM, SSM >::AESM(ParamType *aesm_params,
	AMParams *am_params, SSMParams *ssm_params) : 
	ESM<AM, SSM>(aesm_params, am_params, ssm_params){
	printf("Using Additive variant\n");
	name = "aesm";
	log_fname = "log/mtf_aesm_log.txt";
	time_fname = "log/mtf_aesm_times.txt";
}

template <class AM, class SSM>
void AESM<AM, SSM >::initializePixJacobian(){
	am->initializePixGrad(ssm->getPts());
	ssm->cmptPixJacobian(init_pix_jacobian, am->getInitPixGrad());
}

template <class AM, class SSM>
void AESM<AM, SSM >::updatePixJacobian(){
	// compute pixel gradient of the current image warped with the current warp
	am->updatePixGrad(ssm->getPts());
	record_event("am->updatePixGrad");

	// multiply the pixel gradient with the SSM Jacobian to get the Jacobian of pixel values w.r.t. SSM parameters
	ssm->cmptPixJacobian(curr_pix_jacobian, am->getCurrPixGrad());
	record_event("ssm->cmptPixJacobian");

	ssm->cmptPixJacobian(init_pix_jacobian, am->getInitPixGrad());
	record_event("ssm->cmptPixJacobian (init)");
}

template <class AM, class SSM>
void AESM<AM, SSM >::initializePixHessian(){
	am->initializePixHess(ssm->getPts());
	ssm->cmptPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
}

template <class AM, class SSM>
void AESM<AM, SSM >::updatePixHessian(){
	am->updatePixHess(ssm->getPts());
	record_event("am->updatePixHess");

	ssm->cmptPixHessian(curr_pix_hessian, am->getCurrPixHess(), am->getCurrPixGrad());
	record_event("ssm->cmptPixHessian");

	ssm->cmptPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
	record_event("ssm->cmptPixHessian (init)");
}

template <class AM, class SSM>
void AESM<AM, SSM >::updateSSM(){
	ssm->additiveUpdate(ssm_update);
	record_event("ssm->additiveUpdate");
}

_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(AESM);