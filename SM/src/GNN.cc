#include "mtf/SM/GNN.h"
#include <time.h>
#include <ctime> 

//#include "GNN/memwatch.h"
//#include "GNN/search_graph_knn.h"
//#include "GNN/utility.h"

_MTF_BEGIN_NAMESPACE

GNNParams::GNNParams(int _max_iters, int _n_samples, int _k,
double _epsilon,
double _corner_sigma_d, double _corner_sigma_t,
bool _additive_update, bool _direct_samples,
bool _debug_mode){
	max_iters = _max_iters;
	n_samples = _n_samples;
	k = _k;
	epsilon = _epsilon;
	corner_sigma_d = _corner_sigma_d;
	corner_sigma_t = _corner_sigma_t;
	additive_update = _additive_update;
	direct_samples = _direct_samples;
	debug_mode = _debug_mode;
}

GNNParams::GNNParams(GNNParams *params) :
max_iters(GNN_MAX_ITERS),
n_samples(GNN_N_SAMPLES),
k(GNN_K),
epsilon(GNN_EPSILON),
corner_sigma_d(GNN_CORNER_SIGMA_D),
corner_sigma_t(GNN_CORNER_SIGMA_T),
additive_update(GNN_ADDITIVE_UPDATE),
direct_samples(GNN_DIRECT_SAMPLES),
debug_mode(GNN_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_samples = params->n_samples;
		k = params->k;
		epsilon = params->epsilon;
		corner_sigma_d = params->corner_sigma_d;
		corner_sigma_t = params->corner_sigma_t;
		additive_update = params->additive_update;
		direct_samples = params->direct_samples;
		debug_mode = params->debug_mode;
	}
}

template <class AM, class SSM>
GNN<AM, SSM >::GNN(ParamType *nn_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(nn_params){

	printf("\n");
	printf("initializing Graph based Nearest Neighbor tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_samples: %d\n", params.n_samples);
	printf("k: %d\n", params.k);
	printf("epsilon: %f\n", params.epsilon);
	printf("corner_sigma_d: %f\n", params.corner_sigma_d);
	printf("corner_sigma_t: %f\n", params.corner_sigma_t);
	printf("additive_update: %d\n", params.additive_update);
	printf("direct_samples: %d\n", params.direct_samples);
	printf("debug_mode: %d\n", params.debug_mode);


	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nn";
	log_fname = "log/mtf_nn_log.txt";
	time_fname = "log/mtf_nn_times.txt";
	frame_id = 0;

	ssm_state_size = ssm->getStateSize();
	am_feat_size = am->getDistFeatSize();

	printf("ssm_state_size: %d\n", ssm_state_size);
	printf("am_feat_size: %d\n", am_feat_size);

	//flann_query = new flannMatType(const_cast<double*>(eig_query.data()), 
	//	eig_query.rows(), eig_query.cols());

	//flann_result = new flannResultType(const_cast<int*>(eig_result.data()),
	//	eig_result.rows(), eig_result.cols());
	//flann_dists = new flannMatType(const_cast<double *>(eig_dists.data()),
	//	eig_dists.rows(), eig_dists.cols());

	ssm_update.resize(ssm_state_size);
	ssm_sigma.resize(ssm_state_size);
	ssm_perturbations.resize(ssm_state_size, params.n_samples);
	inv_state_update.resize(ssm_state_size);
}

template <class AM, class SSM>
void GNN<AM, SSM >::initialize(const cv::Mat &corners){

	start_timer();

	eig_dataset.resize(params.n_samples, am_feat_size);
	//eig_query.resize(am_feat_size);
	eig_result.resize(1);
	eig_dists.resize(1);

	ssm->initialize(corners);

	am->initializePixVals(ssm->getPts());
	am->initializeDistFeat();

	baseGeneratorType generator(rand());
	dustributionType gaussian_dist_d(0, params.corner_sigma_d);
	dustributionType gaussian_dist_t(0, params.corner_sigma_t);
	randomGeneratorType rand_gen_d(generator, gaussian_dist_d);
	randomGeneratorType rand_gen_t(generator, gaussian_dist_t);

	Matrix24d rand_d;
	Vector2d rand_t;
	Matrix24d disturbed_corners;
	VectorXd state_update(ssm_state_size);

	printf("building feature dataset...\n");
	for(int sample_id = 0; sample_id < params.n_samples; sample_id++){

		rand_t(0) = rand_gen_t();
		rand_t(1) = rand_gen_t();
		for(int i = 0; i < 4; i++){
			rand_d(0, i) = rand_gen_d();
			rand_d(1, i) = rand_gen_d();
		}
		disturbed_corners = ssm->getCorners() + rand_d;
		disturbed_corners = disturbed_corners.colwise() + rand_t;
		ssm->estimateWarpFromCorners(state_update, ssm->getCorners(), disturbed_corners);

		if(params.additive_update){
			inv_state_update = -state_update;
			ssm->additiveUpdate(inv_state_update);
		} else{
			ssm->invertState(inv_state_update, state_update);
			ssm->compositionalUpdate(inv_state_update);
		}

		am->updatePixVals(ssm->getPts());
		am->updateDistFeat(eig_dataset.row(sample_id).data());


		ssm_perturbations.col(sample_id) = state_update;

		// reset SSM to previous state
		if(params.additive_update){
			ssm->additiveUpdate(state_update);
		} else{
			ssm->compositionalUpdate(state_update);
		}
	}

	// Build graph
	printf("Building the graph...\n");
	Nodes = build_graph(eig_dataset.data(), params.k, params.n_samples, 
		am_feat_size);

	ssm->getCorners(cv_corners_mat);

	end_timer();
	write_interval(time_fname, "w");
}

template <class AM, class SSM>
void GNN<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		init_timer();

		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		am->updateDistFeat();
		record_event("am->updateDistFeat");

		gnn::indx_dist * knns = search_graph(Nodes, const_cast<double*>(am->getDistFeat()),
			eig_dataset.data(), params.k, 1, params.n_samples, am_feat_size);

		printf("Best match found at index %d with distance %f\n", knns[0].idx, knns[0].dist);

		ssm_update = ssm_perturbations.col(knns[0].idx);

		prev_corners = ssm->getCorners();

		if(params.additive_update){
			ssm->additiveUpdate(ssm_update);
			record_event("ssm->additiveUpdate");
		} else{
			ssm->compositionalUpdate(ssm_update);
			record_event("ssm->compositionalUpdate");
		}

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		write_data(time_fname);

		if(update_norm < params.epsilon){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}

		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners_mat);
}
template <class AM, class SSM>
gnn::node*  GNN<AM, SSM >::build_graph(double *X, int k, int n_samples, int n_dims){
	gnn::node *Nodes = static_cast<gnn::node*>(malloc(n_samples*sizeof(gnn::node)));
	for(int i = 0; i < n_samples; i++)
	{
		Nodes[i].nns_inds = static_cast<int*>(malloc(k*sizeof(int)));
		//   memset(Nodes[i].nns_inds, -1, k*sizeof(int));
		Nodes[i].capacity = k;
		Nodes[i].size = 0;
	}

	//  struct indx_dist *dists = malloc(n_samples * sizeof(struct indx_dist));
	//  check_pointer(dists, "Couldn't malloc dists");
	gnn::indx_dist* dists = static_cast<gnn::indx_dist*>(malloc((k + 1) * sizeof(gnn::indx_dist)));

	int nns_ind;
	//  int *nns_ind = malloc(k*sizeof(int));
	//  check_pointer(nns_ind, "Couldn't malloc nns_ind");
	double *query;
	//  int *query = malloc(n_dims*sizeof(int));
	//  check_pointer(query, "Couldn't malloc query");

	for(int i = 0; i < n_samples; i++){
		query = X + (i*n_dims);
		knn_search2(query, dists, X, n_samples, n_dims, k + 1);   // index of 1st node is 0
		for(int j = 0; j < k; j++){
			nns_ind = dists[j + 1].idx;
			add_node(&Nodes[i], nns_ind);
		}
	}
	free(dists);
	return Nodes;
}


template <class AM, class SSM>
void GNN<AM, SSM >::knn_search2(double *Q, gnn::indx_dist *dists, 
	double *X, int rows, int cols, int k){
	// Faster version of knn_search
	// Calculates the distance of query to all data points in X and returns the sorted dist array
	/*  for (i=0; i<rows; i++)
	{
	dists[i].dist = dist_func(Q, X+i*cols, cols);
	dists[i].idx = i;
	}
	mergesort(dists, 0, rows-1);
	*/
	int index, ii, count = 0;
	//int capacity = k;
	for(index = 0; index < rows; index++){
		double *point = X + index*cols;

		for(ii = 0; ii < count; ++ii) {
			if(dists[ii].idx == ii) continue; //return false;
		}
		//addPoint(point);
		double dist = (*am)(Q, point, cols);
		if(count < k){
			dists[count].idx = index;
			dists[count].dist = dist;
			++count;
		} else if(dist < dists[count - 1].dist || (dist == dists[count - 1].dist &&
			index < dists[count - 1].idx)) {
			//         else if (dist < dists[count-1]) {
			dists[count - 1].idx = index;
			dists[count - 1].dist = dist;
		} else {
			continue;   //  return false;
		}

		int i = count - 1;
		while(i >= 1 && (dists[i].dist < dists[i - 1].dist || (dists[i].dist == dists[i - 1].dist &&
			dists[i].idx < dists[i - 1].idx))){
			swap_int(&dists[i].idx, &dists[i - 1].idx);
			swap_double(&dists[i].dist, &dists[i - 1].dist);
			i--;
		}

		//return false;
	}
}

template <class AM, class SSM>
void GNN<AM, SSM >::knn_search11(double *Q, gnn::indx_dist *dists, double *X, 
	int rows,int cols, int k, int *X_inds){
	// Faster version of knn_search1
	// Calculates the distance of query to all data points in X and returns the sorted dist array

	int count = 0;
	for(int i = 0; i < rows; i++){

		for(int j = 0; j < count; ++j) {
			if(dists[j].idx == j) continue; //return false;
		}
		double *point = X + X_inds[i] * cols;
		double dist = (*am)(Q, point, cols);
		if(count < k){
			dists[count].idx = i;
			dists[count].dist = dist;
			++count;
		} else if(dist < dists[count - 1].dist || (dist == dists[count - 1].dist &&
			i < dists[count - 1].idx)) {
			//         else if (dist < dists[count-1]) {
			dists[count - 1].idx = i;
			dists[count - 1].dist = dist;
		} else {
			continue;      //  return false;
		}

		int ii = count - 1;
		while(ii >= 1 && (dists[ii].dist < dists[ii - 1].dist || (dists[ii].dist == dists[ii - 1].dist &&
			dists[ii].idx < dists[ii - 1].idx))){
			swap_int(&dists[ii].idx, &dists[ii - 1].idx);
			swap_double(&dists[ii].dist, &dists[ii - 1].dist);
			ii--;
		}
	}
}


template <class AM, class SSM>
gnn::indx_dist *GNN<AM, SSM >::search_graph(gnn::node *Nodes, double *Xq, double *X,
	int n_samples, int n_dims, int NNs, int K){
	// NNs: graph_size

	//struct stats* Stat = allocate_stats();
	double *query = static_cast<double*>(malloc(n_dims*sizeof(double)));

	gnn::indx_dist *tnn_dists, *gnn_dists, *knns, *visited_nodes;

	tnn_dists = static_cast<gnn::indx_dist*>(malloc(/*n_samples*/K * sizeof(gnn::indx_dist))); //true nn-dists
	int gnns_cap = K; //NNs*3;
	gnn_dists = static_cast<gnn::indx_dist*>(malloc(gnns_cap * sizeof(gnn::indx_dist))); // graph nn-dists

	int dist_cnt, depth, knns_size;

	for(int j = 0; j < n_dims; j++)
		query[j] = Xq[n_dims + j];

	tnn_dists = static_cast<gnn::indx_dist*>(realloc(tnn_dists, /*n_samples*/ K * sizeof(gnn::indx_dist)));


	// knn_search(query, tnn_dists, X, n_samples, n_dims);
	knn_search2(query, tnn_dists, X, n_samples, n_dims, K);


	//tnn_dists = realloc(tnn_dists, K * sizeof(struct indx_dist)); // shrinks the size to K   

	// Graph search time 
	int visited_cap = K * 4;  //avg depth = 4
	int visited = 0;   // number of visited nodes
	visited_nodes = static_cast<gnn::indx_dist*>(malloc(visited_cap * sizeof(gnn::indx_dist)));

	int rand_node, r;
	double *x_row;
	double parent_dist, dd;

	depth = 0;
	dist_cnt = 0;
	rand_node = my_rand(0, n_samples - 1);
	r = rand_node;
	x_row = &X[r*n_dims]; //X+r*n_dims;
	parent_dist = (*am)(query, x_row, n_dims);
	// parent_dist = my_dist(query, x_row, n_dims);

	visited_nodes[0].idx = r;
	visited_nodes[0].dist = parent_dist;
	visited++;

	while(1){
		//      X1 = sample_X(Nodes, r, X); //contains the neighbors of node r
		if(Nodes[r].size > gnns_cap) //Nodes[r].size != gnns_size)
		{
			gnns_cap = Nodes[r].size;
			gnn_dists = static_cast<gnn::indx_dist*>(realloc(gnn_dists, gnns_cap * sizeof(gnn::indx_dist)));
		}

		// knn_search(query, gnn_dists, X1, Nodes[r].size, n_dims);
		// knn_search1(query, gnn_dists, X, Nodes[r].size, n_dims, Nodes[r].nns_inds);
		knn_search11(query, gnn_dists, X, Nodes[r].size, n_dims, K, Nodes[r].nns_inds);

		//      free(X1); 

		int m = min(K, Nodes[r].size);
		if((visited + m) > visited_cap)
		{
			do {
				visited_cap *= 2;
			} while(visited_cap < (visited + m));
			visited_nodes = static_cast<gnn::indx_dist*>(realloc(visited_nodes, visited_cap *sizeof(gnn::indx_dist)));
		}
		for(int i = 0; i < m; i++)
		{
			visited_nodes[visited + i].idx = Nodes[r].nns_inds[gnn_dists[i].idx];
			visited_nodes[visited + i].dist = gnn_dists[i].dist;
		}
		visited = visited + m;

		if(parent_dist <= gnn_dists[0].dist)
			break;
		else
			dd = gnn_dists[0].dist;

		r = Nodes[r].nns_inds[gnn_dists[0].idx];
		depth++;
		dist_cnt += Nodes[r].size;
		x_row = &X[r*n_dims]; // X+r*n_dims
		// parent_dist = my_dist(query, x_row, n_dims);
		parent_dist = (*am)(query, x_row, n_dims);
	}
	//gnns_size = K;
	//gnn_dists = realloc(gnn_dists, gnns_size * sizeof(struct indx_dist));
	//check_pointer(gnn_dists, "Couldn't realloc gnn_dists");

	// Given visited_nodes and their dists, selects the first knns and puts into gnns_dists
	pick_knns(visited_nodes, visited, &gnn_dists, K, &gnns_cap);
	knns = intersect(gnn_dists, tnn_dists, K, &knns_size);


	free(query);
	free(gnn_dists);
	free(tnn_dists);
	return knns;
}

template <class AM, class SSM>
void GNN<AM, SSM >::add_node(gnn::node *node_i, int nn){
	int size = node_i->size++;
	if(size >= node_i->capacity)
	{
		node_i->nns_inds = static_cast<int*>(realloc(node_i->nns_inds, /*size*2*/ (size + 10)*sizeof(int)));
		node_i->capacity = /*size*2*/ size + 10;
	}
	node_i->nns_inds[size] = nn;
}

template <class AM, class SSM>
int GNN<AM, SSM >::found(int *inds, int ind, int num_inds){
	for(int i = 0; i < num_inds; i++)
	{
		if(inds[i] == ind)
			return 1;
	}
	return 0;
}

//-------------------------------
template <class AM, class SSM>
void GNN<AM, SSM >::swap_int(int *i, int *j){
	int temp;
	temp = *i;
	*i = *j;
	*j = temp;
}

//-------------------------------
template <class AM, class SSM>
void GNN<AM, SSM >::swap_double(double *i, double *j){
	double temp;
	temp = *i;
	*i = *j;
	*j = temp;
}


//--------------------------------
//-------------------------------
template <class AM, class SSM>
void GNN<AM, SSM >::mergesort(gnn::indx_dist array[], int left, int right){
	int splitpos;
	if(left < right){
		splitpos = (left + right) / 2;
		mergesort(array, left, splitpos);
		mergesort(array, splitpos + 1, right);
		merge(array, left, splitpos, right);
	}
}

template <class AM, class SSM>
void GNN<AM, SSM >::merge(gnn::indx_dist array[], int left, int splitpos, int right)
{
	int size_arr1 = splitpos - left + 1;
	int size_arr2 = right - splitpos;
	gnn::indx_dist arr1[size_arr1];
	gnn::indx_dist arr2[size_arr2];
	int k, i = 0, j = 0;

	for(i = 0; i < size_arr1; i++)
	{
		arr1[i].dist = array[left + i].dist;
		arr1[i].idx = array[left + i].idx;
	}

	for(j = 0; j < size_arr2; j++)
	{
		arr2[j].dist = array[splitpos + j + 1].dist;
		arr2[j].idx = array[splitpos + j + 1].idx;
	}
	i = 0; j = 0;
	for(k = left; k <= right; k++)
	{
		if(j == size_arr2)
		{
			array[k].dist = arr1[i].dist;
			array[k].idx = arr1[i].idx;
			i++;
		} else if(i == size_arr1)
		{
			array[k].dist = arr2[j].dist;
			array[k].idx = arr2[j].idx;
			j++;
		} else if(arr1[i].dist <= arr2[j].dist)
		{
			array[k].dist = arr1[i].dist;
			array[k].idx = arr1[i].idx;
			i++;
		} else
		{
			array[k].dist = arr2[j].dist;
			array[k].idx = arr2[j].idx;
			j++;
		}
	}
}

//--------------------------------
template <class AM, class SSM>
int GNN<AM, SSM >::my_rand(int lb, int ub){
	//  time_t sec;
	//  time(&sec);
	//  srand((unsigned int) sec);
	return (rand() % (ub - lb + 1) + lb);
}

//--------------------------------
template <class AM, class SSM>
double GNN<AM, SSM >::my_dist(int *v1, int *v2, int vlen){
	double sum = 0;
	for(int i = 0; i < vlen; i++)
		sum = sum + (v1[i] - v2[i])*(v1[i] - v2[i]);//pow(v1[i]-v2[i], 2);
	return sum;//sqrt(sum); 
}
template <class AM, class SSM>
gnn::indx_dist * GNN<AM, SSM >::intersect(gnn::indx_dist *gnns, gnn::indx_dist *tnns,
	int K, int *knns_size){
	gnn::indx_dist *knns = static_cast<gnn::indx_dist*>(malloc(K*sizeof(gnn::indx_dist)));
	int found, s = 0;
	for(int i = 0; i < K; i++)
	{
		found = 0;
		for(int j = 0; j < K; j++)
			if(gnns[i].idx == tnns[j].idx)
			{
				found = 1;  break;
			}
		if(found)
		{
			knns[s].idx = gnns[i].idx;
			knns[s++].dist = gnns[i].dist;
		}
	}
	// s might be 0, no realloc to 0
	//knns = realloc(knns, s*sizeof(struct indx_dist)); //no need anymore
	//check_pointer(knns, "Couldn't realloc knns"); // no need anymore
	*knns_size = s;

	return knns;
}

//-------------------------------
template <class AM, class SSM>
void GNN<AM, SSM >::pick_knns(gnn::indx_dist *vis_nodes, int visited, 
	gnn::indx_dist **gnn_dists, int K, int *gnns_cap){
	qsort(vis_nodes, visited, sizeof(vis_nodes[0]), gnn::cmp_qsort); //Ascending...
	if(K > *gnns_cap){
		*gnns_cap = K;
		*gnn_dists = static_cast<gnn::indx_dist*>(realloc(*gnn_dists, K*sizeof(gnn::indx_dist))); // not needed? not sure
	}
	int found = 0, ii = 0, jj = 0;
	(*gnn_dists)[jj].idx = vis_nodes[ii].idx;
	(*gnn_dists)[jj].dist = vis_nodes[ii].dist;
	ii++; jj++;

	while(jj < K){
		//  i++;
		found = 0;
		for(int j = 0; j < jj; j++)
			if((*gnn_dists)[j].idx == vis_nodes[ii].idx){
				found = 1; break;
			}

		if(found){
			ii++; continue;
		} else{
			(*gnn_dists)[jj].idx = vis_nodes[ii].idx;
			(*gnn_dists)[jj].dist = vis_nodes[ii].dist;
			jj++; ii++;
		}
	}
}

//--------------------------------
template <class AM, class SSM>
int* GNN<AM, SSM >:: sample_X(gnn::node *Nodes, int node_ind, int n_dims, int *X){
	int *X1 = static_cast<int*>(malloc(Nodes[node_ind].size * n_dims * sizeof(int)));
	for(int i = 0; i < Nodes[node_ind].size; i++)
		for(int j = 0; j < n_dims; j++)
			X1[i*n_dims + j] = X[Nodes[node_ind].nns_inds[i] * n_dims + j];

	return X1;
}



_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(GNN);
