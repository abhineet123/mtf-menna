#include "mtf/SM/RKLT.h"

_MTF_BEGIN_NAMESPACE

RKLTParams::RKLTParams(
bool _enable_spi, bool _enable_feedback,
bool _failure_detection, double _failure_thresh,
bool _debug_mode){
	enable_spi = _enable_spi;
	enable_feedback = _enable_feedback;
	failure_detection = _failure_detection;
	failure_thresh = _failure_thresh;
	debug_mode = _debug_mode;
}
RKLTParams::RKLTParams(RKLTParams *params) :
enable_spi(RKLT_ENABLE_SPI),
enable_feedback(RKLT_ENABLE_FEEDBACK),
failure_detection(RKLT_FAILURE_DETECTION),
failure_thresh(RKLT_FAILURE_THRESH),
debug_mode(RKLT_DEBUG_MODE){
	if(params){
		enable_spi = params->enable_spi;
		enable_feedback = params->enable_feedback;
		failure_detection = params->failure_detection;
		failure_thresh = params->failure_thresh;
		debug_mode = params->debug_mode;
	}
}
template<class AM, class SSM>
RKLT<AM, SSM>::RKLT(ParamType *rklt_params,
	GridBase *_grid_tracker, TemplTrackerType *_templ_tracker) :
	TrackerBase(), params(rklt_params),
	templ_tracker(_templ_tracker), grid_tracker(_grid_tracker){
	printf("\n");
	printf("Initializing RKL tracker with:\n");
	printf("enable_spi: %d\n", params.enable_spi);
	printf("enable_feedback: %d\n", params.enable_feedback);
	printf("failure_detection: %d\n", params.failure_detection);
	printf("failure_thresh: %f\n", params.failure_thresh);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("templ_tracker: %s with:\n", templ_tracker->name.c_str());
	printf("\t appearance model: %s\n", templ_tracker->getAM()->name.c_str());
	printf("\t state space model: %s\n", templ_tracker->getSSM()->name.c_str());
	printf("\n");

	if(params.enable_spi){
		if(!templ_tracker->supportsSPI()){
			printf("Template tracker does not support SPI so disabling it\n");
			params.enable_spi = false;
		} else if(grid_tracker->getResX() != templ_tracker->getAM()->getResX() ||
			grid_tracker->getResY() != templ_tracker->getAM()->getResY()){
			printf("Sampling resolution of the template tracker: %d x %d is not same as the grid size: %d x %d so disabling SPI\n",
				templ_tracker->getAM()->getResX(), templ_tracker->getAM()->getResY(), grid_tracker->getResX(), grid_tracker->getResY());
			params.enable_spi = false;
		} else{
			printf("SPI is enabled\n");
		}
	}
	if(params.failure_detection){
		printf("Template tracker failure detection is enabled with a threshold of %f\n",
			params.failure_thresh);
		grid_corners_mat.create(2, 4, CV_64FC1);
	}
}
template<class AM, class SSM>
void  RKLT<AM, SSM>::initialize(const cv::Mat &cv_img, const cv::Mat &corners){
	grid_tracker->initialize(cv_img, corners);
	templ_tracker->initialize(cv_img, corners);
	cv_corners_mat = templ_tracker->getRegion();
}
template<class AM, class SSM>
void  RKLT<AM, SSM>::initialize(const cv::Mat &corners){
	grid_tracker->initialize(corners);
	templ_tracker->initialize(corners);
	cv_corners_mat = templ_tracker->getRegion();
}

template<class AM, class SSM>
void  RKLT<AM, SSM>::update(const cv::Mat &cv_img){
	grid_tracker->update(cv_img);
	templ_tracker->setRegion(grid_tracker->getRegion());
	if(params.enable_spi){
		templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
	}
	templ_tracker->update(cv_img);
	postUpdateProc();
}

template<class AM, class SSM>
void  RKLT<AM, SSM>::update(){
	grid_tracker->update();
	templ_tracker->setRegion(grid_tracker->getRegion());
	if(params.enable_spi){
		templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
	}
	templ_tracker->update();
	postUpdateProc();
}
template<class AM, class SSM>
void  RKLT<AM, SSM>::postUpdateProc(){
	cv_corners_mat = templ_tracker->getRegion();
	if(params.failure_detection){
		double corner_diff = cv::norm(cv_corners_mat, grid_tracker->getRegion());
		if(corner_diff > params.failure_thresh){
			cv_corners_mat = grid_tracker->getRegion();
			return;
		}
	}
	if(params.enable_feedback){
		grid_tracker->setRegion(cv_corners_mat);
	}
}

_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(RKLT);

