#include "mtf/SM/CascadeTracker.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

CascadeParams::CascadeParams(bool _enable_feedback){
	enable_feedback = _enable_feedback;
}
CascadeParams::CascadeParams(CascadeParams *params) :
enable_feedback(CASC_ENABLE_FEEDBACK){
	if(params){
		enable_feedback = params->enable_feedback;
	}
}

CascadeTracker::CascadeTracker(const vector<TrackerBase*> _trackers, ParamType *casc_params) :
TrackerBase(), params(casc_params), trackers(_trackers){
	n_trackers = trackers.size();

	printf("\n");
	printf("Initializing Cascade tracker with:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("trackers: ");
	name = "casc: ";
	for(int i = 0; i < n_trackers; i++){
		name = name + trackers[i]->name + " ";
		printf("%d: %s ", i + 1, trackers[i]->name.c_str());
	}
	printf("\n");

	if(params.enable_feedback){
		printf("Feedback is enabled\n");
	}
}

void CascadeTracker::initialize(const cv::Mat &img, const cv::Mat &corners){
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->initialize(img, corners);
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}

void CascadeTracker::initialize(const cv::Mat &corners){
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		//printf("Initializing tracker %d\n", tracker_id);
		trackers[tracker_id]->initialize(corners);
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}
void CascadeTracker::update(){
	trackers[0]->update();
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		//printf("tracker: %d ", tracker_id - 1);
		//utils::printMatrix<double>(trackers[tracker_id - 1]->getRegion(),
		//	"region is: ");
		trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
		//printf("tracker: %d ", tracker_id);
		//utils::printMatrix<double>(trackers[tracker_id]->getRegion(),
		//	"region set to: ");
		trackers[tracker_id]->update();
	}
	if(params.enable_feedback){
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}
void CascadeTracker::update(const cv::Mat &img){
	trackers[0]->update(img);
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
		trackers[tracker_id]->update(img);
	}
	if(params.enable_feedback){
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}
void CascadeTracker::setRegion(const cv::Mat& corners) {
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setRegion(corners);
	}
}
_MTF_END_NAMESPACE


