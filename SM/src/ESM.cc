#include "mtf/SM/ESM.h"
#include "mtf/Utilities/miscUtils.h"
#include <time.h>
#include "opencv2/imgproc/imgproc.hpp"
#include <stdexcept>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
ESM<AM, SSM >::ESM(ParamType *esm_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(esm_params){
	printf("\n");
	printf("Initializing ESM tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("epsilon: %f\n", params.epsilon);
	printf("jac_type: %d\n", params.jac_type);
	printf("hess_type: %d\n", params.hess_type);
	printf("sec_ord_hess: %d\n", params.sec_ord_hess);
	printf("enable_spi: %d\n", params.enable_spi);
	printf("spi_thresh: %f\n", params.spi_thresh);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "esm";
	log_fname = "log/mtf_esm_log.txt";
	time_fname = "log/mtf_esm_times.txt";

	frame_id = 0;
	max_pix_diff = 0;

	if(params.enable_spi){
#ifndef DISABLE_SPI
		if(!ssm->supportsSPI())
			throw std::domain_error("ESM::SSM does not support SPI");
		if(!am->supportsSPI())
			throw std::domain_error("ESM::AM does not support SPI");

		printf("Using Selective Pixel Integration\n");
		pix_mask.resize(am->getPixCount());
		ssm->setSPIMask(pix_mask.data());
		am->setSPIMask(pix_mask.data());
		rel_pix_diff.resize(am->getPixCount());
		pix_mask2.resize(am->getPixCount());
		pix_mask_img = cv::Mat(am->getResX(), am->getResY(), CV_8UC1, pix_mask2.data());
		spi_win_name = "pix_mask_img";
	}
#endif
	switch(params.jac_type){
	case JacType::Original:
		printf("Using original ESM Jacobian\n");
		break;
	case JacType::DiffOfJacs:
		printf("Using Difference of Jacobians\n");
		break;
	default:
		throw std::invalid_argument("Invalid Jacobian type provided");
	}

	const char *hess_order = params.sec_ord_hess ? "Second" : "First";
	switch(params.hess_type){
	case HessType::Original:
		printf("Using %s order original ESM Hessian\n", hess_order);
		break;
	case HessType::SumOfStd:
		printf("Using Sum of %s order Standard Hessians\n", hess_order);
		break;
	case HessType::SumOfSelf:
		printf("Using Sum of %s order Self Hessians\n", hess_order);
		break;
	case HessType::InitialSelf:
		printf("Using %s order Initial Self Hessian\n", hess_order);
		break;
	case HessType::CurrentSelf:
		printf("Using %s order Current Self Hessian\n", hess_order);
		break;
	case HessType::Std:
		printf("Using %s order Standard Hessian\n", hess_order);
		break;
	default:
		throw std::invalid_argument("Invalid Hessian type provided");
	}

	ssm_update.resize(ssm->getStateSize());
	jacobian.resize(ssm->getStateSize());
	hessian.resize(ssm->getStateSize(), ssm->getStateSize());

	init_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());
	curr_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());

	if(params.jac_type == JacType::Original || params.hess_type == HessType::Original){
		mean_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());
	}
	if(params.hess_type == HessType::SumOfSelf){
		init_self_hessian.resize(ssm->getStateSize(), ssm->getStateSize());
	}
	if(params.sec_ord_hess){
		init_pix_hessian.resize(ssm->getStateSize()*ssm->getStateSize(), am->getPixCount());
		if(params.hess_type != HessType::InitialSelf){
			curr_pix_hessian.resize(ssm->getStateSize()*ssm->getStateSize(), am->getPixCount());
			if(params.hess_type == HessType::Original){
				mean_pix_hessian.resize(ssm->getStateSize()*ssm->getStateSize(), am->getPixCount());
			}
		}
	}
}

template <class AM, class SSM>
void ESM<AM, SSM >::initialize(const cv::Mat &corners){
	start_timer();

	am->clearInitStatus();
	ssm->clearInitStatus();

	frame_id = 0;
	ssm->initialize(corners);
	am->initializePixVals(ssm->getPts());

	if(params.enable_spi){ initializeSPIMask(); }

	initializePixJacobian();
	if(params.sec_ord_hess){
		initializePixHessian();
	}
	am->initialize();
	am->initializeGrad();
	am->initializeHess();

	if(params.hess_type == HessType::InitialSelf || params.hess_type == HessType::SumOfSelf){
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
		} else{
			am->cmptSelfHessian(hessian, init_pix_jacobian);
		}
		if(params.hess_type == HessType::SumOfSelf){
			init_self_hessian = hessian;
		}
	}
	ssm->getCorners(cv_corners_mat);

	end_timer();
	write_interval(time_fname, "w");
}

template <class AM, class SSM>
void ESM<AM, SSM >::setRegion(const cv::Mat& corners){
	ssm->setCorners(corners);
	// since the above command completely resets the SSM state including its initial points,
	// any quantities that depend on these, like init_pix_jacobian and init_pix_hessian,
	// must be recomputed along with quantities that depend on them in turn.
	ssm->cmptInitPixJacobian(init_pix_jacobian, am->getInitPixGrad());
	if(params.sec_ord_hess){
		ssm->cmptInitPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
	}
	if(params.hess_type == HessType::InitialSelf || params.hess_type == HessType::SumOfSelf){
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
		} else{
			am->cmptSelfHessian(hessian, init_pix_jacobian);
		}
		if(params.hess_type == HessType::SumOfSelf){
			init_self_hessian = hessian;
		}
	}
	ssm->getCorners(cv_corners_mat);
}

template <class AM, class SSM>
void ESM<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int iter_id = 0; iter_id < params.max_iters; iter_id++){
		init_timer();

		// extract pixel values from the current image at the latest known position of the object
		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		if(params.enable_spi){ updateSPIMask(); }

		updatePixJacobian();
		if(params.jac_type == JacType::Original || params.hess_type == HessType::Original){
			mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
			record_event("mean_pix_jacobian");
		}
		if(params.sec_ord_hess && params.hess_type != HessType::InitialSelf){
			updatePixHessian();
		}

		// compute the prerequisites for the gradient functions
		am->update();
		record_event("am->update");

		// update the gradient of the error norm w.r.t. current pixel values
		am->updateCurrGrad();
		record_event("am->updateCurrGrad");

		// update the gradient of the error norm w.r.t. initial pixel values
		am->updateInitGrad();
		record_event("am->updateInitGrad");

		cmptJacobian();
		cmptHessian();

		ssm_update = -hessian.colPivHouseholderQr().solve(jacobian.transpose());
		record_event("ssm_update");

		prev_corners = ssm->getCorners();
		updateSSM();

		//double update_norm = ssm_update.lpNorm<1>();
		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		write_data(time_fname);

		if(update_norm < params.epsilon){
			if(params.debug_mode){
				printf("n_iters: %d\n", iter_id + 1);
			}
			break;
		}

		if(params.enable_spi){ showSPIMask(); }

		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners_mat);
}

template <class AM, class SSM>
void ESM<AM, SSM >::cmptJacobian(){
	switch(params.jac_type){
	case JacType::Original:
		// take the mean at the level of the jacobian of the pixel values wrt SSM parameters, 
		//then use this mean jacobian to compute the Jacobian of the error norm wrt SSM parameters
		am->cmptCurrJacobian(jacobian, mean_pix_jacobian);
		record_event("am->cmptCurrJacobian");
		break;
	case JacType::DiffOfJacs:
		// compute the mean difference between the Jacobians of the error norm w.r.t. initial AND current values of SSM parameters
		am->cmptDifferenceOfJacobians(jacobian, init_pix_jacobian, curr_pix_jacobian);
		jacobian *= 0.5;
		record_event("am->cmptDifferenceOfJacobians");
		break;
	}
}

template <class AM, class SSM>
void ESM<AM, SSM >::cmptHessian(){
	switch(params.hess_type){
	case HessType::InitialSelf:
		break;
	case HessType::Original:
		if(params.sec_ord_hess){
			mean_pix_hessian = (init_pix_hessian + curr_pix_hessian) / 2.0;
			record_event("mean_pix_hessian");
			am->cmptCurrHessian(hessian, mean_pix_jacobian, mean_pix_hessian);
			record_event("am->cmptCurrHessian (second order)");
		} else{
			am->cmptCurrHessian(hessian, mean_pix_jacobian);
			record_event("am->cmptCurrHessian (first order)");
		}
		break;
	case HessType::SumOfStd:
		if(params.sec_ord_hess){
			am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian,
				init_pix_hessian, curr_pix_hessian);
			record_event("am->cmptSumOfHessians (second order)");
		} else{
			am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian);
			record_event("am->cmptSumOfHessians (first order)");
		}
		hessian *= 0.5;
		break;
	case HessType::SumOfSelf:
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, curr_pix_jacobian, curr_pix_hessian);
			record_event("am->cmptSelfHessian (second order)");
		} else{
			am->cmptSelfHessian(hessian, curr_pix_jacobian);
			record_event("am->cmptSelfHessian (first order)");
		}
		hessian = (hessian + init_self_hessian) * 0.5;
		break;
	case HessType::CurrentSelf:
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, curr_pix_jacobian, curr_pix_hessian);
			record_event("am->cmptSelfHessian (second order)");
		} else{
			am->cmptSelfHessian(hessian, curr_pix_jacobian);
			record_event("am->cmptSelfHessian (first order)");
		}
		break;
	case HessType::Std:
		if(params.sec_ord_hess){
			am->cmptCurrHessian(hessian, curr_pix_jacobian, curr_pix_hessian);
			record_event("am->cmptCurrHessian (second order)");
		} else{
			am->cmptCurrHessian(hessian, curr_pix_jacobian);
			record_event("am->cmptCurrHessian (first order)");
		}
		break;
	}
}

template <class AM, class SSM>
void ESM<AM, SSM >::initializePixJacobian(){
	ssm->initializeGradPts(am->getGradOffset());
	am->initializePixGrad(ssm->getGradPts());
	ssm->cmptInitPixJacobian(init_pix_jacobian, am->getInitPixGrad());
}

template <class AM, class SSM>
void ESM<AM, SSM >::updatePixJacobian(){
	// compute pixel gradient of the current image warped with the current warp
	ssm->updateGradPts(am->getGradOffset());
	record_event("ssm->updateGradPts");

	am->updatePixGrad(ssm->getGradPts());
	record_event("am->updatePixGrad New");

	// multiply the pixel gradient with the SSM Jacobian to get the Jacobian of pixel values w.r.t. SSM parameters
	ssm->cmptInitPixJacobian(curr_pix_jacobian, am->getCurrPixGrad());
	record_event("ssm->cmptInitPixJacobian");
}

template <class AM, class SSM>
void ESM<AM, SSM >::initializePixHessian(){
	ssm->initializeHessPts(am->getHessOffset());
	am->initializePixHess(ssm->getPts(), ssm->getHessPts());
	ssm->cmptInitPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
}

template <class AM, class SSM>
void ESM<AM, SSM >::updatePixHessian(){
	ssm->updateHessPts(am->getHessOffset());
	record_event("ssm->updateHessPts");

	am->updatePixHess(ssm->getPts(), ssm->getHessPts());
	record_event("am->updatePixHess New");

	ssm->cmptInitPixHessian(curr_pix_hessian, am->getCurrPixHess(), am->getCurrPixGrad());
	record_event("ssm->cmptInitPixHessian");
}

template <class AM, class SSM>
void ESM<AM, SSM >::updateSSM(){
	ssm->compositionalUpdate(ssm_update);
	record_event("ssm->compositionalUpdate");
}

// support for Selective Pixel Integration
template <class AM, class SSM>
void ESM<AM, SSM >::initializeSPIMask(){
	cv::namedWindow(spi_win_name);
	max_pix_diff = am->getInitPixVals().maxCoeff() - am->getInitPixVals().minCoeff();
	utils::printScalar(max_pix_diff, "max_pix_diff");
}

template <class AM, class SSM>
void ESM<AM, SSM >::updateSPIMask(){
#ifndef DISABLE_SPI
	rel_pix_diff = (am->getInitPixVals() - am->getCurrPixVals()) / max_pix_diff;
	record_event("rel_pix_diff");

	pix_mask = rel_pix_diff.cwiseAbs().array() < params.spi_thresh;
	record_event("pix_mask");

	if(params.debug_mode){
		int active_pixels = pix_mask.count();
		utils::printScalar(active_pixels, "active_pixels", "%d");
	}
#endif	
}
template <class AM, class SSM>
void ESM<AM, SSM >::showSPIMask(){
#ifndef DISABLE_SPI
	if(params.enable_spi){
		for(int pix_id = 0; pix_id < am->getPixCount(); pix_id++){
			int x = pix_mask(pix_id);
			pix_mask2(pix_id) = x * 255;
		}
		cv::Mat pix_mask_img_resized;
		cv::resize(pix_mask_img, pix_mask_img_resized, cv::Size(300, 300));
		imshow(spi_win_name, pix_mask_img_resized);
	}
#endif
}
_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(ESM);