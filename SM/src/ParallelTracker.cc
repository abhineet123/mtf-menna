#include "mtf/SM/ParallelTracker.h"
#include "mtf/Utilities/miscUtils.h"

#ifdef ENABLE_TBB
#include "tbb/tbb.h" 
#endif

_MTF_BEGIN_NAMESPACE

const char* ParallelParams::operator()(EstimationMethod _estimation_method) const {
	switch(_estimation_method) {
	case EstimationMethod::MeanOfCorners:
		return "MeanOfCorners";
	case EstimationMethod::MeanOfState:
		return "MeanOfState";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
ParallelParams::ParallelParams(EstimationMethod _estimation_method, bool _reset_to_mean,
	bool _auto_reinit, double _reinit_err_thresh) {
	estimation_method = _estimation_method;
	reset_to_mean = _reset_to_mean;
	auto_reinit = _auto_reinit;
	reinit_err_thresh = _reinit_err_thresh;

}
ParallelParams::ParallelParams(ParallelParams *params) :
estimation_method(static_cast<EstimationMethod>(PARL_ESTIMATION_METHOD)),
reset_to_mean(PARL_RESET_TO_MEAN),
auto_reinit(PARL_AUTO_REINIT),
reinit_err_thresh(PARL_REINIT_ERR_THRESH){
	if(params) {
		estimation_method = params->estimation_method;
		reset_to_mean = params->reset_to_mean;
		auto_reinit = params->auto_reinit;
		reinit_err_thresh = params->reinit_err_thresh;
	}
}

ParallelTracker::ParallelTracker(const vector<TrackerBase*> _trackers, ParamType *parl_params) :
TrackerBase(), curr_img_updated(false), failure_detected(false),
params(parl_params), trackers(_trackers) {
	n_trackers = trackers.size();

	printf("\n");
	printf("Initializing generalized Parallel tracker with:\n");
	printf("reset_to_mean: %d\n", params.reset_to_mean);
	printf("auto_reinit: %d\n", params.auto_reinit);
	printf("reinit_err_thresh: %f\n", params.reinit_err_thresh);
	printf("n_trackers: %d\n", n_trackers);
	printf("trackers: ");
	name = "prl: ";
	for(int i = 0; i < n_trackers; i++) {
		name = name + trackers[i]->name + " ";
		printf("%d: %s ", i + 1, trackers[i]->name.c_str());
	}
	printf("\n");

	mean_corners_cv.create(2, 4, CV_64FC1);
	if(params.auto_reinit){
		curr_img.copyTo(prev_img);
	}
}

void ParallelTracker::initialize(const cv::Mat &img, const cv::Mat &corners)  {
	curr_img = img;
	curr_img_updated = true;
	initialize(corners);
}

void ParallelTracker::update(const cv::Mat &img)  {
	curr_img = img;
	curr_img_updated = true;
	update();
}


void ParallelTracker::initialize(const cv::Mat &corners)  {
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(curr_img_updated) {
			trackers[tracker_id]->initialize(curr_img, corners);
		} else {
			trackers[tracker_id]->initialize(corners);
		}
	}
	if(curr_img_updated) {
		curr_img_updated = false;
	}
	cv_corners_mat = corners;
	if(params.auto_reinit){
		corners.copyTo(prev_corners);
		curr_img.copyTo(prev_img);
	}
}

void ParallelTracker::update()  {
	mean_corners_cv.setTo(cv::Scalar(0));
#ifdef ENABLE_TBB
	parallel_for(tbb::blocked_range<size_t>(0, n_trackers),
		[&](const tbb::blocked_range<size_t>& r) {
		for(size_t tracker_id = r.begin(); tracker_id != r.end(); ++tracker_id) {
#else
	for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
#endif
		if(curr_img_updated) {
			trackers[tracker_id]->update(curr_img);
		} else {
			trackers[tracker_id]->update();
		}
		mean_corners_cv += (trackers[tracker_id]->getRegion() - mean_corners_cv) / (tracker_id + 1);
	}
#ifdef ENABLE_TBB
		});
#endif
	if(curr_img_updated) {
		curr_img_updated = false;
	}
	if(params.auto_reinit){
		if(failure_detected){
			failure_detected = false;
		} else{
			for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
				double corner_change_norm = cv::norm(trackers[tracker_id]->getRegion(), mean_corners_cv, cv::NORM_L2) / 8.0;
				if(corner_change_norm > params.reinit_err_thresh){
					failure_detected = true;
					break;
				}
			}
			if(failure_detected){
				printf("Reinitializing trackers...\n");
				for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
					trackers[tracker_id]->initialize(prev_img, prev_corners);
				}
				update(curr_img);
				return;
			}
		}
	}

	if(params.reset_to_mean) {
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			trackers[tracker_id]->setRegion(mean_corners_cv);
		}
	}
	if(params.auto_reinit){
		mean_corners_cv.copyTo(prev_corners);
		curr_img.copyTo(prev_img);
	}
}


void ParallelTracker::setRegion(const cv::Mat& corners)   {
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++) {
		trackers[tracker_id]->setRegion(corners);
	}
	corners.copyTo(mean_corners_cv);
	corners.copyTo(prev_corners);
}

_MTF_END_NAMESPACE
