#include "mtf/SM/CascadeSM.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

CascadeSMParams::CascadeSMParams(bool _enable_feedback){
	enable_feedback = _enable_feedback;
}
CascadeSMParams::CascadeSMParams(CascadeSMParams *params) :
enable_feedback(CASC_SM_ENABLE_FEEDBACK){
	if(params){
		enable_feedback = params->enable_feedback;
	}
}
template<class AM, class SSM>
CascadeSM<AM, SSM>::CascadeSM(const vector<SM*> _trackers, ParamType *casc_params) :
SM(), params(casc_params), trackers(_trackers){
	n_trackers = trackers.size();

	printf("\n");
	printf("Initializing Cascade of Search Methods with:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("Search methods: ");
	name = "casc: ";
	for(int i = 0; i < n_trackers; i++){
		name = name + trackers[i]->name + " ";
		printf("%d: %s ", i + 1, trackers[i]->name.c_str());
	}
	printf("\n");
	printf("appearance model: %s\n", trackers[0]->getAM()->name.c_str());
	printf("state space model: %s\n", trackers[0]->getSSM()->name.c_str());

	if(params.enable_feedback){
		printf("Feedback is enabled\n");
	}
}

template<class AM, class SSM>
void CascadeSM<AM, SSM>::initialize(const cv::Mat &img, const cv::Mat &corners){
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->initialize(img, corners);
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}

template<class AM, class SSM>
void CascadeSM<AM, SSM>::initialize(const cv::Mat &corners){
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		//printf("Initializing tracker %d\n", tracker_id);
		trackers[tracker_id]->initialize(corners);
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}
template<class AM, class SSM>
void CascadeSM<AM, SSM>::update(){
	trackers[0]->update();
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		//printf("tracker: %d ", tracker_id - 1);
		//utils::printMatrix<double>(trackers[tracker_id - 1]->getRegion(),
		//	"region is: ");
		trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
		//printf("tracker: %d ", tracker_id);
		//utils::printMatrix<double>(trackers[tracker_id]->getRegion(),
		//	"region set to: ");
		trackers[tracker_id]->update();
	}
	if(params.enable_feedback){
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}
template<class AM, class SSM>
void CascadeSM<AM, SSM>::update(const cv::Mat &img){
	trackers[0]->update(img);
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
		trackers[tracker_id]->update(img);
	}
	if(params.enable_feedback){
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
	}
	//cv_corners = trackers[n_trackers - 1]->cv_corners;
}


template<class AM, class SSM>
void CascadeSM<AM, SSM>::setRegion(const cv::Mat& corners) {
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setRegion(corners);
	}
}

template<class AM, class SSM>
void CascadeSM<AM, SSM>::setSPIMask(const bool *_spi_mask)  {
	spi_mask = _spi_mask;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setSPIMask(_spi_mask);
	}
}
template<class AM, class SSM>
void CascadeSM<AM, SSM>::clearSPIMask()  {
	spi_mask = nullptr;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->clearSPIMask();
	}
}

template<class AM, class SSM>
void CascadeSM<AM, SSM>::setInitStatus()  {
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->setInitStatus();
	}
}
template<class AM, class SSM>
void CascadeSM<AM, SSM>::clearInitStatus()  {
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		trackers[tracker_id]->clearInitStatus();
	}
}

template<class AM, class SSM>
bool CascadeSM<AM, SSM>::supportsSPI() {
	bool supports_spi = true;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
		if(!trackers[tracker_id]->supportsSPI())
			return false;
	}
	return true;
}

_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(CascadeSM);

