# ------------------------------------------------------------------------------------ #
# ---------------------------------- Search Methods ---------------------------------- #
# ------------------------------------------------------------------------------------ #
BASE_CLASSES += SM/SearchMethod SM/GridBase
SEARCH_METHODS = FCLK PF GNN ESM AESM ICLK FALK IALK FCGD
COMPOSITE =  GridTracker GridTrackerCV  RKLT CascadeTracker CascadeSM ParallelTracker ParallelSM
SEARCH_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${SEARCH_METHODS}))
COMPOSITE_OBJS = $(addprefix ${BUILD_DIR}/,$(addsuffix .o, ${COMPOSITE}))
SEARCH_HEADERS = $(addprefix SM/, $(addsuffix .h, ${SEARCH_METHODS}))
COMPOSITE_HEADERS = $(addprefix SM/, $(addsuffix .h, ${COMPOSITE}))
MTF_HEADERS += $(addprefix ${MTF_HEADER_INSTALL_DIR}/, ${SEARCH_HEADERS}  ${COMPOSITE_HEADERS})  

GRID_FLAGS = 
PRL_FLAGS = 
ESM_FLAGS = 
IC_FLAGS =
FC_FLAGS = 
FA_FLAGS = 
NN_FLAGS = 

el ?= 0
et ?= 0
ed ?= 0
emg ?= 1
icd ?= 0
icl ?= 0
ict ?= 0
fcd ?= 0
fct ?= 0
fad ?= 0
fat ?= 0
iat ?= 0
nn ?= 1
prltbb ?= 0
prlomp ?= 0
grtbb ?= 1
efd ?= 0

ifeq (${nn}, 1)
SEARCH_METHODS += NN
LIB_MTF_LIBS += -lflann -lhdf5
RUN_MTF_LIBS += -lhdf5
else
RUN_MTF_FLAGS += -D DISABLE_NN
endif

ifeq (${prltbb}, 1)
PRL_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${grtbb}, 1)
GRID_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${et}, 1)
ESM_FLAGS += -D ENABLE_PROFILING
endif
ifeq (${ed}, 1)
ESM_FLAGS += -D LOG_ESM_DATA
endif
ifeq (${el}, 1)
ESM_FLAGS += -D LOG_ESM_DATA -D ENABLE_PROFILING
endif
ifeq (${emg}, 0)
ESM_FLAGS += -D DISABLE_MEAN_GRADIENT
endif

ifeq (${icd}, 1)
IC_FLAGS += -D LOG_ICLK_DATA
endif
ifeq (${ict}, 1)
IC_FLAGS += -D LOG_ICLK_TIMES 
endif

ifeq (${fcd}, 1)
FC_FLAGS += -D LOG_FCLK_DATA
endif
ifeq (${fct}, 1)
FC_FLAGS += -D ENABLE_PROFILING 
endif

ifeq (${fad}, 1)
FA_FLAGS += -D LOG_FALK_DATA
endif
ifeq (${fat}, 1)
FA_FLAGS += -D LOG_FALK_TIMES 
endif
ifeq (${iat}, 1)
IA_FLAGS += -D LOG_IALK_TIMES 
endif
ifeq (${nnt}, 1)
NN_FLAGS += -D ENABLE_PROFILING
endif



${BUILD_DIR}/HACLK.o: SM/HACLK.cc SM/HACLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${HAC_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/ICLK.o: SM/ICLK.cc SM/ICLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IC_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FCLK.o: SM/FCLK.cc SM/FCLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FALK.o: SM/FALK.cc SM/FALK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FA_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/IALK.o: SM/IALK.cc SM/IALK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IA_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/IALK2.o: SM/IALK2.cc SM/IALK2.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IA_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FCGD.o: SM/FCGD.cc SM/FCGD.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/HESM.o: SM/HESM.cc SM/HESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
		${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@

${BUILD_DIR}/AESM.o: SM/AESM.cc SM/AESM.h SM/ESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ESM_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@

${BUILD_DIR}/ESM.o: SM/ESM.cc SM/ESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ESM_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FESMBase.o: SM/FESMBase.cc SM/FESMBase.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FESMBase_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/FESM.o: SM/FESM.cc SM/FESM.h SM/FESMBase.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FESM_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/PF.o: SM/PF.cc SM/PF.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${PF_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/NN.o: SM/NN.cc SM/NN.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${NN_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/GNN.o: SM/GNN.cc SM/GNN.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${GNN_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
${MTF_LIB_INSTALL_DIR}/libgnn.so: SM/GNN/libgnn.so
	cp -f $< $@
SM/GNN/libgnn.so: SM/GNN/build_graph.h SM/GNN/build_graph.cc SM/GNN/search_graph_knn.h SM/GNN/search_graph_knn.cc SM/GNN/utility.cc SM/GNN/utility.h SM/GNN/memwatch.cc SM/GNN/memwatch.h 
	$(MAKE) -C SM/GNN --no-print-directory
	
# ------------------------------------------------------------------------------------ #
# ---------------------------------- Composite ---------------------------------- #
# ------------------------------------------------------------------------------------ #

${BUILD_DIR}/CascadeTracker.o: SM/CascadeTracker.cc SM/CascadeTracker.h TrackerBase.h Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
${BUILD_DIR}/CascadeSM.o: SM/CascadeSM.cc SM/CascadeSM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/ParallelTracker.o: SM/ParallelTracker.cc SM/ParallelTracker.h TrackerBase.h ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${PRL_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
${BUILD_DIR}/ParallelSM.o: SM/ParallelSM.cc SM/ParallelSM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${PRL_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/GridTrackerCV.o: SM/GridTrackerCV.cc SM/GridTrackerCV.h SM/GridBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/RKLT.o: SM/RKLT.cc SM/RKLT.h ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${GRID_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/GridTracker.o: SM/GridTracker.cc SM/GridTracker.h ${STATE_SPACE_HEADERS} ${BASE_HEADERS} Utilities/miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${GRID_FLAGS} $< ${FLAGS64} ${FLAGSCV} -o $@	
