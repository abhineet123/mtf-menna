#ifndef SEARCH_METHOD_H
#define SEARCH_METHOD_H

#include "mtf/TrackerBase.h"
#include "mtf/Macros/common.h"

_MTF_BEGIN_NAMESPACE

template<class AM, class SSM>
class SearchMethod : public TrackerBase{
protected:
	AM *am;
	SSM *ssm;

public:
	using TrackerBase::initialize;
	using TrackerBase::update;

	typedef typename  AM::ParamType AMParams;
	typedef typename SSM::ParamType SSMParams;

	SearchMethod(AMParams *am_params, SSMParams *ssm_params) :
		TrackerBase(),
		spi_mask(nullptr){
		am = new AM(am_params);
		ssm = new SSM(am->getResX(), am->getResY(), ssm_params);
		cv_corners_mat.create(2, 4, CV_64FC1);
	}
	SearchMethod() : TrackerBase(),
		spi_mask(nullptr){
		cv_corners_mat.create(2, 4, CV_64FC1);
	}

	virtual ~SearchMethod(){
		if(am){delete(am);}
		if(ssm) { delete(ssm); }
	}
	void initialize(const cv::Mat &img, const cv::Mat &corners) override{
		am->setCurrImg(img);
		initialize(corners);
	}
	void update(const cv::Mat &img) override{
		am->setCurrImg(img);
		update();
	}

	const cv::Mat& getRegion() override{
		return cv_corners_mat;
	}
	// default implementation for SMs where the AM processing in the current frame
	// does not depend on the results obtained in the last frame
	void setRegion(const cv::Mat& corners) override{
		ssm->setCorners(corners);
		ssm->getCorners(cv_corners_mat);
	}

	const bool *spi_mask;
	virtual void setSPIMask(const bool *_spi_mask){
		spi_mask = _spi_mask;
		am->setSPIMask(_spi_mask);
		ssm->setSPIMask(_spi_mask);
	}
	virtual void clearSPIMask(){
		spi_mask = nullptr;
		am->clearSPIMask();
		ssm->clearSPIMask();
	}

	virtual void setInitStatus(){
		am->setInitStatus();
		ssm->setInitStatus();
	}
	virtual void clearInitStatus(){
		am->clearInitStatus();
		ssm->clearInitStatus();
	}

	virtual bool supportsSPI(){ return am->supportsSPI() && ssm->supportsSPI(); }

	// return true if the tracker requires the raw RGB image
	virtual bool rgbInput() const override{ return am->rgbInput(); }

	// direct access to the underlying AM and SSM
	virtual AM* getAM() const { return am; }
	virtual SSM* getSSM() const { return ssm; }

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
// partial specialization for a search method with a 
// composite or implicit appearance model that cannot be expressed as a class
template <class SSM>
class SearchMethod<void, SSM> : public TrackerBase{
protected:

	SSM *ssm;

public:

	typedef typename SSM::ParamType SSMParams;
	
	SearchMethod(int resx, int resy,
		SSMParams *ssm_params) : 
		TrackerBase(),
		spi_mask(nullptr){
		ssm = new SSM(resx, resy, ssm_params);
		cv_corners_mat.create(2, 4, CV_64FC1);
	}
	virtual ~SearchMethod(){
		delete(ssm);
	}

	const cv::Mat& getRegion() override{
		return cv_corners_mat;
	}
	// default implementation for SMS where the AM processing in the current frame
	// does not depend on the results obtained in the last frame
	void setRegion(const cv::Mat& corners) override{
		ssm->setCorners(corners);
		ssm->getCorners(cv_corners_mat);
	}

	const bool *spi_mask;
	virtual void setSPIMask(const bool *_spi_mask){
		spi_mask = _spi_mask;
		ssm->setSPIMask(_spi_mask);
	}
	virtual void clearSPIMask(){
		spi_mask = nullptr;
		ssm->clearSPIMask();
	}
	virtual bool supportsSPI(){ return ssm->supportsSPI(); }
	virtual SSM* getSSM() const { return ssm; }

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

_MTF_END_NAMESPACE

#endif
