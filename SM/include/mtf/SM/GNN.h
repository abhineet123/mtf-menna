#ifndef GNN_H
#define GNN_H

#include "SearchMethod.h"

#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>

//#include "GNN/build_graph.h"

#define GNN_MAX_ITERS 10
#define GNN_K 250
#define GNN_EPSILON 0.01
#define GNN_CORNER_SIGMA_D 0.06
#define GNN_CORNER_SIGMA_T 0.04
#define GNN_N_SAMPLES 1000
#define GNN_ADDITIVE_UPDATE 1
#define GNN_DIRECT_SAMPLES 1
#define GNN_DEBUG_MODE false
#define GNN_CORNER_GRAD_EPS 1e-5

_MTF_BEGIN_NAMESPACE

struct GNNParams{
	int max_iters; //! maximum iterations of the GNN algorithm to run for each frame
	int n_samples;
	int k;

	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations	
	double corner_sigma_d;
	double corner_sigma_t;

	bool additive_update;
	bool direct_samples;


	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	GNNParams(int _max_iters, int _n_samples, int _k,
		double _epsilon,
		double _corner_sigma_d, double _corner_sigma_t,
		bool _additive_update, bool _direct_samples,
		bool _debug_mode);
	GNNParams(GNNParams *params = nullptr);
};

namespace gnn{
	struct node{
		int *nns_inds;
		int size;
		int capacity;
	};

	struct indx_dist{
		double dist;
		int idx;
	};

	struct mat_size{
		int rows;
		int cols;
	};

	inline int cmp_qsort(const void *a, const void *b){
		gnn::indx_dist *a1 = (gnn::indx_dist *)a;
		gnn::indx_dist *b1 = (gnn::indx_dist *)b;

		// Ascending
		if(a1->dist > b1->dist) return 1;
		else if(a1->dist == b1->dist) return 0;
		else return -1;
	}
}


template<class AM, class SSM>
class GNN : public SearchMethod < AM, SSM > {
private:
	init_profiling();
	char *log_fname;
	char *time_fname;

public:
	//typedef typename AM::DistanceMeasure DistanceMeasure;
	//DistanceMeasure dist_obj;

	typedef boost::minstd_rand baseGeneratorType;
	typedef boost::normal_distribution<> dustributionType;
	typedef boost::variate_generator<baseGeneratorType&, dustributionType> randomGeneratorType;


	typedef GNNParams ParamType;
	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	gnn::node *Nodes;

	int frame_id;
	int am_feat_size;
	int ssm_state_size;

	Matrix3d warp_update;

	Matrix24d prev_corners;
	VectorXd ssm_update;	
	VectorXd inv_state_update;
	VectorXd ssm_sigma;
	MatrixXd ssm_perturbations;

	MatrixXd eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	int best_idx;
	double best_dist;

	GNN(ParamType *nn_params,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~GNN(){}

	void initialize(const cv::Mat &corners) override;
	void update() override;
	gnn::node*  build_graph(double *X, int k, int n_samples, int n_dims);
	//bool cmp_cppsort(struct indx_dist a, struct indx_dist b);
	int my_rand(int lb, int ub);
	int found(int *list, int ind, int list_size);
	double my_dist(int *v1, int *v2, int length);
	void mergesort(gnn::indx_dist arr[], int low, int high);
	void merge(gnn::indx_dist arr[], int low, int split, int high);
	void swap_int(int *i, int *j);
	void swap_double(double *i, double *j);

	//--------------------------------
	void knn_search2(double *Q, gnn::indx_dist *dists, double *X,
		int rows, int cols, int k);
	//--------------------------------
	void knn_search11(double *Q, gnn::indx_dist *dists, double *X, int rows,
		int cols, int k, int *X_inds);

	int min(int a, int b){ return a < b ? a : b; }

	//void search_graph_1nn(int *Xq, int *X, int NNs, int k);
	int *sample_X(gnn::node *Nodes, int node_ind, int n_dims, int *X);
	void pick_knns(gnn::indx_dist *vis_nodes, int visited, gnn::indx_dist **gnn_dists,
		int K, int *gnns_cap);
	gnn::indx_dist* intersect(gnn::indx_dist *gnns, gnn::indx_dist *tnns, int K, int *size);
	gnn::indx_dist *search_graph(gnn::node *Nodes, double *Xq, double *X,
		int n_samples, int n_dims, int NNs, int K);
	void add_node(gnn::node *node_i, int nn);
};
_MTF_END_NAMESPACE

#endif

