#ifndef CASCADE_SM_H
#define CASCADE_SM_H

#include "SearchMethod.h"

#define CASC_SM_ENABLE_FEEDBACK true

_MTF_BEGIN_NAMESPACE

struct CascadeSMParams{
	bool enable_feedback;
	CascadeSMParams(bool _enable_feedback);
	CascadeSMParams(CascadeSMParams *params = nullptr);
};
// run multiple search methods in cascade with the same AM/SSM
template<class AM, class SSM>
class CascadeSM : public SearchMethod < AM, SSM > {

public:
	typedef SearchMethod<AM, SSM> SM;
	typedef CascadeSMParams ParamType;
	ParamType params;

	using SM::name;
	using SM::spi_mask;

	const vector<SM*> trackers;
	int n_trackers;

	CascadeSM(const vector<SM*> _trackers, ParamType *casc_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	void update(const cv::Mat &img) override;
	void setRegion(const cv::Mat& corners)  override;
	const cv::Mat& getRegion()  override{ return trackers[n_trackers - 1]->getRegion(); }

	void setSPIMask(const bool *_spi_mask)  override;
	void clearSPIMask()  override;
	void setInitStatus()  override;
	void clearInitStatus()  override;
	bool supportsSPI() override;
	bool rgbInput() const override{ return false; }
	// though there is no clear answer to what the AM and SSM for a cascade of SMs should be,
	// those of the last tracker in the cascade may make some sense expecially for the SSM
	AM* getAM() const override { return trackers[n_trackers - 1]->getAM(); }
	SSM* getSSM() const override { return trackers[n_trackers - 1]->getSSM(); }
};
_MTF_END_NAMESPACE

#endif

