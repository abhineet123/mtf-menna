#ifndef GRID_TRACKER_CV_H
#define GRID_TRACKER_CV_H

#include "GridBase.h"
#include <vector>

#define GTCV_GRID_SIZE_X 10
#define GTCV_GRID_SIZE_Y 10
#define GTCV_SEARCH_WINDOW_X 10
#define GTCV_SEARCH_WINDOW_Y 10
#define GTCV_PYRAMID_LEVELS 0
#define GTCV_USE_MIN_EIG_VALS 0
#define GTCV_MIN_EIG_THRESH 1e-4
#define GTCV_ESTIMATION_METHOD 0
#define GTCV_RANSAC_REPROJ_THRESH 10.0
#define GTCV_MAX_ITERS 30
#define GTCV_EPSILON 0.01
#define GTCV_SHOW_TRACKERS 0
#define GTCV_SHOW_TRACKER_EDGES 0
#define GTCV_DEBUG_MODE 0

_MTF_BEGIN_NAMESPACE

struct GridTrackerCVParams{

	enum class EstType { RANSAC, LeastMedian, LeastSquares };

	int grid_size_x, grid_size_y;
	int search_window_x, search_window_y;

	int pyramid_levels;
	bool use_min_eig_vals;
	double min_eig_thresh;

	EstType estimation_method;
	double ransac_reproj_thresh;

	int max_iters; //! maximum iterations of the GridTrackerCV algorithm to run for each frame
	double epsilon;

	bool show_trackers;// show the locations of individual patch trackers

	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	GridTrackerCVParams(
		int _grid_size_x, int _grid_size_y,
		int _search_window_x, int _search_window_y,
		int _pyramid_levels, bool _use_min_eig_vals,
		double _min_eig_thresh, EstType _estimation_method,
		double _ransac_reproj_thresh, 
		int _max_iters, double _epsilon,bool _show_trackers,
		bool _debug_mode);
	GridTrackerCVParams(GridTrackerCVParams *params = nullptr);
};

template<class SSM>
class GridTrackerCV : public GridBase{

public:
	typedef GridTrackerCVParams ParamType;
	ParamType params;

	typedef ParamType::EstType EstType;

	SSM *ssm;
	typedef typename SSM::ParamType SSMParams;


	cv::Mat curr_img_float, curr_img, prev_img;
	cv::Mat curr_pts_mat, prev_pts_mat;

	std::vector<cv::Point2f> curr_pts, prev_pts;
	int n_pts;
	cv::Size search_window;
	cv::TermCriteria lk_termination_criteria;


	cv::Mat warp_mat;
	cv::Mat patch_corners;
	std::vector<uchar> lk_status, pix_mask;
	std::vector<float> lk_error;
	int lk_flags;

	int estimation_method_cv;
	VectorXd ssm_update;

	cv::Mat patch_img, patch_img_uchar;

	Matrix2Xd centroid_offset;

	char* patch_win_name;

	bool rgbInput() const override{ return false; }

	GridTrackerCV(const cv::Mat &cv_img, SSMParams *ssm_params, ParamType *grid_params);

	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void update(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;

	void setRegion(const cv::Mat& corners) override;
	const uchar* getPixMask() override{ return pix_mask.data(); }
	int getResX() override{ return params.grid_size_x; }
	int getResY() override{ return params.grid_size_y; }
	const cv::Mat& getRegion() override{
		ssm->getCorners(cv_corners_mat);
		return cv_corners_mat;
	}

private:
	~GridTrackerCV(){}
	MatrixXi _linear_idx;//used for indexing the sub region locations
	int pause_seq;
	void showTrackers();
};

_MTF_END_NAMESPACE

#endif

