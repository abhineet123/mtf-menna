#ifndef GRID_BASE_H
#define GRID_BASE_H

#include "mtf/TrackerBase.h"
#include "mtf/Macros/common.h"

_MTF_BEGIN_NAMESPACE

class GridBase : public TrackerBase{
public:
	GridBase(){}
	virtual const uchar* getPixMask() = 0;
	virtual int getResX() = 0;
	virtual int getResY() = 0;
};

_MTF_END_NAMESPACE

#endif

