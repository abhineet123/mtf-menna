#ifndef GD_H
#define GD_H

#include "SearchMethod.h"

#define GD_MAX_ITERS 10
#define GD_EPSILON 0.01
#define GD_REC_INIT_ERR_GRAD false
#define GD_DEBUG_MODE false
#define GD_HESS_TYPE 0

_MTF_BEGIN_NAMESPACE

struct FCGDParams{
	int max_iters; //! maximum iterations of the FCGD algorithm to run for each frame
	double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations
	double learning_rate; //! decides if the gradient of the error vector w.r.t. initial pix values
	//! is recomputed every time the vector changes
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time
	int hess_type;

	FCGDParams(int _max_iters, double _epsilon,
		double _learning_rate, bool _debug_mode,
		int _hess_type){
		this->max_iters = _max_iters;
		this->epsilon = _epsilon;
		this->learning_rate = _learning_rate;
		this->debug_mode = _debug_mode;
		this->hess_type = _hess_type;
	}
	FCGDParams(FCGDParams *params = nullptr) : 
		max_iters(GD_MAX_ITERS),
		epsilon(GD_EPSILON),
		learning_rate(GD_REC_INIT_ERR_GRAD), 
		debug_mode(GD_DEBUG_MODE),
		hess_type(GD_HESS_TYPE){
		if(params){
			max_iters = params->max_iters;
			epsilon = params->epsilon;
			learning_rate = params->learning_rate;
			debug_mode = params->debug_mode;
			hess_type = params->hess_type;
		}
	}
};
// Forward Compositional Gradient Descent
template<class AM, class SSM>
class FCGD : public SearchMethod < AM, SSM > {
private:
	ClockType start_time, end_time;
	std::vector<double> proc_times;
	std::vector<char*> proc_labels;
	char *log_fname;
	char *time_fname;

public:
	typedef FCGDParams ParamType;

	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	// Let S = size of SSM state vector and N = resx * resy = no. of pixels in the object patch

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector

	MatrixXd hessian;
	RowVectorXd jacobian;
	//! N x S jacobians of the pix values w.r.t the SSM state vector 
	MatrixXd init_pix_jacobian, curr_pix_jacobian;
	VectorXd ssm_update;

	Matrix24d prev_corners;
	Matrix3d warp_update;	
	int frame_id;
	double learning_rate;

	FCGD(ParamType *fclk_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);

	void initialize(const cv::Mat &corners) override;
	void update() override;
};
_MTF_END_NAMESPACE

#endif

