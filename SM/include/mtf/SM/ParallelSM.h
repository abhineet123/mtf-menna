#ifndef PARALLEL_SM_H
#define PARALLEL_SM_H

#include "SearchMethod.h"

#define PRL_SM_ESTIMATION_METHOD 0
#define PRL_SM_RESET_TO_MEAN 0
#define PRL_SM_AUTO_REINIT 0
#define PRL_SM_REINIT_ERR_THRESH 1

_MTF_BEGIN_NAMESPACE

struct ParallelSMParams {
	enum class EstimationMethod {
		MeanOfCorners,
		MeanOfState
	};
	EstimationMethod estimation_method;
	bool reset_to_mean;
	bool auto_reinit;
	double reinit_err_thresh;
	static const char* toString(EstimationMethod _estimation_method);
	ParallelSMParams(EstimationMethod _estimation_method, bool _reset_to_mean,
		bool _auto_reinit, double _reinit_err_thresh);
	ParallelSMParams(ParallelSMParams *params = nullptr);
};
// run multiple search methods in parallel with the same AM/SSM
template<class AM, class SSM>
class ParallelSM : public SearchMethod<AM, SSM> {

public:

	typedef SearchMethod<AM, SSM> SM;
	using SM::name;
	using SM::cv_corners_mat;
	using SM::ssm;
	using SM::spi_mask;

	using typename SM::SSMParams;

	cv::Mat curr_img, prev_img;
	cv::Mat prev_corners;
	bool curr_img_updated;

	typedef ParallelSMParams ParamType;
	ParamType params;

	typedef ParamType::EstimationMethod EstimationMethod;

	const vector<SM*> trackers;
	int n_trackers;

	cv::Mat mean_corners_cv;

	int ssm_state_size;
	std::vector<VectorXd> ssm_states;
	VectorXd mean_state;

	bool failure_detected;

	ParallelSM(const cv::Mat &img, 
		const vector<SM*> _trackers, ParamType *parl_params,
		int resx, int resy, SSMParams *ssm_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void update(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override { return cv_corners_mat; }
	void setRegion(const cv::Mat& corners)  override;
	// return true if the tracker requires the raw RGB image
	virtual bool rgbInput() const override{ return false; }

	void setSPIMask(const bool *_spi_mask)  override;
	void clearSPIMask()  override;
	void setInitStatus()  override;
	void clearInitStatus()  override;
	bool supportsSPI() override;
	// there is no clear answer to what the AM for a parallel chain of SMs should be,
	AM* getAM() const override { return trackers[n_trackers-1]->getAM(); }
	SSM* getSSM() const override { return ssm; }
};


_MTF_END_NAMESPACE

#endif

