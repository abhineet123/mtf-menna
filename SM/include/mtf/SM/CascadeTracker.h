#ifndef CASCADE_TRACKER_H
#define CASCADE_TRACKER_H

#include "mtf/TrackerBase.h"
#include "mtf/Macros/common.h"


#define CASC_ENABLE_FEEDBACK true

_MTF_BEGIN_NAMESPACE

struct CascadeParams{
	bool enable_feedback;
	CascadeParams(bool _enable_feedback);
	CascadeParams(CascadeParams *params = nullptr);
};

class CascadeTracker : public TrackerBase {

public:

	typedef CascadeParams ParamType;
	ParamType params;

	const vector<TrackerBase*> trackers;
	int n_trackers;

	CascadeTracker(const vector<TrackerBase*> _trackers, ParamType *casc_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	void update(const cv::Mat &img) override;
	void setRegion(const cv::Mat& corners)  override;
	const cv::Mat& getRegion()  override{ return trackers[n_trackers - 1]->getRegion(); }
};
_MTF_END_NAMESPACE

#endif

