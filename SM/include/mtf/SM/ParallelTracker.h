#ifndef PARALLEL_TRACKER_H
#define PARALLEL_TRACKER_H

#include "mtf/TrackerBase.h"
#include "mtf/Macros/common.h"

#define PARL_ESTIMATION_METHOD 0
#define PARL_RESET_TO_MEAN 0
#define PARL_AUTO_REINIT 0
#define PARL_REINIT_ERR_THRESH 1

_MTF_BEGIN_NAMESPACE

struct ParallelParams {
	enum class EstimationMethod {
		MeanOfCorners,
		MeanOfState
	};
	EstimationMethod estimation_method;
	bool reset_to_mean;
	bool auto_reinit;
	double reinit_err_thresh;
	const char* operator()(EstimationMethod _estimation_method) const;
	ParallelParams(EstimationMethod _estimation_method, bool _reset_to_mean,
		bool _auto_reinit, double _reinit_err_thresh);
	ParallelParams(ParallelParams *params = nullptr);
};

// run multiple trackers in parallel
class ParallelTracker : public TrackerBase {

public:
	typedef ParallelParams ParamType;
	ParamType params;

	cv::Mat curr_img, prev_img;
	cv::Mat prev_corners;
	bool curr_img_updated;
	bool failure_detected;

	typedef ParamType::EstimationMethod EstimationMethod;

	const vector<TrackerBase*> trackers;
	int n_trackers;

	cv::Mat mean_corners_cv;

	ParallelTracker(const vector<TrackerBase*> _trackers, ParamType *parl_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void update(const cv::Mat &img) override;

	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override {
		return mean_corners_cv;
	}
	void setRegion(const cv::Mat& corners)  override;
};

_MTF_END_NAMESPACE

#endif

